<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    public function handle($request, Closure $next, $role)
    {
        if( auth()->user()->role_id !== (int)$role )
        {
            return redirect()->route('home');
        }
        return $next($request);
    }
}

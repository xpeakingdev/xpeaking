<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;


class StatusUserMiddleware
{
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if((int)auth()->user()->status == User::NOTVERIFIED){
                return redirect()->route('user.verified');
            }
            else if((int)auth()->user()->status == User::LOCKED){
                return redirect()->route('user.locked');
            }
            return $next($request);
        }
        
        return redirect()->route('login');
    }
}

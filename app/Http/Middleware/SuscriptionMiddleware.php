<?php

namespace App\Http\Middleware;

use Closure;
use App\Song;
use App\Suscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class SuscriptionMiddleware
{
    public function handle($request, Closure $next)
    {
        $routeName = request()->route()->getName();
        $idSong = $request->route('id');
        $slugSong = $request->route('slug');
        $suscription = false;
        
        if($routeName == 'player.song'){
            $now = new Carbon();
             // VALID SUSCRIPTION
            if(Auth::check()){
                $suscriptionData = Suscription::where('active', 1)
                                            ->where('user_id', auth()->user()->id)
                                            ->where('expires_at', '>=', $now)
                                            ->first();
                if($suscriptionData){
                    $suscription = true;
                }
            }

            if($idSong && $slugSong){
                $song = Song::where('id', $idSong)->where('slug', $slugSong)->first();
                if($song){
                    if(!$song->free){
                        if(Auth::check()){
                            if(!$suscription){
                                return redirect()->route('student.suscription');
                            }
                        }
                        else{
                            return redirect()->route('login');
                        }
                    }
                    return $next($request);
                }
            }
            return redirect()->route('home');
        }
        
        return $next($request);
    }
}

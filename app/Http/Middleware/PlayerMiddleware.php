<?php

namespace App\Http\Middleware;

use Closure;

class PlayerMiddleware
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    // public function handle($request, Closure $next, $role)
    // {
    //     if( auth()->user()->role_id !== (int)$role )
    //     {
    //         return redirect()->route('home');
    //     }
    //     return $next($request);
    // }
}

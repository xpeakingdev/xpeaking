<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
            {
                return [
                    'username' => 'required|min:4',
                    'password' => 'required|min:4'
                ];
            }
        }
     
    }
}

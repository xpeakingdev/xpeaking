<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DictionaryEnglishSpanish;
use App\DictionarySpanishEnglish;
use Illuminate\Support\Facades\Artisan;
use App\Helpers\Helper;
use App\Subtitle;
use App\Language;
// use JoggApp\LaravelGoogleTranslate\GoogleTranslateFacade;
use Google\Cloud\Translate\TranslateClient;
use Storage;


class SuperAdminController extends Controller
{
    public function uploadDictionary(){
        $data = file_get_contents('storage/dictionary/es-en-enwiktionary.txt');
        $dataList = explode("\n", $data);
        $dictionary = [];
        foreach($dataList as $line){
            $lineArray = explode("::", $line);

            // GER FIRST WORD
            $word = "";
            if(isset($lineArray[0])){
                $firstWord = explode(" ", $lineArray[0]);
                $word = strtolower($firstWord[0]);
            }

            $translationList = explode(",", $lineArray[count($lineArray) - 1]);
            $translation = "";

            // GET TRANSLATIONS
            foreach($translationList as $lineTranslation){
                $clearM = str_replace("{m}", "", $lineTranslation);
                $clearF = str_replace("{f}", "", $clearM);
                $clearFP = str_replace("{f-p}", "", $clearF);
                $clearMP = str_replace("{m-p}", "", $clearFP);
                
                // IF NOT EMPTY
                if(strlen($clearMP)>0){
                    if(strlen($translation)>0){
                        $translation .= ",";
                    }
                    $translation .= trim($clearMP);
                }
            }

            $repeat = false;
            foreach($dictionary as $lineDict){
                if($lineDict["word"] == $word){
                    $repeat = true;
                }
            }

            if(!$repeat){
                array_push($dictionary, [
                    "word" => $word,
                    "translation" => $translation
                ]);
            }

        }

        foreach($dictionary as $traduction){
            $word = new DictionarySpanishEnglish([
                "word" => $traduction["word"],
                "translation" => $traduction["translation"],
                "phonetic" => ""
            ]);

            $word->save();
        }

        dd("EXITO");
    }

    public function uploadPhonetic(){
        $data = file_get_contents('storage/dictionary/es_ES_ipa.dsl.txt');
        $dataList = explode("\n\n", $data);
        $phonetics = [];
        foreach($dataList as $line){
            $dataPhonetics = "";
            $lineArray = explode("\n\t", $line);
            if(count($lineArray)>1){
                $clearM = str_replace("[m1]", "", $lineArray[1]);
                $clearM2 = str_replace("[/m]", "", $clearM);
    
                $listPhonetics = explode(",", $clearM2);
                
                foreach($listPhonetics as $linePhonetic){
                    if(strlen($dataPhonetics)>0){
                        $dataPhonetics .= ",";
                    }
                    $dataPhonetics .= str_replace("/", "", $linePhonetic);
                }
    
                array_push($phonetics, [
                    "word" => $lineArray[0],
                    "phonetic" => $dataPhonetics
                ]);
            }

        }
        Storage::disk('local')->put('phonetics/en.json', response()->json($phonetics));
        dd($phonetics);

        foreach($phonetics as $phonetic){
            $word = DictionarySpanishEnglish::where("word", $phonetic["word"])->first();

            if($word){
                $word->phonetics = $phonetic["phonetic"];
                $word->save();
            }

        }

        dd("EXITO");
    }

    public function uploadSubtitle(){
        $from = 'en';
        $to = 'es';
        $word = 'what';
        Artisan::call('cache:clear');
        Artisan::call('config:cache');
        Artisan::call('route:clear');
        Artisan::call('view:clear');

        $response = file_get_contents('https://translate.google.com/translate_a/single?sl='.$from.'&tl='.$to.'&hl=gl&dt=at&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&otf=1&ssel=5&tsel=5&kc=4&q='.$word.'&client=tw-ob&ie=UTF-8');

        $translation = json_decode($response, true);
        $translate = [];
        // dd($translation);

        foreach($translation as $index => $line){
            if($index==0){
                $wordTranslation = $line[0][0];
                $wordOriginal = $line[0][1];
                $translate["word"] = $wordOriginal;
                $translate["translation"] = $wordTranslation;
            }
            else if($index==1){
                foreach($line as $indexType => $typeWord){
                    if($typeWord[0] == "preposición" || $typeWord[0] == "conxunción"){
                        $translate["prepositions"] = $typeWord[1];
                        dd(1);
                    }
                    else if($typeWord == "adverbio"){
                        $translate["adbervs"] = $typeWord[1];
                    }
                    else if($typeWord == "adxectivo" || $typeWord ==""){
                        $translate["adxectivo"] = $typeWord[1];
                    }
                    else if($typeWord == "pronome" || $typeWord == ""){
                        $translate["pronouns"] = $typeWord[1];
                    }
                }
            }
        }
        
        dd($translate);

    }

    public function scraping(){
        $response = file_get_contents('https://www.wordreference.com/es/translation.asp?tranword=what');

        $translation = json_decode($response, true);
        dd($response);

    }

    public function upLoadWords(){
        // INSTANCE GOOGLE TRANSLATE
        $total = [];
        $googleTranslate = new TranslateClient([
            'key' => config('googletranslate.api_key')
        ]);
        
        // LOAD SUBTITLES
        $subtitles = Subtitle::orderBy('id')->with('language')->get();
        $languages = Language::orderBy('name')->get();

        foreach($subtitles as $subtitle){
            foreach($languages as $language){
                if($language->id != $subtitle->language_id){
                    // $words = Helper::getWords('selena-en.srt', 0, 0);
                    $words = Helper::getWords($subtitle->file, $subtitle->language, $language);
                    $phonetics = Helper::getPhonetics($subtitle->language);
                    $count = 0;
                    // array_push($total, $words);
                    // SEARCH PHONETIC
                    
                    foreach($words as $word){
            
                        $translate = $googleTranslate->translate($word, [
                            'source' => $subtitle->language->code,
                            'target' => $language->code
                        ]);
            
                        // INSERT WORD
            
                        $dataWord = [
                            "word" => $word,
                            "translation" => str_replace('&#39;', '', $translate["text"]),
                            
                        ];
            
                        // GET AUDIO
                        $audio = Helper::getAudio($word, $subtitle->language, "");
                        if($audio){
                            $dataWord["audio"] = $audio;
                        }
                        
                        foreach($phonetics as $phonetic){
                            if($word == $phonetic["word"]){
                                $dataWord["phonetics"] = $phonetic["phonetic"];
                            }
                        }
            
                        if($subtitle->language->code=="en" && $language->code=="es"){
                            $newWord = new DictionaryEnglishSpanish($dataWord);
                        }
                        if($subtitle->language->code=="es" && $language->code=="en"){
                            $newWord = new DictionarySpanishEnglish($dataWord);
                        }
                        
                        if($newWord->save()){
                            $count++;
                        }
                    }
                    
                }
            }
            if($count){
                array_push($total, $count);
            }
        }

        dd($total);

    }
}

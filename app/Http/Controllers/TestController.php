<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\Welcome;
use App\User;
use Carbon\Carbon;
use Google\Cloud\Speech\V1p1beta1\SpeechClient;
use Google\Cloud\Speech\V1p1beta1\RecognitionAudio;
use Google\Cloud\Speech\V1p1beta1\RecognitionConfig;
use Google\Cloud\Speech\V1p1beta1\RecognitionConfig\AudioEncoding;
use Storage;
use Illuminate\Support\Facades\File;
use FFMpeg;

class TestController extends Controller
{
    public function index(){
        // 2979ff
        $user = User::first();
        Mail::to("cristhianquispeq@gmail.com")->send(new Welcome($user));
        return config('mail.from.address');
    }

    public function showDate(){
        return Carbon::now()->format('yy-m-d');
    }
    public function speech()
    {
    	$general = [];
    	$content = Storage::get('/subtitles/voice.mp3');
        
    	$audio = (new RecognitionAudio())
    			->setContent($content);
    	# The audio file's encoding, sample rate and language
		$config = new RecognitionConfig([
		    'encoding' => AudioEncoding::MP3,
		    'sample_rate_hertz' =>16000,
            'audio_channel_count' => 2,
		    'language_code' => 'en-US'
		]);

		$client = new SpeechClient([
            'credentials' => storage_path('google/xpeaking-f4d4eacc7c14.json')
        ]);
        $response = $client->recognize($config, $audio);
        foreach ($response->getResults() as $result) {
		    $alternatives = $result->getAlternatives();
		    $mostLikely = $alternatives[0];
		    $transcript = $mostLikely->getTranscript();
		    array_push($general, $transcript);
		}
		$client->close();
    	dd($general);
    }
    public function speechtest(Request $request)
    {
        // dd($request->file);
        $success = false;
        $words = $str_arr = explode (",", $request->text);
        $message = "Oh no, tuvimos un problema";
        $correct = [];
        if($request->file !== 'undefined' && $request->file){
            // en caso de que el mensaje sea un archivo
            $general = [];

            $extension = $request->file->getClientOriginalExtension();
            $name = $request->file('name');

            $file = $request->file('file');

            $filename = $file->getClientOriginalName(); 

            $location = storage_path('app/public/temp/audio');

            // $file->move($location,$filename);
            // dd($general);
            $audio = (new RecognitionAudio())
                ->setContent(file_get_contents($file));
            # The audio file's encoding, sample rate and language
            $config = new RecognitionConfig([
                'encoding' => AudioEncoding::MP3,
                'sample_rate_hertz' =>16000,
                'audio_channel_count' => 2,
                'language_code' => 'en-US'
            ]);
            $client = new SpeechClient([
                'credentials' => storage_path('google/xpeaking-f4d4eacc7c14.json')
            ]);
            $response = $client->recognize($config, $audio);
            foreach ($response->getResults() as $result) {
                $alternatives = $result->getAlternatives();
                $mostLikely = $alternatives[0];
                $transcript = $mostLikely->getTranscript();
                array_push($general, $transcript);
            }
            $client->close();
            if (count($general)>0) {

                $resultado = $general[0];
                $wordsgoogle = explode(" ", $resultado);
                // dd($words,$wordsgoogle);

                // if (count($words)>=count($wordsgoogle)) {
                    
                    for ($i = 0; $i < count($words) ; $i++) {
                        if (!empty($wordsgoogle[$i])) {
                            similar_text(strtolower($words[$i]),strtolower($wordsgoogle[$i]),$percent);
                            // dd($words[$i],$wordsgoogle[$i],$percent);
                            if ($percent>=80) {
                                array_push($correct,true);
                            }
                            else{
                                if(!empty($wordsgoogle[$i+1])) {
                                    similar_text($words[$i],$wordsgoogle[$i+1],$percent);
                                    if ($percent>=80) {
                                        array_push($correct,true);
                                    }
                                    else{
                                        if (!empty($wordsgoogle[$i-1])) {
                                            similar_text($words[$i],$wordsgoogle[$i-1],$percent);
                                            if ($percent>=80) {
                                                array_push($correct,true);
                                            }
                                            else {
                                                array_push($correct,false);
                                            }
                                        }
                                        else{
                                            array_push($correct,false);
                                        }
                                    }
                                }
                                else{
                                    if (!empty($wordsgoogle[$i-1])) {
                                        similar_text($words[$i],$wordsgoogle[$i-1],$percent);
                                        if ($percent>=80) {
                                            array_push($correct,true);
                                        }
                                        else{
                                            array_push($correct,false);
                                        }
                                    }
                                    else{
                                        array_push($correct,false);
                                    }
                                }
                                
                            }
                        }
                        else{
                            array_push($correct,false);
                        }
                    }
                // }
                $success = true;
                $message = "Puedes Mejorar";
                $count = 0;
                foreach ($correct as $value) {
                    if ($value == true) {
                        $count++;
                    }
                }
                $percent = ($count*100)/count($words);
                if ($percent>50) {
                    $message = "Excelente";
                }
                return response()->json([
                    "success" => $success,
                    "comparewords" => $correct,
                    'message' => $message
                ]);
            }
            else{
                $message = "El audio no está claro";
            }
            $success = false;
            return response()->json([
                "success" => $success,
                "comparewords" => null,
                "message" => $message
            ]);
        
        }
        return response()->json([
            "success" => $success
        ]);
    }
}

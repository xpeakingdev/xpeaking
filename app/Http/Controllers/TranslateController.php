<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DictionaryEnglishSpanish;
use App\DictionarySpanishEnglish;

class TranslateController extends Controller
{
    public function translate(Request $request){
        $translate = [
            "word" => $request->word,
            "translation" => "",
            "phonetic" => null,
            "audio" => null,
            "loaded" => true
        ];
        $word = null;
        if($request->sourceLanguage == "en" && $request->targetLanguage == "es"){
            $word = DictionaryEnglishSpanish::where('word', $request->word)->first();
        }
        else if($request->sourceLanguage == "es" && $request->targetLanguage == "en"){
            $word = DictionarySpanishEnglish::where('word', $request->word)->first();
        }
        
        if($word){
            $success = true;
            $translation = $word->translation;
            $phonetics = explode(",", $word->phonetics);
            $phonetic = '';
            if(isset($phonetics[0])){
                $phonetic = $phonetics[0];
            }
            $translate = [
                "word" => $word->word,
                "translation" => $translation,
                "phonetic" => $phonetic,
                "audio" => $word->audio,
                "loaded" => true
            ];
        }

        return response()->json([
            "word" => $translate
        ]);
    }
}

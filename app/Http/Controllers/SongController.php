<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Song;

class SongController extends Controller
{
    public function show($id, $slug){

        $song = Song::where('id', $id)
                    ->where('slug', $slug)
                    ->where('published', 1)
                    ->with('artists')
                    ->with('genders')
                    ->with('languages')
                    ->with('subtitles')
                    ->first();
        if($song){
            return view('content.song.index', compact('song'));
        }
        else{
            return redirect()->route('home');
        }
    }
}

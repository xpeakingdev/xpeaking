<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\SuscriptionHelper;
use App\User;
use App\Song;
use App\Artist;

class HomeController extends Controller
{

    public function home()
    {
        if(Auth::check()){
            SuscriptionHelper::CheckSuscription(Auth::user()->id);
            $songs = Song::orderBy('order')
                        ->where('important', 1)
                        ->where('published', 1)
                        ->with('artists')
                        ->take(10)
                        ->get();

            $artists = Artist::orderBy('order')
                            ->where('important', 1)
                            ->take(10)
                            ->get();

            return view('home.auth', compact('songs', 'artists'));
        }
        else{
            $songs = Song::orderBy('order')
                    ->where('free', 1)
                    ->where('published', 1)
                    ->with('artists')
                    ->take(10)
                    ->get();
        }

        return view('home.guest', compact('songs'));
    }
    public function hometest()
    {
        return view('home.index');
    }

    public function verified(){
        if(Auth::check()){
            if((int)auth()->user()->status == User::NOTVERIFIED){
                return view('status.not-verified');
            }
        }
    }

    public function locked(){
        if(Auth::check()){
            if((int)auth()->user()->status == User::LOCKED){
                return view('status.locked');
            }
        }
    }

    public function test(){
        return "hola";
    }

    public function dataUserAuth(){
        $user = auth()->user();
        return response()->json([
            "user" => $user
        ]);
    }
    public function ambassador()
    {
        return view('home.ambassador');
    }
}

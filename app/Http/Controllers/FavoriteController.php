<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArtistFavorite;
use App\FavoriteSong;

class FavoriteController extends Controller
{
    public function storeArtists(Request $request){
        $success = false;
        $favoriteStatus = false;
        $student = auth()->user()->student;
        
        $exist = ArtistFavorite::where('student_id', $student->id)->where('artist_id',$request->artistId)->first();

        if($exist){
            $exist->delete();
        }
        else{
            $favorite = new ArtistFavorite([
                "student_id" => $student->id,
                "artist_id" => $request->artistId
            ]);
            
            if($favorite->save()){
                $success = true;
                $favoriteStatus = true;
            }
        }



        return response()->json([
            "success" => $success,
            "favorite" => $favoriteStatus
        ]);
    }

    public function listArtists(){
        $student = auth()->user()->student;
        $favorites = ArtistFavorite::where('student_id', $student->id)->get();
        return response()->json([
            "favorites" => $favorites
        ]);
    }

    public function listSongs(){
        $student = auth()->user()->student;
        $favorites = FavoriteSong::where('student_id', $student->id)->get();
        return response()->json([
            "favorites" => $favorites
        ]);
    }

    public function storeSongs(Request $request){
        $success = false;
        $favoriteStatus = false;
        $student = auth()->user()->student;
        
        $exist = FavoriteSong::where('student_id', $student->id)->where('song_id',$request->songId)->first();

        if($exist){
            $exist->delete();
        }
        else{
            $favorite = new FavoriteSong([
                "student_id" => $student->id,
                "song_id" => $request->songId
            ]);
            
            if($favorite->save()){
                $success = true;
                $favoriteStatus = true;
            }
        }



        return response()->json([
            "success" => $success,
            "favorite" => $favoriteStatus
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\DictionarySpanishEnglish;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Helpers\StatisticHelper;
use App\FavoriteSong;
use App\GemsHistory;
use Carbon\Carbon;
use App\Subtitle;
use App\Language;
use App\Student;
use App\Tracing;
use App\Report;
use App\User;
use App\Song;
use App\Role;
use App\UserEvaluation;
use Session;


class PlayerController extends Controller
{
    public function player($id, $slug){

        $song = Song::where('id', $id)
                    ->where('slug', $slug)
                    ->where('published', 1)
                    ->with('artists')
                    ->first();
        $passed = false;

        if($song){
            if(Auth::check() || $song->free || Auth::user()->role->id==Role::ADMIN){
                $passed = true;
            }
        }
        else{
            return redirect()->route('home');
        }

        if($passed){
            return view('content.player.song.index', compact('song'));
        }
        else{
            return redirect()->route('login');
        }
    }

    public function getData(Request $request){
        $audioRoute = 'https://xpeakingprueba.s3.us-east-2.amazonaws.com';
        $languages = Language::orderBy('name')->get();
        $user = null;
        $targetLanguage = null;
        $learnLanguage = null;

        $song = Song::where('id', $request->id)
        ->where('slug', $request->slug)
        ->with('artists')
        ->with('subtitles')
        ->with('languages')
        ->first();

        if($song){
            $song->favorite = false;
        }

        if(Auth::check()){
            if(auth()->user()->role->id==Role::STUDENT){
                $user = User::where('id', auth()->user()->id)
                            ->select('id', 'username', 'email', 'picture')
                            ->with('profile')
                            ->with('student.languages')
                            ->first();
                            
                $favorite = FavoriteSong::where('student_id', $user->student->id)
                                        ->where('song_id', $song->id)
                                        ->first();
                $lastest = StatisticHelper::TestInfo($user->id,$song->id);
                foreach($user->student->languages as $sTL){
                    if($sTL->speak){
                        $targetLanguage = $sTL;
                    }
                    else if($sTL->learn){
                        $learnLanguage = $sTL;
                    }
                }

                if($favorite){
                    $song->favorite = true;
                }
            }
        }
        
        // SUBTITLES
        $subtitlesList = Subtitle::where('song_id', $song->id)
                                ->where('active', true)
                                ->with('language')
                                ->get();
        if (!auth::check() || auth()->user()->role->id == Role::ADMIN) {
            $user = new User;
            $user->username = 'useruncheck';
            $student = new Student;
            $student->gems = 0;
            $student->gems_available = 0;
            // $languages = Language::where('code','es')->get();
            $user->student = $student;
            $user->student->languages = $languages;
            $targetLanguage = Language::where('code','es')->first();
            $learnLanguage = Language::where('code','en')->first();
            $subtitlesList = Subtitle::where('song_id', $song->id)
                                ->with('language')
                                ->get();
            $lastest = null;
        }    
        $subtitles = [];
        if($targetLanguage){
            $langTarget = $targetLanguage;
        }
        foreach ($subtitlesList as $index => $subtitle) {

            if($targetLanguage){
                if($subtitle->language->id==$targetLanguage->id){
                    $langTarget = $learnLanguage;
                }
            }
            else{
                if($subtitle->language->name=="Español"){
                    $langTarget = $subtitle->language;
                }
            }
            $suntitleFormated = Helper::getSubtitle($subtitle->file, $subtitle->language, $langTarget);
            
            array_push($subtitles, [
                "file" => $subtitle->file,
                "status" => $subtitle->status,
                "native" => $subtitle->native,
                "language" => $subtitle->language,
                "formated" => $suntitleFormated,
                "active" => $subtitle->native,
                "currentLine" => [],
            ]);

        }

        return response()->json([
            "lastest" => $lastest,
            "user" => $user,
            "song" => $song,
            "subtitles" => $subtitles,
            "languages" => $languages,
            "audioRoute" => $audioRoute
        ]);
    }
    
    public function gemsStore(Request $request){
        $success = false;
        $gems = 0;
        if(auth()->user()->student){
            $student = Student::where('user_id', auth()->user()->id)->first();
            $student->gems = intval($student->gems) + intval($request->gems);
            if($student->save()){
                $gems = $student->gems;
                $success = true;
                $gemsHistory = new GemsHistory([
                    'description' => 'POINTS WON',
                    'gems' => $request->gems,
                    'student_id' => $student->id
                ]);
                $gemsHistory->save();
            }
        }

        return response()->json([
            "success" => $success,
            "gems" => $gems
        ]);
    }

    public function report(Request $request){
        $userId = auth()->user()->id;
        $status = "warning";
        $message = "No pudimos guardar su reporte, intentelo de nuevo mas tarde por favor";
        $exist = Report::where('user_id', $userId)
                        ->where('word', $request->word)
                        ->where('source_language_id', $request->sourceLanguageId)
                        ->where('target_language_id', $request->targetLanguageId)
                        ->first();
        $permited = true;
        if($exist){
            $dateNow = new Carbon();
            $dateReport = new Carbon($exist->created_at);
            $hours = $dateNow->diffInHours($dateReport);
            if($hours<1){
                $permited = false;
                $message ="Puedes enviarnos reportes de una misma traduccion cada 24 horas";
            }
        }

        if($permited){
            $report = new Report([
                "word" => $request->word,
                "content" => $request->content,
                "source_language_id" => $request->sourceLanguageId,
                "target_language_id" => $request->targetLanguageId,
                "user_id" => $userId
            ]);

            if($report->save()){
                $status = "success";
                $message = "Recibimos tu reporte exitosamente, lo atenderemos muy pronto";
            }
        }

        return response()->json([
            "status" => $status,
            "message" => $message
        ]);
    }

    public function setView(Request $request){
        if(Auth::check()){
            if(auth()->user()->role->id == Role::STUDENT){
                $now = new Carbon();
                $date = $now->toDateString();
                $timeStart = $now->toTimeString();
                $tracing = new Tracing([
                    "date" => $date,
                    "time_start" => $timeStart,
                    "user_id" => auth()->user()->id,
                    "song_id" => $request->songId
                ]);
                $tracing->save();
            }
        }
        return response()->json([
            "success" => true
        ]);
    }
}

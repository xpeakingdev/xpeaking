<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Dictionary;
use App\Language;
use App\DictionarySpanishEnglish;
use App\DictionaryEnglishSpanish;

class DictionaryController extends Controller
{
    public function index(){
        return view('user.dictionary.index');
    }

    public function list(Request $request){
        $student = auth()->user()->student;
        $languageSource = $request->languageSource;
        $languageTarget = $request->languageTarget;

        $dictionary = Dictionary::where('student_id', $student->id)
                                ->where('source_language_id', $languageSource)
                                ->where('target_language_id', $languageTarget)
                                ->get();
        $wordSearch = [];
        $words = [];
        foreach($dictionary as $wordS){
            array_push($wordSearch, $wordS->word);
        }

        if($languageSource == Language::SPANISH && $languageTarget == Language::ENGLISH){
            $words = DictionarySpanishEnglish::whereIn('word', $wordSearch)->get();
        }
        else if($languageSource == Language::ENGLISH && $languageTarget == Language::SPANISH){
            $words = DictionaryEnglishSpanish::whereIn('word', $wordSearch)->get();
        }

        return response()->json([
            "words" => $words
        ]);
    }

    public function delete(Request $request){
        $success = true;
        $student = auth()->user()->student;
        $languageSource = $request->languageSource;
        $languageTarget = $request->languageTarget;
        $word = $request->word;

        $dictionary = Dictionary::where('student_id', $student->id)
                                ->where('source_language_id', $languageSource)
                                ->where('target_language_id', $languageTarget)
                                ->where('word', $word)
                                ->first();
        if($dictionary){
            $success = true;
            $dictionary->delete();
        }

        return response()->json([
            "success" => $success
        ]);
    }
}

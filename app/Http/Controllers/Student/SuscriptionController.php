<?php

namespace App\Http\Controllers\Student;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Helpers\SuscriptionHelper;
use App\Suscription;
use App\Sponsorship;
use Carbon\Carbon;
use App\History;
use App\Order;
use App\Plan;
use App\User;


class SuscriptionController extends Controller
{
    public function index(){
        return view('user.suscription.index');
    }

    public function dataSuscription(){

        SuscriptionHelper::CheckSuscription(Auth::user()->id);
        $suscription = null;
        $order = null;
        $dateRenewal= null;

        $user = auth()->user();
        $pointsUser = $user->points_available;
        $suscription = Suscription::where('active', 1)
                                ->where('user_id', $user->id)
                                ->get();
        
        $order = Order::where('status', Order::PENDING)
                    ->where('user_id', $user->id)
                    ->first();
        $orderTotals = Order::where([['status',Order::PENDING],['description', 'Register'],['user_id',$user->id]])->first();
        $new = false;
        if($orderTotals){
            $new = true;
        }
        if(count($suscription)>=1){
            $expires= new Carbon($suscription[0]->expires_at);
            foreach ($suscription as $key => $value) {
                $compare = new Carbon($value->expires_at);
                if ($compare > $expires) {
                    $expires = $compare;
                }
            }
            $now = new Carbon();
            $daysBetween = $now->FloatDiffInDays($expires);
            $dateRenewal = $expires->day . " de " . Helper::getMonthName($expires->month - 1) . " del " . $expires->year;
        }
        // else if($order->description == ){
            
        // }
        else{
            $suscription = Null;
        }

        return response()->json([
            "new"=>$new,
            "suscription" => $suscription,
            "order" => $order,
            "dateRenewal" => $dateRenewal,
            "pointsUser" => $pointsUser
        ]);
    }
    public function payment(Request $request){
        $user = auth()->user();
        $plan = Plan::find($request->planId);
        $order = null;
        $suscription = null;
        $now = Carbon::now();
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos procesar el pago';
        $paymentMode = $request->payMode;
        $order = Order::where('status', Order::PENDING)
                    ->where('id', $request->orderId)
                    ->first();
        $pointsRequired = intval(intval($order->ammount) * Helper::valuePoint());

        $suscription = Suscription::where('user_id', $user->id)
                                ->where('active', 1)
                                ->get()
                                ->last();

        if($order){
            if($pointsRequired <= $user->points_available){
                $order->payment_mode = $paymentMode;
                $order->status = Order::COMPLETE;
                $order->date_payment = $now;
                $order->points = $pointsRequired;
                $order->plan_id = $plan->id;
                $order->paid_by = Auth::user()->id;
                if($order->save()){
                    $now = new Carbon();
                    $success = true;
                    Helper::UpdateSponsorship($user->id,true,0);
                    Helper::UpdateMailExpired($user->id);
                    $message = "Tu suscripción ha sido PAGADA exitosamente.";

                    $suscriptionConfirm = Suscription::where('active', 1)
                                                ->where('user_id', $order->user_id)
                                                ->where('expires_at', '>=', $now)
                                                ->get();
                    if (count($suscriptionConfirm)>=1) {
                        $dateInit=  new Carbon($suscriptionConfirm[0]->expires_at);
                        $dateEnd = Carbon::parse($suscriptionConfirm[0]->expires_at)->addMonth();
                    }
                    else{
                        $dateInit = Carbon::now();
                        $dateEnd = Carbon::now()->addMonth();
                    }


                    $suscription = new Suscription([
                        "active" => true,
                        "init_at" => $dateInit,
                        "expires_at" => $dateEnd,
                        "user_id" => $order->user_id,
                        "plan_id" => $plan->id,
                        "order_id" => $order->id
                    ]);
                    $suscription->save();

                    $user->points_available = $user->points_available - $pointsRequired;
                    $user->save();
                    Helper::setPointsSponsors($order->user_id, $plan->id, $order->id);
                    $history = new History([
                        "description" => History::DESCRIPTIONS_PAYMENT_MY_SUSCRIPTION,
                        "points" => ($pointsRequired * -1),
                        "user_id" => $user->id,
                        "order_id" => $order->id,
                        "type" => History::RENEWAL
                    ]);
                    $history->save();
                    
                    // Helper::setPointsSponsors($user->id, $plan->id, $order->id);
                }

            }
            else{
                $message ="Lo sentimos, no cuentas con los puntos suficientes para pagar tu suscripcion";
            }
        }


        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function history(Request $request){
        $user = auth()->user();
        $orders = [];
        $ordersList = Order::orderBy('date_payment', 'desc')
                        ->where('status', Order::COMPLETE)
                        ->where('user_id', $user->id)
                        ->get();

        foreach($ordersList as $order){

            $paymentsMethods = [
                Order::CARD => "Tarjeta",
                Order::TRANSFER => "Transferencia",
                Order::POINTS => "Puntos",
                Order::FREE => "Gratis"
            ];

            $userPayment = $user->username;
            if($order->paid_by){
                $userP = User::find($order->paid_by);
                if($userP){
                    $userPayment = $userP->username;
                }
            }
            // FORMAT DATE PAY
            if ($order->date_payment) {
                $datePay = (new Carbon($order->date_payment))->format('d/m/yy - H:i');
            }
            else{
                $datePay = '';
            }
            
            $dateOrder = (new Carbon($order->created_at))->format('d/m/yy - H:i');

            $ammount = 'S/ ' . $order->ammount;
            if($order->payment_mode == Order::POINTS){
                $ammount = $order->points;
            }

            array_push($orders, [
                "id" => $order->Id,
                "dateCreated" => $dateOrder,
                "datePayment" => $datePay,
                "ammount" => $ammount,
                "paymentMethod" => $paymentsMethods[$order->payment_mode],
                "userPayment" =>  $userPayment
            ]);
        }

        return response()->json([
            "orders" => $orders
        ]);
    }

    public function uploadFile(Request $request){
        $user = auth()->user();
        $success = false;
        $message = 'Oh no, no pudimos subir el archivo';
        $order = Order::find($request->orderId);
        if($order){
            if($request->file){
                $date = new Carbon();
                $route = 'orders/files/';
                $name = 'voucher-'. $order->id. '-' . $user->id .'.'.$request->extension;
                Storage::disk('public')->put($route.$name, File::get($request->file));
                $order->file = $name;
                $order->upload_file = $date;
                
                if($order->save()){
                    $success = "true";
                    $message = "Archivo guardado, Tu suscripción aún está pendiente de aprobación";
                }
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }
}

<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Student;
use App\ArtistFavorite;
use App\FavoriteSong;

class FavoriteController extends Controller
{
    public function index(){
        return view('user.favorite.index');
    }

    public function songs(){
        $student = auth()->user()->student;
        $favorites = Student::where('id', $student->id)->with('songs.artists')->first();
        $songs = $favorites->songs;
        // $favorites = ArtistFavorite::where('student_id', $student->id)->get();
        return response()->json([
            "songs" => $songs
        ]);
    }

    public function artists(){
        $student = auth()->user()->student;
        $favorites = Student::where('id', $student->id)->with('artists')->first();
        $artists = $favorites->artists;
        // $favorites = ArtistFavorite::where('student_id', $student->id)->get();
        return response()->json([
            "artists" => $artists
        ]);
    }
}

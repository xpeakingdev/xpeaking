<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserEvaluation;

class EvaluationController extends Controller
{
    //
    public function TranscripTest(Request $request)
    {
    	$message = 'Sigue intentando';
        similar_text(strtolower($request->textoriginal),strtolower($request->texttranscribe),$percent);
        switch ($percent) {
        	case ($percent>0 && $percent<=20):
        		$message = 'Sigue esforzándote';
        		break;
        	case ($percent>20 && $percent<=40):
        		$message = '!Qué bien!';
        		break;
        	case ($percent>40 && $percent<=60):
        		$message = '¡Súper Bien!';
        		break;
        	case ($percent>60 && $percent<=80):
        		$message = '¡Genial!';
        		break;
        	case ($percent>80 && $percent<=100):
        		$message = '¡Maravilloso!';
        		break;
        	default:
        		// code...
        		break;
        }

        return response()->json([
        	"message" => $message,
        	"percentage" => intval($percent).'%'
        ]);

    }
    public function SaveResults(Request $request)
    {
        $test = new UserEvaluation;
        $test->type_test = 0;
        $test->score = 0;
        $test->challenge = 0;
        $test->range = 0;
        $test->user_id = 0;
        $test->song_id = 0;
    }
}

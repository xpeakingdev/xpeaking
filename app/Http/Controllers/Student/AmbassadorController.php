<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\PaymentSuscriptionSponsor;
use App\Sponsorship;
use App\Suscription;
use App\User;
use App\Range;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\History;
use App\Plan;
use App\Order;

class AmbassadorController extends Controller
{
    public function index(){
        return view('user.ambassador.index');
    }

    public function list(Request $request){
        $user = auth()->user();
        $levels = $user->limit_levels;
        $students = [];
        $sponsorIds = [$user->id];
        
        for($i=1; $i <= $levels; $i++){
            $actives = 0;
            $total = 0;
            $idRelatives = [];
            foreach($sponsorIds as $id){
                
                $sponsors = Sponsorship::where([['active',true],['sponsor_id', $id]])
                                        ->get();
                // if ($i==4 &&$id==10) {
                //     dd($sponsors);
                // }
                $total += count($sponsors);
                foreach($sponsors as $sponsor){
                    array_push($idRelatives, $sponsor->sponsored_id);
                    // VALID ACTIVE
                    $validActive = User::where('id', $sponsor->sponsored_id)
                                        ->where('status', User::ACTIVE)
                                        ->whereHas('suscriptions', function($q){
                                            $q->where('active', true);
                                        })
                                        ->first();
                    if($validActive){
                        $actives++;
                    }
                    // if($sponsor->active){
                    //     $actives++;
                    // }
                }
            }
            array_push($students, [
                "level" => $i,
                "actives" => $actives,
                "total" => $total
            ]);
            $sponsorIds = $idRelatives;
        }

        return response()->json([
            "students" => $students,
            "user" => $user
        ]);
    }

    public function teamDetail(Request $request){
        $range = Range::find($request->rangeId);
        if($range){
            $user = auth()->user();
            $levels = $user->limit_levels;
            $students = [];
            $sponsorIds = [$user->id];
            $actives = 0;
            
            for($i=1; $i <= $levels; $i++){
                $idRelatives = [];
                foreach($sponsorIds as $id){
                    $sponsors = Sponsorship::where([['active',true],['sponsor_id', $id]])
                                            ->get();
                    foreach($sponsors as $sponsor){
                        array_push($idRelatives, $sponsor->sponsored_id);
                        // VALID ACTIVE
                        $validActive = User::where('id', $sponsor->sponsored_id)
                                            ->with('suscriptions')
                                            ->first();
                        if($i == $range->level && $validActive){
                            $sponsorMain = User::where('id', $sponsor->sponsor_id)->first();
                            $validActive["active"] = false;
                            $date = (new Carbon($validActive["created_at"]))->format('d/m/yy - H:i');
                            $validActive["date"] = $date;
                            $validActive["sponsor"] = $sponsorMain->username;
                            foreach($validActive->suscriptions as $suscription){
                                if($suscription->active){
                                    $validActive["active"] = true;
                                }
                            }

                            $order = Order::where('user_id', $validActive->id)
                                        ->where('status', Order::COMPLETE)
                                        ->first();
                            $datePayment = '----';
                            if($order){
                                if ($order->date_payment) {
                                    $datePayment = (new Carbon($order->date_payment))->format('d/m/yy - H:i');
                                }
                                else{
                                    $datePayment = '';
                                }
                            }

                            $validActive["date_payment"] = $datePayment;

                            array_push($students, $validActive);

                        }
                        // if($sponsor->active){
                        //     $actives++;
                        // }
                    }
                }
                $sponsorIds = $idRelatives;
            }
        }
        
        return response()->json([
            "students" => $students
        ]);
    }

    public function userData(Request $request){
        $user = User::where('id', $request->userId)
                    ->with('profile')
                    ->with('student.languages')
                    ->with('profile.city.region.country')
                    ->first();

        $age = null;

        if($user->profile->birthdate){
            $date = new Carbon();
            $birthdate = new Carbon($user->profile->birthdate);
            $age = $date->diffInYears($birthdate);
        }

        $active = false;
        $suscription = Suscription::where('active', true)
                                    ->where('user_id', $user->id)
                                    ->first();
        if($suscription){
            $active = true;
        }

        return response()->json([
            "user" => $user,
            "age" => $age,
            "active" => $active
        ]);
    }

    public function pointsResume(){
        $user = auth()->user();
        $pointsTotal = 0;
        $pointsAvailable = $user->points_available;

        $history = History::where('user_id', $user->id)
                            ->orderBy('created_at', 'desc')
                            ->get();
        foreach($history as $hist){
            $hist["date"]= (new Carbon($hist->created_at))->format('d/m/yy - H:i');
            if($hist->type == History::INVITED || $hist->type == History::REFERRED){
                $pointsTotal += $hist->points;
            }
        }
        $order = Order::where('user_id', $user->id)
                                        ->where('status', Order::PENDING)
                                        ->first();
        $plan = Plan::where('recommended', true)->first();
        if ($order) {
            $plan->price = $order->ammount;
        }

        return response()->json([
            "history" => $history,
            "pointsTotal" => $pointsTotal,
            "pointsAvailable" => $pointsAvailable,
            "plan" => $plan
        ]);
    }

    public function transferPoints(Request $request){
        $user = User::find(auth()->user()->id);
        $userTransfer = User::where('username', $request->username)->first();
        $points = $request->points;
        
        if($userTransfer){
            $user->points_available = $user->points_available - $points;
            if($user->save()){
                $history = new History([
                    "description" => (History::DESCRIPTIONS_TRASNFER_TO_POINTS . $userTransfer->username),
                    "points" => ($points*-1),
                    "type" => History::TRANSTO,
                    "user_id" => $user->id
                ]);
                $history->save();
                
                $userTransfer->points = $userTransfer->points + $points;
                $userTransfer->points_available = $userTransfer->points_available + $points;
                $userTransfer->save();

                $historyTransfer = new History([
                    "description" => (History::DESCRIPTIONS_TRASNFER_FROM_POINTS . $user->username),
                    "points" => $points,
                    "type" => History::TRANSFROM,
                    "user_id" => $userTransfer->id
                ]);
                $historyTransfer->save();
            }
        }

        return response()->json([
            "success" => true
        ]);
    }

    public function paymentSuscription(Request $request){
        $user = auth()->user();
        $userPayed = User::where('username', $request->username)->first();
        $plan = Plan::find($request->planId);
        $order = null;
        $suscription = null;
        $now = Carbon::now();
        $success = false;
        $new = false;
        $message = 'Oh no, algo salio mal, no pudimos procesar el pago';
        $paymentMode = Order::POINTS;

        if ($user->status == User::NOTVERIFIED) {
            return response()->json([
                "success" => $success,
                "message" => 'El usuario no ha confirmado su correo'
            ]);
        }
        if($userPayed){
            if($userPayed->status == User::ACTIVE){
                $order = Order::where('status', Order::PENDING)
                                ->where('user_id', $userPayed->id)
                                ->first();
                if($order){
                    $points = intval(Helper::valuePoint() * intval($order->ammount));
                    $message = 'El usuario que ingreso ya cuenta con una suscripcion activa';
                    $order->payment_mode = $paymentMode;
                    $order->status = Order::COMPLETE;
                    $order->date_payment = $now;
                    // $order->points = $points;
                    $order->points = $points;
                    $order->paid_by = $user->id;
                    $order->plan_id = $plan->id;
                        
            
                    if($order->save()){
                        $success = true;
                        $message = "Pago procesado exitosamente";
                        $now = new Carbon();
                        $suscriptionConfirm = Suscription::where('active', 1)
                                                ->where('user_id', $order->user_id)
                                                ->where('expires_at', '>=', $now)
                                                ->get();
                        if (count($suscriptionConfirm)>=1) {
                            $dateInit=  new Carbon($suscriptionConfirm[0]->expires_at);
                            $dateEnd = Carbon::parse($suscriptionConfirm[0]->expires_at)->addMonth();
                        }
                        else{
                            $dateInit = Carbon::now();
                            $dateEnd = Carbon::now()->addMonth();
                        }
                        $suscription = new Suscription([
                            "active" => true,
                            "init_at" => $dateInit,
                            "expires_at" => $dateEnd,
                            "user_id" => $order->user_id,
                            "plan_id" => $plan->id,
                            "order_id" => $order->id
                        ]);
                        $suscription->save();
                        Helper::UpdateMailExpired($userPayed->id);
                        Helper::UpdateSponsorship($userPayed->id,true,0);
                        $user->points_available = $user->points_available - $points;
                        $user->save();

                        $history = new History([
                            "description" => (History::DESCRIPTIONS_PAYMENT_SUSCRIPTION_SPONSOR . $userPayed->username),
                            "points" => ($points * -1),
                            "user_id" => $user->id,
                            "order_id" => $order->id,
                            "type" => History::PAYSPONSOR
                        ]);
                        $history->save();
                        
                        $userPayed["userPay"] = $user->username;
                        Mail::to($userPayed->email)->send(new PaymentSuscriptionSponsor($userPayed));
                        // Helper::addPointsSponsor($order->user_id, $plan->id, $order->id);
                        Helper::setPointsSponsors($userPayed->id, $plan->id, $order->id);
                    }
                }
                else{
                    $message = 'El usuario ya cuenta con una Suscripción Activa';
                }
            }
            else{
                $message = 'El usuario no se encuentra activo';
            }
        }
        else{
            $message = 'Usuario no encontrado';
        }


        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }
}

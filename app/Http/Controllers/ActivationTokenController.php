<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActivationAccount;
use App\ActivationToken;
use App\Helpers\Helper;
use App\User;
use Session;

class ActivationTokenController extends Controller
{
    public function activate($token){
        $tokenUser = ActivationToken::where('token', $token)->first();
        if($tokenUser){
            $tokenUser->user->activate($token);
            session(['xpNotification' => 'verified']);
            return redirect()->route('home');
        }
        else{
            return redirect()->route('home');
        }
    }

    public function locked(){
        return view('auth.locked');
    }

    public function resend(){
        $user = User::find(auth()->user()->id);
        $user->generateToken();
        Mail::to(auth()->user()->email)->send(new ActivationAccount($user));
        Helper::notification('success', 'Se reenvio un email de verificacion de cuenta a su correo electronico');
        return redirect()->route('home');
    }
}

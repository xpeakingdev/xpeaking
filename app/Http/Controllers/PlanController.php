<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use App\Helpers\Helper;

class PlanController extends Controller
{
    public function recommended(){
        $plan = Plan::where('recommended', true)->first();
        $paymentMethods = Helper::paymentMethods();
        return response()->json([
            "plan" => $plan,
            "paymentMethods" => $paymentMethods
        ]);
    }
}

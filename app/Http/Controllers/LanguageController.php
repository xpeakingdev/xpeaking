<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Language;

class LanguageController extends Controller
{
    public function list(){
        $languages = Language::orderBy('name')->get(['id','name', 'code']);
        return response()->json([
            'languages' => $languages
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class PublicPhotosController extends Controller
{
    //
    public function Profilephoto($route)
    {
    	$path = storage_path() . '/app/public/users/'.$route ; // puedes poner cualquier ubicacion que quieras dentro del storage

        if(!File::exists($path)) abort(404); // Si el archivo no existe
        // obtener el archivo con la ruta
        $file = File::get($path);
        $type = File::mimeType($path);
        // armar la respuesta
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        // importante el ob_clean
        if (ob_get_length()) ob_clean();
        return $response;
    }
    public function VoucherPhoto($route)
    {
    	$path = storage_path() . '/app/public/orders/files/'.$route ; // puedes poner cualquier ubicacion que quieras dentro del storage

        if(!File::exists($path)) abort(404); // Si el archivo no existe
        // obtener el archivo con la ruta
        $file = File::get($path);
        $type = File::mimeType($path);
        // armar la respuesta
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        // importante el ob_clean
        if (ob_get_length()) ob_clean();
        return $response;
    }
}

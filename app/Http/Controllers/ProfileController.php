<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Mail\MailConfirmChanges;
use Illuminate\Http\Request;
use App\ArtistRestricted;
use App\GenderRestricted;
use App\LanguageStudent;
use App\RestrictedSong;
use App\Helpers\Helper;
use App\Sponsorship;
use App\Suscription;
use Carbon\Carbon;
use App\Language;
use App\Country;
use App\Profile;
use App\Artist;
use App\Gender;
use App\Token;
use App\User;
use App\Song;

class ProfileController extends Controller
{
    public function profile(){
        return view('user.profile.index');
    }

    public function resume(){
        $user = User::where('id', auth()->user()->id)
                    ->with('profile')
                    ->with('student.languages')
                    ->with('profile.city.region.country')
                    ->first();

        $age = null;

        if($user->profile->birthdate){
            $date = new Carbon();
            $birthdate = new Carbon($user->profile->birthdate);
            $age = $date->diffInYears($birthdate);
        }

        $active = false;
        $suscription = Suscription::where('active', true)
                                    ->where('user_id', $user->id)
                                    ->first();
        if($suscription){
            $active = true;
        }

        return response()->json([
            "user" => $user,
            "age" => $age,
            "active" => $active
        ]);
    }

    public function getData(){
        $countries = Country::orderBy('name')->with('regions.cities')->get();
        $languages = Language::orderBy('name')->get();
        $typesDocuments = User::typesDocuments();
        $user = User::where('id', auth()->user()->id)
                    ->with('profile')
                    ->with('student.languages')
                    ->with('profile.city.region.country')
                    ->first();

        $sponsorship = Sponsorship::where('sponsored_id', $user->id)->first();
        $sponsor = null;
        if($sponsorship){
            $sponsor = User::where('id', $sponsorship->sponsor_id)->select('username')->first();
        }

        return response()->json([
            "countries" => $countries,
            "user" => $user,
            "sponsor" => $sponsor,
            "languages" => $languages,
            "typesDocuments" => $typesDocuments
        ]);
        
    }

    public function storeProfile(Request $request){

        $success = false;
        $error = null;

        $profile = Profile::find(auth()->user()->profile->id);
        $user = User::where('id',auth()->user()->id)->with('student.languages')->first();

        $birthdate = null;
        if($request["profile"]["birthdate"]){
            $date = new Carbon($request["profile"]["birthdate"]);
            $birthdate = $date->format("yy-m-d");
        }

        // $validaEmail = User::where('email', $request["profile"]["email"])->where('id', '<>', auth()->user()->id)->count();
        // if($validaEmail>0){
        //     $error = 'El email ingresado no se encuentra disponible';
        // }
        $sex = null;
        if($request["profile"]["sex"] ==! 0){
            $sex = $request["profile"]["sex"];
        }
        $profile->sex = $sex;

        $profile->name = $request["profile"]["name"];        
        $profile->last_name = $request["profile"]["last_name"];        
        $profile->phone = $request["profile"]["phone"];        
        $profile->address = $request["profile"]["address"];      
        $profile->birthdate = $request["profile"]["birthdate"];      
        $profile->document = $request["profile"]["document"];      
        $profile->payname = $request["profile"]["payname"];      

        if($request["update"]["cityId"]==0){
            $profile->city_id = NULL;
        }
        else{
            $profile->city_id = $request["update"]["cityId"];
        }

        if($request["update"]["typeDocument"]==0){
            $profile->type_document = NULL;
        }
        else{
            $profile->type_document = $request["update"]["typeDocument"];
        }

        if($profile->save()){
            $success = true;

            if($request["update"]["languageId"] ==! 0){
                $exist = false;
                foreach($user->student->languages as $language){
                    if($language->language_id == $request["update"]["languageId"] && $language->speak){
                        $exist = true;
                        LanguageStudent::where('student_id', $user->student->id)
                                ->where('language_id', $request["update"]["languageId"])
                                ->where('speak', true)
                                ->update(['principal' => true]);
                    }
                }

                if(!$exist){
                    $languageStudent = new LanguageStudent([
                        "speak" => true,
                        "learn" => false,
                        "principal" => true,
                        "language_id" => $request["update"]["languageId"],
                        "student_id" => $user->student->id
                    ]);
                    $languageStudent->save();
                }

                LanguageStudent::where('student_id', $user->student->id)
                                ->where('speak', true)
                                ->where('principal', true)
                                ->where('language_id', '<>', $request["update"]["languageId"])
                                ->update(['principal' => false]);
                
            }
            
        }


        // if(!$error){

        // }
        

        return response()->json([
            "success" => $success,
            "error" => $error
        ]);

    }

    public function updatePassword(Request $request){
        $success = false;
        $error = null;

        if($request->passwod =! $request->confirmPassword){
            $error = 'Las contraseñas no coinciden';
        }

        if(!$error){
            $user = User::find(auth()->user()->id);
            $user->password = Hash::make($request->password);
            if($user->save()){
                $success = true;
            }
            else{
                $error = 'Algo salio mal, no se actualizaron los cambios';
            }
        }

        return response()->json([
            "success" => $success,
            "error" => $error
        ]);
    }

    public function getLocked($search){
        $genders = Gender::orderBy('name')->where('name', 'like', $search.'%')->get();
        $artists = Artist::orderBy('stage_name')->where('stage_name', 'like', $search.'%')->get();
        $songs = Song::orderBy('name')->where('name', 'like', $search.'%')->get();

        return response()->json([
            "results" => [
                "genders" => $genders,
                "artists" => $artists,
                "songs" => $songs
            ]
        ]);
    }
    
    public function listRestricted(){
        $genderRestricted = [];
        $artistRestricted = [];
        $restrictedSong = [];

        $student = auth()->user()->student;

        $genderRestricted = $student->genderRestricted;
        $artistRestricted = $student->artistRestricted;
        $restrictedSong = $student->restrictedSong;

        return response()->json([
            "restricted" => [
                "genders" => $genderRestricted,
                "artists" => $artistRestricted,
                "songs" => $restrictedSong
            ]
        ]);
    }

    public function storeRestricted(Request $request){

        $success = false;
        $message = "El token ingresado no es valido";
        $token = Token::where('token', $request->token)
                    ->where('type', 'confirm_changes')
                    ->where('user_id', auth()->user()->id)
                    ->first();
        if($token){
            $student = auth()->user()->student;
            $success = true;
            $message = "Cambios guardados exitosamente";
            
            $deleteArtistRestrict = ArtistRestricted::whereIn('artist_id', $request["removed"]["artists"])
                                                    ->where('student_id', $student->id)
                                                    ->delete();
    
            $deleteGenderRestrict = GenderRestricted::whereIn('gender_id', $request["removed"]["genders"])
                                                    ->where('student_id', $student->id)
                                                    ->delete();
    
            $deleteSongRestrict = RestrictedSong::whereIn('song_id', $request["removed"]["songs"])
                                                    ->where('student_id', $student->id)
                                                    ->delete();
    
            foreach ($request["restricted"]["artists"] as $artist) {
                $storeAritstRestricted = ArtistRestricted::firstOrNew([
                    "student_id" => $student->id,
                    "artist_id" => $artist["artist_id"]
                ]);
                $storeAritstRestricted->save();
            }
    
            foreach ($request["restricted"]["genders"] as $gender) {
                $storeGenderRestricted = GenderRestricted::firstOrNew([
                    "student_id" => $student->id,
                    "gender_id" => $gender["gender_id"]
                ]);
                $storeGenderRestricted->save();
            }
    
            foreach ($request["restricted"]["songs"] as $song) {
                $storeSongRestricted = RestrictedSong::firstOrNew([
                    "student_id" => $student->id,
                    "song_id" => $song["song_id"]
                ]);
                $storeSongRestricted->save();
            }
        }
        

        
        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function mailConfirm(Request $request){
        $success = true;
        $user = auth()->user();
        $token = Token::firstOrNew(
            [ "user_id" => $user->id, "type" => 'confirm_changes' ],
            [ "token" => Helper::generateToken() ]
        );
        $token->save();
        $user["token"] = $token->token;
        Mail::to($user->email)->send(new MailConfirmChanges($user));
        return response()->json([
            "success" => $success
        ]);
    }

    public function storePicture(Request $request){
        $success = true;
        $message = 'Algo salio mal, no se actualizo imagen de perfil';
        $picture = '';

        $user = User::find(auth()->user()->id);
        if($request->hasFile('picture')){
            $route = 'users/';
            $name = 'profile-'.substr(str_shuffle(Helper::CHARACTERS), 0, 6).'-'.$user->id.'.'.$request->extension;
            Storage::disk('public')->put($route.$name, File::get($request->picture));
            $user->picture = $name;
            if($user->save()){
                $success = true;
                $message ="Imagen de perfil actualizada exitosamente";
            }
            
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}

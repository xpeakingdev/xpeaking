<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\ArtistFavorite;
use App\FavoriteSong;
use App\Sponsorship;
use App\Helpers\Helper;
use App\Artist;
use App\Song;
use App\Suscription;
use App\Gender;
use App\Role;
use App\Range;
use Carbon\Carbon;
use Auth;


class ContentController extends Controller
{
    public function songs(){
        $songs = Song::orderBy('order')
                    ->where('published', 1)
                    ->with('artists')
                    ->take(10)
                    ->get();
        return response()->json([
            "songs" => $songs
        ]);
    }

    public function songsImportant(){
        $songs = Song::orderBy('order')
                    ->where('important', 1)
                    ->where('published', 1)
                    ->with('artists')
                    ->take(10)
                    ->get();
        return response()->json([
            "songs" => $songs
        ]);
    }

    public function artists(){
        $artists = Artist::orderBy('order')
                            ->where('published', 1)
                            ->get();
        return response()->json([
            "artists" => $artists
        ]);
    }

    public function artist($slug){
        $favorites = [];
        if(Auth::check()){
            if(auth()->user()->role->id == Role::STUDENT){
                $student = auth()->user()->student;
                $favorites = ArtistFavorite::where('student_id', $student->id)->get();
            }
        }
        $authenticate = Auth::check();
        $artist = Artist::where('slug', $slug)
                        ->with('songs')
                        ->with('genders')
                        ->first();
        return response()->json([
            "artist" => $artist,
            "authenticate" => $authenticate,
            "favorites" => $favorites
        ]);
    }

    public function songInfo($id, $slug){
        $song = Song::where('id', $id)
                    ->where('slug', $slug)
                    ->where('published', 1)
                    ->with('artists')
                    ->with('genders')
                    ->with('languages')
                    ->with('subtitles')
                    ->first();
        $authenticate = Auth::check();
        $favorites = [];
        if(Auth::check()){
            if(auth()->user()->role->id == Role::STUDENT){
                $student = auth()->user()->student;
                $favorites = FavoriteSong::where('student_id', $student->id)->get();
            }
        }

        return response()->json([
            "song" => $song,
            "authenticate" => $authenticate,
            "favorites" => $favorites
        ]);
    }

    public function validSponsor(Request $request){
        $success = false;
        $message = "El nombre de usuario que lo invitó no es valido";
        $sponsorLimit = Helper::sponsorLimit();
        $user = User::where('username', $request->username)
                    ->where('role_id', Role::STUDENT)
                    ->first();
        if($user){
            if($user->status == User::ACTIVE){
                $sponsors = Sponsorship::where('sponsor_id', $user->id)->where('active', true)->get();
                $suscriptions = Suscription::where('user_id', $user->id)->where('active', true)->first();
                if(count($sponsors) >= $sponsorLimit){
                    $message = "Lo sentimos, el usuario invitado que ingresaste no puede realizar mas invitaciones";
                }
                else if(!$suscriptions){
                    $message = "Lo sentimos, el usuario invitado no puede realizar invitaciones por el momento";
                }
                else{
                    $success = true;
                }
            }
            else{
                $message = "¡Lo sentimos! El usuario que te invitó se encuentra bloqueado / inactivo.";
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function ranges(){
        $ranges = Range::orderBy('level')
                        ->with('awards')
                        ->get();
        return response()->json([
            "ranges" => $ranges
        ]);
    }

    public function validUser($username){
        $error = null;
        $user = User::where('username', $username)->first();
        if(!$user){
            $error = "Este usuario no existe";
        }
        else{
            if($user->status != User::ACTIVE){
                $error = "Este usuario se encuentra inactivo";
            }
        }

        return response()->json([
            "user" => $user,
            "error" => $error
        ]);
    }

    public function valuePoint(){
        $valuePoint = Helper::valuePoint();
        return response()->json([
            "valuePoint" => $valuePoint
        ]);
    }

    public function statusList(){
        $statusList = [
            "active" => User::ACTIVE,
            "locked" => User::LOCKED,
            "notverified" => User::NOTVERIFIED
        ];

        return response()->json([
            "statusList" => $statusList
        ]);
    }

    public function genders(){
        $genders = Gender::orderBy('name')->select('id', 'name')->get();
        return response()->json([
            "genders" => $genders
        ]);
    }

    public function songsAll(){
        return view('content.song.all');
    }

    public function songsAllData(Request $request){
        // $page = 0;
        // $items = 20;
        // if($request->page){
        //     $page = $request->page;
        // }
        // $block = $page * $items;
        $gender = $request->gender;
        $language = $request->language;
        $year = $request->year;

        $songs = Song::orderBy('date_published', 'desc')
                    ->whereHas('genders', function($q) use($gender){
                        if($gender){
                            $q->where('gender_id', $gender);
                        }
                    })
                    ->whereHas('languages', function($q) use($language){
                        if($language){
                            $q->where('language_id', $language);
                        }
                    })
                    ->where(function($q) use($year){
                        if($year){
                            $dateFilter = $year.'-01-01';
                            $q->where('release', $dateFilter);
                        }
                    })
                    ->where('published', 1)
                    ->with('artists')
                    ->with('genders')
                    // ->offset($block)
                    // ->limit($items)
                    ->paginate(25);

        return response()->json([
            "songs" => $songs,
            'pagination' => [
                'total' => $songs->total(),
                'per_page' => $songs->perPage(),
                'current_page' => $songs->currentPage(),
                'last_page' => $songs->lastPage(),
                'from' => $songs->firstItem(),
                'to' => $songs->lastItem()
            ]
        ]);
    }

    public function artistsAll(){
        return view('content.artist.all');
    }

    public function artistsAllData(Request $request){
        
        $gender = $request->gender;
        $language = $request->language;
        $year = $request->year;

        $artists = Artist::orderBy('important', 'desc')
                        ->orderBy('order')
                        ->orderBy('stage_name')
                        ->where('published', 1)
                        ->whereHas('genders', function($q) use($gender){
                            if($gender){
                                $q->where('gender_id', $gender);
                            }
                        })
                        ->whereHas('languages', function($q) use($language){
                            if($language){
                                $q->where('language_id', $language);
                            }
                        })
                        ->paginate(25);

        return response()->json([
            "artists" => $artists,
            'pagination' => [
                'total' => $artists->total(),
                'per_page' => $artists->perPage(),
                'current_page' => $artists->currentPage(),
                'last_page' => $artists->lastPage(),
                'from' => $artists->firstItem(),
                'to' => $artists->lastItem()
            ]
        ]);
    }

    public function setView(Request $request){
        $success = false;
        $song = Song::find($request->songId);
        if($song){
            $song->views += 1;
            if($song->save()){
                $success = true;
            }
            
        }

        return response()->json([
            "success" => $success
        ]);
    }

    public function getSongPrev(){
        $year = 0;
        $song = Song::orderBy('year_release')->first();
        if($song){
            $date = new Carbon($song->release);
            $year = $date->year;
        }

        return response()->json([
            "year" => $year
        ]);
    }

    public function searchContent(Request $request){
        $type = $request->type;
        $gender = $request->gender;
        $language = $request->language;
        $year = $request->year;
        $yearFrom = null;
        $yearTo = null;
        if($year){
            $yearFrom = new Carbon($year.'-01-01');
            $yearTo = new Carbon($year.'-12-31');
        }
        $search = $request->search;
        
        $registers = [];

        if($type == "songs"){
            $registers = Song::orderBy('id')
                            ->whereHas('genders', function($q) use($gender){
                                if($gender){
                                    $q->where('id', $gender);
                                }
                            })
                            ->whereHas('languages', function($q) use($language){
                                if($language){
                                    $q->where('id', $language);
                                }
                            })
                            ->where(function($q) use($year){
                                if($year){
                                    $q->where('release', '>=', $yearFrom)
                                      ->where('release', '<=', $yearTo);
                                }
                            })
                            ->where('name', 'like', '%'.$search.'%')
                            ->where('published', 1)
                            ->with('genders')
                            ->with('languages')
                            ->with('artists')
                            ->get();
        }
        else if($type == "artists"){
            $registers = Artist::orderBy('id')
                            ->whereHas('genders', function($q) use($gender){
                                if($gender){
                                    $q->where('id', $gender);
                                }
                            })
                            ->whereHas('languages', function($q) use($language){
                                if($language){
                                    $q->where('id', $language);
                                }
                            })
                            ->where(function($q) use($year){
                                if($year){
                                    $q->where('release', '>=', $yearFrom)
                                      ->where('release', '<=', $yearTo);
                                }
                            })
                            ->where('stage_name', 'like', '%'.$search.'%')
                            ->with('genders')
                            ->with('languages')
                            ->get();
        }

        return response()->json([
            "registers" => $registers,
            "type" => $type,
            "gender" => $gender,
            "language" => $language,
            "year" => $year
        ]);

    }

    public function searchAll(Request $request){
        $search = preg_replace('/[^A-Za-z0-9-]+/','-',strtolower($request->search));
        // $search = strtolower($request->search);
        
        $songs = Song::where('name', 'like', '%'.$search.'%')
                    ->where('published', 1)
                    ->orderBy('slug')
                    ->with('artists')
                    // ->whereHas('genders', function($q) use($search){
                    //     $q->where('name', 'like', '%'.$search.'%');
                    // })
                    // ->whereHas('artists', function($q) use($search){
                    //     $q->where('stage_name', 'like', '%'.$search.'%');
                    // })
                    ->take(50)
                    ->get();
        $artists = Artist::where('slug', 'like', '%'.$search.'%')
                        ->orderBy('stage_name')
                        ->where('published', 1)
                        // ->whereHas('genders', function($q) use($search){
                        //     $q->where('name', 'like', '%'.$search.'%');
                        // })
                        ->take(50)
                        ->get();

        /*
        $songs = DB::table("songs")
                    ->orderBy('name')
                    ->join('artists', 'users.id', '=', 'contacts.user_id')
                    ->whereRaw(' lower(name) like "%'.$search.'%" ')
                    ->take(50)
                    ->get();

        $artists = DB::table("artists")
                    ->orderBy('stage_name')
                    ->whereRaw(' lower(stage_name) like "%'.$search.'%" ')
                    ->take(50)
                    ->get();
        */
        
        return response()->json([
            "songs" => $songs,
            "artists" => $artists
        ]);
    }
    
}

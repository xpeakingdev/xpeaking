<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActivationAccount;
use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;
use App\ActivationToken;
use App\LanguageStudent;
use App\Helpers\Helper;
use App\Mail\Welcome;
use App\Sponsorship;
use App\Suscription;
use Carbon\Carbon;
use App\History;
use App\Student;
use App\Profile;
use App\Order;
use App\Plan;
use App\User;
use App\Role;
use Session;
use Auth;




class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'languageNative' => ['required'],
            // 'name' => ['required', 'string', 'max:250'],
            // 'last_name' => ['required', 'string', 'max:250'],
            // 'user.languageNative' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:6', 'confirmed'],
            // 'agree' => ['required']
        ],
        [
            // 'languageNative.required' => 'Es necesario que seleccione su idioma nativo.'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        
        $student = Student::create([
            'user_id' => $user->id
        ]);

        // Mail::to($user->email)->send(new PleaseActivateYourAccount($user));

        return $user->generateToken();
    }

    public function registerUser(Request $request){

        $errors = [];
        $success = null;

        $plan = $request->plan;
        $card = $request->card;

        $userData = $request->user;
        if(!$userData["languageNative"]){
            array_push($errors, [
                "error" => 'languageNative',
                "message" => 'Es necesario elegir su idioma nativo'
            ]);
        }

        // if(!$userData["languageLearn"]){
        //     array_push($errors, [
        //         "error" => 'languageLearn',
        //         "message" => 'Es necesario elegir el idioma que desea aprender'
        //     ]);
        // }

        if(!$userData["email"]){
            array_push($errors, [
                "error" => 'email',
                "message" => 'El email es obligatorio'
            ]);
        }

        if(!$userData["username"]){
            array_push($errors, [
                "error" => 'username',
                "message" => 'El nombre de usuario es obligatorio'
            ]);
        }

        if(!$userData["password"]){
            array_push($errors, [
                "error" => 'password',
                "message" => 'La contraseña es obligatoria'
            ]);
        }

        $validPhone = "/^9[0-9]+/";
        if(!$userData["phone"]){
            array_push($errors, [
                "error" => 'phone',
                "message" => 'El celular es obligatorio'
            ]);
        }
        else{
            if(strlen($userData["phone"]) !== 9 || !preg_match($validPhone, $userData["phone"])){
                array_push($errors, [
                    "error" => 'phone',
                    "message" => 'El celular es obligatorio'
                ]);
            }
        }

        if(!$userData["agree"]){
            array_push($errors, [
                "error" => 'password',
                "message" => 'Debe aceptar los terminos y condiciones'
            ]);
        }

        if($userData["password"] != $userData["confirmPassword"]){
            array_push($errors, [
                "error" => 'passwordConfirm',
                "message" => 'Las contraseñas ingresadas no coinciden'
            ]);
        }
        $userData["username"] = strtolower($userData["username"]);
        $validUser = User::where('email', $userData["email"])
                          ->orWhere('username', $userData["username"])
                          ->first();
        if($validUser){
            if($validUser["email"] == $userData["email"]){
                array_push($errors, [
                    "error" => 'uniqEmail',
                    "message" => 'El email ingresado no se encuentra disponible'
                ]);
            }
            
            if($validUser["username"] == $userData["username"]){
                array_push($errors, [
                    "error" => 'uniqUsername',
                    "message" => 'El nombre de usuario ingresado no se encuentra disponible'
                ]);
            }
        }

        $validInvited = null;
        if($userData["invited"]){

            $sponsorLimit = 0;
            $validInvited = User::where('username', $userData["invited"])
                                ->where('role_id', Role::STUDENT)
                                ->first();
            if($validInvited){
                $confirmedSponsor = Order::where('user_id',$validInvited->id)
                                    ->where('description','Register')
                                    ->where('status',Order::COMPLETE)->first();
                if ($confirmedSponsor) {
                    
                }
                else{
                    array_push($errors, [
                        "error" => 'invited',
                        "message" => 'El usuario que lo invitó aún no es alumno'
                    ]);
                }
                
                if($validInvited->status == User::ACTIVE){
                    $sponsorLimit = $validInvited->limit_invitations;
                    $sponsors = Sponsorship::where('sponsor_id', $validInvited->id)->get();
                    $suscriptions = Suscription::where('user_id', $validInvited->id)->where('active', true)->first();
                    if(count($sponsors) >= $sponsorLimit){
                        array_push($errors, [
                            "error" => 'invited',
                            "message" => '¡Lo sentimos! El usuario que te invitó ya no tiene más invitaciones'
                        ]);
                    }
                    // else if(!$suscriptions){
                    //     array_push($errors, [
                    //         "error" => 'invited',
                    //         "message" => "¡Lo sentimos! El usuario que te invitó no puede realizar invitaciones"
                    //     ]);
                    // }
                }
                else{
                    array_push($errors, [
                        "error" => 'locked',
                        "message" => '¡Lo sentimos! El usuario que te invitó se encuentra bloqueado / inactivo.'
                    ]);
                }
                
            }
            else{
                array_push($errors, [
                    "error" => 'invited',
                    "message" => 'El nombre de usuario que lo invitó no es valido'
                ]);
            }
        }

        if(count($errors)==0 && $plan){
            $user = new User([
                "username" => $userData["username"],
                "email" => $userData["email"],
                "password" => Hash::make($userData["password"]),
                "status" => User::NOTVERIFIED,
                "permission_role" => true,
                "role_id" => Role::STUDENT
            ]);

            if($user->save()){
                $user->generateToken();
                $student = new Student([
                    "user_id" => $user->id
                ]);

                $profile = new Profile([
                    'user_id' => $user->id,
                    'phone' => $userData["phone"]
                ]);
                if($student->save() && $profile->save()){

                    // SAVE LANGUAGES
                    $languageSpeak = new LanguageStudent([
                        "speak" => true,
                        "learn" => false,
                        "principal" => true,
                        "language_id" => $userData["languageNative"],
                        "student_id" => $student->id
                    ]);
    
                    // $languageLearn = new LanguageStudent([
                    //     "speak" => false,
                    //     "learn" => true,
                    //     "language_id" => $userData["languageLearn"],
                    //     "student_id" => $student->id
                    // ]);

                    $languageSpeak->save();
                    // $languageLearn->save();
                    // $pointsPlan = intval($plan->price);
                            // Helper::addPointsSponsor($validInvited->id, $user->id);

                    $statusOrder = Order::PENDING;
                    $activeSponsorShip = false;
                    if($card["methodPayment"] == Plan::CARD){
                        $statusOrder = Order::COMPLETE;
                        $activeSponsorShip = true;
                    }
                    
                    $order = new Order([
                        "payment_mode" => $card["methodPayment"],
                        "description" => "Register",
                        "ammount" => $plan["price"],
                        "status" => $statusOrder,
                        "date_pay" => Carbon::now(),
                        "points" => 0,
                        "user_id" => $user->id,
                        "plan_id" => $plan["id"]
                    ]);
                    if($validInvited){
                        Helper::storeSponsorship($validInvited->id, $user->id, $activeSponsorShip);
                        // POR EL MOMENTO SUSPENDIDO EL DESCUENTO DE 6 SOLES
                        $order->ammount = $plan["price"];
                    }
                    $order->save();
                    
                    // $history = new History([
                    //     "description" => History::DESCRIPTIONS_ADD_POINTS_REGISTER,
                    //     "points" => $pointsPlan,
                    //     "user_id" => $user->id,
                    //     "order_id" => null
                    // ]);
                    // $history->save();
                    // $user->points = $pointsPlan;
                    // $user->save();
                        
                    // session(['notification' => 'Usuario registrado exitosamente, active su cuenta por favor']);
                    // session(['typeNotification' => 'success']);
                    $success = true;
                    Mail::to($user->email)->send(new ActivationAccount($user));
                    Auth::loginUsingId($user->id);
                    Auth::logout();
                    Session::flush();
                    session(['xpNotification' => 'registered']);
                }
                else{
                    $user->delete();
                    $tokens = ActivationToken::where('user_id', $user->id)->delete();
                    array_push($errors, [
                        "error" => 'createdStudent',
                        "message" => 'Ocurrio un error al registrar la informacion'
                    ]);
                }
            }
            else{
                array_push($errors, [
                    "error" => 'createdUser',
                    "message" => 'Ocurrio un error al registrar la informacion'
                ]);
            }
        }

        return response()->json([
            "errors" => $errors,
            "success" => $success
        ]);
        
    }

    public function validUserSponsor($username){
        $user = User::where('username', $username)->where('role_id', Role::STUDENT)->first();
        if($user){
            return view('auth.register')->with('username', $user->username);
        }
        else{
            return redirect()->route('home');
        }
    }

    // public function registered(Request $request, $user){
    //     $this->guard()->logout();
    //     return redirect()->route('login')->withSuccess('Registro exitoso, Hemos enviado un enlace de verificacion de tu cuenta a tu correo.');
    // }
}

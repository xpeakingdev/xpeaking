<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Mail\SendLinkResetPassword;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Helpers\SuscriptionHelper;
use App\User;
use Session;
use Auth;
// use App\Mail\SendLinkResetPassword;
// use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(LoginRequest $request){
        $errors = [];
        $success = false;

        $credentials = $request->validated();

        $user = User::where('username', $request->username)->first();

        if($user){
            $credentials["email"] = $user->email;
            if($user->status == User::LOCKED){
                Helper::sessionSet([
                    'error'=>'status',
                    'message' =>  'Tu usuario ha sido bloqueado. Contáctate con nosotros para mayor información'
                ]);
            }
            else if($user->status == User::NOTVERIFIED){
                Helper::sessionSet([
                    'error'=>'status',
                    'message' =>  'Aún no has verificado tu cuenta de correo electrónico, hazlo ahora mismo para empezar de una vez.'
                ]);
            }
            else{
                if(Auth::attempt($credentials)){
                    Auth::logoutOtherDevices($request->password);
                    return redirect()->route('home');
                    // Auth::attempt($credentials)
                    // Hash::check($password, $user->password
                    // $success = true;
                    // return redirect()->route('home');
                }
                else{
                    Helper::sessionSet([
                        'error'=>'login',
                        'message' =>  'La contraseña ingresada es incorrecta'
                    ]);
                    // array_push($errors, [
                    //     "error" => 'login',
                    //     "message" => 'La contraseña ingresada es incorrecta'
                    // ]);
                }
            }
        }
        else{
            Helper::sessionSet([
                'error'=>'login',
                'message' =>  'El nombre de usuario no existe'
            ]);
        }   
        
        return back()->withInput(request(['username']));

    }   

    public function showLoginForm(){
        return view('auth.login')->with('origin', 'home');
    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect()->route('home');
    }

    public function showLinkRequestForm(){
        return view('auth.passwords.email');
    }

    public function sendResetLinkEmail(){
        $errors = [];
        $success = null;
        
        $user = User::where('email', request()->email)->first();
        // return $user;
        if($user){
            DB::table('password_resets')->where('email', $user->email)->delete();
            $token = $user->generateTokenGlobal();
            $user->token = $token;
            $email = DB::table('password_resets')->insert([
                'email' => $user->email,
                'token' => $token
            ]);
            if($email){
                Mail::to($user->email)->send(new SendLinkResetPassword($user));
                // Session::flash('resend', 'Se envio un email de restablecimiento de contraseña a su correo electronico');
            
                $success = true;
                session([
                    'xpNotification'=>'resetPasswordEmail'
                ]);
            }
            else{
               
            }
            
        }
        else{
            array_push($errors, [
                "error" => 'languageNative',
                "message" => 'El email ingresado no coincide no nuestros registros'
            ]);
        }

        return response()->json([
            "errors" => $errors,
            "success" => $success
        ]);
    }

    public function showResetForm($token){
        $data = DB::table('password_resets')->where('token', $token)->first();
        if($data){
            // $user = User::where('email', $data->email)->first();            
            return view('auth.passwords.reset', compact('token'));
        }
        else{
            abort(404);
        }
    }

    public function passwordUpdate(Request $request){
       
        $errors = [];
        $success = null;

        if(!$request->email){
            array_push($errors, [
                "error" => 'email',
                "message" => 'Es necesario ingresar su email'
            ]);
        }

        if(!$request->token){
            array_push($errors, [
                "error" => 'token',
                "message" => 'Codigo de restablecimiento invalido'
            ]);
        }

        if(!$request->email){
            array_push($errors, [
                "error" => 'password',
                "message" => 'Ingrese su nueva contraseña'
            ]);
        }

        if($request->password != $request->confirmPassword){
            array_push($errors, [
                "error" => 'passwordConfirm',
                "message" => 'Las contraseñas ingresadas no coinciden'
            ]);
        }

        $token = DB::table('password_resets')
                    ->where('token', $request->token)
                    ->where('email', $request->email)
                    ->first();
        
        if($token){
            $user = User::where('email', $request->email)->first();
            $user->password = Hash::make($request->password);
            if($user->save()){
                $token = DB::table('password_resets')
                ->where('token', $request->token)
                ->where('email', $request->email)
                ->delete();
                $success = true;
                session(['xpNotification' => 'passwordChanged']);
            }
            else{
                array_push($errors, [
                    "error" => 'error',
                    "message" => 'Ocurrio un error al intentar restablecer contraseña'
                ]);
            }
        }
        else{
            array_push($errors, [
                "error" => 'token',
                "message" => 'Codigo de restablecimiento invalido'
            ]);
        }

        return response()->json([
            "errors" => $errors,
            "success" => $success
        ]);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artist;
use App\Song;

class ArtistController extends Controller
{
    public function show($slug){

        $artist = Artist::where('slug', $slug)
                        ->with('songs.artists')
                        ->with('genders')
                        ->first();

        return view('content.artist.index', compact('artist'));
    }
}

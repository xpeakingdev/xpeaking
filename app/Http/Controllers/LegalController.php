<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LegalController extends Controller
{
    public function terms(){
        return view('content.legal.terms');
    }

    public function agreement(){
        return view('content.legal.agreement');
    }

    public function privacy(){
        return view('content.legal.privacy');
    }
}

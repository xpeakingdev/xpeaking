<?php

namespace App\Http\Controllers\Dashboard\Dictionaries;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Aws\Credentials\Credentials;
use Aws\Sts\StsClient;
use Aws\S3\S3Client;
use Aws\Polly\PollyClient;
use App\DictionaryEnglishSpanish;
use App\DictionarySpanishEnglish;
use App\Language;
use Session;
use Artisan;
use Storage;

class DictionariesController extends Controller
{
    //
    public function list()
    {
    	# code...
    	$dictionaryEnglishSpanish = DictionaryEnglishSpanish::orderBy('word','asc')->get();
        foreach ($dictionaryEnglishSpanish as $word) {
            if (empty($word->translation) || empty($word->phonetics) || empty($word->audio)) {
                $word['incomplete'] = true;
            }
            else{
                $word['incomplete'] = false;
            }
        }
    	$dictionarySpanishEnglish = DictionarySpanishEnglish::orderBy('word','asc')->get();
        foreach ($dictionarySpanishEnglish as $word) {
            if (empty($word->translation) || empty($word->phonetics) || empty($word->audio)) {
                $word['incomplete'] = true;
            }
            else{
                $word['incomplete'] = false;
            }
        }
    	return response()->json([
    		"DictionaryEnglishSpanish" => $dictionaryEnglishSpanish,
    		"DictionarySpanishEnglish" => $dictionarySpanishEnglish
    	]);
    }
    public function edit(Request $request)
    {
    	switch ($request->filterid) {
    		case 0:
    			$word = DictionaryEnglishSpanish::find($request->id);
    			break;
    		case 1:
    			$word = DictionarySpanishEnglish::find($request->id);
    			break;

    		default:
    			# code...
    			break;
    	}
    	return response()->json([
    		"word" => $word
    	]);
    }
    public function save(Request $request)
    {
    	$success = false;
        $message = 'Oh no, algo salio mal, no pudimos guardar la informacion';

        $Wordata = $request->word;
        if ($Wordata['id']!=null) {
        	switch ($request->filterid) {
	    		case 0:
	    			$word = DictionaryEnglishSpanish::find($request->word['id']);
	    			break;
	    		case 1:
	    			$word = DictionarySpanishEnglish::find($request->word['id']);
	    			break;

	    		default:
	    			# code...
	    			break;
    		}
        }
        else{
        	switch ($request->filterid) {
	    		case 0:
	    			$word = new DictionaryEnglishSpanish;
	    			break;
	    		case 1:
	    			$word = new DictionarySpanishEnglish;
	    			break;

	    		default:
	    			# code...
	    			break;
    		}

        }
        // dd($word);
        $word->word = $Wordata['word'];
        $word->translation = $Wordata['translation'];
        $word->phonetics = $Wordata['phonetics'];
        $word->audio = $Wordata['audio'];
        if ($word->save()) {
            $success = true;
            $message = "Informacion registrada exitosamente";
        }
        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }
    public function audio(Request $request)
    {
        switch ($request->filterid) {
            case 0:
                $code = Language::CODE_ENGLISH;
                break;
            case 1:
                $code = Language::CODE_SPANISH;
                break;
            default:
                # code...
                break;
        }
        $route =  'https://xpeakingprueba.s3.us-east-2.amazonaws.com/'.$code.'/'.$request->word.'.mp3';
        // COMPROBAR SI EXISTE LA RUTA
            $h = get_headers($route);
            $status = array();
            preg_match('/HTTP\/.* ([0-9]+) .*/', $h[0] , $status);
        $validate = $status[1] == 200;
        $message = 'El archivo si existe actualmente';
        $audiotext = $request->word.'.mp3';
        if (!$validate) {
            $message = 'el archivo no existe, se genero un nuevo audio';
            $awsAccessKeyId = config('filesystems.disks.s3.key');
            $awsSecretKey   = config('filesystems.disks.s3.secret');
            $clientnew = StsClient::factory(array(
                'profile' => 'default',
                'region' => 'us-east-2',
                'version' => 'latest'
            ));
            $result= $clientnew->getSessionToken();
            $client_polly = new PollyClient([ 'version' => 'latest',
             'credentials' => [
                    'key'    => $result['Credentials']['AccessKeyId'],
                    'secret' => $result['Credentials']['SecretAccessKey'],
                    'token'  => $result['Credentials']['SessionToken']
                ],
              'region' => 'us-east-2' ]);
            if($code=="en"){
                $voice = 'Matthew';
            }
            else if($code=="es"){
                $voice = 'Enrique';
            }
            $result_polly = $client_polly->synthesizeSpeech([ 'OutputFormat' => 'mp3', 'Text' => $request->word, 'TextType' => 'text', 'VoiceId' => $voice ]);
            $resultData=$result_polly->get('AudioStream')->getContents();
            $s3bucket = 'xpeakingprueba'; 
            $s3region = 'us-east-2';
            $filename = $request->word.'.mp3';
            $client_s3 = new S3Client([ 'version' => 'latest',
                'credentials' => [
                    'key'    => $result['Credentials']['AccessKeyId'],
                    'secret' => $result['Credentials']['SecretAccessKey'],
                    'token'  => $result['Credentials']['SessionToken']
                ],
              'region' => $s3region ]);
            $saved = Storage::disk('s3')->put($code.'/'.$filename, $resultData, 'public');
            if($saved){
                $audiotext = $filename;
            }
            else{
                $audiotext = null;
            }
        }
        return response()->json([
            "route" => $route,
            "audiotext" => $audiotext,
            "message" => $message
        ]);
        
    }
    public function delete(Request $request)
    {
        $message = "No se pudo completar la operación";
        $success = false;
        switch ($request->filterid) {
            case 0:
                $word = DictionaryEnglishSpanish::find($request->id);
                break;
            case 1:
                $word = DictionarySpanishEnglish::find($request->id);
                break;

            default:
                # code...
                break;
        }
        if ($word->delete()) {
            $success = true;
            $message = "Eliminado exitosamente";
        }
        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }
}

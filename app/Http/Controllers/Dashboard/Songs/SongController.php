<?php

namespace App\Http\Controllers\Dashboard\Songs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Song;
use Carbon\Carbon;
use App\GenderSong;
use App\ArtistSong;
use App\LanguageSong;

class SongController extends Controller
{
    public function list(Request $request){
        $dateFrom = null;
        $dateTo = null;
        if($request->dateFrom){
            $dateFrom = (new Carbon($request->dateFrom))->startOfDay()->format('yy-m-d H:i:s');
        }
            
        if($request->dateTo){
            $dateTo = (new Carbon($request->dateTo))->endOfDay()->format('yy-m-d H:i:s');
        }

        $songs = Song::with('genders')
                    ->with('artists')
                    ->orderBy('updated_at', 'desc')
                    ->where( function($q) use($dateFrom, $dateTo){
                        if($dateFrom && $dateTo){
                            $q->where('created_at', '>=', $dateFrom)
                            ->where('created_at', '<=', $dateTo);
                        }
                        else if($dateFrom){
                            $q->where('created_at', '>=', $dateFrom);
                        }
                        else if($dateTo){
                            $q->where('created_at', '<=', $dateTo);
                        }
                    } )
                    ->get();

        foreach($songs as $index=>$song){
            if($song->date_published){
                $song->date_published = (new Carbon($song->date_published))->format('d/m/yy - H:i');
            }
            $song->created = (new Carbon($song->created_at))->format('d/m/yy - H:i');
            $song->index = ($index + 1);
        }

        return response()->json([
            "songs" => $songs
        ]);
    }

    public function getData(Request $request){
        $song = Song::where('id', $request->id)
                    ->with('genders')
                    ->with('artists')
                    ->with('subtitles')
                    ->with('languages')
                    ->first();
        return response()->json([
            "song" => $song
        ]);
    }

    public function store(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos guardar la informacion';

        $songData = $request->song;
        $removed = $request->removed;
        $news = $request->news;
        
        if($songData["id"]){
            $song = Song::where('id', $songData["id"])
                        ->first();
            $exist = Song::where('slug', $songData["slug"])
                        ->where('id', '<>', $songData["id"])
                        ->first();
        }
        else{
            $song = new Song();
            $exist = Song::where('slug', $songData["slug"])
                        ->first();
            $song->published = true;
            $song->date_published = new Carbon();
        }

        if($exist){
            $message = 'El slug de la cancion ya existe';
        }
        else{
            $song->name = $songData["name"];
            $song->slug = $songData["slug"];
            $song->cover = $songData["cover"];
            $song->duration = $songData["duration"];
            $song->video = $songData["video"];
            $song->original_link = $songData["original_link"];
            $song->order = $songData["order"];
            $song->year_release = $songData["year_release"];
            $song->free = ($songData["free"]?true:false);
            // $song->published = ($songData["published"]?true:false);
            $song->important = ($songData["important"]?true:false);
            $song->published = false;

            // if($song->published){
            //     $song->date_published = new Carbon();
            // }

            if($song->save()){

                // LANGUAGE SONG
                $languageSong = LanguageSong::where('song_id', $song->id)
                                            ->first();
                if($languageSong){
                    if($languageSong->language_id !== $songData["language"]["id"]){
                        $languageSong->delete();
                        $languageSong = new LanguageSong([
                            "song_id" => $song->id,
                            "language_id" => $songData["language"]["id"]
                        ]);
                        $languageSong->save();
                    }
                }
                else{
                    $languageSong = new LanguageSong([
                        "song_id" => $song->id,
                        "language_id" => $songData["language"]["id"]
                    ]);
                    $languageSong->save();
                }
                
                // REMOVE GENDERS
                if(count($removed["genders"])>0){
                    $gendersRemoved = GenderSong::whereIn('gender_id', $removed["genders"])
                                                ->where('song_id', $song->id)
                                                ->delete();
                }

                // REMOVE ARTISTS
                if(count($removed["artists"])>0){
                    $artistsRemoved = ArtistSong::whereIn('artist_id', $removed["artists"])
                                                ->where('song_id', $song->id)
                                                ->delete();
                }

                // NEWS GENDERS
                foreach($news["genders"] as $gender){
                    $genderSong = new GenderSong([
                        "gender_id" => $gender,
                        "song_id" => $song->id
                    ]);
                    $genderSong->save();
                }

                // NEWS ARTISTS
                foreach($news["artists"] as $artist){
                    $artistSong = new ArtistSong([
                        "artist_id" => $artist,
                        "song_id" => $song->id,
                        "autor" => 1
                    ]);
                    $artistSong->save();
                }

                $success = true;
                $message = "Informacion registrada exitosamente";
            }

        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
        
    }

    public function changeVerified(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos actualizar la informacion';

        $song = Song::find($request->id);
        if($song){
            $song->verified = $request->verified;
            if($song->save()){
                $success = true;
                $message = 'Informacion actualizada exitosamente';
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function changePublished(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos actualizar la informacion';
        $datePublished = null;

        $song = Song::find($request->id);
        if($song){
            $song->published = $request->published;

            if($song->published){
                $datePublished = (new Carbon())->format('d/m/yy - H:i');
                $song->date_published = new Carbon();
            }

            if($song->save()){
                $success = true;
                $message = 'Informacion actualizada exitosamente';
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message,
            "datePublished" => $datePublished
        ]);
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Songs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Song;

class PreviewSongController extends Controller
{
    //
    public function getsong($id, $slug)
    {
    	$song = Song::where('id', $id)
                    ->where('slug', $slug)
                    ->with('artists')
                    ->first();

        return view('content.player.song.index', compact('song'));
        
    }
}

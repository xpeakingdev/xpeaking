<?php

namespace App\Http\Controllers\Dashboard\Songs\Subtitles;
use Aws\Credentials\Credentials;
use Aws\Sts\StsClient;
use Aws\S3\S3Client;
use Aws\Polly\PollyClient;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use App\Helpers\phpEditSubtitles;
use \Done\Subtitles\Subtitles;
use App\Subtitle;
use App\Language;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\DictionaryEnglishSpanish;
use App\DictionarySpanishEnglish;
use App\User;
use Session;
use Artisan;
use Storage;

class SrtfileController extends Controller
{
    //
    public function show($id){
        // $awsAccessKeyId = config('filesystems.disks.s3.key');
        // $awsSecretKey   = config('filesystems.disks.s3.secret');
        // $clientnew = StsClient::factory(array(
        //     'profile' => 'default',
        //     'region' => 'us-east-2',
        //     'version' => 'latest'
        // ));
        // $result= $clientnew->getSessionToken();
        // $client_polly = new PollyClient([ 'version' => 'latest',
        //  'credentials' => [
        //         'key'    => $result['Credentials']['AccessKeyId'],
        //         'secret' => $result['Credentials']['SecretAccessKey'],
        //         'token'  => $result['Credentials']['SessionToken']
        //     ],
        //   'region' => 'us-east-2' ]);

        // $result_polly = $client_polly->synthesizeSpeech([ 'OutputFormat' => 'mp3', 'Text' => 'Hello, this is an example PHP script which saves an mp3 file created by AWS Polly to your choosen S3 bucket.', 'TextType' => 'text', 'VoiceId' => 'Amy' ]);
        // $resultData=$result_polly->get('AudioStream')->getContents();
        // $s3bucket = 'xpeakingprueba'; 
        // $s3region = 'us-east-2';
        // $filename = time().'-polly.mp3';
        // $client_s3 = new S3Client([ 'version' => 'latest',
        //     'credentials' => [
        //         'key'    => $result['Credentials']['AccessKeyId'],
        //         'secret' => $result['Credentials']['SecretAccessKey'],
        //         'token'  => $result['Credentials']['SessionToken']
        //     ],
        //   'region' => $s3region ]);
        // $result_s3 = $client_s3->putObject([ 'Key' => $filename, 'ACL' => 'public-read', 'Body' => $resultData, 'Bucket' => $s3bucket, 'ContentType' => 'audio/mpeg' ]);
        // dd($result_s3);
    	$languages = Language::orderBy('name')->get();
    	$id = explode('SUB10', $id,2);
    	$subtitle = Subtitle::find($id[1]);
    	$formated = [];
    	// $data=Helper::getSubtitle($subtitle->file,$subtitle->language,);
    	$subtitleContent = Storage::get("subtitles/".$subtitle->file);
    	$subtitleData = str_replace("\r", "", $subtitleContent);
        $subtitleParseOne = explode("\n\n", $subtitleData);
        foreach ($subtitleParseOne as $indexParse => $element) {
            $parseElement = explode("\n", $element);

            if(isset($parseElement[0]) && isset($parseElement[1]) && isset($parseElement[2])){
                $timeFormat = explode(" --> ", $parseElement[1]);
                $words = explode(" ", $parseElement[2]);
                $listWords = $words;
                $startorigin = Helper::Timecomplete($timeFormat[0]);
                $endorigin = Helper::Timecomplete($timeFormat[1]);
                array_push($formated, [
                    "id" => $parseElement[0],
                    "start" => Helper::formatTime($timeFormat[0]),
                    "startorigin"=> $startorigin,
                    "endorigin"=> $endorigin,
                    "end" => Helper::formatTime($timeFormat[1]),
                    "content" => $parseElement[2],
                    
                ]);
            }
        }
        $data = [
            "formated" => $formated,
            "subtitle" => $subtitle,
            "song" => $subtitle->song,
        ];
        $subtitles = Subtitles::load(Storage::get("subtitles/".$subtitle->file),'srt');

        $st = new phpEditSubtitles();
		$st->setFile(Storage::get("subtitles/".$subtitle->file));
		
        return view('dashboard.subtitle_editor.editor', compact('data'));
    }
    public function update($id)
    {
    	# code...
    }
    public function store(Request $request)
    {
    	$success=false;
    	//path de subtitulo original
    	$subtitlepath = Subtitle::find($request->id);
    	// subtitulos que reemplazaran
    	$replacesubtitles = $request->updatesubtitle;
    	$newsubtitle = new Subtitles();
    	// obtener archivo de subtitulo original
		$subtitles = Subtitles::load(Storage::get("subtitles/".$subtitlepath->file),'srt');
		// for del array del internal format del archivo srt

		// poner en public el internal_format de vendor/mantas-done/Subtitles.php
		for ($i=0; $i <count($subtitles->internal_format) ; $i++) { 
            $notfind=true;
            foreach ($replacesubtitles as $key => $value) {
                if ($i==$value['order']) {
                    $newsubtitle->add($value['timestart'],$value['timeend'],$value['content']);
                    $notfind=false;
                }
            }
            if ($notfind) {

                    $newsubtitle->add($subtitles->internal_format[$i]['start'],$subtitles->internal_format[$i]['end'],$subtitles->internal_format[$i]['lines']);

            }
			
		}
		$newsubtitle->save($subtitlepath->file);
        if ($newsubtitle) {
            $deletefile=Storage::delete("subtitles/".$subtitlepath->file);
            //IMPORTANTE PARA MOVER LOS ARCHIVOS
            $filename =File::move($subtitlepath->file,storage_path('app/public/subtitles/'.$subtitlepath->file));
            if ($filename) {
                $success=true;
            }
            // $ad->file('name.srt')->store('/subtitles');
            // ;
            return response()->json([
                "message" => $success
            ]);
        }
        else{
            return response()->json([
                "message" => $success
            ]);
        }

		
    }
    public function download($id)
    {
        $subtitle = Subtitle::find($id);
        $file = storage_path('app/public/subtitles/'.$subtitle->file);
        if ($file) {
            $headers = array(
              'Content-Type: application/srt',
            );

            return Response::download($file, $subtitle->song->slug.'_subtitle_'.$subtitle->language->name.'.srt', $headers);
        }
        else{
            return view('errors.404');
        }
        
    }
}

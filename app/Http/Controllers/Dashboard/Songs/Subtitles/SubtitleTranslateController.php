<?php

namespace App\Http\Controllers\Dashboard\Songs\Subtitles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Google\Cloud\Translate\TranslateClient;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Aws\Credentials\Credentials;
use Aws\Sts\StsClient;
use Aws\S3\S3Client;
use Aws\Polly\PollyClient;
use Illuminate\Support\Facades\Response;
use \Done\Subtitles\Subtitles;
use App\Song;
use App\Subtitle;
use App\Language;
use App\User;
use App\DictionaryEnglishSpanish;
use App\DictionarySpanishEnglish;
use Carbon\Carbon;
use Session;
use Artisan;

class SubtitleTranslateController extends Controller
{
    public function getSubtitle(Request $request)
    {

    	$subtitle = Subtitle::where([['language_id', $request->id],['song_id',$request->songId]])
                            ->with('song')
                            ->with('language')
                            ->first();
        $exists = Subtitle::where([['language_id', $request->target],['song_id',$request->songId]])->first();
        $base = Language::find($request->id);
        $target = Language::find($request->target);

        $googleTranslate = new TranslateClient([
            'key' => config('googletranslate.api_key')
        ]);
        if ($exists) {
            return response()->json([
                "error" => true
            ]);
        }
        $formated = [];
        $subtitleContent = Storage::get("subtitles/".$subtitle->file);
        $subtitleData = str_replace("\r", "", $subtitleContent);
        $subtitleParseOne = explode("\n\n", $subtitleData);
        foreach ($subtitleParseOne as $indexParse => $element) {
            $parseElement = explode("\n", $element);

            if(isset($parseElement[0]) && isset($parseElement[1]) && isset($parseElement[2])){
                $timeFormat = explode(" --> ", $parseElement[1]);
                $words = explode(" ", $parseElement[2]);
                $listWords = $words;
                if ($request->googletranslate) {
                    $translateSE = $googleTranslate->translate($parseElement[2], [
                        'source' => $base->code,
                        'target' => $target->code
                    ]);
                    $translation = str_replace('&#39;', '', $translateSE["text"]);
                }
                else{
                    $translation = '';
                }
                array_push($formated, [
                    "id" => $parseElement[0],
                    "start" => Helper::formatTime($timeFormat[0]),
                    "end" => Helper::formatTime($timeFormat[1]),
                    "startorigin"=>str_replace(',','.',$timeFormat[0]),
                    "endorigin"=>str_replace(',','.',$timeFormat[1]),
                    "content" => $parseElement[2],
                    "translation" => $translation
                    
                ]);
            }
        }
        return response()->json([
            "formated" => $formated
        ]);
    }
    public function storesubtitle(Request $request)
    {
        $success = false;
        $exists = Subtitle::where([['language_id', $request->lang],['song_id',$request->songId]])->first();
        if ($exists) {
            return response()->json([
                "success"=>$success
            ]);
        }
        else{
            $song = Song::find($request->songId);
            $lang = Language::find($request->lang);
            // Crear Archivo Subtitle
            $subtitles = new Subtitles();

            $subtitlepath = new Subtitle;
            $subtitlepath->status = 0;
            $subtitlepath->native = false;
            $subtitlepath->active = false;
            $subtitlepath->language_id = $lang->id;
            $subtitlepath->song_id = $song->id;
            $subtitlepath->file = $song->name.'-Translate-code-'.$lang->code.'.srt';
            foreach ($request->lines as $key => $value) {
                $subtitles->add($value['timeStart'],$value['timeEnd'],$value['words']);
                if ($value['words'] == null) {
                    return response()->json([
                        "success"=>$success
                    ]);
                }
            }

            if ($subtitles->save($subtitlepath->file)) {
                $subtitlepath->status = 70;
                $subtitlepath->save();
                $filename =File::move($subtitlepath->file,storage_path('app/public/subtitles/'.$subtitlepath->file));
                $success = true;
            }
        }
        return response()->json([
            "success"=>$success
        ]);
    }
}

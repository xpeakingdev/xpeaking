<?php

namespace App\Http\Controllers\Dashboard\Songs\Subtitles;

use App\Http\Controllers\Controller;
use CollectionPHP;
use Illuminate\Support\Collection as Collection;
use Illuminate\Http\Request;
use App\Song;
use App\Subtitle;
use App\Language;
use App\Evaluation;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\DictionaryEnglishSpanish;
use App\DictionarySpanishEnglish;
use Google\Cloud\Translate\TranslateClient;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class SubtitlesController extends Controller
{
    public function list(Request $request){
        $subtitles = Subtitle::where('song_id', $request->songId)
                            ->with('language')
                            ->get();

        foreach($subtitles as $subtitle){
            $subtitle->created = (new Carbon($subtitle->created_at))->format('d/m/yy');
        }

        return response()->json([
            "subtitles" => $subtitles
        ]);
    }

    public function setActive(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos actualizar la informacion';

        $subtitle = Subtitle::find($request->id);
        if($subtitle){
            $subtitle->active = $request->active;
            if($subtitle->save()){
                $success = true;
                $message = 'Informacion actualizada exitosamente';
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function getData(Request $request){
        $subtitle = Subtitle::where('id', $request->id)
                            ->with('language')
                            ->first();
        return response()->json([
            "subtitle" => $subtitle
        ]);
    }

    public function store(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos guardar la informacion';

        $subtitleData = $request->subtitle;
        
        if($subtitleData["id"]){
            $subtitle = Subtitle::where('id', $subtitleData["id"])
                        ->first();
        }
        else{
            $subtitle = new Subtitle();
        }

        $subtitle->status = $subtitleData["status"];
        $subtitle->native = $subtitleData["native"];
        $subtitle->active = $subtitleData["active"];
        $subtitle->language_id = $subtitleData["language"]["id"];
        $subtitle->song_id = $subtitleData["songId"];

        if($subtitle->save()){
            $success = true;
            $message = "Informacion registrada exitosamente";
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function previewFile(Request $request){
        $subtitle = Subtitle::where('id', $request->id)
                            ->with('song')
                            ->with('language')
                            ->first();
        $evaluations = Evaluation::where('subtitle_id',$subtitle->id)->get();
        $lines = Helper::getFormatSubtitle($subtitle->file);
        $activeva = false;
        if (count($evaluations)>0) {
            foreach ($lines as $key => $line) {
                foreach ($evaluations as $value) {
                    if ($key == $value->line_number) {
                        $lines[$key]['active'] = $value->active;
                        $lines[$key]['type'] = $value->type;
                    }
                }
            }
            $activeva = true;
        }
        return response()->json([
            "lines" => $lines,
            "native" => boolval($subtitle->native),
            "active" => $activeva
        ]);
    }

    public function wordsExist(Request $request){

        $language = Language::find($request->languageId);
        $languages = Language::orderBy('name', 'desc')->where('id', '<>', $language->id)->get();
        $dictionaries = [];
        $dictionariesPrev = [];
        $collection=array();
        $wordssaved = Collection::make($collection);
        $words = $request->words;
        $googleTranslate = new TranslateClient([
            'key' => config('googletranslate.api_key')
        ]);    
        if($language->code == Language::CODE_SPANISH){
            $dictionarySE = DictionarySpanishEnglish::whereIn('word', $request->words)->get();
            $languageSE = Language::where('code', Language::CODE_ENGLISH)->first();
            array_push($dictionariesPrev,[
                "language" => $languageSE,
                "dictionary" => $dictionarySE
            ]);
        }
        else if($language->code == Language::CODE_ENGLISH){
            $dictionaryES = DictionaryEnglishSpanish::whereIn('word', $request->words)->get();
            $languageES = Language::where('code', Language::CODE_SPANISH)->first();
            array_push($dictionariesPrev,[
                "language" => $languageES,
                "dictionary" => $dictionaryES
            ]);
        }

        foreach($dictionariesPrev as $dictionaryData){
            foreach($dictionaryData["dictionary"] as $dictionary){
                $index = array_search($dictionary["word"], $words);
                if($index >= 0){
                    array_splice($words, $index , 1);
                }
            }
        }
        
        foreach($languages as $lang){
            $phonetics = Helper::getPhonetics($language);
            foreach($words as $word){
                $translation = [];
                
                $translateSE = $googleTranslate->translate($word, [
                    'source' => $language->code,
                    'target' => $lang->code
                ]);
                
                $translation["word"] = $word;
                $translation["translation"] = str_replace('&#39;', '', $translateSE["text"]);
                $translation["audio"] = Helper::getAudio($word, $language, "");
                $translation["phonetics"] = '';
                foreach($phonetics as $phonetic){
                    if($word == $phonetic["word"]){
                        $translation["phonetics"] = $phonetic["phonetic"];
                    }
                }
                if($language->code==Language::CODE_ENGLISH && $lang->code==Language::CODE_SPANISH){
                    $newWord = new DictionaryEnglishSpanish($translation);
                    $newWord->save();
                }
                if($language->code==Language::CODE_SPANISH && $lang->code==Language::CODE_ENGLISH){
                    $newWord = new DictionarySpanishEnglish($translation);
                    $newWord->save();
                }

                if (empty($NewWord->translation) || empty($NewWord->phonetics) || empty($NewWord->audio)) {
                    $NewWord['incomplete'] = true;
                }
                else{
                    $NewWord['incomplete'] = false;
                }
                $wordssaved->push($newWord);
            }
        }
        
        if($language->code == Language::CODE_SPANISH){
            $dictionarySEOriginal = DictionarySpanishEnglish::whereIn('word', $request->words)->get();
            foreach($dictionarySEOriginal as $value){
                $wordssaved->push($value);
            }
            $languageSEOriginal = Language::where('code', Language::CODE_ENGLISH)->first();
            array_push($dictionaries,[
                "language" => $languageSEOriginal,
                "dictionary" => $wordssaved
            ]);
        }
        else if($language->code == Language::CODE_ENGLISH){
            $dictionaryESOriginal = DictionaryEnglishSpanish::whereIn('word', $request->words)->get();
            foreach($dictionaryESOriginal as $value){
                if (empty($value->translation) || empty($value->phonetics) || empty($value->audio)) {
                    $value['incomplete'] = true;
                }
                else{
                    $value['incomplete'] = false;
                }
                $wordssaved->push($value);
            }
            $languageESOriginal = Language::where('code', Language::CODE_SPANISH)->first();
            array_push($dictionaries,[
                "language" => $languageESOriginal,
                "dictionary" => $wordssaved
            ]);
        }
        
        return response()->json([
            "dictionaries" => $dictionaries
        ]);
    }

    public function saveFile(Request $request){
        $success = false;
        $message = 'Algo salio man, no pudimos registrar la informacion';
        if($request->file){
            $subtitle = Subtitle::where('id', $request->subtitleId)
                                ->with('language')
                                ->first();
            if($subtitle){
                $date = new Carbon();
                $route = 'subtitles/';
                $name = $subtitle->id.'-'.$subtitle->song_id.'-'.$subtitle->language->code.'-'.$request->name;
            }
            Storage::disk('public')->put($route.$name, File::get($request->file));
            $subtitle->file = $name;
            if($subtitle->save()){
                $success = true;
                $message = 'Archivo guardado exitosamente';
            }
        }
        
        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function saveWords(Request $request){
        $success = false;
        $message = 'Algo salio man, no pudimos registrar la informacion';
        $subtitle = Subtitle::where('id', $request->subtitleId)
                            ->with('language')
                            ->first();
        if($subtitle){
            $words = $request->words;
            foreach($words as $word){
                foreach($word["dictionaries"] as $dictionary){
                    if($subtitle->language->code == Language::CODE_SPANISH && $dictionary["language"]["code"] == Language::CODE_ENGLISH){
                        $wordDictionary = DictionarySpanishEnglish::where('word', $word["word"])
                                            ->first();
                    }
                    else if($subtitle->language->code == Language::CODE_ENGLISH && $dictionary["language"]["code"] == Language::CODE_SPANISH){
                        $wordDictionary = DictionaryEnglishSpanish::where('word', $word["word"])
                                            ->first();
                    }

                    if(isset($wordDictionary)){
                        $wordDictionary->translation = $dictionary["translation"];
                        $wordDictionary->phonetics = $word["phonetics"];
                        $wordDictionary->save();
                    }

                }
            }
            $success = true;
            $message = 'Informacion registrada exitosamente';
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function delete(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal , no pudimos eliminar la informacion';
        $subtitle = Subtitle::find($request->id);
        if($subtitle){
            $subtitle->delete();
            $success = true;
            $message = 'Informacion eliminada exitosamente';
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }
    public function SaveEvaluation(Request $request)
    {
        $success = false;
        $evaluations = Evaluation::where("subtitle_id",$request->id)->get();
        $lines = $request->lines;
        if (count($evaluations) == count($lines)) {
            foreach ($lines as $key => $line) {
                foreach ($evaluations as $value) {
                    if ($key == $value->line_number) {
                        $value->active = $lines[$key]['active'];
                        $value->type = $lines[$key]['type'];
                        $value->save();
                        $success = true;
                    }
                }
            }
        }
        else{
            foreach ($lines as $key => $line) {
                $evaluation = new Evaluation;
                $evaluation->active = $line['active'];
                $evaluation->type = $line['type'];
                $evaluation->line_number = $key;
                $evaluation->subtitle_id = $request->id;
                $evaluation->save();
                $success = true;
            }
        }
        return response()->json([
            "success" => $success
        ]);
        
        
    }
}
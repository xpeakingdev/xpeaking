<?php

namespace App\Http\Controllers\Dashboard\Languages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Language;

class LanguageController extends Controller
{
    public function list(Request $request){

        $languages = Language::orderBy('name')
                            ->get();

        foreach($languages as $index=>$language){
            $language->index = ($index + 1);
        }

        return response()->json([
            "languages" => $languages
        ]);
    }

    public function getData(Request $request){
        $language = Language::where('id', $request->id)
                    ->first();

        return response()->json([
            "language" => $language
        ]);
    }

    public function store(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos guardar la informacion';

        $languageData = $request->language;
        
        if($languageData["id"]){
            $language = Language::where('id', $languageData["id"])
                        ->first();
            $exist = Language::where('name', $languageData["name"])
                        ->where('id', '<>', $languageData["id"])
                        ->first();
        }
        else{
            $language = new Language();
            $exist = Language::where('name', $languageData["name"])
                            ->first();
        }

        if($exist){
            $message = 'El lenguaje ya existe';
        }
        else{

            $language->name = $languageData["name"];
            $language->code = $languageData["code"];

            if($language->save()){
                $success = true;
                $message = "Informacion registrada exitosamente";
            }

        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
        
    }
}

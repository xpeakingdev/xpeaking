<?php

namespace App\Http\Controllers\dashboard\finances;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Suscription;
use Carbon\Carbon;
use App\Order;
use App\Plan;
use App\Student;
use App\User;
use App\Role;

class FinancesController extends Controller
{
    public function list(){
        $orders = Order::orderBy('date_payment', 'desc')
                        ->where('status', Order::PENDING)
                        ->get();

        return response()->json([
            "orders" => $orders
        ]);
    }

    public function resume(Request $request){
        $dateFrom = null;
        $dateTo = null;

        if($request->dateFrom){
            $dateFrom = (new Carbon($request->dateFrom))->startOfDay()->format('yy-m-d H:i:s');
        }

        if($request->dateTo){
            $dateTo = (new Carbon($request->dateTo))->endOfDay()->format('yy-m-d H:i:s');
        }


        $total = 0;
        $threeDays = 0;
        $twoDays = 0;
        $oneDay = 0;
        $expired = 0;
        $noSuscriptions = 0;
        $data = [];
        $now = Carbon::now();
        $dateFilter = Carbon::now()->addDays(5);
        $codeVerify = User::NOTVERIFIED;
        $paymentMode = [
            "card" => Order::CARD,
            "transfer" => Order::TRANSFER,
            "points" => Order::POINTS,
            "free" =>Order::FREE
        ];

        $orders = Order::orderBy('updated_at', 'desc')
                        ->where('status', Order::PENDING)
                        ->with('user')
                        ->where( function($q) use($dateFrom, $dateTo){
                            if($dateFrom && $dateTo){
                                $q->where('created_at', '>=', $dateFrom)
                                  ->where('created_at', '<=', $dateTo);
                            }
                            else if($dateFrom){
                                $q->where('created_at', '>=', $dateFrom);
                            }
                            else if($dateTo){
                                $q->where('created_at', '<=', $dateTo);
                            }
                        } )
                        ->get();

        $suscriptions = Suscription::orderBy('expires_at')
                                    ->where('active', false)
                                    ->where( function($q) use($dateFrom, $dateTo, $dateFilter){
                                        if($dateFrom && $dateTo){
                                            $q->where('expires_at', '>=', $dateFrom)
                                              ->where('expires_at', '<=', $dateTo);
                                        }
                                        else if($dateFrom){
                                            $q->where('expires_at', '>=', $dateFrom);
                                        }
                                        else if($dateTo){
                                            $q->where('expires_at', '>=', $dateTo);
                                        }
                                        else{
                                            $q->where('expires_at', '<=', $dateFilter);
                                        }
                                    } )
                                    ->with('user')
                                    ->get();

        $noSuscriptions = count($orders);
        foreach($orders as $index => $order){
            $dateOrder = new Carbon($order->created_at);
            $daysExpired = $now->diffInDays($dateOrder, false);

            $dateFormat = new Carbon($order->created_at);

            array_push($data, [
                "id" => $order->id,
                "user" => $order->user,
                "created_at" => $dateFormat->format('d/m/yy - H:i'),
                "expired" => "Sin suscripcion",
                "type" => "order",
                "date_pay" => null,
                "file" => $order->file,
                "index" => ($index + 1)
            ]);
        }

        foreach($suscriptions as $suscription){
            if ($suscription->order->status ==  Order::PENDING) {
                $dateSuscription = new Carbon($suscription->expires_at);
                $daysExpired = $now->diffInDays($dateSuscription, false);
                if($daysExpired === 3){
                    $threeDays ++;
                }
                else if($daysExpired === 2){
                    $twoDays ++;
                }
                else if($daysExpired === 1){
                    $oneDay ++;
                }
                else if($daysExpired<0){
                    $expired ++;
                }

                $dateFormat = new Carbon($suscription->created_at);
                array_push($data, [
                    "id" => $suscription->id,
                    "user" => $suscription->user,
                    "created_at" => $dateFormat->format('d/m/yy - H:i'),
                    "expired" => $daysExpired,
                    "type" => "suscription",
                    "date_pay" => $dateSuscription->format('d/m/yy - H:i')
                ]);
            }
        }

        return response()->json([
            "resume" => [
                "total" => $total,
                "threeDays" => $threeDays,
                "twoDays" => $twoDays,
                "oneDay" => $oneDay,
                "expired" => $expired,
                "noSuscriptions" => $noSuscriptions
            ],
            "paymentMode" => $paymentMode,
            "data" => $data,
            "codeVerify" =>$codeVerify
        ]);
    }

    public function profits(Request $request){
        $dateFrom = null;
        $dateTo = null;
        $typeFilter = null;
        $typeFilterValue = null;

        if($request->type){
            $typeFilter = $request->type;
            if($request->idType){
                $typeFilterValue = $request->idType;
            }
        }

        if($request->dateFrom){
            $dateFrom = (new Carbon($request->dateFrom))->startOfDay()->format('yy-m-d H:i:s');
        }

        if($request->dateTo){
            $dateTo = (new Carbon($request->dateTo))->endOfDay()->format('yy-m-d H:i:s');
        }

        $registers = [];
        $now = Carbon::now();
        $profits = [
            "ammount" => 0,
            "sendInvoice" => 0,
            "suscriptions" => 0,
            "cards" => 0,
            "transfers" => 0,
            "points" => 0,
            "free" => 0,
            "tickets" => 0,
            "invoices" => 0,
            "news" => 0,
            "renewal" => 0
        ];

        $methodsPayment = [
            "card" => Order::CARD,
            "transfer" => Order::TRANSFER,
            "points" => Order::POINTS,
            "free" => Order::FREE
        ];

        $typeDocuments = [
            "ticket" => Order::TICKET,
            "invoice" => Order::INVOICE
        ];

        $orders = Order::orderBy('date_payment', 'desc')
                        ->where('status', Order::COMPLETE)
                        ->with('user.suscriptions')
                        ->with('suscription')
                        ->where( function($q) use($dateFrom, $dateTo){
                            if($dateFrom && $dateTo){
                                $q->where('created_at', '>=', $dateFrom)
                                  ->where('created_at', '<=', $dateTo);
                            }
                            else if($dateFrom){
                                $q->where('created_at', '>=', $dateFrom);
                            }
                            else if($dateTo){
                                $q->where('created_at', '<=', $dateTo);
                            }
                        } )
                        ->where( function($q) use($typeFilter, $typeFilterValue){
                            if($typeFilter){
                                if($typeFilter == 'methodsPayment'){
                                    $q->where('payment_mode', $typeFilterValue);
                                }
                                else if($typeFilter == 'typeDocuments'){
                                    $q->where('type_document', $typeFilterValue);
                                }
                            }
                        } )
                        ->get();
        foreach($orders as $index => $order){
            // GET IF PAY OTHER USER
            $userPayment = $order->user->username;
            if($order->paid_by){
                $user = User::find($order->paid_by);
                if($user){
                    $userPayment = $user->username;
                }
            }
            
            // FORMAT DATE PAY
            if ($order->date_payment) {
                $datePay = (new Carbon($order->date_payment))->format('d/m/yy - H:i');
            }
            else{
                $datePay = '';
            }
            
            $dateOrder = (new Carbon($order->created_at))->format('d/m/yy - H:i');

            // CREATE REGISTER
            $register = [
                "id" => $order->id,
                "username" => $order->user->username,
                "userPay" => $userPayment,
                "date_order" => $dateOrder,
                "date" => $datePay,
                "methodPayment" => $order->payment_mode,
                "typeDocument" => $order->type_document,
                "sendInvoice" => $order->send_invoice,
                "index" => ($index + 1),
                "file" => $order->file
            ];

            // COUNT SUSCRIPTIONS
            $profits["suscriptions"]++;

            // // INCREMENT TOTAL AMMOUNT
            $profits["ammount"] += floatval($order->ammount);
            
            // COUNT SEND INVOICES
            if($order->send_invoice){
                $profits["sendInvoice"]++;
            }

            // COUNT TRANSFERS
            if($order->payment_mode == Order::TRANSFER){
                $profits["transfers"]++;
            }
            else if($order->payment_mode == Order::CARD){
                $profits["cards"]++;
            }
            // COUNT POINTS
            else if($order->payment_mode == Order::POINTS){
                $profits["points"]++;              
            }
            else if($order->payment_mode == Order::FREE){
                $profits["free"]++;              
            }

            // COUNT TYPE DOCUMENT
            if($order->type_document == Order::TICKET){
                $profits["tickets"]++;
            }
            else if($order->type_document == Order::INVOICE){
                $profits["invoices"]++;
            }

            // COUNT NEW USER RENEWAL
            if(count($order->user->suscriptions)>1){
                $profits["renewal"]++;
                $register["type"] = "Reconsumo";
            }
            else{
                $profits["news"] ++;
                $register["type"] = "Nuevo";
            }

            // ADD REGISTERS
            array_push($registers, $register);
        }

        return response()->json([
            "methodsPayment" => $methodsPayment,
            "typeDocuments" => $typeDocuments,
            "registers" => $registers,
            "profits" => $profits
        ]);
    }

    public function sendInvoice(Request $request){
        $success = false;
        $message = "Lo sentimos no pudimos guardar la informacion";

        $typeDocument = $request->typeDocument;
        $orderId = $request->orderId;
        $sendInvoice = false;
        if($typeDocument){
            $sendInvoice = true;
        }

        $order = Order::find($orderId);
        if($order){
            $order->type_document = $typeDocument;
            $order->send_invoice = $sendInvoice;
            if($order->save()){
                $success = true;
                $message = "Informacion registrada con exito";
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }
    public function expenses()
    {
        $users = User::where('role_id',Role::STUDENT)
                        ->where('status',User::ACTIVE)->get();
        $total = 0;
        $available = 0;
        foreach ($users as $user) {
            $total += $user->points;
            $available += $user->points_available;
        }
        $difference = $total - $available;
        return response()->json([
            'total' => $total,
            'difference' => $difference,
            'available' => $available
        ]);
    }
}

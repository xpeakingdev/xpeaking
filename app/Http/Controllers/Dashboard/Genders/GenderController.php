<?php

namespace App\Http\Controllers\Dashboard\Genders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Gender;

class GenderController extends Controller
{
    public function list(Request $request){

        $genders = Gender::orderBy('name')
                        ->with('artists')
                        ->with('songs')
                        ->get();

        foreach($genders as $index=>$gender){
            $gender->index = ($index + 1);
        }

        return response()->json([
            "genders" => $genders
        ]);
    }

    public function getData(Request $request){
        $gender = Gender::where('id', $request->id)
                    ->first();

        return response()->json([
            "gender" => $gender
        ]);
    }

    public function store(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos guardar la informacion';

        $genderData = $request->gender;
        
        if($genderData["id"]){
            $gender = Gender::where('id', $genderData["id"])
                        ->first();
            $exist = Gender::where('name', $genderData["name"])
                        ->where('id', '<>', $genderData["id"])
                        ->first();
        }
        else{
            $gender = new Gender();
            $exist = Gender::where('name', $genderData["name"])
                        ->first();
        }

        if($exist){
            $message = 'El genero ya existe';
        }
        else{

            $gender->name = $genderData["name"];

            if($gender->save()){
                $success = true;
                $message = "Informacion registrada exitosamente";
            }

        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
        
    }
}

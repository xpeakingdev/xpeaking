<?php

namespace App\Http\Controllers\Dashboard\Orders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Suscription;
use Carbon\Carbon;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\Welcome;
use App\Mail\RenovationSuscription;
use App\Plan;

class OrderController extends Controller
{
    public function paymentStore(Request $request){
        $ordersearch = Order::where('id', $request->payment["id"])->first();
        $user =$ordersearch->user;
        $order = null;
        $suscription = null;
        $type = $request["payment"]["type"];
        $now = Carbon::now();
        $success = false;
        $new = true;
        $message = "Oh no, algo salio mal, no pudimos procesar el pago";
        if ($user->status != User::ACTIVE) {
            return response()->json([
                "success" => $success,
                "message" => 'El correo de la cuenta aún no esta activado, por favor ponte en contacto con el usuario'
            ]);
        }
        $paymentMode = 0;
        if($request["payment"]["paymentMode"]==Order::CARD){
            $paymentMode = Order::CARD;
        }
        else if($request["payment"]["paymentMode"]==Order::TRANSFER){
            $paymentMode = Order::TRANSFER;
        }
        else if($request["payment"]["paymentMode"]==Order::POINTS){
            $paymentMode = Order::POINTS;
        }
        else if($request["payment"]["paymentMode"]==Order::FREE){
            $paymentMode = Order::FREE;
        }
        
        if($paymentMode!=0){
            if($type == 'order'){
                $order = Order::where('id', $request->payment["id"])->first();
                $plan = Plan::find($order->plan_id);
                $points = Helper::valuePoint() * intval($plan->price);
                $order->payment_mode = $paymentMode;
                $order->status = Order::COMPLETE;
                $order->date_payment = $now;
                // $order->points = $points;
                $order->points = 0;
                $order->paid_by = $user->id;
                if ($paymentMode == Order::FREE) {
                    $order->ammount = 0.00;
                    $order->paid_by = Auth::user()->id;
                }
                else if($paymentMode == Order::POINTS){
                    $order->points = $points;
                    $order->paid_by = Auth::user()->id;
                }
            }
            else if($type == 'suscription'){
                $searchSuscription = Suscription::where('id', $request->payment["id"])
                                                ->first();
                $plan = Plan::find($searchSuscription->plan_id);
                $points = Helper::valuePoint() * intval($plan->price);
                $order = new Order([
                    "payment_mode" => $paymentMode,
                    "ammount" => $plan->price,
                    "status" => Order::COMPLETE,
                    "date_payment" => $now,
                    // "points" => $points,
                    "points" => 0,
                    "paid_by" => $user->id,
                    "user_id" => $searchSuscription->user_id,
                    "plan_id" => $plan->id
                ]);
            }
            
            if($order){
                // TOTAL ORDERS
                $orderTotals = Order::where('user_id', $order->user_id)->get();
                if(count($orderTotals)>1){
                    $new = false;
                }

                if($order->save()){
                    $now = new Carbon();
                    $success = true;
                    $message = "Pago procesado exitosamente";
                    $suscriptionConfirm = Suscription::where('active', 1)
                                                ->where('user_id', $order->user_id)
                                                ->where('expires_at', '>=', $now)
                                                ->get();
                    if (count($suscriptionConfirm)>=1) {
                        $dateInit=  new Carbon($suscriptionConfirm[0]->expires_at);
                        $dateEnd = Carbon::parse($suscriptionConfirm[0]->expires_at)->addMonth();
                    }
                    else{
                        $dateInit = Carbon::now();
                        $dateEnd = Carbon::now()->addMonth();
                    }
                    
                    
                    $suscription = new Suscription([
                        "active" => true,
                        "init_at" => $dateInit,
                        "expires_at" => $dateEnd,
                        "user_id" => $order->user_id,
                        "plan_id" => $plan->id,
                        "order_id" => $order->id
                    ]);
                    $suscription->save();
                    Helper::UpdateMailExpired($user->id);
                    Helper::UpdateSponsorship($user->id,true,0);
                    if($new){
                        Mail::to($user->email)->send(new Welcome($user));
                    }
                    else{
                        Mail::to($user->email)->send(new RenovationSuscription($user));
                    }
                    // Helper::addPointsSponsor($order->user_id, $plan->id, $order->id);
                    if ($paymentMode == Order::FREE) {
                        # code...
                    }
                    else{
                        Helper::setPointsSponsors($order->user_id, $plan->id, $order->id);
                    }
                    
                }
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
         
    }
}

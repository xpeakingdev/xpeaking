<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index(){
        // $date = Carbon::now()->format('d/m/yy');
        // dd($date);
        return view('dashboard.index');
    }
    public function permissions()
    {
    	$permissions = Auth::user()->permissions;
    	$sendpermission = array();
    	if ($permissions) {
    		foreach ($permissions as $value) {
    			$sendpermission[$value->name] = true;
    		}
    		
    	}
    	return response()->json([
    		'permissions' => $sendpermission
    	]);
    }
}

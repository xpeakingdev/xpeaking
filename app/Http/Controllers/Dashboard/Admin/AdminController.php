<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\Role;
use App\Permission;
use App\PermissionUser;

class AdminController extends Controller
{
    //
    public function adminlist(Request $request)
    {
    	$dateFrom = null;
        $dateTo = null;
        $filterActive = null;
        $filterLocked = null;

        if($request->active){
            $filterActive = $request->active;
        }

        if($request->dateFrom){
            $dateFrom = (new Carbon($request->dateFrom))->startOfDay()->format('yy-m-d H:i:s');
        }

        if($request->dateTo){
            $dateTo = (new Carbon($request->dateTo))->endOfDay()->format('yy-m-d H:i:s');
        }

        $registers = [];
        $now = Carbon::now();
        $resume = [
            "total" => 0,
            "active" => 0,
            "inactive" => 0
        ];
        $types = [Role::SUPERADMIN,Role::ADMIN];
        $admins = User::wherein('role_id',$types)
    					->with('role')
    					->with('permissions')
        				->orderBy('updated_at', 'desc')
                        ->where( function($q) use($dateFrom, $dateTo){
                            if($dateFrom && $dateTo){
                                $q->where('created_at', '>=', $dateFrom)
                                  ->where('created_at', '<=', $dateTo);
                            }
                            else if($dateFrom){
                                $q->where('created_at', '>=', $dateFrom);
                            }
                            else if($dateTo){
                                $q->where('created_at', '<=', $dateTo);
                            }
                        } )
                        ->get();
        // return response()->json(["students" => $students]);  

        return response()->json([
            "registers" => $admins
        ]);
    }
    public function UserPermissions(Request $request)
    {
    	$user = User::where('username',$request->username)->first();
    	$permissions = $user->permissions;
    	$basepermission = Permission::all();
    	$sendpermission = array();
    	if ($permissions) {
    		foreach ($basepermission as $value) {
    			if ($permissions->contains($value)) {
    				$value['active'] = true;
    			}
    			else{
    				$value['active'] = false;
    			}
    		}
    		
    	}
    	return response()->json([
    		'permissions' => $basepermission
    	]);
    }
    public function changePermission(Request $request)
    {
    	$success = false;
    	$user = User::where('username',$request->username)->first();
    	if ($request->verify) {
    		$permission = new PermissionUser;
    		$permission->permission_id = $request->idpermission;
    		$permission->user_id = $user->id;
    		if($permission->save()){
    			$success = true;
			}
    	}
    	else{
    		$permission= PermissionUser::where([['permission_id',$request->idpermission],['user_id',$user->id]])->first();
    		if ($permission->delete()) {
    			$success = true;
    		}
    	}
    	return response()->json([
    		"success" => $success
    	]);
    }
    public function deleteadmin(Request $request)
    {
        $message = 'Oh no, no se pudo eliminar al usuario';
    	$success = false;
    	$admin = User::where('id',$request->username)->first();
        if ($admin->id == Auth::user()->id) {
            $message = 'No puedes eliminarte a ti mismo';
            return response()->json([
                "message" => $message,
                "success" => $success
            ]);
        }
    	$permission = PermissionUser::where('user_id',$admin->id)->delete();
    	if ($admin->delete()) {
            $message = 'Administrador eliminado exitosamente';
    		$success = true;
    	}
    	return response()->json([
            "message" => $message,
    		"success" => $success
    	]);
    }
    public function getadmininfo(Request $request)
    {
        $user = User::find($request->userid);
        return response()->json([
            'user' => $user
        ]);
    }
    public function register(Request $request)
    {
        $errors = [];
        $success = null;

        $userData = $request->user;

        if(!$userData["email"]){
            array_push($errors, [
                "error" => 'email',
                "message" => 'El email es obligatorio'
            ]);
        }

        if(!$userData["username"]){
            array_push($errors, [
                "error" => 'username',
                "message" => 'El nombre de usuario es obligatorio'
            ]);
        }

        if(!$userData["password"]){
            array_push($errors, [
                "error" => 'password',
                "message" => 'La contraseña es obligatoria'
            ]);
        }

        // $validPhone = "/^9[0-9]+/";
        // if(!$userData["phone"]){
        //     array_push($errors, [
        //         "error" => 'phone',
        //         "message" => 'El celular es obligatorio'
        //     ]);
        // }
        // else{
        //     if(strlen($userData["phone"]) !== 9 || !preg_match($validPhone, $userData["phone"])){
        //         array_push($errors, [
        //             "error" => 'phone',
        //             "message" => 'El celular es obligatorio'
        //         ]);
        //     }
        // }

        if($userData["password"] != $userData["confirmPassword"]){
            array_push($errors, [
                "error" => 'passwordConfirm',
                "message" => 'Las contraseñas ingresadas no coinciden'
            ]);
        }
        $userData["username"] = strtolower($userData["username"]);
        $validUser = User::where('email', $userData["email"])
                          ->orWhere('username', $userData["username"])
                          ->first();
        if($validUser){
            if($validUser["email"] == $userData["email"]){
                array_push($errors, [
                    "error" => 'uniqEmail',
                    "message" => 'El email ingresado no se encuentra disponible'
                ]);
            }
            
            if($validUser["username"] == $userData["username"]){
                array_push($errors, [
                    "error" => 'uniqUsername',
                    "message" => 'El nombre de usuario ingresado no se encuentra disponible'
                ]);
            }
        }

        if(count($errors)==0){
            $user = new User([
                "username" => $userData["username"],
                "email" => $userData["email"],
                "password" => Hash::make($userData["password"]),
                "status" => User::ACTIVE,
                "permission_role" => true,
                "role_id" => Role::ADMIN
            ]);

            if($user->save()){
                $success = true;
            }
            else{
                array_push($errors, [
                    "error" => 'createdUser',
                    "message" => 'Ocurrio un error al registrar la informacion'
                ]);
            }
        }

        return response()->json([
            "errors" => $errors,
            "success" => $success
        ]);
    }
    public function changeadmininfo(Request $request)
    {
        $errors = [];
        $success = null;

        $userData = $request->user;

        if(!$userData["email"]){
            array_push($errors, [
                "error" => 'email',
                "message" => 'El email es obligatorio'
            ]);
        }

        if(!$userData["username"]){
            array_push($errors, [
                "error" => 'username',
                "message" => 'El nombre de usuario es obligatorio'
            ]);
        }
        if ($request->changepassword) {
            if(!$userData["password"]){
                array_push($errors, [
                    "error" => 'password',
                    "message" => 'La contraseña es obligatoria'
                ]);
            }

            if($userData["password"] != $userData["confirmPassword"]){
                array_push($errors, [
                    "error" => 'passwordConfirm',
                    "message" => 'Las contraseñas ingresadas no coinciden'
                ]);
            }
        }
        
        $userData["username"] = strtolower($userData["username"]);
        $User = User::find($request->iduser);
        if($User){
            if($User->email != $userData["email"]){
                $email = User::where('email',$userData["email"])->first();
                if ($email) {
                    array_push($errors, [
                        "error" => 'uniqEmail',
                        "message" => 'El email ingresado no se encuentra disponible'
                    ]);
                }
            }
            
            if($User->username != $userData["username"]){
                $name = User::where('username',$userData["username"])->first();
                if ($name) {
                    array_push($errors, [
                        "error" => 'uniqUsername',
                        "message" => 'El nombre de usuario ingresado no se encuentra disponible'
                    ]);
                }
            }
        }

        if(count($errors)==0){
            $User->username = $userData["username"];
            $User->email = $userData["email"];
            if ($request->changepassword) {
                $User->password = Hash::make($userData["password"]);
            }
            if($User->save()){
                $success = true;
            }
            else{
                array_push($errors, [
                    "error" => 'createdUser',
                    "message" => 'Ocurrio un error al registrar la informacion'
                ]);
            }
        }

        return response()->json([
            "errors" => $errors,
            "success" => $success
        ]);
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Profile;
use App\Country;

class ProfileController extends Controller
{
    public function profile(){
        return view('dashboard.profile.index');
    }

    public function getUser(){
        $user = User::where('id', auth()->user()->id)
                ->with('role')
                ->with('profile.city.region.country')
                ->first();

        $age = null;

        if($user){
            if(!$user->profile){
                $profile = new Profile([
                    "user_id" => $user->id
                ]);
                $profile->save();
                $user->profile = $profile;
            }

            if($user->profile->birthdate){
                $date = new Carbon();
                $birthdate = new Carbon($user->profile->birthdate);
                $age = $date->diffInYears($birthdate);
            }
            
        }
        return response()->json([
            "user" => $user,
            "age" => $age
        ]);
    }

    public function getDataProfile(){
        $countries = Country::orderBy('name')->with('regions.cities')->get();
        $typesDocuments = User::typesDocuments();
        $user = User::where('id', auth()->user()->id)
                    ->with('profile')
                    ->with('profile.city.region.country')
                    ->first();

        return response()->json([
            "countries" => $countries,
            "user" => $user
        ]);
    }

    public function storeProfile(Request $request){
        $success = false;
        $error = null;

        $profile = Profile::find(auth()->user()->profile->id);
        $user = User::where('id',auth()->user()->id)->first();

        $birthdate = null;
        if($request["profile"]["birthdate"]){
            $date = new Carbon($request["profile"]["birthdate"]);
            $birthdate = $date->format("yy-m-d");
        }

        $sex = null;
        if($request["profile"]["sex"] ==! 0){
            $sex = $request["profile"]["sex"];
        }
        $profile->sex = $sex;

        $profile->name = $request["profile"]["name"];        
        $profile->last_name = $request["profile"]["last_name"];        
        $profile->phone = $request["profile"]["phone"];        
        $profile->address = $request["profile"]["address"];      
        $profile->birthdate = $request["profile"]["birthdate"];      
        $profile->document = $request["profile"]["document"];      
        $profile->payname = $request["profile"]["payname"];      

        if($request["update"]["cityId"]==0){
            $profile->city_id = NULL;
        }
        else{
            $profile->city_id = $request["update"]["cityId"];
        }

        if($request["update"]["typeDocument"]==0){
            $profile->type_document = NULL;
        }
        else{
            $profile->type_document = $request["update"]["typeDocument"];
        }

        if($profile->save()){
            $success = true;
        }

        return response()->json([
            "success" => $success,
            "error" => $error
        ]);
    }

    public function storePassword(Request $request){
        $success = false;
        $error = null;

        if($request->passwod =! $request->confirmPassword){
            $error = 'Las contraseñas no coinciden';
        }

        if(!$error){
            $user = User::find(auth()->user()->id);
            $user->password = Hash::make($request->password);
            if($user->save()){
                $success = true;
            }
            else{
                $error = 'Algo salio mal, no se actualizaron los cambios';
            }
        }

        return response()->json([
            "success" => $success,
            "error" => $error
        ]);
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Helpers\SuscriptionHelper;
use App\AdminOperation;
use Carbon\Carbon;

class SuscriptionController extends Controller
{
    //
    public function dayavailable()
    {
    	$available=true;
    	$now = new Carbon();
    	$operation= AdminOperation::where('type' , AdminOperation::SUSCRIPCION)
    								->whereYear('created_at' , $now->year)
    								->whereMonth('created_at' , $now->month)
    								->whereDay('created_at' , $now->day)->get();
		if (count($operation)==1) {
			$available=false;
		}
		return response()->json([
            "available" => $available
        ]);
    }
    public function checksuscription()
    {
    	$available=true;
    	$status = false;
    	$now = new Carbon();
    	$operation= AdminOperation::where('type' , AdminOperation::SUSCRIPCION)
    								->whereYear('created_at' , $now->year)
    								->whereMonth('created_at' , $now->month)
    								->whereDay('created_at' , $now->day)->get();
		if (count($operation)==1) {
			$available=false;
		}

		if ($available==true) {
	    	SuscriptionHelper::Adminchecksuscription();
	    	$newoperation = new AdminOperation([
	    		"type" => AdminOperation::SUSCRIPCION,
	    		"user_id" => Auth::user()->id
	    	]);
	    	if ($newoperation->save()) {
	    		$status = true; 
	    	}
	    	
		}
    	
    	return response()->json([
            "status" => $status,
        ]);
    }
}

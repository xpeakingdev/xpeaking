<?php

namespace App\Http\Controllers\Dashboard\Artists;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\ArtistLanguage;
use App\ArtistGender;
use App\Artist;
use App\Language;

class ArtistController extends Controller
{
    public function list(Request $request){
        $dateFrom = null;
        $dateTo = null;
        if($request->dateFrom){
            $dateFrom = (new Carbon($request->dateFrom))->startOfDay()->format('yy-m-d H:i:s');
        }
            
        if($request->dateTo){
            $dateTo = (new Carbon($request->dateTo))->endOfDay()->format('yy-m-d H:i:s');
        }

        $artists = Artist::with('genders')
                    ->with('songs')
                    ->orderBy('updated_at', 'desc')
                    ->where( function($q) use($dateFrom, $dateTo){
                        if($dateFrom && $dateTo){
                            $q->where('date_published', '>=', $dateFrom)
                            ->where('date_published', '<=', $dateTo);
                        }
                        else if($dateFrom){
                            $q->where('date_published', '>=', $dateFrom);
                        }
                        else if($dateTo){
                            $q->where('date_published', '<=', $dateTo);
                        }
                    } )
                    ->get();

        foreach($artists as $index=>$artist){
            if($artist->date_published){
                $artist->date_published = (new Carbon($artist->date_published))->format('d/m/yy - H:i');
            }
            $artist->created = (new Carbon($artist->created_at))->format('d/m/yy - H:i');
            $artist->index = ($index + 1);
        }

        return response()->json([
            "artists" => $artists
        ]);
    }

    public function getData(Request $request){
        $artist = Artist::where('id', $request->id)
                    ->with('genders')
                    ->with('songs')
                    ->first();
        $listLangs = [];
        $languages = Language::orderBy('name')->get();
        $artistLanguage = ArtistLanguage::where('artist_id', $artist->id)->get();
        foreach ($artistLanguage as $artLang) {
            foreach ($languages as $lang) {
                if($artLang->language_id == $lang->id){
                    array_push($listLangs, $lang);
                }
            }
        }

        $artist->languages = $listLangs;

        return response()->json([
            "artist" => $artist
        ]);
    }

    public function store(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos guardar la informacion';

        $artistData = $request->artist;
        $removed = $request->removed;
        $news = $request->news;
        
        if($artistData["id"]){
            $artist = Artist::where('id', $artistData["id"])
                        ->first();
            $exist = Artist::where('slug', $artistData["slug"])
                        ->where('id', '<>', $artistData["id"])
                        ->first();
        }
        else{
            $artist = new Artist();
            $exist = Artist::where('slug', $artistData["slug"])
                        ->first();
        }

        if($exist){
            $message = 'El slug de la cancion ya existe';
        }
        else{
            // $artist->published = $artistData["published"];
            // if($artistData["published"]){
            //     $artist->date_published = new Carbon();
            // }

            $artist->stage_name = $artistData["stage_name"];
            $artist->slug = $artistData["slug"];
            $artist->picture = $artistData["picture"];
            $artist->order = $artistData["order"];
            $artist->important = ($artistData["important"]?true:false);


            if($artist->save()){
                
                // REMOVE GENDERS
                if(count($removed["genders"])>0){
                    $gendersRemoved = ArtistGender::whereIn('gender_id', $removed["genders"])
                                                ->where('artist_id', $artist->id)
                                                ->delete();
                }
                
                 // REMOVE LANGUAGES
                 if(count($removed["languages"])>0){
                    $languagesRemoved = ArtistLanguage::whereIn('language_id', $removed["languages"])
                                                ->where('artist_id', $artist->id)
                                                ->delete();
                }

                // NEWS GENDERS
                foreach($news["genders"] as $gender){
                    $artistGender = new ArtistGender([
                        "gender_id" => $gender,
                        "artist_id" => $artist->id
                    ]);
                    $artistGender->save();
                }

                 // NEWS LANGUAGES
                 foreach($news["languages"] as $language){
                    $artistLanguage = new ArtistLanguage([
                        "language_id" => $language,
                        "artist_id" => $artist->id
                    ]);
                    $artistLanguage->save();
                }

                $success = true;
                $message = "Informacion registrada exitosamente";
            }

        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
        
    }

    public function changePublished(Request $request){
        $success = false;
        $message = 'Oh no, algo salio mal, no pudimos actualizar la informacion';
        $datePublished = null;

        $artist = Artist::find($request->id);
        if($artist){
            $artist->published = $request->published;

            if($artist->published){
                $datePublished = (new Carbon())->format('d/m/yy - H:i');
                $artist->date_published = new Carbon();
            }

            if($artist->save()){
                $success = true;
                $message = 'Informacion actualizada exitosamente';
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message,
            "datePublished" => $datePublished
        ]);
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Students;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DictionarySpanishEnglish;
use App\Mail\ReportPlayerAttended;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Helper;
use Carbon\Carbon;
use App\Language;
use App\Student;
use App\Report;
use App\User;
use App\Song;
use App\Role;
use Session;
class ReportsController extends Controller
{
    //
    public function datalist()
    {
    	$reports = Report::with('source_language')->with('target_language')->with('user')->orderBy('created_at','desc')->get();
        foreach ($reports as $report) {
            $report->created = (new Carbon($report->created_at))->format('d/m/yy - H:i');
        }
    	return response()->json([
            "reports" => $reports,
            
        ]);
    }
    public function updatereport(Request $request)
    {
    	$success = false;
    	$report = Report::find($request->report_id);
    	$report->resolved = $request->locked;
    	if ($report->save()) {
    		$success = true;
    		if ($request->locked == true) {
    			$mainsubject = 'Estamos muy agradecidos por su observación, nuestro equipo ya se hizo cargo del inconveniente. Siga disfrutando de más canciones y aprendiendo su idioma preferido';
    		
	    		$data=[
	                'user'=>$report->user,
	                'subject'=>$mainsubject

	            ];
	            Mail::to($report->user->email)->send(new ReportPlayerAttended($data));
    		}
    		
    	}
    	
    	return response()->json([
            "success" => $success,
            
        ]);
    }
}

<?php

namespace App\Http\Controllers\Dashboard\Students;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Sponsorship;
use App\Suscription;
use Carbon\Carbon;
use App\Student;
use App\History;
use App\Order;
use App\Tracing;
use App\Range;
use App\User;
use App\Profile;
use App\Language;
use App\Country;

class StudentController extends Controller
{
    public function resume(Request $request){
        $dateFrom = null;
        $dateTo = null;
        $filterActive = null;
        $filterLocked = null;
        $codeVerify = User::NOTVERIFIED;

        if($request->active){
            $filterActive = $request->active;
        }

        if($request->dateFrom){
            $dateFrom = (new Carbon($request->dateFrom))->startOfDay()->format('yy-m-d H:i:s');
        }

        if($request->dateTo){
            $dateTo = (new Carbon($request->dateTo))->endOfDay()->format('yy-m-d H:i:s');
        }

        $registers = [];
        $now = Carbon::now();
        $resume = [
            "total" => 0,
            "active" => 0,
            "inactive" => 0
        ];

        $students = Student::orderBy('updated_at', 'desc')
                        ->with('user.suscriptions')
                        ->with(['user.tracing' => function($q){
                            $q->distinct();
                        }])
                        ->where( function($q) use($dateFrom, $dateTo){
                            if($dateFrom && $dateTo){
                                $q->where('created_at', '>=', $dateFrom)
                                  ->where('created_at', '<=', $dateTo);
                            }
                            else if($dateFrom){
                                $q->where('created_at', '>=', $dateFrom);
                            }
                            else if($dateTo){
                                $q->where('created_at', '<=', $dateTo);
                            }
                        } )
                        ->get();
        // return response()->json(["students" => $students]);
        foreach($students as $index => $student){
            $dateFirstOrder = '----';
            $order = Order::orderBy('date_pay')->where('user_id', $student->user->id)->first();
            if($order){
                $dateFirstOrder = (new Carbon($order->date_pay))->format('d/m/yy - H:i');
            }
            // CREATE REGISTER
            $dateReg = (new Carbon($student->created_at))->format('d/m/yy - H:i');
            $active = false;
            foreach($student->user->suscriptions as $suscription){
                if($suscription->active){
                    $active = true;
                }
            }

            if($active){
                $resume["active"]++;
            }
            else{
                $resume["inactive"]++;
            }

            $resume["total"]++;

            $register = [
                "id" => $student->id,
                "username" => $student->user->username,
                "email" => $student->user->email,
                "date" => $dateReg,
                "active" => $active,
                "status" => $student->user->status,
                "invitations" => $student->user->limit_invitations,
                "levels" => $student->user->limit_levels,
                "tracing" => $student->user->tracing,
                'payment' => $dateFirstOrder,
                "index" => ($index + 1)
            ];
            
            // ADD REGISTERS
            if(($filterActive == null || $filterActive == $active) && count($student->user->suscriptions)>0){
                array_push($registers, $register);
            }
            
        }

        return response()->json([
            "registers" => $registers,
            "resume" => $resume,
            "codeVerify" => $codeVerify
        ]);
    }

    public function verifyaccount(Request $request){
        $success = false;
        $message = "Oh no, no pudimos guardar la informacion";

        $status = null;
        if($request->verify){
            $status = User::ACTIVE;
        }
        else{
            $status = User::NOTVERIFIED;
        }

        $user = User::where('username', $request->username)->first();
        if($user && $status){
            $user->status = $status;
            if($user->save()){
                $success = true;
                $message = "Usuario activado exitosamente";
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function locked(Request $request){
        $success = false;
        $message = "Oh no, no pudimos guardar la informacion";

        $status = null;
        if($request->locked){
            $status = User::LOCKED;
        }
        else{
            $status = User::ACTIVE;
        }

        $user = User::where('username', $request->username)->first();
        if($user && $status){
            $user->status = $status;
            if($user->save()){
                $success = true;
                $message = "Usuario bloqueado exitosamente";
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function userData(Request $request){
        $username = $request->username;
        $user = User::where('username', $username)
                    ->with('profile')
                    ->with('student.languages')
                    ->with('profile.city.region.country')
                    ->first();
        // $sponsorship = Sponsorship::where('sponsored_id',$user->id)->first();
        // $sponsor = User::find($sponsorship->sponsor_id);
        $countries = Country::orderBy('name')->with('regions.cities')->get();
        $languages = Language::orderBy('name')->get();
        $typesDocuments = User::typesDocuments();
        $age = null;
        $active = false;
        $codeLocked = User::LOCKED;
        if($user){

            $sponsorship = Sponsorship::where('sponsored_id', $user->id)->first();
            if($sponsorship){
                $sponsor = User::find($sponsorship->sponsor_id);
                $user["sponsor"] = $sponsor->username;
            }
            else{
                $user["sponsor"] = 'Sin Patrocinador';
            }
    
            if($user->profile->birthdate){
                $date = new Carbon();
                $birthdate = new Carbon($user->profile->birthdate);
                $age = $date->diffInYears($birthdate);
            }
    
            $suscription = Suscription::where('active', true)
                                        ->where('user_id', $user->id)
                                        ->first();
            if($suscription){
                $active = true;
            }
        }


        return response()->json([
            "countries" => $countries,
            "languages" => $languages,
            "typesDocuments" => $typesDocuments,
            "user" => $user,
            "codeLocked" => $codeLocked,
            "age" => $age,
            "active" => $active,

        ]);
    }

    public function limitInvitations(Request $request){
        $success = false;
        $message = 'No se pudo actualizar la informacion';
        $username = $request->username;
        $user = User::where('username', $username)->first();
        if($user){
            $user->limit_invitations = $request->invitations;
            if($user->save()){
                $success = true;
                $message = 'Los cambios han sido GUARDADOS exitosamente.';
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }
    public function userDataStore(Request $request)
    {
        $success = false;
        $error = null;

        $profile = Profile::find($request->user['profile']['id']);
        $user = User::where('id',$request->user['id'])->with('student.languages')->first();

        $birthdate = null;
        if($request->user["profile"]["birthdate"]){
            $date = new Carbon($request->user["profile"]["birthdate"]);
            $birthdate = $date->format("yy-m-d");
        }
        $sex = null;
        if($request->user["profile"]["sex"] ==! 0){
            $sex = $request->user["profile"]["sex"];
        }
        $profile->sex = $sex;

        $profile->name = $request->user["profile"]["name"];        
        $profile->last_name = $request->user["profile"]["last_name"];        
        $profile->phone = $request->user["profile"]["phone"];            
        $profile->birthdate = $request->user["profile"]["birthdate"];          
        $user->email = $request->user["email"];
        if($profile->save() && $user->save()){
            $success = true;
        }

        return response()->json([
            "success" => $success,
            "error" => $error
        ]);
    }

    public function limitILevels(Request $request){
        $success = false;
        $message = 'No se pudo actualizar la informacion';
        $username = $request->username;
        $user = User::where('username', $username)->first();
        if($user){
            $user->limit_levels = $request->levels;
            if($user->save()){
                $success = true;
                $message = 'Los cambios han sido GUARDADOS exitosamente.';
            }
        }

        return response()->json([
            "success" => $success,
            "message" => $message
        ]);
    }

    public function listRestrictered(Request $request){
        $genderRestricted = [];
        $artistRestricted = [];
        $restrictedSong = [];

        $user = User::where('username', $request->username)->with('student')->first();
        $student = $user->student;

        $genderRestricted = $student->genderRestricted;
        $artistRestricted = $student->artistRestricted;
        $restrictedSong = $student->restrictedSong;

        return response()->json([
            "restricted" => [
                "genders" => $genderRestricted,
                "artists" => $artistRestricted,
                "songs" => $restrictedSong
            ]
        ]);
    }

    public function getSuscription(Request $request){
        $user = User::where('username', $request->username)->first();
        $suscription = Suscription::where('active', 1)
                                ->with('plan')
                                ->where('user_id', $user->id)
                                ->get();
        $suscriptioncheck = null;
        if(count($suscription)>=1){
            $expires= new Carbon($suscription[0]->expires_at);
            $init_at = new Carbon($suscription[0]->init_at);
            foreach ($suscription as $key => $value) {
                $compare = new Carbon($value->expires_at);
                $compareinit = new Carbon($value->init_at);
                if ($compare > $expires) {
                    $expires = $compare;
                }
                if ($compareinit < $init_at) {
                    $init_at = $compareinit;
                }
            }
            $now = new Carbon();
            $daysBetween = $now->FloatDiffInDays($expires);
            $suscriptioncheck["expires_at"] = $expires->day . "/" . Helper::getMonthName($expires->month - 1) . "/" . $expires->year;
            $suscriptioncheck["init_at"] = $init_at->day . "/" . Helper::getMonthName($init_at->month - 1) . "/" . $init_at->year;
            $suscriptioncheck["planprice"] = $suscription[0]->plan->price;
        }

        $orders = [];
        $order = Order::where('status', Order::PENDING)
                        ->where('user_id', $user->id)
                        ->first();
        if ($order) {
            $datecreate = new Carbon($order->created_at);
            $order['create'] = $datecreate->day . "/" . Helper::getMonthName($datecreate->month - 1) . "/" . $datecreate->year;
            
        }
        $ordersList = Order::orderBy('date_payment', 'desc')
                        ->where('status', Order::COMPLETE)
                        ->where('user_id', $user->id)
                        ->get();
        foreach($ordersList as $orde){

            $paymentsMethods = [
                Order::CARD => "Tarjeta",
                Order::TRANSFER => "Transferencia",
                Order::POINTS => "Puntos",
                Order::FREE => "Gratis"
            ];

            $userPayment = $user->username;
            if($orde->paid_by){
                $userP = User::find($orde->paid_by);
                if($userP){
                    $userPayment = $userP->username;
                }
            }
            // FORMAT DATE PAY
            if ($orde->date_payment) {
                $datePay = (new Carbon($orde->date_payment))->format('d/m/yy - H:i');
            }
            else{
                $datePay = '';
            }
            
            $dateOrder = (new Carbon($orde->created_at))->format('d/m/yy - H:i');

            $ammount = 'S/ ' . $orde->ammount;
            
            if($orde->payment_mode == Order::POINTS){
                $ammount = $orde->points;
            }

            array_push($orders, [
                "id" => $orde->Id,
                "dateCreated" => $dateOrder,
                "datePayment" => $datePay,
                "ammount" => $ammount,
                "paymentMethod" => $paymentsMethods[$orde->payment_mode],
                "userPayment" =>  $userPayment
            ]);
        }

        return response()->json([
            "suscription" => $suscriptioncheck,
            "order" => $order,
            "orders" => $orders
        ]);
    }

    public function getDataUser(Request $request){
        $user = User::where('username', $request->username)->first();
        return response()->json([
            "user" => $user
        ]);
    }

    public function pointsResume(Request $request){
        $user = User::where('username', $request->username)->first();
        $pointsTotal = 0;
        $pointsAvailable = $user->points_available;

        $history = History::where('user_id', $user->id)
                            ->orderBy('created_at', 'desc')
                            ->get();
        foreach($history as $hist){
            $hist["date"]= (new Carbon($hist->created_at))->format('d/m/yy - H:i');
            if($hist->type == History::INVITED || $hist->type == History::REFERRED){
                $pointsTotal += $hist->points;
            }
        }

        return response()->json([
            "history" => $history,
            "pointsTotal" => $pointsTotal,
            "pointsAvailable" => $pointsAvailable
        ]);
    }

    public function teamDetail(Request $request){
        $range = Range::find($request->rangeId);
        if($range){
            $user = User::where('username', $request->username)->first();
            $levels = $user->limit_levels;
            $students = [];
            $sponsorIds = [$user->id];
            $actives = 0;
            
            for($i=1; $i <= $levels; $i++){
                $idRelatives = [];
                foreach($sponsorIds as $id){
                    $sponsors = Sponsorship::where([['active',true],['sponsor_id', $id]])
                                            ->get();
                    foreach($sponsors as $sponsor){
                        array_push($idRelatives, $sponsor->sponsored_id);
                        // VALID ACTIVE
                        $validActive = User::where('id', $sponsor->sponsored_id)
                                            ->where('status', User::ACTIVE)
                                            ->with('suscriptions')
                                            ->first();
                        if($i == $range->level){
                            $sponsorMain = User::where('id', $sponsor->sponsor_id)->first();
                            $validActive["active"] = false;
                            $date = (new Carbon($validActive["created_at"]))->format('d/m/yy - H:i');
                            $validActive["date"] = $date;
                            $validActive["sponsor"] = $sponsorMain->username;
                            foreach($validActive->suscriptions as $suscription){
                                if($suscription->active){
                                    $validActive["active"] = true;
                                }
                            }
                            array_push($students, $validActive);

                        }
                        // if($sponsor->active){
                        //     $actives++;
                        // }
                    }
                }
                $sponsorIds = $idRelatives;
            }
        }
        
        return response()->json([
            "students" => $students
        ]);
    }

    public function listAmbasador(Request $request){
        $user = User::where('username', $request->username)->first();
        $levels = $user->limit_levels;
        $students = [];
        $sponsorIds = [$user->id];
        
        for($i=1; $i <= $levels; $i++){
            $actives = 0;
            $total = 0;
            $idRelatives = [];
            foreach($sponsorIds as $id){

                $sponsors = Sponsorship::where([['active',true],['sponsor_id', $id]])
                                        ->get();
                $total += count($sponsors);
                foreach($sponsors as $sponsor){
                    array_push($idRelatives, $sponsor->sponsored_id);
                    // VALID ACTIVE
                    $validActive = User::where('id', $sponsor->sponsored_id)
                                        ->where('status', User::ACTIVE)
                                        ->whereHas('suscriptions', function($q){
                                            $q->where('active', true);
                                        })
                                        ->first();
                    if($validActive){
                        $actives++;
                    }
                    // if($sponsor->active){
                    //     $actives++;
                    // }
                }
            }
            array_push($students, [
                "level" => $i,
                "actives" => $actives,
                "total" => $total
            ]);
            $sponsorIds = $idRelatives;
        }

        return response()->json([
            "students" => $students,
            "user" => $user
        ]);
    }

    public function getTracing(Request $request){
        $user = User::where('username', $request->username)
                    ->with('tracing.artists')
                    ->first();
        // return response()->json(["user" => $user]);
        foreach($user->tracing as $tracing){
            $tracingData = Tracing::where('song_id', $tracing->id)
                                ->where('user_id', $user->id)
                                ->first();
            $tracing->date = (new Carbon($tracingData->date))->format('d/m/yy');
            $tracing->time_start = (new Carbon('2000-01-01 ' . $tracingData->time_start))->toTimeString();
        }
        return response()->json([
            "user" => $user
        ]);
    }

}

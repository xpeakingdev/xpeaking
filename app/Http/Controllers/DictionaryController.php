<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dictionary;

class DictionaryController extends Controller
{
    public function dictionary(){
        $student = auth()->user()->student;
        $dictionaries = Dictionary::where('student_id', $student->id)->get();
        return response()->json([
            "dictionaries" => $dictionaries
        ]);
    }

    public function dictionaryStore(Request $request){
        $success = true;
        $student = auth()->user()->student;
        $exist = Dictionary::where('student_id', $student->id)
                            ->where('word', $request->word)
                            ->where('source_language_id', $request->sourceLanguageId)
                            ->where('target_language_id', $request->targetLanguageId)
                            ->first();
        if($exist){
            $exist->delete();
        }
        else{
            $word = new Dictionary([
                "student_id" => $student->id,
                "word" => $request->word,
                "source_language_id" => $request->sourceLanguageId,
                "target_language_id" => $request->targetLanguageId
            ]);
            $word->save();
        }
        return response()->json([
            "success" => $success
        ]);
    }
}

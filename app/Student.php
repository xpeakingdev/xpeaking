<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $fillable = [
        'user_id',
        'gems',
        'gems_available'
    ];

    // RELATIONS

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function languages(){
        return $this->belongsToMany(Language::class, 'language_student')->select('name', 'code', 'speak', 'learn', 'principal', 'language_id');
    }

    public function favoritesSongs(){
        return $this->hasMany(FavoriteSong::class);
    }

    public function artistsFavorites(){
        return $this->hasMany(ArtistFavorite::class);
    }

    public function songs(){
        return $this->belongsToMany(Song::class, 'favorite_song')->orderBy('name');
    }

    public function artists(){
        return $this->belongsToMany(Artist::class, 'artist_favorite')->orderBy('stage_name');
    }

    public function dictionaries(){
        return $this->hasMany(Dictionary::class);
    }

    public function artistRestricted(){
        return $this->belongsToMany(Artist::class, 'artist_restricted')->select('stage_name', 'artist_id');
    }

    public function genderRestricted(){
        return $this->belongsToMany(Gender::class, 'gender_restricted')->select('name', 'gender_id');
    }

    public function restrictedSong(){
        return $this->belongsToMany(Song::class, 'restricted_song')->select('name', 'song_id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suscription extends Model
{
    protected $table = 'suscriptions';
    protected $fillable = [
        "active",
        "init_at",
        "expires_at",
        "user_id",
        "plan_id",
        "order_id"
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function plan(){
        return $this->belongsTo(Plan::class);
    } 

    public function order(){
        return $this->belongsTo(Order::class);
    }
}

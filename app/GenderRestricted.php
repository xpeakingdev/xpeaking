<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenderRestricted extends Model
{
    protected $table = "gender_restricted";
    protected $fillable = ["gender_id", "student_id"];
}

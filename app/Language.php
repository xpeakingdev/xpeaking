<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';
    protected $fillable = ['name'];

    CONST SPANISH = 1;
    CONST ENGLISH = 2;

    CONST CODE_SPANISH = "es";
    CONST CODE_ENGLISH = "en";

    // RELATIONS
    public function students(){
        return $this->belongsToMany(Student::class, 'language_student');
    }

    public function artists(){
        return $this->belongsToMany(Artist::class, 'artist_gender')->select('native');
    }

    public function songs(){
        return $this->belongsToMany(Song::class, 'language_song');
    }

    public function subtitles(){
        return $this->hasMany(Subtitle::class);
    }

    public function dictionaries(){
        return $this->hasMany(Dictionary::class);
    }
}

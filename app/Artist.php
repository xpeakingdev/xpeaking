<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $table = "artists";
    protected $fillable = [
        "name",
        "last_name",
        "stage_name",
        "picture",
        // "active",
        "important",
        "published",
        "date_published",
        "views",
        "order"
    ];

    // RELATIONS
    public function genders(){
        return $this->belongsToMany(Gender::class, 'artist_gender');
    }

    public function languages(){
        return $this->belongsToMany(Language::class, 'artist_language')->select('native');
    }

    public function songs(){
        return $this->belongsToMany(Song::class, 'artist_song');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubtiTle extends Model
{
    protected $table = 'subtitles';
    protected $fillable = [
        "file",
        "status",
        "active",
        "language_id",
        "song_id"
    ];

    // RELATIONS
    public function language(){
        return $this->belongsTo(Language::class);
    }

    public function song(){
        return $this->belongsTo(Song::class);
    }
}

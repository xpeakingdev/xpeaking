<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Range extends Model
{
    protected $table = 'ranges';
    protected $fillable = [
        'description',
        'limit',
        'level',
        'active'
    ];

    // RELATIONS
    public function awards(){
        return $this->hasMany(Award::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = 'tokens';
    protected $fillable = ['token', 'type', 'user_id'];

    // RELATIONS
    public function user(){
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistLanguage extends Model
{
    protected $table = 'artist_language';
    protected $fillable = ["artist_id", "language_id"];

    // RELATIONS
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsorship extends Model
{
    protected $table = 'sponsorships';
    protected $fillable = ['sponsor_id', 'points', 'sponsored_id', 'active'];
    
}

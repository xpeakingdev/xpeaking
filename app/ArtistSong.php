<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistSong extends Model
{
    protected $table = 'artist_song';
    protected $fillable = ["author", "artist_id", "song_id"];
}

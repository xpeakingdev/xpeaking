<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    protected $fillable = [
        "name",
        "active",
        "show"
    ];

    public function plans(){
        return $this->belongsToMany(Plan::class);
    }
}

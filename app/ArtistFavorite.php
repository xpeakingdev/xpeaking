<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistFavorite extends Model
{
    protected $table = 'artist_favorite';
    protected $fillable = ["student_id", "artist_id"];

    public function student(){
        return $this->belongsTo(Student::class);
    }
}

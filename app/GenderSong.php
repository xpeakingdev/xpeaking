<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenderSong extends Model
{
    protected $table = 'gender_song';
    protected $fillable = ["gender_id", "song_id"];

}

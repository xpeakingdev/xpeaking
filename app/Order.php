<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    // PAYMENT MODE
    CONST CARD = 1;
    CONST TRANSFER = 2;
    CONST POINTS = 3;
    CONST FREE = 4;

    // STATUS
    CONST PENDING = 1;
    CONST COMPLETE = 2;

    // BOLETA
    CONST TICKET = 1;
    // FACTURA
    CONST INVOICE = 2;

    protected $table = 'orders';
    protected $fillable = [
        "payment_mode",
        "description",
        "ammount",
        "type_document",
        "send_invoice",
        "status",
        "date_pay",
        "date_payment",
        "points",
        "paid_by",
        "user_id",
        "plan_id",
        "file",
        "upload_file"
    ];
    

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function plan(){
        return $this->belongsTo(Plan::class);
    }

    public function suscription(){
        return $this->hasOne(Suscription::class);
    }

    public function history(){
        return $this->hasOne(History::class);
    }
}

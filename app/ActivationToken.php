<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivationToken extends Model
{
    protected $table = 'activations_tokens';
    protected $fillable = ['token', 'user_id'];
    protected  $primaryKey = 'token';
    protected $dates = ['created_at', 'updated_at'];
    public $incrementing = false;
    // public $timestamps = false;

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}

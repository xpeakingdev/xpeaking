<?php

namespace App\Helpers;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Mail\SuscriptionNotification;
use Illuminate\Support\Facades\Mail;
use App\ClientOperation;
use App\Suscription;
use App\Order;
use App\Role;
use App\Plan;
use App\User;
use Storage;

class SuscriptionHelper{

    
    public static function CheckSuscription($user_id)
    {
        // fecha actual
        $now = new Carbon();
        $user = User::find($user_id);
        // VALID SUSCRIPTION
        
        if(Auth::check() && ($user->role_id == Role::STUDENT) && (SuscriptionHelper::CheckOrderClientPay($user_id))){
            $checkmail = ClientOperation::where('user_id',$user->id)
                                    ->where('operation',ClientOperation::MAILEXPIRED)
                                    ->where('status',true)->get();
            $checkmailtwo = ClientOperation::where('user_id',$user->id)
                                        ->where('operation',ClientOperation::MAILSUS)
                                        ->where('status',1)
                                        ->whereYear('created_at' , $now->year)
                                        ->whereMonth('created_at' , $now->month)
                                        ->whereDay('created_at' , $now->day)->get();

            if (count($checkmail) == 0 && count($checkmailtwo) == 0) {
                $suscriptionData = Suscription::where('active', 1)
                    ->where('user_id', $user_id)
                    ->get();
                if(count($suscriptionData)>0){
                    foreach ($suscriptionData as $key => $data) {
                        $expires = new Carbon($data->expires_at);
                        if ($now > $expires) {
                            $data->active = 0;
                            $data->save();
                        }
                    }
                    $suscriptionConfirm = Suscription::where('active', 1)
                                                ->where('user_id', $user->id)
                                                ->where('expires_at', '>=', $now)
                                                ->get();
                    if(count($suscriptionConfirm) >= 1){
                        $expires = new Carbon($suscriptionConfirm[0]->expires_at);
                        $daysBetween = $now->DiffInDays($expires);
                        if($daysBetween <= 7){
                            SuscriptionHelper::ordercheck($user_id,'renovacion');
                        }
                        switch ($daysBetween) {
                            case 7:
                                SuscriptionHelper::sendmail($user,$daysBetween);
                                
                                break;
                            case 3:
                                SuscriptionHelper::sendmail($user,$daysBetween);
                                break;
                            case 1:
                                SuscriptionHelper::sendmail($user,$daysBetween);
                                break;
                            case 0:
                                SuscriptionHelper::sendmail($user,$daysBetween);
                                break;
                            default:
                                # code...
                                break;
                        }
                    }
                    else{
                        SuscriptionHelper::ordercheck($user_id,'vencido');
                    }
                }
                else{
                    SuscriptionHelper::ordercheck($user_id,'vencido');
                }
            }
        }
        
    }
    public static function ordercheck($user_id,$status)
    {
        $order = Order::where('status', Order::PENDING)
                    ->where('user_id', $user_id)
                    ->first();
        $now = new Carbon();
        $user = User::find($user_id);
        if(!$order){
            $plan = Plan::where('recommended', true)->first();
            $order = new Order([
                "description" => "Renovacion",
                "ammount" => $plan->price,
                "status" => Order::PENDING,
                "date_pay" => $now,
                "points" => 0,
                "user_id" => $user_id,
                "plan_id" => $plan->id
            ]);
            $order->save();
            if ($status=='vencido') {
                SuscriptionHelper::sendmail($user,'vencido');
            }
        }

    }
    public static function sendmail($user,$subject)
    {
        if (SuscriptionHelper::CheckmailClient($user,$subject)==true) {
            if (!is_numeric($subject)) {
                $mainsubject='tu Suscripción ha vencido';
            }
            else{
                if ($subject == 0) {
                    $mainsubject='tu Suscripción vence hoy';
                }
                else{
                    $mainsubject='tu Suscripción está por vencer en '.$subject.' día(s)';
                }
            }
            $data=[
                'user'=>$user,
                'subject'=>$mainsubject

            ];
            Mail::to($user->email)->send(new SuscriptionNotification($data));

            if (!is_numeric($subject)) {
                $checkmail = ClientOperation::where('user_id',$user->id)
                                    ->where('operation',ClientOperation::MAILEXPIRED)->get();
                if (count($checkmail) == 1) {
                    $checkmail->status = true;
                }
                else{
                    $checkmail = new ClientOperation([
                        "operation" => ClientOperation::MAILEXPIRED,
                        "user_id" => $user->id,
                        "status" => true
                    ]);
                }
                $checkmail->save();
            }
            else{
                $operation = new ClientOperation([
                    "operation" => ClientOperation::MAILSUS,
                    "user_id" => $user->id,
                    "status" => true
                ]);
                $operation->save();
            }

            
        }
    }

    public static function Adminchecksuscription()
    {
        $users = User::where('role_id', Role::STUDENT)
                        ->where('status',User::ACTIVE)->get();
        foreach ($users as $key => $user) {
            SuscriptionHelper::CheckSuscription($user->id);
        }
    }

    public static function CheckmailClient($user,$subject)
    {
        $status = true;
        $now = new Carbon();
        if ($subject=='vencido') {
            $checkmail = ClientOperation::where('user_id',$user->id)
                                    ->where('operation',ClientOperation::MAILEXPIRED)->get();
            if (count($checkmail) == 0) {
                $status = true;
            }
            else if($checkmail[0]->status == true){
                $status = false;
            }
        }
        else{
            $checkmail = ClientOperation::where('user_id',$user->id)
                                        ->where('operation',ClientOperation::MAILSUS)
                                        ->where('status',1)
                                        ->whereYear('created_at' , $now->year)
                                        ->whereMonth('created_at' , $now->month)
                                        ->whereDay('created_at' , $now->day)->get();
            
            if (count($checkmail)==1) {
                $status = false;
            }
        }
        return $status;
    }
    public static function CheckOrderClientPay($user_id)
    {
        $clientsuscription = false;
        $now = new Carbon();

        $suscriptions = Suscription::where('active', 1)
                        ->where('user_id', $user_id)
                        ->where('expires_at','>=',$now)
                        ->get();
        if (count($suscriptions) == 2) {
            foreach ($suscriptions as $suscrip) {
                if ($suscrip->order->status == Order::PENDING) {
                    $clientsuscription = true;
                    return $clientsuscription;
                }
            }
            return $clientsuscription;
        }
        else{
            $clientsuscription = true;
            return $clientsuscription;
        }
    }

}
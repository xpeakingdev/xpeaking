<?php

namespace App\Helpers;
use App\User;
use App\UserEvaluation;
use Session;
use Artisan;
use Storage;
use Carbon\Carbon;

class StatisticHelper{
	public static function TestInfo($user_id,$song_id)
	{
		$totaltests = UserEvaluation::where('user_id',$user_id)
                                    ->where('song_id',$song_id)
                                    ->orderBy('updated_at', 'DESC')->get();
        $activedays = 0;
        if (count($totaltests)>0) {
            $day1 = new Carbon($totaltests[0]->updated_at);
            foreach ($totaltests as $test) {
                $day2 = new Carbon($test->updated_at);
                if ($day2->DiffInDays($day1)>0) {
                	$activedays++;
                	$day1 = $day2;
                }
            }
            $expires = new Carbon($suscriptionConfirm[0]->updated_at);
                $daysBetween = $now->DiffInDays($expires);
        }
        $last = UserEvaluation::where('user_id',$user_id)
                            ->where('song_id',$song_id)
        					->latest()->first();
       	if ($last) {
       		$latest['challenge'] = $last->challenge;
	        $latest['range'] = $last->range;
	        $latest['days'] = $activedays;
       	}
       	else{
       		$latest['challenge'] = 0;
	        $latest['range'] = 5;
	        $latest['days'] = 0;
       	}
        
        return $latest;
	}

}
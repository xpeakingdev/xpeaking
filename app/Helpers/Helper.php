<?php

namespace App\Helpers;
use Illuminate\Support\Facades\File;
use App\DictionaryEnglishSpanish;
use App\DictionarySpanishEnglish;
use Aws\Credentials\Credentials;
use Aws\Polly\PollyClient;
use Aws\S3\S3Client;
use Aws\Sts\StsClient;
use App\Sponsorship;
use App\Suscription;
use App\Evaluation;
use App\ClientOperation;
use App\History;
use App\Order;
use App\Plan;
use App\User;
use Session;
use Artisan;
use Storage;

class Helper{

    const CHARACTERS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    public static function Timecomplete($time)
    {
        $miliSecondsString = explode(",", $time);
        $miliSeconds = $miliSecondsString[1];
        $timeString = explode(":", $miliSecondsString[0]);
        $hour = intval($timeString[0]);
        $minutes = intval($timeString[1]);
        $seconds = intval($timeString[2]);
        $totalSeconds = $seconds + ($minutes*60) + ($hour*60*60);
        $timeResult['hour'] = $hour;
        $timeResult['minutes'] = $minutes;
        $timeResult['seconds'] = $seconds;;
        $timeResult['miliseconds'] = $miliSeconds;
        
        return $timeResult;
    }

    public static function formatTime($time)
    {
        $miliSecondsString = explode(",", $time);
        $miliSeconds = $miliSecondsString[1];
        $timeString = explode(":", $miliSecondsString[0]);
        $hour = intval($timeString[0]);
        $minutes = intval($timeString[1]);
        $seconds = intval($timeString[2]);
        $totalSeconds = $seconds + ($minutes*60) + ($hour*60*60);
        $timeResult = $totalSeconds.'.'.$miliSeconds;
        return $timeResult;
    }

    public static function getWords($filename, $sourceLanguage, $targetLanguage){
        $routeFile = 'subtitles/'.$filename;
        $totalWords = [];
        if(Storage::disk('public')->exists($routeFile)){

            $subtitleContent = file_get_contents('storage/'.$routeFile);
            $subtitleData = str_replace("\r", "", $subtitleContent);
            $subtitleParseOne = explode("\n\n", $subtitleData);
    
            // CHARACTERS NOT PERMITED
            // , "'"
            $characters = [",", ";", ".", ":", "(", ")", "¿", "?"];
            
            // GET WORDS
            foreach ($subtitleParseOne as $indexParse => $element) {
                $parseElement = explode("\n", $element);
                if(isset($parseElement[0]) && isset($parseElement[1]) && isset($parseElement[2])){
                    $timeFormat = explode(" --> ", $parseElement[1]);
                    $words = explode(" ", $parseElement[2]);
                    foreach($words as $w){

                        if(strlen(trim($w))>0){
                            // if(in_array($w[0], $characters)) {
                            //     $w = substr($w, 1);
                            // }
        
                            // if(in_array(substr($w, -1), $characters)){
                            //     $w = substr($w, 0, -1);
                            // }
                            $wordTrimed = $w;
                            foreach($characters as $character){
                                $wordTrimed = trim($wordTrimed, $character);
                            }
        
                            // $w = str_replace("-", " ", $w);
        
                            if(!in_array(strtolower($wordTrimed), $totalWords)){
                                array_push($totalWords, strtolower($wordTrimed));
                            }
                        }

                    }
                }
            }

            // SEARCH WORDS EXISTS
            if($sourceLanguage->code=="en" && $targetLanguage->code=="es"){
                $exists = DictionaryEnglishSpanish::whereIn('word', $totalWords)->select("word")->get();
            }
            else if($sourceLanguage->code=="es" && $targetLanguage->code=="en"){
                $exists = DictionarySpanishEnglish::whereIn('word', $totalWords)->select("word")->get();
            }

            // DELETE WORDS EXISTS
            foreach($exists as $exist){
                $indexExist = array_search($exist->word, $totalWords);
                if($indexExist!=-1){
                    unset($totalWords[$indexExist]);
                }
            }
        }
        return $totalWords;
    }

    public static function getFormatSubtitle($filename){
        $routeFile = 'subtitles/'.$filename;
        $lines = [];
        if(Storage::disk('public')->exists($routeFile)){

            $subtitleContent = Storage::get($routeFile);
            $subtitleData = str_replace("\r", "", $subtitleContent);
            $subtitleParseOne = explode("\n\n", $subtitleData);
    
            // CHARACTERS NOT PERMITED
            // , "'"
            $characters = [",", ";", ".", ":", "(", ")", "¿", "?"];

            foreach ($subtitleParseOne as $indexParse => $element) {
                $parseElement = explode("\n", $element);
                if(isset($parseElement[0]) && isset($parseElement[1]) && isset($parseElement[2])){
                    $timeFormat = explode(" --> ", $parseElement[1]);
                    $words = explode(" ", $parseElement[2]);
                    array_push($lines, [
                        "words" => $words,
                        "time" => $timeFormat,
                        "active" => true,
                        "type" => Evaluation::TRANSCRIPT
                    ]);
                }
            }
        }
        return $lines;
    }

    public static function getPhonetics($language){

        $file = null;
        $phonetics = [];
        if($language->code=="en"){
            $file = 'dictionary/en_US_ipa.dsl.txt';
        }
        else if($language->code=="es"){
            $file = 'dictionary/es_ES_ipa.dsl.txt';
        }

        if(Storage::disk('public')->exists($file)){
            $data = Storage::get($file);
            $dataList = explode("\n\n", $data);
            foreach($dataList as $line){
                $dataPhonetics = "";
                $lineArray = explode("\n\t", $line);
                if(count($lineArray)>1){
                    $clearM = str_replace("[m1]", "", $lineArray[1]);
                    $clearM2 = str_replace("[/m]", "", $clearM);
        
                    $listPhonetics = explode(",", $clearM2);
                    
                    foreach($listPhonetics as $linePhonetic){
                        if(strlen($dataPhonetics)>0){
                            $dataPhonetics .= ",";
                        }
                        $clearPOne = str_replace("ˌ", "", $linePhonetic);
                        $clearPhonetic = str_replace("/", "", $clearPOne);
                        $clearPhonetic = str_replace("ˈ", "", $clearPhonetic);
                        // if(chr(substr($clearPhonetic, 0, 1)) == chr("'")){
                        //     $clearPhonetic = substr($clearPhonetic, -2);
                        // }
                        $dataPhonetics .= $clearPhonetic;
                    }

                    $wordLine = $lineArray[0];
                    if(substr($lineArray[0], 0, 1) == "'"){
                        $wordLine = substr($lineArray[0], 1);
                    }
        
                    array_push($phonetics, [
                        "word" => $wordLine,
                        "phonetic" => $dataPhonetics
                    ]);
                }
    
            }
        }

        return $phonetics;
    }

    public static function getAudio($word, $language, $voice){
        $awsAccessKeyId = config('filesystems.disks.s3.key');
        $awsSecretKey   = config('filesystems.disks.s3.secret');
        $clientnew = StsClient::factory(array(
            'profile' => 'default',
            'region' => 'us-east-2',
            'version' => 'latest'
        ));
        // 
        $result= $clientnew->getSessionToken();
        $client_polly = new PollyClient([
            'version' => 'latest',
            'credentials' => [
                'key'    => $result['Credentials']['AccessKeyId'],
                'secret' => $result['Credentials']['SecretAccessKey'],
                'token'  => $result['Credentials']['SessionToken']
            ],
            'region' => 'us-east-2' ]);
        
        $voice = '';
        if($language->code=="en"){
            $voice = 'Matthew';
        }
        else if($language->code=="es"){
            $voice = 'Enrique';
        }

        $result = $client_polly->synthesizeSpeech([
            'OutputFormat' => 'mp3',
            'Text'         => $word,
            'TextType'     => 'text',
            'VoiceId'      => $voice,
        ]);
        
        $resultData     = $result->get('AudioStream')->getContents();

        $saved = Storage::disk('s3')->put($language->code.'/'.$word.'.mp3', $resultData, 'public');

        if($saved){
            return $word.'.mp3';
        }
        else{
            return null;
        }
    }

    public static function getSubtitle($file, $sourceLanguage, $targetLanguage){
        // FORMATED FILE
        //check Storage::get
        $subtitleContent = Storage::get('/subtitles/' . $file);
        $subtitleData = str_replace("\r", "", $subtitleContent);
        $subtitleParseOne = explode("\n\n", $subtitleData);
        $formated = [];

        foreach ($subtitleParseOne as $indexParse => $element) {
            $parseElement = explode("\n", $element);
            if(isset($parseElement[0]) && isset($parseElement[1]) && isset($parseElement[2])){
                $timeFormat = explode(" --> ", $parseElement[1]);
                $words = explode(" ", $parseElement[2]);
                $listWords = [];
                foreach($words as $w){

                    // SEARCH WORD DB
                    $wordDB = null;
                    $phonetic = "";
                    $translation = "";
                    $audio = "";
                    if($sourceLanguage->code=='en' && $targetLanguage->code=='es'){
                        $wordDB = DictionaryEnglishSpanish::where('word', $w)->first();
                        if($wordDB){
                            if($wordDB->phonetics){
                                $listPhonetics = explode(",", $wordDB->phonetics);
                                $phonetic = $listPhonetics[0];
                            }
                            if($wordDB->translation){
                                $listTranslation = explode(",", $wordDB->translation);
                                $translation = $listTranslation[0];
                            }
                            $audio = $wordDB->audio;
                        }
                    }
                    else if($sourceLanguage->code=='es' && $targetLanguage->code=='en'){
                        $wordDB = DictionarySpanishEnglish::where('word', $w)->first();
                        if($wordDB){
                            if($wordDB->phonetics){
                                $listPhonetics = explode(",", $wordDB->phonetics);
                                $phonetic = $listPhonetics[0];
                            }
                            if($wordDB->translation){
                                $listTranslation = explode(",", $wordDB->translation);
                                $translation = $listTranslation[0];
                            }
                            $audio = $wordDB->audio;
                        }
                    }

                    array_push($listWords, [
                        "word" => $w,
                        "translation" => $translation,
                        "phonetic" => $phonetic,
                        "audio" => $audio
                    ]);
                }

                array_push($formated, [
                    "id" => $parseElement[0],
                    "start" => Helper::formatTime($timeFormat[0]),
                    "end" => Helper::formatTime($timeFormat[1]),
                    "content" => $parseElement[2],
                    "words" => $listWords,
                    "show" => false,
                    "showTranslation" => false
                ]);
            }
        }

        return $formated;
    }

    public static function generateToken(){
        return rand(1000, 9999);
    }

    public static function paymentMethods(){
        $paymenyMethod = [
            [
                "name" => "Tarjeta",
                "value"  => Order::CARD
            ],
            [
                "name" => "Puntos",
                "value"  => Order::POINTS
            ],
            [
                "name" => "Transferencia",
                "value" => Order::TRANSFER
            ]
        ];
        return $paymenyMethod;
    }

    public static function sponsorLimit(){
        return 4;
    }

    // LIMIT ADD POINTS REFERENCES
    public static function limitScaleSponsor(){
        return 10;
    }

    public static function pointsForSponsor(){
        return 1;
    }

    public static function addPointsSponsor($userId, $planId, $orderId = null){
        // USER SPONSOR
        // // sponsorUserId
        // USER INVITED
        // // sponsoredUserId
        $start = 1;
        $end = Helper::limitScaleSponsor();

        $user = User::where('id', $userId)->first();
        $plan = Plan::where('id', $planId)->first();

        $user->points += intval($plan->price);
        $user->save();

        $history = new History([
            "description" => History::DESCRIPTIONS_ADD_POINTS_RENEWAL,
            "points" => intval($plan->price),
            "user_id" => $user->id,
            "order_id" => $orderId
        ]);
        $history->save();


        // REGISTER SPONSORSHIP
        $sponsorship = new Sponsorship([
            "sponsor_id" => $sponsorUserId,
            "sponsored_id" => $sponsoredUserId,
            "points" => Helper::pointsForSponsor(),
            "active" => true
        ]);
        $sponsorship->save();

        // REGISTER HISTORY POINTS
        $history = new History([
            "description" => History::DESCRIPTIONS_ADD_POINTS_INVITED,
            "points" => Helper::pointsForSponsor(),
            "user_id" => $sponsorUserId,
            "order_id" => null
        ]);
        $history->save();

        // UPDATE POINTS USER
        $user = User::find($sponsorUserId);
        $user->points += Helper::pointsForSponsor();
        $user->save();
        $sponsoredId = $user->id;

        for ($start; $start <= $end ; $start++) { 
            $sponsorship = Sponsorship::where('sponsored_id', $sponsoredId)->first();
            if($sponsorship){

                $suscriptions = Suscription::where('user_id', $sponsorship->sponsor_id)->where('active', true)->first();
                if($suscriptions){
                    // REGISTER HISTORY POINTS
                    $historySp = new History([
                        "description" => History::DESCRIPTIONS_ADD_POINTS_INVITED,
                        "points" => Helper::pointsForSponsor(),
                        "user_id" => $sponsorship->sponsor_id,
                        "order_id" => null
                    ]);
                    $historySp->save();
    
                    // UPDATE POINTS USER
                    $userSp = User::find($sponsorship->sponsor_id);
                    $userSp->points += Helper::pointsForSponsor();
                    $userSp->save();
                }

                $sponsoredId = $sponsorship->sponsor_id;

            }
            else{
                break;
            }
        }
    }

    public static function storeSponsorship($sponsorId, $sponsoredId, $activeSponsorShip){
        $sponsorship = new Sponsorship([
            "sponsor_id" => $sponsorId,
            "sponsored_id" => $sponsoredId,
            "points" => 0,
            "active" => $activeSponsorShip
        ]);
        $sponsorship->save();
    }
    public static function UpdateSponsorship($sponsoredId, $activeSponsorShip,$points)
    {
        $sponsorship = Sponsorship::where('sponsored_id',$sponsoredId)->first();
        if ($sponsorship) {
            $sponsorship->active = $activeSponsorShip;
            if ($points) {
                $sponsorship->points = $points;
            }
            $sponsorship->save();
        }
        
    }

    public static function UpdateMailExpired($user_id)
    {
        $checkmail = ClientOperation::where('user_id',$user_id)
                                    ->where('operation',ClientOperation::MAILEXPIRED)->first();
        if ($checkmail) {
            $checkmail->status = false;
            $checkmail->save();
        }
    }

    public static function valuePoint(){
        $valuePoint = 1;
        return $valuePoint;
    }

    public static function setPointsSponsors($sponsoredId, $planId, $orderId = null){
        // GET PLAN
        $plan = Plan::find($planId);
        $pointsPlan = Helper::valuePoint() * intval($plan->price);
        $pointsAdd = 1;
        $order = Order::find($orderId);
        // USER RENOVATION
        $registered = true;
        // UPDATE SPONSORSHIP
        $sponsorship = Sponsorship::where([['active',true],['sponsored_id', $sponsoredId]])
                                    ->first();
        if($sponsorship){
            $sponsorship->active = true;
            $sponsorship->points = $pointsAdd;
            // $sponsorship->points = $pointsPlan;
            $sponsorship->save();
            $registered = false;
            // UPDATE POINTS USER
            $user = User::where('id', $sponsoredId)
                        ->first();

            $limitLevels = 10;
            $sponsorId = $sponsorship->sponsor_id;
            $sponsoredLevelId = $sponsorship->sponsored_id;
            for($i = 1; $i <= $limitLevels; $i++){
                if($sponsorId){
                    $sponsor = User::where('id', $sponsorId)
                                    ->first();
                    // $userSponsored = User::find($sponsoredLevelId);
                    $suscription = Suscription::where('active', true)
                                                ->where('user_id', $sponsor->id)
                                                ->first();
                    $sponsorshipUp = Sponsorship::where([['active',true],['sponsored_id', $sponsor->id]])->first();

                    $sponsorId = null;
                    if($sponsorshipUp){
                        $sponsorId = $sponsorshipUp->sponsor_id;
                        // $sponsoredLevelId = $sponsorshipUp->sponsored_id;
                    }
                    
                    if($suscription && $sponsor->status == User::ACTIVE){
                        $sponsor->points += $pointsAdd;
                        $sponsor->points_available += $pointsAdd;
                        $sponsor->save();
    
                        $descriptionUp = $user->username . " - " . $i . "° nivel";
                        $type = History::INVITED;
                        if($i>1){
                            // $descriptionUp = History::DESCRIPTIONS_ADD_POINTS_REFERS . $userSponsored->username;
                            $type = History::REFERRED;
                        }
                        $historyUp = new History([
                            "description" => $descriptionUp,
                            "points" => $pointsAdd,
                            "type" => $type,
                            "user_id" => $sponsor->id
                        ]);
                        $historyUp->save();
                    }
                    // dd($sponsor);
                }
            }
        }
        
        
        
        /*
        if($user && $order->payment_mode != Order::POINTS){
            // $user->points += $pointsPlan;
            // $user->points_available += $pointsPlan;

            // PENDING
            $user->points += $pointsAdd;
            $user->points_available += $pointsAdd;

            $user->save();

            $description = History::DESCRIPTIONS_ADD_POINTS_REGISTER;
            if(!$registered){
                $description = History::DESCRIPTIONS_ADD_POINTS_RENEWAL;
                $history = new History([
                    "description" => $description,
                    "points" => $pointsAdd,
                    // "points" => $pointsPlan,
                    "type" => History::REGISTER,
                    "user_id" => $user->id,
                    "order_id" => $orderId
                ]);
                $history->save();
            }

        }
        */

        // SET POINTS REFERS
        
        
    }

    // SESSIONS
    public static function sessionSet($list){
        foreach($list as $index => $item){
            Session::flash($index, $item);
        }
    }

    public static function getMonthName($monthNumber){
        $months = [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "eptiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ];
        return $months[$monthNumber];
    }
}
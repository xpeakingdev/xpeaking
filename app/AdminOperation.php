<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminOperation extends Model
{
    //
    CONST SUSCRIPCION= 'Verificar Suscripcion';
    protected $table = "admin_operations";
    protected $fillable = [
        'type',
        'status',
        'user_id'
        
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
}

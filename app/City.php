<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $fillable = ['name', 'region_id', 'country_id'];

    // RELATIONS
    public function country(){
        $this->belongsTo(Country::class);
    }

    public function region(){
        return $this->belongsTo(Region::class);
    }
}

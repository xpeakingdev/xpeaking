<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DictionaryEnglishSpanish extends Model
{
    protected $table = 'dictionary_english_spanish';
    protected $fillable = ["word", "translation", "phonetics", "audio"];
}

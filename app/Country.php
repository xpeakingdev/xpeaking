<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $fillable = ['name', 'code', 'picture'];

    // RELATIONS
    public function regions(){
        return $this->hasMany(Region::class)->orderBy('name');
    }

    public function cities(){
        return $this->hasMany(City::class);
    }

    // SCOPES
    public function scopeGetCountries($query){
        $query->orderBy('name')
              ->with('regions', function($q){
                  $q->orderBy('name')
                    ->with('cities', function($qq){
                        $qq->orderBy('name');
                    });
              });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    //Tipo de evaluacion
    CONST NONE = 0;
    CONST TRANSLATE = 1;
    CONST TRANSCRIPT = 2;
    CONST SPEECH = 3;
    CONST COMPLETE = 4;
}

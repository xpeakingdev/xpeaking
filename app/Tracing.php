<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracing extends Model
{
    protected $table = 'tracing';
    protected $fillable = [
        "date",
        "time_start",
        "time_end",
        "user_id",
        "song_id"
    ];

    // RELATIONS
    
}

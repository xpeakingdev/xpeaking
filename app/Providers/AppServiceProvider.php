<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('path.public',function(){
            return'/home/xpeaking/public_html';
        });
    }

    public function boot()
    {
        Schema::defaultStringLength(191);
        if(env('REDIRECT_HTTPS'))
        {
            if(isset($url)){
                $url->forceSchema('https');
            }
        }

    }
}

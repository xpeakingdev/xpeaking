<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestrictedSong extends Model
{
    protected $table = "restricted_song";
    protected $fillable = ["song_id", "student_id"];
}

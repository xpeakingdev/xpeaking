<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $fillable = [
        "word",
        "content",
        "resolved",
        "source_language_id",
        "target_language_id",
        "user_id"
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function source_language()
    {
        return $this->hasOne(Language::class,'id','source_language_id');
    }
    public function target_language()
    {
        return $this->hasOne(Language::class,'id','target_language_id');
    }
}

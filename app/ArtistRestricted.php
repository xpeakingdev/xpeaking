<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistRestricted extends Model
{
    protected $table = "artist_restricted";
    protected $fillable = ["artist_id", "student_id"];
}

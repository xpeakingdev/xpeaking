<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    protected $table = 'songs';
    protected $fillable = [
        "name",
        "slug",
        "cover",
        "release",
        "published",
        "date_published",
        "free",
        "important",
        "verified",
        "order"
    ];

    // RELATIONS
    public function artists(){
        return $this->belongsToMany(Artist::class, 'artist_song');
    }

    public function genders(){
        return $this->belongsToMany(Gender::class, 'gender_song');
    }

    public function languages(){
        return $this->belongsToMany(Language::class, 'language_song');
    }

    public function subtitles(){
        return $this->hasMany(Subtitle::class);
    }

    public function tracing(){
        return belongsToMany(User::class, 'tracing', 'song_id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const SUPERADMIN=1;
    const ADMIN=2;
    const STUDENT=3;
    const LISTROLES=[
        "admin" =>2,
        "student" =>3
    ];

    protected $table = 'roles';
    protected $fillable = ['name', 'description'];
    
    // RELATIONS
    public function users(){
        return $this->hasMany(User::class);
    }
    public function permissions(){

        return $this->belongsToMany('App\Permission','permission_role');
    }
}
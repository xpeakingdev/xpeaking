<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageStudent extends Model
{
    protected $table = 'language_student';
    protected $fillable = ['speak', 'learn', 'principal', 'language_id', 'student_id'];

    
}

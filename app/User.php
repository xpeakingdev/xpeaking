<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Str;

use Auth;

class User extends Authenticatable
{
    use Notifiable;

    const DNI = 1;
    const RUC = 2;

    const TYPEDOCUMENT = [
        "dni" => 1,
        "ruc" => 2
    ];

    CONST ACTIVE = 1;
    CONST LOCKED = 2;
    CONST NOTVERIFIED = 3;

    protected $fillable = [
        'username', 'email', 'password', 'status', 'permission_role', 'role_id', 'points', 'points_available', 'limit_invitations', 'limit_levels'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // RELATIONS
    public function student(){
        return $this->hasOne(Student::Class);
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function suscriptions(){
        return $this->hasMany(Suscription::class);
    }

    public function history(){
        return $this->hasMany(History::class);
    }

    public function reports(){
        return $this->hasMany(Report::class);
    }

    public function tokens(){
        return $this->hasMany(Token::class);
    }

    public function tracing(){
        return $this->belongsToMany(Song::class, 'tracing', 'user_id');
    }
    public function adminoperations(){
        return $this->hasMany(AdminOperation::class);
    }
    public function clientoperations($value='')
    {
        return $this->hasMany(ClientOperation::class);
    }

    // FUNCTIONS

    public static function typesDocuments(){
        return [
            [ "name" => "DNI", "value" => User::DNI ],
            [ "name" => "RUC", "value" => User::RUC ]
        ];
    }

    public function activate($token){
        $tokenUser = ActivationToken::where('token', $token)->first();
        $user = User::find($tokenUser->user_id);
        $user->status = User::ACTIVE;
        $user->save();
        Auth::login($user);
        $tokenUser->delete();
    }

    public function token(){
        return $this->hasOne(ActivationToken::class, 'user_id');
    }

    public function generateToken(){
        $this->token()->create([
            'token' => Str::random(60),
            'user_id' => $this->id
        ]);

        return $this;
    }

    public function generateTokenGlobal(){
        return Str::random(60);
    }
    public function permissions(){

        return $this->belongsToMany('App\Permission','permission_user');
    }
}

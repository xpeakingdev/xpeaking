<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $table = 'dictionaries';
    protected $fillable = ['word', 'student_id', 'source_language_id', 'target_language_id'];

    public function student(){
        return $this->belongsTo(Student::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageSong extends Model
{
    protected $table = 'language_song';
    protected $fillable = ["language_id", "song_id"];
}

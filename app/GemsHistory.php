<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GemsHistory extends Model
{
    protected $table = 'gems_history';
    protected $fillable = ['description', 'gems', 'student_id'];

    CONST DESCRIPTIONS = [
        "POINTS WON"
    ];
    
    public function student(){
        return $this->hasMany(Student::Class);
    }
}

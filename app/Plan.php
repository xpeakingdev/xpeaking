<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{

    CONST CARD = 1;
    CONST POINTS = 2;

    protected $table = 'plans';
    protected $fillable = [
        "name",
        "price",
        "description",
        "months",
        "active"
    ];

    // RELATIONS
    public function services(){
        return $this->belongsToMany(Service::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function suscriptions(){
        return $this->hasMany(Suscription::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientOperation extends Model
{
    //
    CONST MAILSUS= 'Mail de notificación';
    CONST MAILEXPIRED= 'Mail Suscripción Expirada';
    protected $table = "client_operations";
    protected $fillable = [
        'operation',
        'status',
        'user_id'
        
    ];
}

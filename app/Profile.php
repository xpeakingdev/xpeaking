<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = [
        'name',
        'last_name',
        'type_document',
        'document',
        'payname',
        'address',
        'gender',
        'birthdate',
        'sex',
        'user_id',
        'city_id',
        'phone'
    ];

    // RELATIONS
    public function city(){
        return $this->belongsTo(City::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}

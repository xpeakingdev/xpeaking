<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteSong extends Model
{
    protected $table = 'favorite_song';
    protected $fillable = ["student_id", "song_id"];

    public function student(){
        return $this->belongsTo(Student::class);
    }
}

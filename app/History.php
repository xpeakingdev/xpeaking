<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'historys';
    protected $fillable = [
        "description",
        "points",
        "type",
        "user_id",
        "order_id"
    ];

    CONST DESCRIPTIONS_ADD_POINTS_INVITED = 'Puntos por invitado - ';
    CONST DESCRIPTIONS_ADD_POINTS_RENEWAL = 'Puntos por renovación de suscripción';
    CONST DESCRIPTIONS_ADD_POINTS_REGISTER = 'Puntos por registro';
    CONST DESCRIPTIONS_ADD_POINTS_REFERS = 'Puntos por refererido - ';
    CONST DESCRIPTIONS_TRASNFER_TO_POINTS = 'Transferí puntos a ';
    CONST DESCRIPTIONS_TRASNFER_FROM_POINTS = 'Transferencia puntos de ';
    CONST DESCRIPTIONS_PAYMENT_SUSCRIPTION_SPONSOR = 'Pago de suscripción a ';
    CONST DESCRIPTIONS_PAYMENT_MY_SUSCRIPTION = 'Pago de mi suscripción';

    // TYPES
    CONST INVITED = 1;
    CONST REFERRED = 2;
    CONST REGISTER = 3;
    CONST RENEWAL = 4;
    CONST TRANSTO = 5;
    CONST TRANSFROM = 6;
    CONST PAYSPONSOR = 7;

    public function user(){
        return $this->belongsTo(History::class);
    }

    public function order(){
        return $this->belongsTo(Order::class);
    }
}

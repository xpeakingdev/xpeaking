<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $table = 'awards';
    protected $fillable = [
        'description',
        'active',
        'range_id',
        'picture'
    ];

    // RELATIONS
    public function range(){
        return $this->belongsTo(Range::class);
    }
}

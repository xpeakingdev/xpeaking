<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $table = 'genders';
    protected $fillable = ["name"];

    // RELATIONS
    public function artists(){
        return $this->belongsToMany(Artist::class, 'artist_gender');
    }

    public function songs(){
        return $this->belongsToMany(Song::class, 'gender_song');
    }
}

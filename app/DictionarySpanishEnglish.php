<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DictionarySpanishEnglish extends Model
{
    protected $table = 'dictionary_spanish_english';
    protected $fillable = ["word", "translation", "phonetics", "audio"];
}

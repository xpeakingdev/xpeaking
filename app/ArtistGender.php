<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistGender extends Model
{
    protected $table = 'artist_gender';
    protected $fillable = ["artist_id", "gender_id"];
}

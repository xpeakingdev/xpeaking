document.addEventListener('DOMContentLoaded',  function(){

    window.onscroll = function (){
        const listMenu = document.querySelector('.full-data-profile')
        if(listMenu){
            if(listMenu.classList.contains('show')){
                if(window.scrollY >= 1){
                    listMenu.classList.remove('full-data-profile-margin')
                }
                else{
                    listMenu.classList.add('full-data-profile-margin')
                }
            }
        }

        // NAV SEARCH
        const navSearch = document.querySelector('.xp-nav-search')
        if(navSearch){
            if(window.scrollY >= 1){
                navSearch.classList.add('xp-top')
            }
            else{
                navSearch.classList.remove('xp-top')
            }
        }

    }

    const navAuthArrow = document.querySelector('.xp-full-dropbtn-icon')
    if(navAuthArrow){
        navAuthArrow.addEventListener('click', function(){
            const navContentAuth = document.querySelector('.xp-full-dropdown-content')

        })
    }

    /*
    document.addEventListener('wheel', (event)=>{
        console.log(document.body.scoll)
        const listMenu = document.querySelector('.full-data-profile')
        if(listMenu){
            if(listMenu.classList.contains('show')){
                if(event.deltaY >= 1){
                    listMenu.classList.remove('full-data-profile-margin')
                }
                else{
                    listMenu.classList.add('full-data-profile-margin')
                }
            }
        }
    })
    */

   document.addEventListener('wheel', function(event){
        const dashboardNav = document.querySelector('.xp-dashboard-nav')
        if(dashboardNav){
            if(event.deltaY >= 1){
                dashboardNav.classList.add('xp-dashboard-nav-top')
            }
            else{
                dashboardNav.classList.remove('xp-dashboard-nav-top')
            }
        }
   })

    // animacion de scroll

    let screen = document.getElementById('xp-jumbotron-content');
    document.getElementById('btn-scroll').addEventListener("click", function(event){
            event.preventDefault();
            scrollTo(document.documentElement,screen.clientHeight,800)
    });

    function scrollTo(element, to, duration) {
        var start = element.scrollTop,
            change = to - start,
            currentTime = 0,
            increment = 20;
            
        var animateScroll = function(){        
            currentTime += increment;
            var val = Math.easeInOutQuad(currentTime, start, change, duration);
            element.scrollTop = val;
            if(currentTime < duration) {
                setTimeout(animateScroll, increment);
            }
        };
        animateScroll();
    }

    //t = current time
    //b = start value
    //c = change in value
    //d = duration
    Math.easeInOutQuad = function (t, b, c, d) {
      t /= d/2;
        if (t < 1) return c/2*t*t + b;
        t--;
        return -c/2 * (t*(t-2) - 1) + b;
    };
    window.onscroll = function(){
        var scroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (scroll>0) {
            document.getElementById('btn-scroll').classList.add('btn-scroll-show')
        }else{
            document.getElementById('btn-scroll').classList.remove('btn-scroll-show')
        }
    }
//    COLLAPSES
    // const navsCollapse = document.querySelectorAll('.xp-collapse')
    // if(navsCollapse){
    //     navsCollapse.forEach(navColl => {
    //         navColl.addEventListener('click', (e) => {
    //             navMain = null
    //             if(e.target.classList.contains('xp-collapse')){
    //                 navMain = e.target
    //             }
    //             else{
    //                 if(e.target.parentNode){
    //                     if(e.target.parentNode.classList.contains('xp-collapse')){
    //                         navMain = e.target.parentNode
    //                     }
    //                     else{
    //                         if(e.target.parentNode.parentNode){
    //                             if(e.target.parentNode.parentNode.classList.contains('xp-collapse')){
    //                                 navMain = e.target.parentNode.parentNode
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
                
    //             if(navMain){
    //                 let expanded = navMain.getAttribute('aria-expanded')
    //                 if(!expanded){
                        
    //                 }
    //             }
    //         })
    //     });
    // }

})
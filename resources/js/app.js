/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.$ = window.jQuery = require('jquery')

import BootstrapVue from 'bootstrap-vue' //Importing
Vue.use(BootstrapVue);

//support vuex
import Vuex from 'vuex'
Vue.use(Vuex)
import storeData from "./store/index"

const store = new Vuex.Store(
   storeData
)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// GLOBAL
Vue.component('loader', require('./components/global/LoadComponent.vue').default);
Vue.component('loader-section', require('./components/global/LoadSectionComponent.vue').default);
Vue.component('alert', require('./components/global/AlertComponent.vue').default);
Vue.component('loader-complete', require('./components/global/LoadCompleteComponent.vue').default);
Vue.component('loader-icon', require('./components/global/LoadIconComponent.vue').default);

// GLOBAL PARTIALS
Vue.component('partials-nav-auth', require('./components/global/partials/SearchNavComponent.vue').default);
Vue.component('search-nav', require('./components/global/partials/NavAuthComponent.vue').default);
Vue.component('search', require('./components/global/partials/SearchComponent.vue').default);
Vue.component('pagination', require('./components/global/partials/PaginationComponent.vue').default);

// LANDING
Vue.component('free-songs', require('./components/content/songs/FreeSongsComponent.vue').default);

// CONTENT
Vue.component('content-songs-list', require('./components/content/songs/ContentSongsListComponent.vue').default);
Vue.component('content-songs-list-important', require('./components/content/songs/ContentSongListImportantComponent.vue').default);
Vue.component('content-artists-list', require('./components/content/artists/ContentAritstsListComponent.vue').default);
Vue.component('content-artist-info', require('./components/content/artists/ArtistInfoComponent.vue').default);
Vue.component('content-artists-all', require('./components/content/artists/ArtistsAllComponent.vue').default);
Vue.component('content-song-info', require('./components/content/songs/SongInfoComponent.vue').default);
Vue.component('content-songs-all', require('./components/content/songs/SongsAllComponent.vue').default);


// AUTH
Vue.component('login', require('./components/auth/LoginComponent.vue').default);
Vue.component('register', require('./components/auth/RegisterComponent.vue').default);
Vue.component('recover-password', require('./components/auth/RecoverPasswordComponent.vue').default);
Vue.component('change-password', require('./components/auth/ChangePasswordComponent.vue').default);

// USER - PROFILE
Vue.component('user-account', require('./components/user/account/MyAccountComponent.vue').default);
Vue.component('account-achievements', require('./components/user/account/AchievementsComponent.vue').default);
Vue.component('account-profile', require('./components/user/account/ProfileComponent.vue').default);
Vue.component('account-configuration', require('./components/user/account/ConfigurationComponent.vue').default);

// USER AMBASSADOR TEAM
Vue.component('user-ambassador', require('./components/user/ambassador/team/UserAmbassadorComponent.vue').default);
Vue.component('user-ambassador-resume', require('./components/user/ambassador/AmbassadorResumeComponent.vue').default);
Vue.component('user-ambassador-team', require('./components/user/ambassador/team/AmbassadorTeamComponent.vue').default);
Vue.component('user-ambassador-detail', require('./components/user/ambassador/team/AmbassadorDetailComponent.vue').default);
Vue.component('user-ambassador-information', require('./components/user/ambassador/team/AmbassadorInformationComponent.vue').default);

// USER AMBASSADOR POINTS
Vue.component('user-ambassador-points', require('./components/user/ambassador/points/AmbassadorPointsComponent.vue').default);

// USER DICTIONARY
Vue.component('user-dictionary', require('./components/user/dictionary/UserDictionaryComponent.vue').default);

// USER FAVORITES
Vue.component('user-favorites', require('./components/user/favorites/UserFavoritesComponent.vue').default);
Vue.component('user-favorites-artists', require('./components/user/favorites/UserFavoritesArtistsComponent.vue').default);
Vue.component('user-favorites-songs', require('./components/user/favorites/UsersFavoritesSongsComponent.vue').default);

// USER SUSCRIPTION
Vue.component('user-suscription', require('./components/user/suscription/UserSuscriptionComponent.vue').default);
Vue.component('user-my-suscription', require('./components/user/suscription/UsersMySuscriptionComponent.vue').default);
Vue.component('user-suscription-history', require('./components/user/suscription/UserSuscriprionHistoryComponent.vue').default);

// FUNCTIONALITY
Vue.component('xp-player', require('./components/player/XpPlayerComponent.vue').default);
// Vue.component('methodology', require('./components/player/MethodologyComponent.vue').default);

// DASHBOARD
Vue.component('dashboard', require('./components/dashboard/DashboardComponent.vue').default);

// DASHBOARD - FINANCES
Vue.component('finances-pending-pay', require('./components/dashboard/finances/PendingPayComponent').default);
Vue.component('finances-profits', require('./components/dashboard/finances/ProfitsComponent').default);
Vue.component('finances-expenses', require('./components/dashboard/finances/ExpensesComponent').default);

// DASHBOARD - STUDENTS
// PROFILE
Vue.component('students-list', require('./components/dashboard/students/StudentsComponent').default);
Vue.component('students-ranking', require('./components/dashboard/students/RankingComponent').default);
Vue.component('students-admins', require('./components/dashboard/students/AdminsComponent').default);
Vue.component('students-profile', require('./components/dashboard/students/profile/StudentsProfileComponent').default);
Vue.component('students-information', require('./components/dashboard/students/profile/StudentsInformationComponent').default);
Vue.component('students-configuration', require('./components/dashboard/students/profile/StudentsConfigurationComponent').default);

// USER-ADMINS
Vue.component('admins-list', require('./components/dashboard/admins/AdminListComponent').default);
Vue.component('admins-create', require('./components/dashboard/admins/CreateAdminComponent').default);


//REPORTS
Vue.component('students-reports',require('./components/dashboard/students/reports/ReportlistComponent').default);

// TEAM
Vue.component('dashboard-user-ambassador', require('./components/dashboard/students/ambassador/team/UserAmbassadorComponent.vue').default);
Vue.component('dashboard-user-ambassador-resume', require('./components/dashboard/students/ambassador/AmbassadorResumeComponent.vue').default);
Vue.component('dashboard-user-ambassador-team', require('./components/dashboard/students/ambassador/team/AmbassadorTeamComponent.vue').default);
Vue.component('dashboard-user-ambassador-detail', require('./components/dashboard/students/ambassador/team/AmbassadorDetailComponent.vue').default);
Vue.component('dashboard-user-ambassador-information', require('./components/dashboard/students/ambassador/team/AmbassadorInformationComponent.vue').default);
Vue.component('dashboard-user-ambassador-points', require('./components/dashboard/students/ambassador/points/AmbassadorPointsComponent.vue').default);

// TRACING
Vue.component('dashboard-user-tracing', require('./components/dashboard/students/tracing/StudentTracingComponent.vue').default);


// DASHBOARD - EMBASSADORS
Vue.component('students-invitations', require('./components/dashboard/ambassadors/AmbassadorInvitationsComponent').default);

// DASHBOARD - PROFILE
Vue.component('admin-profile', require('./components/dashboard/profile/ProfileComponent').default);
Vue.component('admin-profile-account', require('./components/dashboard/profile/ProfileAccountComponent').default);
Vue.component('admin-profile-configuration', require('./components/dashboard/profile/ProfileConfigurationComponent').default);


// DASHBOARD - SONGS
Vue.component('dashboard-songs', require('./components/dashboard/songs/SongsComponent').default);
Vue.component('dashboard-songs-list', require('./components/dashboard/songs/SongsListComponent').default);
Vue.component('dashboard-songs-create', require('./components/dashboard/songs/SongsCreateComponent').default);
Vue.component('dashboard-songs-preview', require('./components/dashboard/songs/SongPreviewcomponent').default);

// DASHBOARD - SONGS SUBTITLES
Vue.component('dashboard-songs-subtitles', require('./components/dashboard/songs/subtitles/SongSubtitlesComponent').default);
Vue.component('dashboard-songs-subtitles-create', require('./components/dashboard/songs/subtitles/SongSubtitlesCreateComponent').default);
Vue.component('dashboard-songs-subtitles-preview-file', require('./components/dashboard/songs/subtitles/SongSubtitlePreviewFileComponent').default);
Vue.component('dashboard-songs-subtitles-upload-file', require('./components/dashboard/songs/subtitles/SongSubtitleUploadFileComponent').default);
Vue.component('dashboard-songs-subtitles-translate', require('./components/dashboard/songs/subtitles/subtitle_creator/SongSubtitleTranslateComponent').default);

// DASHBOARD - SONGS SUBTITLES EDITOR
Vue.component('dashboard-subtitle-editor', require('./components/dashboard/songs/subtitle_editor/SubtitleMainEditorComponent').default);

// DASHBOARD ARTISTS
Vue.component('dashboard-artists', require('./components/dashboard/artists/ArtistsComponent').default);
Vue.component('dashboard-artists-list', require('./components/dashboard/artists/ArtistsListComponent').default);
Vue.component('dashboard-artists-create', require('./components/dashboard/artists/ArtistsCreateComponent').default);
Vue.component('dashboard-artists-preview', require('./components/dashboard/artists/ArtistsPreviewcomponent').default);

// DASHBOARD GENDERS
Vue.component('dashboard-genders', require('./components/dashboard/genders/GendersComponent').default);
Vue.component('dashboard-genders-list', require('./components/dashboard/genders/GendersListComponent').default);
Vue.component('dashboard-genders-create', require('./components/dashboard/genders/GendersCreateComponent').default);

// DASHBOARD LANGUAGES
Vue.component('dashboard-languages', require('./components/dashboard/languages/LanguagesComponent').default);
Vue.component('dashboard-languages-list', require('./components/dashboard/languages/LanguagesListComponent').default);
Vue.component('dashboard-languages-create', require('./components/dashboard/languages/LanguagesCreateComponent').default);

// DASHBOARD DICTIONARIES
Vue.component('dashboard-dictionaries', require('./components/dashboard/dictionaries/DictionariesComponent').default);
Vue.component('dashboard-dictionaries-list', require('./components/dashboard/dictionaries/DictionariesListComponent').default);
Vue.component('dashboard-dictionaries-create', require('./components/dashboard/dictionaries/DictionariesCreateComponent').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});

<div class="navbar-area {{ $classNavigation }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <nav class="navbar">
                    @auth
                        <a class="navbar-brand" href="{{ route('home') }}">
                            <img id="xp-logo" src="{{ asset('img/logo-color.svg') }}" alt="Logo" class="xp-logo xp-logo-large-auth">
                            <img src="{{ asset('img/logo-icon.svg') }}" alt="Logo" class="xp-logo xp-logo-short-auth">
                        </a>
                    @endauth
                    @guest
                        <a class="navbar-brand" href="{{ route('home') }}">
                            <img id="xp-logo" src="{{ asset('img/logo-color.svg') }}" alt="Logo" class="xp-logo xp-logo-large">
                            <img src="{{ asset('img/logo-color.svg') }}" alt="Logo" class="xp-logo xp-logo-short">
                        </a>
                    @endguest
                    {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarEight" aria-controls="navbarEight" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                    </button> --}}

                    <div class="sub-menu-bar xp-submenu-header {{ Auth::check()?'justify-content-between submenu-space-auth':'justify-content-end submenu-space-notauth' }}" id="navbarEight">

                        {{-- SEARCH --}}
                        @auth
                            <partials-nav-auth></partials-nav-auth>
                        @endauth
                    
                        {{-- END SEARCH --}}
                        
                        <ul class="navbar-nav m-0 ml-0 ml-md-2">
                            @auth
                                @include('partials.navigations.auth')
                            @endauth
    
                            @guest
                                <!-- <li class="nav-item">
                                    <a class="xp-button xp-button-line xp-button-large" href="{{ route('register') }}">
                                        Suscríbete
                                    </a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="xp-button xp-button-primary xp-button-large" href="{{ route('login') }}">
                                        Iniciar Sesión
                                    </a>
                                </li>
                            @endguest
                            
                        </ul>
                    </div>

                </nav> <!-- navbar -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</div> <!-- navbar area -->
<!-- Menu -->
<div class="xp-jumbotron">
    <div class="xp-jumbotron-content" id="xp-jumbotron-content">
        <div class="container xp-jumbotron-body">
            <div class="row content-main-jumbo">
                <div class="col-xs-12 col-lg-6">
                    <div class="col-md-12">
                        <p class="text-center xp-lan-title">Aprende idiomas con canciones</p>
                    </div>
                    <div class="col-md-12">
                        <p class="text-center xp-lan-subtitle">"La forma más fácil, rápida y divertida de aprender un nuevo idioma"</p>
                    </div>
                    <div class="col-md-12 content-btn-promotion mx-auto align-items-center w-100">
                        <a class="btn btn-start-promotion btn-landing w-100 py-2" href="{{ route('register') }}">
                                <p class="xp-button-title">¡Empieza hoy!</p>
                                <p class="xp-button-subtitle">Suscripción: S/ 29.00 al mes</p>
                        </a>
                    </div>
                </div>
                <div class="xplan-video-content-first col-xs-12 col-lg-6">
                    <div class="xplan-video-content">
                        <iframe class="xplan-video-source" src="https://www.youtube.com/embed/wkwR7_pkxA4?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <a class="btn-scroll-container" id="btn-scroll" href="" role="button">
                    <span class="btn-float-scroll" aria-hidden="true"></span>
                </a>
                
            </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript">
    document.addEventListener('DOMContentLoaded', ()=>{
    })
    
    // .height
    // list.scrollTop += heightChild
</script> -->
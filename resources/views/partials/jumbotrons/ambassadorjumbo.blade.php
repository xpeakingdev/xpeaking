<div class="xp-jumbotron">
    <div class="xp-jumbotron-content amba-content-jumbo" id="xp-jumbotron-content">
        <div class="container xp-jumbotron-body">
            <div class="row content-main-jumbo">
                <div class="col-xs-12 col-lg-6">
                    <div class="col-md-12">
                        <p class="text-center xp-amba-title">Conviértete en un Embajador Xpeaking</p>
                    </div>
                    <div class="col-md-12">
                        <p class="text-center xp-lan-subtitle" style="color:white;">¡Recomienda Xpeaking a tus amigos!<br>Acumula puntos por cada recomendación y canjéalos por increíbles premios.</p>
                    </div>
                    <div class="col-md-12 content-btn-promotion mx-auto align-items-center w-100">
                        <a class="btn btn-start-amba btn-landing w-100 py-3" href="{{ route('register') }}">
                                <p class="xp-button-title-amba">¡Únete hoy!</p>
                        </a>
                    </div>
                </div>
                <div class="xplan-video-content-first col-xs-12 col-lg-6">
                    <div class="xplan-video-content">
                        <iframe class="xplan-video-source" src="https://www.youtube.com/embed/imqPu7i0WnQ?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <a class="btn-scroll-container" id="btn-scroll" href="" role="button">
                    <span class="btn-float-scroll" aria-hidden="true"></span>
                </a>
                
            </div>
        </div>
    </div>
    <div class="rest-jumbo-amba">
        
    </div>
</div>
<!-- <script type="text/javascript">
    document.addEventListener('DOMContentLoaded', ()=>{
    })
    
    // .height
    // list.scrollTop += heightChild
</script> -->
<div class="xp-jumbotron">
    <div class="xp-jumbotron-content">
        <div class="container xp-jumbotron-body">
            <div class="row m-0 w-100 d-flex justify-content-start align-items-center xp-jumbotron-body-item mb-3">
                <div class="col-12 p-0 d-flex flex-column align-items-center">
                    <h3 class="text-white text-left xp-title">APRENDE IDIOMAS CON CANCIONES</h3>
                    <p class="text-white text-left xp-subtitle mt-4">
                        La forma más fácil, rápida y divertida de aprender idiomas.
                    </p>
                    <p class="text-white text-left xp-subtitle mt-1">
                        Aprende a tu propio ritmo, desde cualquier lugar y dispositivo.
                    </p>
                    <div class="w-100 d-flex justify-content-center mt-4">
                        <a class="xp-button xp-button-border-primary xp-button-large xp-text-white" href="#home">
                            Ver video
                        </a>
                        <a class="xp-button xp-button-primary ml-5 xp-button-large" href="{{ route('register') }}">
                            Empieza hoy
                        </a>
                    </div>
                </div>
            </div>
            <div class="row m-0 w-100 d-flex justify-content-center align-items-center xp-jumbotron-body-item mt-5">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="xp-card-sponsors">
                        <span class="xp-card-sponsors-text">
                            Contamos con el respaldo de:
                        </span>
                        <div class="xp-card-sponsors-content mt-3">
                            <div class="xp-card-sponsors-item mr-4">
                                <img src="{{ asset('img/openf.png') }}" alt="">
                            </div>
                            <div class="xp-card-sponsors-item">
                                <img src="{{ asset('img/cci.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- bd-example -->
<div class="preloader">
    <div class="xp-loader xp-loader-music" v-bind:class="{ 'd-none': !show}">
        <div class="xp-loader-music-content">
            <div class="xp-loader-music-bar"></div>
            <div class="xp-loader-music-bar"></div>
            <div class="xp-loader-music-bar"></div>
        </div>
    </div>
</div>
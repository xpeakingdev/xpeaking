    <div class="footer-widget xp-footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6 px-md-4">
                    <div class="footer-link">
                        <div class="d-flex pr-3 pr-md-5 pl-3 pl-md-0">
                            <img class="image-footer" src="{{ asset('img/logo-white.png') }}" alt="">
                        </div>
                        <ul>
                            <li class=""><a href="#" class="pb-2">Aprende idiomas con canciones</a></li>
                        </ul>
                    </div> <!-- footer link -->
                </div>
                <div class="col-lg-3 col-sm-6 px-md-4">
                    <div class="footer-link">
                        <h6 class="footer-title">CONTÁCTANOS</h6>
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="far fa-envelope mr-2"></i> hola@xpeaking.com
                                </a>
                            </li>
                            <li>
                                <a href="https://wa.me/51939992192" target="_blank">
                                    <i class="fab fa-whatsapp mr-2"></i> +51 939 992 192
                                </a>
                            </li>
                        </ul>
                    </div> <!-- footer link -->
                </div>
                <div class="col-lg-3 col-sm-6 px-md-4">
                    <div class="footer-link">
                        <h6 class="footer-title">LEGALIDAD</h6>
                        <ul>
                            <li><a href="{{ route('terms') }}">Términos de uso</a></li>
                            <li><a href="{{ route('privacy') }}">Política de Privacidad </a></li>
                            <li><a href="{{ route('agreement') }}">Acuerdo con Embajadores</a></li>
                            <li><a href="{{ route('ambassador') }}">Embajador Xpeaking</a></li>
                        </ul>
                    </div> <!-- footer link -->
                </div>
                <div class="col-lg-3 col-sm-6 px-md-4">
                    <div class="footer-link">
                        <h6 class="footer-title">REDES SOCIALES</h6>
                        <div class="icon-footer-content mt-2 d-flex">
                            <a href="https://wa.me/51939992192" class="xp-social-link ml-0" target="_blank">
                                <img class="icon-footer" src="{{ asset('img/whatsapp.svg') }}" alt="Whatsapp">
                            </a>
                            <a href="https://www.facebook.com/Xpeaking-2048702118582496" class="xp-social-link" target="_blank">
                                <img class="icon-footer" src="{{ asset('img/facebook.svg') }}" alt="Facebook">
                            </a>
                            <a href="https://www.instagram.com/xpeakingperu" class="xp-social-link" target="_blank">
                                <img class="icon-footer" src="{{ asset('img/instagram.svg') }}" alt="Instragram">
                            </a>
                            <a href="https://www.youtube.com/channel/UCUZ1o67dzDqvvD-vokcRA9Q/featured?view_as=subscriber" class="xp-social-link" target="_blank">
                                <img class="icon-footer" src="{{ asset('img/youtube.svg') }}" alt="Youtube">
                            </a>
                        </div>
                        <!-- <div class="d-flex px-2 mt-4">
                            <a class="xp-button xp-button-primary xp-button-large" href="{{ route('login') }}">
                                verificar certificado
                            </a>
                        </div> -->
                    </div> <!-- footer link -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- footer widget -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header md-ambassador-landing">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        </div>
      </div>
      
    </div>
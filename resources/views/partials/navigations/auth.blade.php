{{-- <li class="nav-item">
    <partials-nav-auth></partials-nav-auth>
</li> --}}
<li class="nav-item">
    <a class="xp-link-icon" href="{{ route('login') }}">
        <i class="fas fa-trophy"></i>
    </a>
</li>
<li class="nav-item">
    <a class="xp-link-icon" href="{{ route('login') }}">
        <i class="fas fa-user-friends"></i>
    </a>
</li>
<li class="nav-item">
    <a class="xp-link-icon" href="{{ route('login') }}">
        <i class="fas fa-bell"></i>
    </a>
</li>
<li class="nav-item navItemDropdwonAuth ml-2">
    <b-dropdown size="lg"  variant="link" toggle-class="text-decoration-none" no-caret>
        <template v-slot:button-content class="p-0">
            
            <div class="xp-user-data">
                <span class="xp-user-data-name d-none d-md-flex">
                    {{ auth()->user()->username }}
                </span>
                <a class="btn-profile xp-collapse xp-collapse-auth p-0 m-0">
                    <span class="xp-user-picture ml-2">
                        @if(auth()->user()->picture)
                            <img src="{{ url('usuario/foto/' . auth()->user()->picture) }}" alt="" class="xp-user-data-avatar xp-picture-set">
                        @else
                            <img src="{{ asset('img/user.svg') }}" alt="" class="xp-user-data-avatar xp-picture-set">
                        @endif
                    </span>
                    <span class="xp-full-dropbtn-icon ml-2" id="xp-arrow-auth-nav"><i class="fas fa-chevron-down"></i></span>
                </a>        
            </div>
            
        </template>
        <b-dropdown-group>
                <div class="xp-collapse-auth-content">
                    <ul class="xp-ds-nav-sub">
                        <li class="xp-ds-nav-sub-li">
                            <ul class="xp-menu-profile">
                                @include('partials.navigations.'.auth()->user()->role->name)
            
                                <li class="xp-menu-profile-item">
                                    <a href="{{ route('logout') }}" class="xp-menu-profile-link">
                                        <img src="{{ asset('img/off.svg') }}" alt="">
                                        Cerrar Sesión
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
        </b-dropdown-group>
    </b-dropdown>

</li>
{{-- 
<li class="nav-item xp-full-dropdown ml-0 ml-md-3">
    <div class="xp-user-data">
        <span class="xp-user-data-name d-none d-md-flex">
            {{ auth()->user()->username }}
        </span>
        <a class="btn-profile xp-collapse xp-collapse-auth xp-full-dropbtn">
            <span class="xp-user-picture ml-2">
                @if(auth()->user()->picture)
                    <img src="{{ url('usuario/foto/' . auth()->user()->picture) }}" alt="" class="xp-user-data-avatar">
                @else
                    <i class="fas fa-user"></i>
                @endif
            </span>
            <span class="xp-full-dropbtn-icon ml-2" id="xp-arrow-auth-nav"><i class="fas fa-chevron-down"></i></span>
        </a>
        <div class="xp-collapse-auth-content xp-full-dropdown-content">
            <ul class="xp-ds-nav-sub">
                <li class="xp-ds-nav-sub-li">
                    <ul class="xp-menu-profile">
                        @include('partials.navigations.'.auth()->user()->role->name)
    
                        <li class="xp-menu-profile-item">
                            <a href="{{ route('logout') }}" class="xp-menu-profile-link">
                                <img src="{{ asset('img/off.svg') }}" alt="">
                                Cerrar Sesión
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <a class="xp-link dropdown-toggle xp-dropdown-link px-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="lni-chevron-down"></i>
    </a>
    <div class="dropdown-menu drop-down-user" aria-labelledby="navbarDropdown">
        <a class="xp-link py-0 my-2 dropdown-item xp-text-default" href="#">
            <i class="lni-user mr-2"></i> Mi Perfil
        </a>
      <div class="dropdown-divider"></div>
        <a class="xp-link py-0 my-2 dropdown-item xp-text-default" href="#">
            <i class="lni-power-switch mr-2"></i> Cerrar session
        </a>
    </div>
</li> --}}
<li class="xp-menu-profile-item">
    <a href="{{ route('dashboard.admin.profile') }}" class="xp-menu-profile-link">
        <img src="{{ asset('img/account.svg') }}" alt="">
        Mi Perfil
    </a>
</li>
<li class="xp-menu-profile-item">
    <a href="{{ route('dashboard') }}" class="xp-menu-profile-link">
        <img src="{{ asset('img/dashboard.svg') }}" alt="">
        Panel de control
    </a>
</li>
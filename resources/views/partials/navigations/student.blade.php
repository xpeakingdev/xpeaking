
    <li class="xp-menu-profile-item">
        <a href="{{ route('profile') }}" class="xp-menu-profile-link">
                @if(auth()->user()->picture)
                    <img class="xp-picture-set" src="{{ url('usuario/foto/' . auth()->user()->picture) }}" alt="">
                @else
                    <img class="xp-picture-set" src="{{ asset('img/user.svg') }}" alt="">
                @endif
            Mi perfil
        </a>
    </li>
    {{-- <li class="xp-menu-profile-item">
        <a href="#" class="xp-menu-profile-link">
            <img src="{{ asset('img/medal.svg') }}" alt="">
            Mi curso
        </a>
    </li> --}}
    <li class="xp-menu-profile-item">
        <a href="{{ route('student.favorites') }}" class="xp-menu-profile-link">
            <img src="{{ asset('img/heart.svg') }}" alt="">
            Mis favoritos
        </a>
    </li>
    <li class="xp-menu-profile-item">
        <a href="{{ route('student.suscription') }}" class="xp-menu-profile-link">
            <img src="{{ asset('img/crown.svg') }}" alt="">
            Suscripcion
        </a>
    </li>
    <li class="xp-menu-profile-item">
        <a href="{{ route('student.dictionary') }}" class="xp-menu-profile-link">
            <img src="{{ asset('img/dictionary.svg') }}" alt="">
            Diccionario
        </a>
    </li>
    <li class="xp-menu-profile-item">
        <a href="{{ route('student.ambassador') }}" class="xp-menu-profile-link">
            <img src="{{ asset('img/start.svg') }}" alt="">
            Embajador
        </a>
    </li>
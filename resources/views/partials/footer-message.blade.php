<div class="footer-copyright xp-footer-copyright">
    <div class="container">
        <div class="row align-items-center">
           <div class="col-12">
               <p class="text xp-footer-copyright-text text-white d-flex align-items-center justify-content-center">
                   Hecho con <i class="fas fa-heart mx-2 xp-text-primary"></i> para ti
               </p>
           </div>
        </div> <!-- row -->
    </div> <!-- container -->
</div> <!-- footer copyright -->
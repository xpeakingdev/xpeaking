@extends('layouts.landing', [
                           'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => false,
                            'showHeader' => false
                        ])

@section('content')
    <div class="xp-abort">
        <div class="xp-abort-content">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8 mx-auto">
                        <div class="xp-card-abort">
                            <div class="card-abort-head">
                                <img src="{{ asset('/img/404.svg') }}" alt="">
                            </div>
                            <div class="card-abort-body">
                                <p class="card-abort-text">
                                    No encontramos lo que buscabas, pero puedes intentarlo aquí:
                                </p>
                            </div>
                            <div class="card-abort-footer">
                                <a href="{{ route('home') }}">
                                    <img src="{{ asset('/img/logo-white.svg') }}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
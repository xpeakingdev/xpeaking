@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
<link href="{{ asset('css/vue-multiselect.min.css') }}" rel="stylesheet" />
{{-- full-data-profile-margin --}}
    <dashboard></dashboard>
@endsection
@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt d-none d-md-flex',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height d-none d-md-flex',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
{{-- full-data-profile-margin --}}
<div class="full-data-profile full-player ">
    <div class="container px-0 px-md-2">
        <div class="row m-0 height-player">
            <div class="col-12 mx-auto height-player px-0">
                <dashboard-subtitle-editor :data-init="{{ json_encode($data) }}"></dashboard-subtitle-editor>
            </div>
        </div>
    </div>
</div>
@endsection
<script>

document.addEventListener('DOMContentLoaded', ()=>{
    const player = new Plyr('#player');
})

</script>
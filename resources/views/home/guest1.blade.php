@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'jumbotron xp-jumbotron',
                            'jumbotron' => true,
                            'footer' => true,
                            'social' => true,
                            'footerMessage' => true,
                            'backgroundHeader' => true,
                            'showHeader' => true
                        ])

@section('jumbotron')
    @include('partials.jumbotrons.jumbotron')
@endsection

@section('content')
<!--====== ABOUT PART START ======-->

    <section id="about" class="xp-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-9">
                    <div class="section-title text-center">
                        <h4 class="title xp-text-primary wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.6s">
                            ¡Acelera tu aprendizaje con Xpeaking! 
                        </h4>
                    </div> <!-- section title -->

                </div>
                <div class="col-12 col-md-10 col-lg-7">
                    <div class="section-title text-center pb-40">
                        <p class="text wow fadeInUp mt-4" data-wow-duration="1.5s" data-wow-delay="1s">
                            Si quieres aprender un idioma desde cero o reforzar lo que aprendes en el colegio, academia o instituto ¡Xpeking es la herramienta ideal para ti! 
                            <span class="font-weight-bold">
                                Sin profesores, sin libros, sin horarios, sin desplazamientos, sin mensualidades caras. 
                            </span>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-10 col-lg-9">
                    <div class="section-title text-center">
                        <h4 class="title xp-text-primary wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.6s">
                            ¿Por qué aprender idiomas con música?
                        </h4>
                    </div> <!-- section title -->

                </div>
                <div class="col-12 col-md-10 col-lg-5">
                    <div class="section-title text-center pb-40">
                        <p class="text wow fadeInUp mt-4" data-wow-duration="1.5s" data-wow-delay="1s">
                            ...Porque está comprobado científicamente que la música es 64% más efectivo que cualquier otro método de aprendizaje.
                            Además, nos brinda los siguientes beneficios:
                        </p>
                    </div>
                </div>
            </div> <!-- row -->
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-about xp-single-about d-sm-flex mt-10 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                        <div class="about-icon xp-icon-image-vertical">
                            <img src="{{ asset('img/facil.svg') }}" alt="Icon">
                        </div>
                        <div class="about-content media-body xp-single-content">
                            <h4 class="about-title">Fácil de aprender</h4>
                            <p class="text">
                                ¡La música es repetitiva! Tiene la extraña habilidad de quedarse atrapada en nuestras mentes, y sin darnos cuenta aprendernos canciones enteras con facilidad.
                            </p>
                        </div>
                    </div> <!-- single about -->
                </div>
                <div class="col-lg-6">
                    <div class="single-about xp-single-about d-sm-flex mt-10 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.4s">
                        <div class="about-icon xp-icon-image-vertical">
                            <img src="{{ asset('img/aceleratuaprendizaje.svg') }}" alt="Icon">
                        </div>
                        <div class="about-content media-body xp-single-content">
                            <h4 class="about-title">Acelera tu aprendizaje</h4>
                            <p class="text">
                                La música estimula el aprendizaje y la memorización de forma natural, afina tu oído y mejora tu pronunciación de forma acelerada. Nos permite ampliar el vocabulario, aprender frases y expresiones de la vida real.
                            </p>
                        </div>
                    </div> <!-- single about -->
                </div>
                <div class="col-lg-6">
                    <div class="single-about xp-single-about d-sm-flex mt-10 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.6s">
                        <div class="about-icon xp-icon-image-vertical">
                            <img src="{{ asset('img/motivacion.svg') }}" alt="Icon">
                        </div>
                        <div class="about-content media-body xp-single-content">
                            <h4 class="about-title">Aumenta la motivación</h4>
                            <p class="text">
                                ¡La música es divertida! Fomenta la autoconfianza y motivación para hablar un nuevo idioma desde el 1er día, independientemente de tu edad y talento. 
                            </p>
                        </div>
                    </div> <!-- single about -->
                </div>
                <div class="col-lg-6">
                    <div class="single-about xp-single-about d-sm-flex mt-10 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.8s">
                        <div class="about-icon xp-icon-image-vertical">
                            <img src="{{ asset('img/flexibilidad.svg') }}" alt="Icon">
                        </div>
                        <div class="about-content media-body xp-single-content">
                            <h4 class="about-title">Flexibilidad</h4>
                            <p class="text">
                                Escuchar diferentes acentos y entonaciones proporciona mayor flexibilidad a nuestro cerebro para adaptarse y reconocer los diferentes sonidos de otro idioma.
                            </p>
                        </div>
                    </div> <!-- single about -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== ABOUT PART ENDS ======-->
    
    <!--====== portfolio PART START ======-->

    <section id="portfolio" class="xp-section xp-bg-black">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center">
                        <h3 class="title xp-text-primary">SELECCIONA UNA CANCIÓN PARA APRENDER INGLÉS</h3>
                    </div> <!-- row -->
                </div>
            </div>
            <div class="">
               
                <div class="d-block mx-auto mt-4">
                    {{-- CAROUSEL SPNGS --}}
                    <content-songs-list :free="{{ json_encode(true) }}" ></content-songs-list>
                    {{-- <div class="xp-slider-card-content">
                        <div class="slider xp-slider-card" id="xp-slider-card-songs-guest">
                            @foreach ($songs as $song)
                                <a class="xp-card-single">
                                    <div class="xp-card-single-header">
                                        <img src="{{ asset('storage/songs/'.$song->cover) }}" alt="">
                                        <div class="xp-card-single-header-alt">

                                        </div>
                                    </div>
                                    <div class="xp-card-single-body">
                                        <div class="xp-card-single-body-title">
                                            <h5 class="">{{ $song->name}}</h5>
                                        </div>
                                        <div class="xp-card-single-body-artist text-white">
                                            @php
                                                $nameArtists = '';
                                                foreach ($song->artists as $artistSong) {
                                                    if(strlen($nameArtists)>0){
                                                        $nameArtists .= ', ';
                                                    }
                                                    $nameArtists .= $artistSong->stage_name;
                                                }
                                            @endphp
                                            <span>{{ $nameArtists }}</span>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <div class="xp-card-single-button prev" id="xp-slider-card-prev-guest">
                            <i class="fas fa-chevron-left"></i>
                        </div>
                        <div class="xp-card-single-button next" id="xp-slider-card-next-guest">
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </div> --}}
                        {{-- <button class="next">NEXT</button> --}}
                    {{-- END CAROUSEL --}}
                </div>

            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== portfolio PART ENDS ======-->
    
    <!--====== PRINICNG STYLE EIGHT START ======-->

    <section id="pricing" class="xp-section xp-bg-primary">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-6">
                    <div class="section-title text-center">
                        <h3 class="title text-white">Plan de Suscripción</h3>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
            <div class="row justify-content-center">
                <div class=" col-md-6 col-lg-4">
                    <div class="section-title text-center mt-2">
                        <p class="text mb-0 mt-0 text-white">
                            Aprende idiomas con tus canciones favoritas
                            (Por el momento sólo inglés está disponible)
                        </p>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
            <div class="row justify-content-center">                
                <div class="col-lg-4 col-md-7 col-sm-9">
                    <div class="pricing-style-one xp-card-pricing mt-30 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <div class="pricing-icon text-center">
                            <img src="{{ asset('img/pricing.svg') }}" alt="">
                        </div>
                        <div class="pricing-header text-center">
                            {{-- <h5 class="sub-title">Basic</h5> --}}
                            <p class="month xp-card-pricing-mount xp-text-primary">
                                <span class="mount-currency xp-text-primary mx-2">
                                    S/  29  <small class="range-currency">/ mes</small>
                                </span>
                            </p>
                        </div>
                        <div class="pricing-list">
                            <ul class="xp-list-options">
                                <li class="xp-list-options-item">
                                    <img class="xp-list-options-icon xp-icon mr-2" src="{{ asset('img/check.svg') }}" alt="">
                                    <span class="xp-list-options-text">
                                        Acceso ilimitado a todo nuestro catálogo de canciones.
                                    </span>
                                </li>
                                {{-- <li class="xp-list-options-item">
                                    <img class="xp-list-options-icon xp-icon mr-2" src="{{ asset('img/check.svg') }}" alt="">
                                    <span class="xp-list-options-text">
                                        Acceso a nuestro Programa de aprendizaje acelerado de idiomas.
                                    </span>
                                </li> --}}
                                <li class="xp-list-options-item">
                                    <img class="xp-list-options-icon xp-icon mr-2" src="{{ asset('img/check.svg') }}" alt="">
                                    <span class="xp-list-options-text">
                                        Acceso 24/7, desde cualquier lugar y dispositivo.
                                    </span>
                                </li>
                                <li class="xp-list-options-item">
                                    <img class="xp-list-options-icon xp-icon mr-2" src="{{ asset('img/check.svg') }}" alt="">
                                    <span class="xp-list-options-text">
                                        Aprende idiomas sin anuncios.
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="pricing-btn rounded-buttons text-center mt-4">
                            <a class="xp-button xp-button-primary xp-button-large" href="{{ route('register') }}">
                                Empieza hoy
                            </a>
                        </div>
                    </div> <!-- pricing style one -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== PRINICNG STYLE EIGHT ENDS ======-->
    
    <!--====== TESTIMONIAL THREE PART START ======-->

    {{-- <section id="testimonial" class="testimonial-area xp-section bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-20">
                        <h3 class="title xp-text-primary">Testimonial</h3>
                    </div> <!-- section title -->
                </div>
            </div> <!-- row -->
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="row testimonial-active">
                        <div class="px-2">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img src="{{ asset('img/circle.svg') }}" alt="Author">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 class="author-name">Isabela Moreira</h6>
                                    <span class="sub-title">CEO, GrayGrids</span>
                                </div>
                            </div> <!-- single testimonial -->
                        </div>
                        <div class="px-2">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img src="{{ asset('img/circle.svg') }}" alt="Author">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 class="author-name">Fiona</h6>
                                    <span class="sub-title">Lead Designer, UIdeck</span>
                                </div>
                            </div> <!-- single testimonial -->
                        </div>
                        <div class="px-2">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img src="{{ asset('img/circle.svg') }}" alt="Author">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 class="author-name">Elon Musk</h6>
                                    <span class="sub-title">CEO, SpaceX</span>
                                </div>
                            </div> <!-- single testimonial -->
                        </div>
                        <div class="px-2">
                            <div class="single-testimonial mt-30 mb-30 text-center">
                                <div class="testimonial-image">
                                    <img src="{{ asset('img/circle.svg') }}" alt="Author">
                                </div>
                                <div class="testimonial-content">
                                    <p class="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 class="author-name">Fajar Siddiq</h6>
                                    <span class="sub-title">CEO, MakerFlix</span>
                                </div>
                            </div> <!-- single testimonial -->
                        </div>
                    </div> <!-- row -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section> --}}

    <!--====== TESTIMONIAL THREE PART ENDS ======-->
    
    <!--====== CLIENT LOGO PART START ======-->

    <section id="client" class="client-logo-area xp-section bg-white">
        <div class="container">
            <div class="row flex-column justify-content-center align-items-center">
                <div class="col-12 col-md-7 col-lg-7">
                    <div class="call-action-content">
                        <h3 class="action-title text-center subtitle xp-text-primary">Acelera tu aprendizaje sin salir de casa, Aprende a tu propio ritmo y sin límites </h3>
                        {{-- <p class="text">We never spam your email</p> --}}
                    </div> <!-- call action content -->
                </div>
                <div class="col-12">
                    <div class="call-action-content mt-45 d-flex justify-content-center">
                        <a class="xp-button xp-button-jade xp-text-dark xp-button-large xp-font-weight-semibold" href="{{ route('register') }}">
                            Empieza hoy
                        </a>
                    </div> <!-- call action content -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>

    <!--====== CLIENT LOGO PART ENDS ======-->
@endsection


@section('scripts')

<script>
    document.addEventListener('DOMContentLoaded', ()=>{
        // $('.slider-for').slick({
        //     slidesToShow: 1,
        //    slidesToScroll: 1,
        //    arrows: false,
        //    fade: true,
        //    asNavFor: '.slider-nav'
        // });

        $('#xp-slider-card-songs-guest').slick({
           slidesToShow: 5,
           slidesToScroll: 1,
           dots: false,
           focusOnSelect: true,
           dotsClass: 'slick-dots slick-thumb',
           nextArrow: $('#xp-slider-card-next-guest'),
           prevArrow: $('#xp-slider-card-prev-guest'),
           customPaging: function(Slide, i) {
            //  var ii = i + 1;
            // var datad = $('.slider-for .cl'+ii).data('dot');

            //     return '<a>'+datad+'</a>'
              },
        });
    })
</script>

@endsection
@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'jumbotron xp-jumbotron',
                            'jumbotron' => true,
                            'footer' => true,
                            'social' => true,
                            'footerMessage' => true,
                            'backgroundHeader' => true,
                            'showHeader' => true
                        ])

@section('jumbotron')
    @include('partials.jumbotrons.ambassadorjumbo')
@endsection

@section('content')
<div class="xplan-container" id="xplan-container">
    <!-- Parte por que elegir xpeaking -->
    <div class="xpamba-choose">
        <div class="xplan align-items-center wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.3s">
            <div class="col-md-12">
                <p class="text-center xp-lan-choose-title">¿Cómo convertirme en Embajador?</p>
            </div>
            <div class="w-75 mx-auto">
                <p class="text-center xp-lan-choose-subtitle">
                    Conviértete en Embajador Xpeaking en 3 simples pasos
                </p>
            </div>
        </div>
        <div class="xplan w-75 mx-auto">
            <div class="xplan row pb-4 wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.3s">
                <div class="col-md-4 my-4">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="xp-icon-image-vertical">
                                <img src="{{ asset('img/regístrate.svg') }}" alt="Icon">
                            </div>  
                        </div>
                        <div class="col-12">
                            <p class="text-center xp-lan-title-choose">Regístrate</p>
                            <p class="xp-amba-choose-subtitle text-justify">Sólo tienes que pagar tu Suscripción mensual en Xpeaking: S/ 29.00. No necesitas realizar pagos adicionales.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 my-4">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="about-icon xp-icon-image-vertical">
                                <img src="{{ asset('img/recomienda.svg') }}" alt="Icon">
                            </div>
                        </div>
                        <div class="col-12">
                            <p class="text-center xp-lan-title-choose">Recomienda</p>
                            <p class="xp-amba-choose-subtitle text-justify">¡Tu whatsapp será tu mejor aliado!<br> Invita a tus amigos a descubrir una nueva forma de aprender idiomas en Xpeaking.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 my-4">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="mx-auto xp-icon-image-vertical">
                                <img src="{{ asset('img/gana.svg') }}" alt="Icon">
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <p class="text-center xp-lan-title-choose">Gana</p>
                            <p class="xp-amba-choose-subtitle text-justify">Gana 1 punto por recomendación hasta el 10° nivel. Luego, canjea tus puntos por increíbles premios.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div  class="my-4 btn-amba-cont">
            <div>
                <div class="align-items-center mt-4">
                    <div class="start-promotion-content mx-auto">
                        <a href="" class="btn btn-start-promotion btn-landing w-100 py-1">
                                <p class="xp-button-title">¡Descargar catálogo de premios!</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Inspire -->
    <div class="section-inspire">
        <div class="mt-4">
            <div class="row m-0 w-100 d-flex justify-content-center align-items-center xp-jumbotron-body-item">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="xp-card-sponsors">
                        <span class="xp-lan-card-sponsors-text">
                            Contamos con el respaldo de:
                        </span>
                        <div class="xp-card-sponsors-content mt-3">
                            <div class="xp-card-sponsors-item mr-4">
                                <img src="{{ asset('img/openf.png') }}" alt="">
                            </div>
                            <div class="xp-card-sponsors-item">
                                <img src="{{ asset('img/cci.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- AMBASSADOR -->
    <div class="xplan-ambassador-content">
        <div class="ambassador-content mx-auto row">
            <div class="ambassador-content-first col-md-8 mx-auto">
                <div class="">
                    <p class="text-center xp-amba-subtitle">
                        "Juntos podemos lograr que más personas tengan la oportunidad de aprender idiomas de forma fácil, rápida y divertida"
                    </p>
                    <br>
                    <p class="text-center xp-amba-subtitle my-3">
                        ¿Te atreves?
                    </p>

                </div>
                <div class="">
                    
                    <div class="btn-ambassador-content col-lg-6 mx-auto">
                        <a class="btn btn-start-amba btn-landing w-100 py-3" href="{{ route('register') }}">
                                <p class="xp-button-title-amba">¡Únete hoy!</p>
                        </a>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
</div>
@endsection
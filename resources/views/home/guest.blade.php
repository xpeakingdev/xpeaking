@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'jumbotron xp-jumbotron',
                            'jumbotron' => true,
                            'footer' => true,
                            'social' => true,
                            'footerMessage' => true,
                            'backgroundHeader' => true,
                            'showHeader' => true
                        ])

@section('jumbotron')
    @include('partials.jumbotrons.jumbotron')
@endsection

@section('content')
<div class="xplan-container" id="xplan-container">
    <div class="xp-lan-background-third mx-auto row mt-4 py-4">
        <!-- <div class="xplan-video my-auto col-xs-12 col-md-6 order-2 order-md-1">
            <div class="xplan-video-content">
                <iframe class="xplan-video-source" src="https://www.youtube.com/embed/wkwR7_pkxA4?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div> -->
        <div class="xp-lan-content-promo-first col-xs-12 col-md-8 order-1 order-md-2 mx-auto">
            <div class="">
                <p class="text-center xp-lan-title-promo">"En Xpeaking aprenderás a tu propio ritmo, desde cualquier lugar y dispositivo. Acceso 24/7"</p>

            </div>
            <div>
                <p class="text-black text-center xp-lan-content-promo">
                    ¡Sin profesores, sin libros, sin horarios, sin desplazamientos, y lo mejor de todo sin mensualidades caras! 
                </p>
            </div>
            <!-- <div class="align-items-center pt-4 col-md-12">
                <div class="xp-demo-zone">
                    <a class="xp-icon-demo">
                        <span class="xp-icon-demo-text">Mira la demo</span>
                    </a>
                </div>
            </div> -->
        </div>
        
    </div>
    <div class="xp-slider-card-content free-songs row xplan-content pt-4">
        <div class="col-12 mt-4">
            <div><p class="text-center xp-lan-title-demo mt-4">¡Demo Gratis!</p></div>
            <div><p class="text-white text-center xp-lan-content-promo">Selecciona una de estas canciones para probar nuestra metodología</p></div>
        </div>
        <div class="free-songs-container my-5">
            <free-songs :songs="{{ $songs }}"></free-songs>
        </div>
        
    </div>
    <!-- Parte por que elegir xpeaking -->
    <div class="xplan-choose">
        <div class="xplan align-items-center wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.3s">
            <div class="col-md-12">
                <p class="text-center xp-lan-choose-title">¿Por qué elegir Xpeaking?</p>
            </div>
            <div class="w-75 mx-auto">
                <p class="text-center xp-lan-choose-subtitle">
                    Porque en Xpeaking ahorrarás 2 de tus activos más importantes:
                <span class="strong-content">¡Tiempo y Dinero!</span> Ya no será necesario probar métodos costosos que duran años y no te brindan el resultado deseado, pero también hay otros motivos: </p>
            </div>
        </div>
        <div class="xplan w-75 mx-auto">
            <div class="xplan row pb-4 wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.3s">
                <div class="col-md-6 my-4">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <div class="xp-icon-image-vertical">
                                <img src="{{ asset('img/diferente.svg') }}" alt="Icon">
                            </div>  
                        </div>
                        <div class="col-md-8">
                            <p class="text-center xp-lan-title-choose">Diferente</p>
                            <p class="xp-lan-choose-subtitle text-justify">Nuestra metodología fomenta la autoconfianza y motivación para hablar el inglés desde el 1er día, independientemente de tu edad, talento y horario.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 my-4">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <div class="about-icon xp-icon-image-vertical">
                                <img src="{{ asset('img/desdecero.svg') }}" alt="Icon">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <p class="text-center xp-lan-title-choose">Desde cero</p>
                            <p class="xp-lan-choose-subtitle text-justify">Si tu prioridad es aprender un idioma desde cero, reforzar o perfeccionar lo que aprendes en el colegio, instituto o universidad ¡Xpeaking es la herramienta ideal para ti!</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="xplan row wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.3s">
                <div class="col-md-6 my-4">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <div class="mx-auto xp-icon-image-vertical">
                                <img src="{{ asset('img/acelera.svg') }}" alt="Icon">
                            </div>
                        </div>
                        <div class="col-md-8 mb-3">
                            <p class="text-center xp-lan-title-choose">Acelera tu aprendizaje</p>
                            <p class="xp-lan-choose-subtitle text-justify">La música estimula la memorización de forma natural, logrando que el aprendizaje de idiomas sea más <span class="strong-content-choose">Fácil, rápido y divertido.</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 my-4">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <div class="mx-auto xp-icon-image-vertical">
                                <img src="{{ asset('img/gana.svg') }}" alt="Icon">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <p class="text-center xp-lan-title-choose">Gana</p>
                            <p class="xp-lan-choose-subtitle text-justify">Acumula puntos por cada recomendación y canjéalos por increíbles premios. Conoce más en <a href="{{ route('ambassador') }}">Programa Embajador</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Plan mensual -->
    <div class="xplan-content mx-auto px-4 my-4">
        <div class="row p-4">
            <div class="plan-sections col-lg-6">
                <div class="col-md-12 my-4">
                    <p class="month-plan-title text-center">Plan de Suscripción mensual</p>
                </div>
                <div class="col-md-12 mt-4 align-items-center w-100 h-100 mx-auto">
                    <a href="{{ route('register') }}" class="btn btn-start-promotion btn-landing w-100">
                        <div class="my-auto py-5">
                                <p class="xp-button-title-plan">S/ 29.00</p>
                                <p class="xp-button-subtitle-plan">¡Empieza hoy!</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="plan-sections my-auto col-lg-6">
                <div class="mt-4">
                    <ul class="check-list">
                        <li class="plan-section-list">Acceso ilimitado a todo nuestro catálogo de canciones.</li>
                        <li class="plan-section-list">Acceso a cualquier hora del día, desde cualquier lugar y dispositivo (100% online).</li>
                        <li class="plan-section-list">Aprende idiomas sin anuncios.</li>
                        <li class="plan-section-list">Idiomas disponibles: Inglés.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Inspire -->
    <div class="section-inspire">
        <div class="mt-5">
            <div class="py-4 inspire-content-title mx-auto">
                <p class="xp-lan-choose-title text-center">Inspiramos a las personas a aprender un nuevo idioma de forma diferente</p>
            </div>
            <div class="">
                <div id="carouselExampleControls" class="carousel slide landing-carousel-content" interval="false">
                  <div class="carousel-inner landing-carousel">
                    <div class="carousel-item landing-carousel-item active">
                        <div class="landing-carousel-item-content">
                            <div class="xplan-video-content">
                                <iframe class="xplan-video-source" src="https://www.youtube.com/embed/za9uONEEg4Q?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item landing-carousel-item">
                        <div class="landing-carousel-item-content">
                            <div class="xplan-video-content">
                                <iframe class="xplan-video-source" src="https://www.youtube.com/embed/za9uONEEg4Q?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item landing-carousel-item">
                        <div class="landing-carousel-item-content">
                            <div class="xplan-video-content">
                                <iframe class="xplan-video-source" src="https://www.youtube.com/embed/za9uONEEg4Q?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>

            </div> <!-- row -->
            <div class="mt-4">
                <div class="align-items-center mt-4">
                    <div class="start-promotion-content mx-auto">
                        <a href="{{ route('register') }}" class="btn btn-start-promotion btn-landing w-100 py-2">
                                <p class="xp-button-title">¡Empieza hoy!</p>
                                <p class="xp-button-subtitle">Suscripción: S/ 29.00 al mes</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-4">
            <div class="row m-0 w-100 d-flex justify-content-center align-items-center xp-jumbotron-body-item">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="xp-card-sponsors">
                        <span class="xp-lan-card-sponsors-text">
                            Contamos con el respaldo de:
                        </span>
                        <div class="xp-card-sponsors-content mt-3">
                            <div class="xp-card-sponsors-item mr-4">
                                <img src="{{ asset('img/openf.png') }}" alt="">
                            </div>
                            <div class="xp-card-sponsors-item">
                                <img src="{{ asset('img/cci.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="xplan-ambassador-content pb-5">
        <div class="ambassador-content mx-auto row">
            <div class="ambassador-content-first col-xs-12 col-md-6">
                <div class="">
                    <p class="text-center xp-lan-title">Recomienda Xpeaking con tus amigos y gana dinero</p>
                </div>
            <div class="">
                    <p class="text-center xp-lan-subtitle">
                        Juntos podemos lograr que más personas tengan la oportunidad de aprender idiomas de forma <span class="strong-content">fácil, rápida y divertida</span>
                    </p>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12 align-items-center mt-4">
                        <div class="w-75 mx-auto">
                            <button class="btn btn-start-ambassador w-100">
                                <div>
                                    <p class="xp-button-title">¡Empieza hoy!</p>
                                </div>
                                <div>
                                    <p class="xp-button-subtitle">Suscripción: S/ 29.00 al mes</p>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ambassador-video my-auto col-xs-12 col-md-6">
                <div class="xplan-video-content">
                    <iframe class="xplan-video-source" src="https://www.youtube.com/embed/za9uONEEg4Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div> -->


</div>
@endsection
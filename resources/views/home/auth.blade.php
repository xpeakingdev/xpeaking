@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => true,
                            'footerMessage' => true,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('jumbotron')
    @include('partials.jumbotrons.jumbotron')
@endsection

@section('content')

    <!--====== portfolio PART START ======-->

    <section id="portfolio" class="xp-section xp-bg-black">
        <div class="container pr-0 pr-md-2">
            <div class="row justify-content-center mx-0">
                <div class="col-lg-12">
                    <div class="xp-section-bar">
                        <div class="xp-section-title">
                            <h5 class="title xp-text-primary">Canciones</h5>
                            <p class="subtitle xp-text-gray-down">TOTAL: {{ count($songs) }}</p>
                        </div>
                        <div class="xp-section-title pr-0 pr-md-3">
                            <a href="{{ route('content.songs.all') }}" class="xp-link-primary xp-hover-underline">Ver todo</a>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="d-block mx-auto mt-4">
                {{-- CAROUSEL SPNGS --}}
                <content-songs-list-important></content-songs-list-important>
                    {{-- <button class="next">NEXT</button> --}}
                {{-- END CAROUSEL --}}
            </div>


            {{-- ARTISTS --}}
            <div class="row justify-content-center mx-0 mt-4 mt-md-5">
                <div class="col-lg-12 mt-2">
                    <div class="xp-section-bar">
                        <div class="xp-section-title">
                            <h5 class="title xp-text-primary">Artistas</h5>
                            <p class="subtitle xp-text-gray-down">TOTAL: {{ count($artists) }}</p>
                        </div>
                        <div class="xp-section-title pr-0 pr-md-3">
                            <a href="{{ route('content.artists.all') }}" class="xp-link-primary xp-hover-underline">Ver todo</a>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="d-block mx-auto mt-4">
                <content-artists-list></content-artists-list>
            </div>
            {{-- END ARTISTS --}}

            <!-- {{-- Students --}}
            <div class="row justify-content-center mx-0 mt-4 mt-md-5">
                <div class="col-lg-12 mt-2">
                    <div class="xp-section-bar">
                        <div class="xp-section-title">
                            <h5 class="title xp-text-primary">Novedades</h5>
                            <p class="subtitle xp-text-gray-down">TOTAL: 1200</p>
                        </div>
                        <div class="xp-section-title pr-0 pr-md-3">
                            <a href="#" class="xp-link-primary xp-hover-underline">Ver todo</a>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="d-block mx-auto mt-4">
                {{-- CAROUSEL ARTISTS --}}
                <content-songs-list></content-songs-list>
                {{-- <div class="xp-slider-card-content">
                    <div class="slider xp-slider-card" id="xp-slider-card-students">
                        @for($i=1; $i<=10; $i++)
                            <div class="xp-card-single">
                                <div class="xp-card-single-header">
                                    <img src="{{ asset('img/songs/song-'. $i .'.jpg') }}" alt="">
                                    <div class="xp-card-single-header-alt">

                                    </div>
                                </div>
                                <div class="xp-card-single-body">
                                    <div class="xp-card-single-body-title">
                                        <h5 class="">Nombre del artista</h5>
                                        <div class="xp-card-single-body-artist text-white">
                                            <span>700 Puntos</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                    <div class="xp-card-single-button prev" id="xp-slider-card-prev-students">
                        <i class="fas fa-chevron-left"></i>
                    </div>
                    <div class="xp-card-single-button next" id="xp-slider-card-next-students">
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </div> --}}
                    {{-- <button class="next">NEXT</button> --}}
                {{-- END CAROUSEL ARTISTS--}}
            </div>
            {{-- END Students --}} -->

        </div> <!-- container -->
    </section>

    <!--====== portfolio PART ENDS ======-->

@endsection

@section('scripts')

<script>
    document.addEventListener('DOMContentLoaded', ()=>{
        // $('.slider-for').slick({
        //     slidesToShow: 1,
        //    slidesToScroll: 1,
        //    arrows: false,
        //    fade: true,
        //    asNavFor: '.slider-nav'
        // });

        $('#xp-slider-card-songs').slick({
           slidesToShow: 5,
           slidesToScroll: 1,
           dots: false,
           focusOnSelect: true,
           dotsClass: 'slick-dots slick-thumb',
           nextArrow: $('#xp-slider-card-next-songs'),
           prevArrow: $('#xp-slider-card-prev-songs'),
           customPaging: function(Slide, i) {
            //  var ii = i + 1;
            // var datad = $('.slider-for .cl'+ii).data('dot');

            //     return '<a>'+datad+'</a>'
              },
        });

        $('#xp-slider-card-artists').slick({
           slidesToShow: 5,
           slidesToScroll: 1,
           dots: false,
           focusOnSelect: true,
           dotsClass: 'slick-dots slick-thumb',
           nextArrow: $('#xp-slider-card-next-artists'),
           prevArrow: $('#xp-slider-card-prev-artists'),
           customPaging: function(Slide, i) {
            //  var ii = i + 1;
            // var datad = $('.slider-for .cl'+ii).data('dot');

            //     return '<a>'+datad+'</a>'
              },
        });

        $('#xp-slider-card-students').slick({
           slidesToShow: 5,
           slidesToScroll: 1,
           dots: false,
           focusOnSelect: true,
           dotsClass: 'slick-dots slick-thumb',
           nextArrow: $('#xp-slider-card-next-students'),
           prevArrow: $('#xp-slider-card-prev-students'),
           customPaging: function(Slide, i) {
            //  var ii = i + 1;
            // var datad = $('.slider-for .cl'+ii).data('dot');

            //     return '<a>'+datad+'</a>'
              },
        });

        $('#xp-slider-card-embassadors').slick({
           slidesToShow: 5,
           slidesToScroll: 1,
           dots: false,
           focusOnSelect: true,
           dotsClass: 'slick-dots slick-thumb',
           nextArrow: $('#xp-slider-card-next-embassadors'),
           prevArrow: $('#xp-slider-card-prev-embassadors'),
           customPaging: function(Slide, i) {
            //  var ii = i + 1;
            // var datad = $('.slider-for .cl'+ii).data('dot');

            //     return '<a>'+datad+'</a>'
              },
        });

        // $('a[data-slide]').click(function(e) {
        //   e.preventDefault();
        //   var slideno = $(this).data('slide');
        //   $('.slider-nav').slick('slickGoTo', slideno - 1);
        // });
    })
</script>

@endsection
@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'xp-main-complete',
                            'jumbotron' => true,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => true,
                            'showHeader' => true
                        ])

@section('jumbotron')
    <div class="xp-main-header">
        <div class="xp-main-header-content">
            <div class="container">
                <change-password :token="{{ json_encode($token) }}"></change-password>
            </div>
        </div>
    </div>
@endsection
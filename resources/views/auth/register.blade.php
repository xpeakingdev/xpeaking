@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'xp-main-complete',
                            'jumbotron' => true,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => true,
                            'showHeader' => true
                        ])

@section('jumbotron')
    <div class="xp-main-header">
        <div class="xp-main-header-content">
            <div class="container">
                @php
                    $usernameData = '';
                    if(isset($username)){
                        $usernameData = $username;
                    } 
                @endphp
                <register :username="{{ json_encode($usernameData) }}" ></register>
            </div>
        </div>
    </div>
@endsection
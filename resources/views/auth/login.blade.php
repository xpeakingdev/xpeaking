@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'xp-main-complete',
                            'jumbotron' => true,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => true,
                            'showHeader' => true
                        ])

@section('jumbotron')
    <div class="xp-main-header">
        <div class="xp-main-header-content">
            <div class="container">
                <div class="row mx-0 mt-5 mt-md-0">
                    <div class="col-12 col-md-6 col-lg-4 mx-auto">
                        <div class="xp-card-login">
                            {{-- LOADER --}}
                            <div class="xp-loader xp-loader-music d-none" id="loader-login">
                                <div class="xp-loader-music-content">
                                    <div class="xp-loader-music-bar"></div>
                                    <div class="xp-loader-music-bar"></div>
                                    <div class="xp-loader-music-bar"></div>
                                </div>
                            </div>
                            {{-- END LOADER --}}
                            <form action="/acceso" method="POST" id="formLogin">
                                @csrf
                                <div class="xp-card-login-content">
                                    <div class="xp-card-login-header">
                                        <div class="xp-card-login-header-title">
                                           Iniciar sesión
                                        </div>
                                    </div>
                                    <div class="xp-card-login-body">
                                        <div class="row m-0">
                                            <div class="col-12 mt-3 xp-form-group-super">
                                                <input class="xp-form-control xp-form-control-white radius-2 xp-form-group-super-field no-spaces formInputSel" placeholder="Usuario" type="text" name="username" min="4" max="100" value="{{ old('username') }}">
                                                <label class="xp-form-group-super-label">
                                                    <span>Usuario</span>
                                                </label>
                                                <span class="xp-form-super-alert xp-text-warning-alt">
                                                    El usuario ingresado no existe
                                                </span>
                                            </div>
                                            <div class="col-12 mt-3 xp-form-group-super">
                                                <input type="password" class="xp-form-control xp-form-control-white radius-2 xp-form-group-super-field with-icon formInputSel" placeholder="Contraseña" id="inputPassword" min="4" max="100" name="password">
                                                <label class="xp-form-group-super-label">
                                                    <span>Contraseña</span>
                                                </label>
                                                <span class="xp-form-group-super-icon" id="viewPass">
                                                    <i class="fas fa-eye"></i>
                                                </span>
                                                <span class="xp-form-super-alert xp-text-warning-alt">
                                                    De 4 a 20 caracteres
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    @if(session('error'))
                                        @if(session('error')!='status')
                                            <div class="d-flex justify-content-center mt-4">
                                                <div class="xp-message-alert xp-message-alert-warning"> 
                                                    <span>{{ session('message') }}</span>
                                                </div>
                                            </div>
                                        @endif
                                    @elseif($errors->any())
                                        <div class="d-flex justify-content-center mt-4">
                                            <div class="xp-message-alert xp-message-alert-warning"> 
                                                <span>{{ $errors->first() }}</span>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="xp-card-login-footer d-flex flex-column">
                                        <button type="submit" class="xp-button xp-button-primary xp-button-margin xp-button-lg xp-button-pd">
                                            Iniciar sesión
                                        </button>
                                        <a class="xp-button xp-button-line xp-button-large xp-button-link-primary text-center mt-2 p-0" href="/acceso/restablecer">
                                            ¿Olvidaste tu contraseña?
                                        </a>
                                        <div class="d-flex mt-4 justify-content-center align-items-center text-white xp-font-1">
                                            <span class="mr-2 text-black">¿Primera vez en Xpeaking?</span>
                                            <a class="xp-button xp-button-line no-padding font-bold text-center font-weight-bold" href="/registro">
                                                ¡Suscríbete!
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- ALERT -->
                    {{-- v-if="alert" --}}
                    @if(session('error'))
                        @if(session('error')=='status')
                            <div class="xp-dashboard-alert" id="alertStatus">
                                <div class="xp-dashboard-card-alert animated zoomIn xp-delay-03s">
                                    <span class="xp-dashboard-alert-close closeAlertStatus">
                                        <i class="fas fa-times"></i>
                                    </span>
                                    <div class="xp-dashboard-card-alert-header">
                                        <span class="xp-dashboard-card-alert-icon xp-text-jade">
                                            <img src="/img/alert.svg" alt="">
                                        </span>
                                    </div>
                                    <div class="xp-dashboard-card-alert-body">
                                        <h4 class="xp-dashboard-card-alert-text xp-text-primary">
                                            {{ session('message') }}
                                        </h4>
                                    </div>
                                    <div class="xp-dashboard-card-alert-footer">
                                        
                                        <div class="d-flex justify-content-between mt-2">
                                            {{-- v-on:click="alert = null" --}}
                                            <button class="xp-button xp-button-danger xp-button-pd w-100 closeAlertStatus">
                                                Entendido
                                            </button>
                                        </div>
                    
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    document.addEventListener('DOMContentLoaded', ()=>{
        let viewPass = document.getElementById('viewPass')
        if(viewPass){
            viewPass.addEventListener('click', ()=>{
                let inputPassword = document.getElementById('inputPassword')
                if(inputPassword){
                    viewPass.innerHTML = (inputPassword.type == "text" ? '<i class="fas fa-eye"></i>' : '<i class="fas fa-eye-slash"></i>')
                    inputPassword.setAttribute('type', (inputPassword.type == "text" ? 'password' : "text"))
                    // <i class="fas fa-eye"></i>
                    // <i class="fas fa-eye-slash"></i>
                }
            })
        }

        // NO SPACES
        let noSpaces = document.querySelectorAll('.no-spaces')
        if(noSpaces){
            noSpaces.forEach(item=>{
                item.addEventListener('keypress', (e)=>{
                    if(e.keyCode == 32){
                        e.preventDefault();
                    }
                })
            })
        }

        // CLOSE ALERT
        let closeAlerts = document.querySelectorAll('.closeAlertStatus')
        if(closeAlerts){
            closeAlerts.forEach(closeA=>{
                closeA.addEventListener('click', ()=>{
                    let alertP = document.getElementById('alertStatus')
                    if(alertP){
                        alertP.classList.add('d-none')
                    }
                })
            })
        }

        // INPUTS
        let inputs = document.querySelectorAll('.formInputSel')
        if(inputs){
            inputs.forEach(itemIn=>{
                if(itemIn.value != ''){
                    itemIn.classList.add('filled')
                }

                itemIn.addEventListener('keyup', (e)=>{
                    if(e.target.value.length>0){
                        itemIn.classList.add('filled')
                    }
                    else{
                        itemIn.classList.remove('filled')
                    }
                })
            })
        }
    })
</script>
@endsection
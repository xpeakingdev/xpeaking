<!doctype html>
<html lang="en">

<head>

    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="theme-color" content="#F8F8F8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--====== Title ======-->
    <title>{{ config('app.name') }}</title>

    <!--====== App css ======-->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{ asset('img/favicon.svg') }}" type="image/svg">

    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="{{ asset('landing/css/bootstrap.min.css') }}">

    <!--====== Line Icons css ======-->
    {{-- <link rel="stylesheet" href="{{ asset('landing/css/LineIcons.css') }}"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="{{ asset('landing/css/magnific-popup.css') }}">

    <!--====== Slick css ======-->
    <link rel="stylesheet" href="{{ asset('landing/css/slick.css') }}">

    <!--====== Animate css ======-->
    <link rel="stylesheet" href="{{ asset('landing/css/animate.css') }}">

    <!--====== Default css ======-->
    <link rel="stylesheet" href="{{ asset('landing/css/default.css') }}">

    <!--====== Style css ======-->
    <link rel="stylesheet" href="{{ asset('landing/css/style.css') }}">

    <!--====== Style Plyr ======-->
    <link rel="stylesheet" href="{{ asset('css/plyr.css') }}">

    <!--====== slick ======-->
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vueperslides.css') }}">

    <!--====== Toastr ======-->
    <link rel="stylesheet" href="{{ asset('css/toastr.css') }}">

    <!--====== Cropper JS ======-->
    <link rel="stylesheet" href="{{ asset('css/cropper.css') }}">
    
    <!--====== Style Custom css ======-->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <!-- --==== LANDING CSS ====-- -->
    <link rel="stylesheet" href="{{ asset('css/landing.css') }}">

</head>

<body>
<div id="app">

    <!--====== ALERTS ======-->
    @if(session('xpNotification'))
        <alert :xp-notification="{{ json_encode(session()->pull('xpNotification')) }}"></alert>
    @endif
    <!--====== PRELOADER PART START ======-->

    <loader-complete :show="true"></loader-complete>

    @include('partials.alerts.alert')

    <!--====== PRELOADER PART ENDS ======-->

    <!--====== NAVBAR PART START ======-->

    @isset($showHeader)
        @if($showHeader)

            <section class="header-area xp-header-area {{ isset($backgroundHeader)?($backgroundHeader?'xp-header-area-background':''):'' }}" 
            
            @isset($backgroundHeader)
                @if ($backgroundHeader)
                    style="background:#f8f8f8;"
                @endif
            @endisset
            
            >
                
                <div id="home" class="slider-area xp-slider-area {{ isset($classSliderArea)?$classSliderArea:'' }}">
                    @include('partials.navigation', ['classNavigation' => (isset($classNavigation)?$classNavigation:'')])
                    @isset($jumbotron)
                        @if($jumbotron)
                            @yield('jumbotron')
                        @endif
                    @endisset
                </div>
        
            </section>
            
        @endif
    @endisset


    <!--====== NAVBAR PART ENDS ======-->
    
    @yield('content')
    
    <!--====== FOOTER FOUR PART START ======-->
    <footer id="footer" class="footer-area xp-footer">
        @isset($footer)
            @if($footer)
                @include('partials.footer')
            @endif
        @endisset
        @isset($footerMessage)
            @if($footerMessage)
                @include('partials.footer-message')
            @endif
        @endisset
    </footer>

    <!--====== FOOTER FOUR PART ENDS ======-->
    
    <!--====== BACK TOP TOP PART START ======-->
    @isset($social)
        @if($social)
            @include('partials.social')
        @endif
    @endisset

    <!--====== BACK TOP TOP PART ENDS ======-->  

    <!--====== PART START ======-->

    <!--====== PART ENDS ======-->
</div>  
    @if(!Auth::check())
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167417558-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-167417558-1');
        </script>
    @endif
    @if(Auth::check( )&& Auth::user()->student)
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167417558-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-167417558-1');
        </script>
    @endif
    <!--====== App js ======-->
    <script src="{{ asset('js/app.js') }}"></script>

    <!--====== jquery js ======-->
    <script src="{{ asset('landing/js/vendor/modernizr-3.6.0.min.js') }}"></script>
    {{-- <script src="{{ asset('template/js/vendor/jquery-1.12.4.min.js') }}"></script> --}}

    <!--====== Bootstrap js ======-->
    <script src="{{ asset('landing/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('landing/js/popper.min.js') }}"></script>

    <!--====== Slick js ======-->
    <script src="{{ asset('landing/js/slick.min.js') }}"></script>

    <!--====== Isotope js ======-->
    <script src="{{ asset('landing/js/isotope.pkgd.min.js') }}"></script>

    <!--====== Images Loaded js ======-->
    <script src="{{ asset('landing/js/imagesloaded.pkgd.min.js') }}"></script>

    <!--====== Magnific Popup js ======-->
    <script src="{{ asset('landing/js/jquery.magnific-popup.min.js') }}"></script>

    <!--====== Scrolling js ======-->
    <script src="{{ asset('landing/js/scrolling-nav.js') }}"></script>
    <script src="{{ asset('landing/js/jquery.easing.min.js') }}"></script>

    <!--====== wow js ======-->
    <script src="{{ asset('landing/js/wow.min.js') }}"></script>

    <!--====== Main js ======-->
    <script src="{{ asset('landing/js/main.js') }}"></script>

    <!--====== Main js ======-->
    <script src="{{ asset('js/plyr.js') }}"></script>
     <!--====== slick ======-->
     <script src="{{ asset('js/slick.js') }}"></script>

    <!--====== Toastr ======-->
    <script src="{{ asset('js/toastr.min.js') }}"></script>

    <!--====== Cropper JS ======-->
    <script src="{{ asset('js/cropper.js') }}"></script>

    {{-- CUSTOM --}}
    <script src="{{ asset('js/custom.js') }}"></script>

    @yield('scripts')

    <script>
        document.addEventListener('DOMContentLoaded', () => {
            toastr.options.progressBar = true

            // const searchInput = document.querySelectorAll('.control__input--search');

            // if(searchInput){
            //     for (i = 0; i < searchInput.length; ++i) {
            //         searchInput[i].addEventListener("change", function() {
            //             if(this.value == '') {
            //                 this.classList.remove('is-not-empty')
            //             } else {
            //                 his.classList.add('is-not-empty')
            //             }
            //         });
            //     }
            // }
        })
    </script>

</body>

</html>

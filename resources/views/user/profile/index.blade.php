@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
{{-- full-data-profile-margin --}}
<div class="full-data-profile">
    <div class="container px-0 px-md-2">
        <user-account></user-account>
    </div>
</div>
@endsection
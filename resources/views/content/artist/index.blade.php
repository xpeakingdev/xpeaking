@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
{{-- full-data-profile-margin --}}
<div class="full-data-profile ">
    <div class="container px-0 px-md-2">
        <div class="row m-0 d-flex justify-content-center align-items-start">
            <div class="col-12 col-md-4 col-lg-4 pr-md-3">
                <content-artist-info :slug="{{ json_encode($artist->slug) }}"></content-artist-info>
            </div>
            <div class="col-12 col-md-8 col-lg-8 pr-0 pl-0 pl-md-3">
                <div class="xp-songs-list">
                    @foreach ($artist->songs as $song)
                        <a class="xp-songs-list-item row m-0" href="{{'/cancion/reproductor/'.$song->id.'/'.$song->slug}}">
                            <span class="col-1 xp-songs-list-item-num">{{ $loop->index + 1 }}</span>
                            <span class="col-2 col-md-3 xp-songs-list-item-cover pr-2 pr-md-0">
                                @php
                                    $origin = "/^(http|https):\/\//";
                                    $evaluate = preg_match($origin, $song->cover);
                                    $pictu = '';
                                    if($evaluate == 1){
                                        $pictu = $song->cover;
                                    }
                                    else{
                                        $pictu = '/storage/songs/'.$song->cover;
                                    }
                                @endphp
                                <figure style="width:100%; height:auto; margin: 0;">
                                    <div class="img-picture" style="background:url(' {{$pictu}} ');">

                                    </div>
                                    {{-- <img src="{{ ($evaluate == 1)?$song->cover:asset('storage/songs/'.$song->cover) }}" alt="" style="height:auto;"> --}}
                                </figure>
                            </span>
                            <span class="col-5 col-md-4 xp-songs-list-item-box-title">
                                <span class="xp-songs-list-item-title">{{ $song->name }}</span>
                                @php
                                    $listArtists = '';
                                    foreach ($song->artists as $artistData) {
                                        if(strlen($listArtists)>0){
                                            $listArtists .= ', ';
                                        }
                                       $listArtists .= $artistData->stage_name;
                                    }  
                                @endphp
                                <span class="xp-songs-list-item-subtitle">{{ $listArtists }}</span>
                            </span>
                            <span class="col-2 xp-songs-list-item-status">
                                <i class="fas fa-check"></i>
                            </span>
                            <span class="col-2 xp-songs-list-item-time">{{ $song->duration }}</span>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
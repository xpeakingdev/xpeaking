@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => true,
                            'social' => true,
                            'footerMessage' => true,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
    <section id="about" class="xp-section py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="section-title text-center">
                        <h4 class="title xp-text-primary">
                            Términos de Uso
                        </h4>
                    </div> <!-- section title -->

                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <p class="text text-legal">
                            Estos Términos de Uso se actualizaron por última vez el 22 de Febrero del 2020.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">1. Introducción</h5>
                        <p class="text text-legal">
                            POR EL PRESENTE DOCUMENTO, TANTO EL USUARIO COMO LA EMPRESA SE OBLIGAN A CUMPLIR LOS TÉRMINOS DE USO QUE SE ESTIPULAN AL USAR CUALQUIERA DE LOS SERVICIOS DE XPEAKING S.A.C. Y/O AL HACER CLIC EN EL BOTÓN REGISTRARME. EL USUARIO CONVIENE EN OBLIGARSE CONFORME A LOS TÉRMINOS; SI NO ESTÁ DE ACUERDO CON TODOS ELLOS, DEBE ABSTENERSE DE UTILIZAR LOS SERVICIOS DE XPEAKING.
                        </p>
                        <p class="text text-legal">
                            Sección previa sobre el arbitraje:
                        </p>
                        <p class="text text-legal">
                            IMPORTANTE: AL ACEPTAR ESTOS TÉRMINOS, EL USUARIO CONVIENE EN RESOLVER CUALQUIER CONTROVERSIA CON XPEAKING EN ARBITRAJE (NUNCA EN SEDE JUDICIAL) Y RENUNCIA A LOS DERECHOS DE PARTICIPACIÓN EN DEMANDAS COLECTIVAS.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            2. Acuerdos adicionales
                        </h5>
                        <p class="text text-legal">
                            Toda la información personal remitida en relación con el uso de los Servicios está sujeta a nuestra Política de privacidad, que, por la presente remisión, se incorporan a estos Términos.
                        </p>
                        <p class="text text-legal">
                            Todo usuario se obliga a No copiar, modificar, distribuir, realizar ingeniería inversa, desfigurar, manchar, mutilar, sabotear o interferir con el Contenido de la empresa y/o los Servicios, Plan de Marketing, Plan de distribución, Plan de crecimiento del servicio u operaciones de la misma, excepto según lo permitido.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            3. Generalidades
                        </h5>
                        <p class="text text-legal">
                            Nuestros servicios permiten a los usuarios ingresar a nuestra plataforma online donde se imparte nuestra metodología de aprendizaje de idiomas a través de canciones, todo aquello por una suscripción mensual de S/ 29.00 (veintinueve soles), no existen moras por el retraso del pago de una suscripción mensual, ni mensualidades acumuladas; entendiéndose, que el usuario sólo tendrá la condición de “alumno” mientras pague su mensualidad, y la condición de Embajador mientras sea alumno y recomiende efectivamente nuestra plataforma. Acepta que Xpeaking puede realizar el cambio de precio, marketing e incluso estos términos de uso cuando lo considere oportuno.
                        </p>
                        <p class="text text-legal">
                            Para suscribirse, el usuario tendrá como opciones de pago: Tarjetas de crédito o débito, Transferencias y Puntos. Si el usuario facilita datos de tarjetas para abonar el pago, por la presente, garantiza que está autorizado a suministrar dicha información y que autoriza a Xpeaking a cobrar el respectivo monto por la adquisición de nuestros servicios. Asimismo, referente al cobro de mensualidades, Xpeaking no efectuará cobros automáticos de la tarjeta brindada.
                        </p>
                        <p class="text text-legal">
                            El usuario debe tener en cuenta que después de 90 días de inactividad (no haber pagado tu suscripción por 3 meses seguidos o 120 días con interrupciones), su cuenta (usuario de registro) será bloqueada y pasará a propiedad de Xpeaking con la finalidad de ser borrada del sistema o transferirle a una nueva persona para un mejor provecho. Respecto a lo precitado, no habrá reclamo alguno, y sin la posibilidad de volverla a recuperar.
                        </p>
                        <p class="text text-legal">
                            Los presentes Términos se actualizan cuando se considere preciso para aclarar nuestras prácticas o para reflejar prácticas nuevas o diferentes, por ejemplo, cuando añadimos funciones nuevas. Además, Xpeaking se reserva el derecho de modificar o alterar, a su sola discreción, los Términos en cualquier momento. Si se trata de un cambio sustancial en los Términos, se comunica al usuario por algún medio visible con una anticipación de 3 días hábiles, por ejemplo, remitiendo un correo electrónico a la dirección especificada en la cuenta señalada en su registro o publicando un aviso en Nuestros Servicios o Redes Sociales. Las modificaciones se aplican desde el mismo día en que se anuncian, salvo manifestación en contrario.
                        </p>
                        <p class="text text-legal">
                            Si el usuario sigue usando los servicios después de entrar en vigor los cambios, implica que los acepta. Conviene que visite los servicios con regularidad para asegurarse de conocer la última versión de los Términos y Condiciones, ya que los Términos revisados sustituyen todas las versiones anteriores.
                        </p>
                        <p class="text text-legal">
                            El usuario es el único responsable de todos los cargos, gastos o costes de servicios adicionales, telefonía y datos asociados al acceso a los servicios y a su uso, así como de la obtención y el mantenimiento de sus teléfonos, el hardware informático y cualquier otro equipo necesario para el acceso y el uso de todos nuestros servicios.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            4. Sobre la metodología
                        </h5>
                        <p class="text text-legal">
                            En nuestra metodología de aprendizaje de idiomas utilizamos como herramienta principal las canciones (videoclip), para lograr que el aprendizaje sea mucho más fácil, rápido y divertido. Es importante aclarar, que estas canciones (videos) no son propias de Xpeaking, sino de los autores, artistas, discográficas y demás titulares que suben sus videos en Youtube, Vimeo, y otras plataformas de video. Nosotros no almacenamos esta información en nuestros servidores.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            5. Usuario Embajador
                        </h5>
                        <p class="text text-legal">
                            El Embajador es un usuario estudiante que paga su suscripción mensual de S/29 (veintinueve soles) e invita a sus amistades a registrarse en Xpeaking, acumula puntos por cada recomendación efectiva, y luego esos puntos los puede canjear por dinero y/o pagar sus suscripciones.  Actualmente 1 punto = S/ 0.50. El usuario Embajador en su campus virtual, tendrá una opción específica para administrar todos sus puntos: Puntos disponibles, ingresos, pagos, canjes, transferencias, etc.
                        </p>
                        <p class="text text-legal">
                            Xpeaking ofrece a todos los Usuarios Embajadores un Programa de Recompensas por estas recomendaciones efectivas (cuando los usuarios pagan su suscripción). El usuario embajador es consciente que las COMISIONES establecidas por Xpeaking son puntos regalos, y que pueden ser redimidos por los diferentes premios que ofrece la compañía. La solicitud de canje se realiza a través de tu campus virtual y sólo procede en las fechas indicadas, teniendo en cuenta que el depósito se efectuará en un plazo no mayor de 5 días hábiles. En el caso de que exista un error en la operación, Xpeaking se reserva el derecho de extraer la cantidad excedida en puntos sin comunicación alguna.
                        </p>
                        <p class="text text-legal">
                            El requisito principal para ser embajador es ser alumno e invitar a otras personas a suscribirse en www.xpeaking.com/usuarioembajador; no existen moras por el retraso del pago de la suscripción, ni mensualidades acumuladas; entendiéndose, que el usuario sólo tendrá la condición de Alumno Embajador para disfrutar de todos los beneficios mientras se encuentre activo en el sistema; es decir, que haya pagado su suscripción.
                        </p>
                        <p class="text text-legal">
                            Todas las actividades realizadas en relación con los Servicios (uso, acceso o de otra naturaleza) deben ser conformes a todas las leyes y normativas vigentes, incluidas, sin limitaciones, las leyes relativas a los derechos de autor y al uso de la Propiedad Intelectual, así como a la identidad privada y personal. Además, queda prohibido acceder a nuestros servicios desde territorios donde el contenido sea ilegal. Quienes accedan a los servicios o los utilicen desde fuera de Perú, lo hacen por iniciativa propia y son responsables de cumplir todas las normas de ámbito local, entre otras, las normas relativas a Internet, los datos, el correo electrónico o la privacidad. El Usuario conviene, asimismo, en acatar la legislación aplicable en materia de transmisión de datos técnicos exportados desde Perú o desde el país donde resida. Si el Usuario utiliza los Servicios o Plataformas de terceros (según se definen en la cláusula siguiente) en otro país que no sea Perú, debe cumplir todas las normas locales relativas a la conducta en las redes y al contenido aceptable.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            6. Descargo de responsabilidad general
                        </h5>
                        <p class="text text-legal">
                            Xpeaking brinda como servicio su metodología de aprendizaje de idiomas a través de su plataforma virtual. No contratamos ni empleamos a nuestros usuarios alumnos, ni embajadores, ni somos responsables de ninguna interacción entre los usuarios. Nos eximimos de toda responsabilidad por controversias, reclamaciones, pérdidas, lesiones o daños y perjuicios de cualquier tipo que surjan de cualquier relación entre los usuarios, ya sean Embajadores y/o alumnos.
                        </p>
                        <p class="text text-legal">
                            Asimismo, si el usuario considera que dentro de los servicios de Xpeaking existe contenido ofensivo, racista, sexista, indecente o censurable, puede solicitar restringir este contenido en su campus virtual en CONFIGURACIONES / RESTRICCIONES o solicitar directamente a la compañía su censura. Para ello, debe ponerse en contacto con nosotros a través del correo hola@xpeaking.com con el asunto INFORMACIÓN CENSURABLE. Además, en la medida en que lo permita la legislación aplicable, Xpeaking se exime de toda responsabilidad por el acceso del usuario al contenido remitido o por el uso que haga de él.
                        </p>
                        <p class="text text-legal">
                            Dentro de nuestros servicios, puede haber acceso a videos y/o enlaces a Plataformas de terceros ("Plataformas de terceros"), directamente o por medio de los usuarios Embajadores. Xpeaking no respalda ninguna de estas Plataformas de terceros, ni las controla en modo alguno; por consecuencia, Xpeaking se exime de toda responsabilidad por las Plataformas de terceros. El Usuario debe tomar las medidas adecuadas para decidir si resulta apropiado acceder a alguna Plataforma de terceros, así como para proteger sus datos personales y su privacidad en ella.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            7. Conducta
                        </h5>
                        <p class="text text-legal">
                            Solamente podrás acceder a los servicios con fines lícitos. Debes respetar la propiedad intelectual, copyright y demás derechos involucrados en los videoclips y toda la información existente propias de Xpeaking (Plataforma, redes sociales y demás). Eres el único responsable del conocimiento y cumplimiento de todas las leyes, reglas y regulaciones relacionadas con el uso de los servicios. Aceptas utilizar los servicios y contenidos en nuestra plataforma para mejorar y acelerar tu aprendizaje de idiomas, pero eres tú el único responsable de tener éxito en este proceso de aprendizaje, te recomendamos utilizar nuestra plataforma diariamente y seguir adecuadamente nuestra metodología.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            8. Obligaciones específicas de los Embajadores
                        </h5>
                        <p class="text text-legal">
                            Consulte en: <a class="xp-link" href="/acuerdo-embajador" target="_blank">Acuerdo con el usuario Embajador.</a>
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            9. Obligaciones específicas de los Usuarios: Alumnos y Embajadores
                        </h5>
                        <p class="text text-legal">
                            Como estudiante, representa, garantiza y asegura que:
                        </p>
                        <p class="text text-legal pl-4">
                            1. Ha leído y entendido la información de precios (véase la sección sobre precios a continuación) antes de usar los servicios o suscribirse, a la que conviene en someterse.
                        </p>
                        <p class="text text-legal pl-4">
                            2. Es mayor de dieciocho (18) años o, en caso contrario, utiliza los servicios con la participación, la supervisión y la aprobación de un progenitor o tutor legal.
                        </p>
                        <p class="text text-legal pl-4">
                            3. Se obliga a contactarse con Xpeaking a través del correo <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> con el asunto PROPIEDAD, cuando algún contenido visual y/o escrito altera la propiedad intelectual de un autor, cuando el contenido no brinda una información veraz o es un contenido que amerita restringirse.
                        </p>
                        <p class="text text-legal pl-4">
                            4. Se obliga a abstenerse de subir, publicar o transmitir de otro modo publicidad no solicitada o no autorizada, materiales promocionales, correo basura, spam, cartas en cadena, estafas piramidales o cualquier otra forma de captación (comercial o de otro tipo) por medio de nuestra plataforma, redes sociales y cualquiera de nuestros servicios.
                        </p>
                        <p class="text text-legal pl-4">
                            5. Se obliga a abstenerse de publicar o suministrar información o contenido inapropiado, ofensivo, racista, sexista, pornográfico, falso, engañoso, incorrecto, ilegítimo, difamatorio, calumnioso o que incite al odio.
                        </p>
                        <p class="text text-legal pl-4">
                            6. Se obliga a abstenerse de copiar contenido y servicios de la empresa, modificarlos, reproducirlos, distribuirlos, exhibirlos o ejecutarlos públicamente, comunicarlos al público, crear obras derivadas de ellos, usarlos o explotarlos de cualquier otra forma, excepto en la medida en que lo permita la empresa con autorización pertinente, según corresponda.
                        </p>
                        <p class="text text-legal pl-4">
                            7. Se obliga a abstenerse de suplantar la identidad de otro usuario o de acceder sin permiso a la cuenta de otro usuario o de sus padres
                        </p>
                        <p class="text text-legal pl-4">
                            8. Se obliga a abstenerse de introducir virus, gusanos, programas espía o cualquier código, archivo o programa informático que pueda o pretenda dañar, sacar provecho o controlar el funcionamiento de cualquier hardware, software o equipo de telecomunicaciones o cualquier otro aspecto de los servicios o su funcionamiento, así como de usar métodos de extracción o búsqueda, robots o cualquier otro medio automatizado para acceder a los servicios.
                        </p>
                        <p class="text text-legal pl-4">
                            9. Se obliga a abstenerse de solicitar datos personales a los estudiantes que atenten contra su privacidad.
                        </p>
                        <p class="text text-legal pl-4">
                            10. Se obliga a actuar el usuario embajador conforme al código de ética, y cuidar la imagen de Xpeaking. El usuario Embajador no podrá aludir que es representante, asesor o tiene algún cargo en Xpeaking; asimismo, no podrá realizar de ningún modo invitaciones a los usuarios de Xpeaking para ser parte de otra compañía o para ser parte de otro equipo, ni podrá promocionar y/o publicitar los servicios de plataformas de terceros.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            10. Registro
                        </h5>
                        <p class="text text-legal">
                            Para utilizar nuestros Servicios, el Usuario debe suscribirse brindando todos sus datos, creando una cuenta con su respectiva contraseña. La información facilitada durante el proceso de registro nos ayuda a ofrecer prestaciones de contenido, atención al cliente o gestión de red y otros servicios de interés.
                        </p>
                        <p class="text text-legal">
                            La información brindada por el usuario es confidencial según nuestra Política de Privacidad. Sin embargo; el Usuario es el único responsable tanto de la confidencialidad de la Cuenta, el nombre de usuario y la contraseña (de forma colectiva, "Cuenta") como de todas las actividades asociadas a ella. El Usuario garantiza que la información de la Cuenta es precisa y veraz en todo momento. Se obliga a: a) No otorgar su usuario y/o contraseña a otras personas. b) Notificarnos de inmediato sobre cualquier uso no autorizado de la Cuenta y cualquier otro problema de seguridad y a c) Salir siempre de la Cuenta (cerrar sesión) cuando termine de usar los Servicios. En la medida en que lo permita la legislación aplicable, nos eximimos de toda responsabilidad por cualquier pérdida o daño que se deriven del incumplimiento de los requisitos anteriores o del uso de la Cuenta, con conocimiento o sin él del Usuario, y que se produzcan antes de comunicarnos el acceso a la Cuenta sin autorización. Asimismo, el usuario se obliga a abstenerse de transferir su usuario a otra persona y de usar cuentas ajenas sin el permiso del titular.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            11. Contenido, licencias y permisos
                        </h5>
                        <p class="text text-legal">
                            Se denomina "contenido" al conjunto de software, tecnología, diseños, materiales, información, comunicaciones, metodología, texto, gráficos, enlaces, elementos artísticos electrónicos, animaciones, ilustraciones, material gráfico, clips de audio, clips de vídeo, fotos, imágenes, opiniones, ideas y otros datos o contenido o materiales susceptibles de protección con derechos de autor, incluidas su selección y su disposición. Se denomina "Contenido de la empresa" el contenido que proporciona Xpeaking en relación con los Servicios, entre otros, el software, los productos y el sitio. Se denomina "Contenido remitido" al contenido que el Usuario transmite a Xpeaking o que carga o publica en ellos. El contenido sigue siendo propiedad de la persona física o jurídica que lo suministre (o de sus filiales o de terceros proveedores) y está protegido, sin limitación alguna, por las diversas leyes de derechos de autor y de propiedad intelectual del Perú y de otros países. Por la presente, el Usuario garantiza que dispone de todas las licencias y autorizaciones y de todos los derechos y permisos indispensables para otorgar a Xpeaking los derechos estipulados en estos Términos y Condiciones con respecto al contenido remitido. Garantiza, asimismo, que Xpeaking no debe obtener licencias, autorizaciones, derechos ni permisos, ni abonar pagos a terceros por el uso o la explotación del contenido remitido conforme a lo dispuesto en estos Términos y Condiciones, por los que exime a Xpeaking de toda responsabilidad ante sí mismo o ante cualquier otra parte.
                        </p>
                        <p class="text text-legal">
                            Por la presente, Xpeaking otorga al usuario una licencia limitada e intransferible sin exclusividad para acceder por medio de los servicios al contenido remitido y al contenido de la empresa, para usarlos con fines únicamente personales y educativos, no comerciales, de acuerdo con los presentes Términos y con cualquier condición o restricción asociada a nuestros servicios. Queda expresamente prohibido cualquier otro uso sin nuestro consentimiento explícito por escrito.
                        </p>
                        <p class="text text-legal">
                            Si un usuario ha pagado su suscripción tendrá acceso a la plataforma (campus virtual) por todo un mes. Pero, si cualquier usuario no tiene ninguna actividad dentro de su cuenta durante noventa (90) días desde su último inicio de sesión, su cuenta será eliminada; es decir, si no inicia sesión durante este plazo establecido su cuenta se eliminará automáticamente y con ello todos los servicios adquiridos, su equipo de embajadores, invitados y sus ganancias; sin  reclamo ni perjuicio alguno.
                        </p>
                        <p class="text text-legal">
                            El usuario se obliga a abstenerse de reproducir, redistribuir, transmitir, ceder, vender, difundir, alquilar, compartir, prestar, modificar, adaptar, editar o licenciar cualquier contenido remitido o contenido de la empresa, así como de crear trabajos derivados o transferirlos o usarlos de ninguna otra manera, sin permiso explícito para hacerlo. Se concede una licencia para acceder al contenido remitido y al contenido de la empresa, no se vende. No obstante a lo anterior, Xpeaking se reserva el derecho de revocar y cancelar la licencia de acceso (suscripción mensual) al Contenido remitido, al Contenido de la empresa, a eliminar y bloquear cualquier usuario (estudiante o embajador).
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            12. Política de Propiedad Intelectual
                        </h5>
                        <p class="text text-legal">
                            XPEAKING RESPETA TODAS LAS LEYES DE DERECHOS DE AUTOR Y PRIVACIDAD, ASÍ COMO OTRAS RELATIVAS AL CONTENIDO Y LA INFORMACIÓN, POR LO QUE NO TOLERA SU VULNERACIÓN. SIN PERJUICIO DE LO ANTERIOR, XPEAKING NO CRIBA EL CONTENIDO REMITIDO, DE MODO QUE EL USUARIO LO EMPLEA BAJO SU RESPONSABILIDAD. XPEAKING SE EXIME DE TODA RESPONSABILIDAD POR DICHO USO. EN PARTICULAR, NINGUNA OPINIÓN, PUBLICACIÓN O APARICIÓN DEL CONTENIDO REMITIDO DISPONIBLE CON LOS SERVICIOS SE PUEDE CONSIDERAR COMO AVAL O GARANTÍA DE QUE NO VULNERAN NINGUNA LEY DE DERECHOS DE AUTOR, DE PRIVACIDAD O EN OTRA MATERIA, NI TAMPOCO DE SU PRECISIÓN, SU UTILIDAD NI SU IDONEIDAD PARA UN FIN CONCRETO. Se deja constancia, que todo contenido remitido (video, texto, imágenes y otros) ha sido brindado por el usuario a Xpeaking, prestando su consentimiento para la publicación en nuestra plataforma y/o redes sociales. Si el usuario, o una tercera persona cree que su contenido remitido vulnera alguna ley o normativa, es impreciso o supone cualquier tipo de riesgo para terceros, tiene la obligación de tomar las medidas oportunas para subsanar la situación. Si cree que el contenido remitido por terceros o el contenido de la empresa vulnera alguna ley o normativa (incluidas, entre otras, las leyes sobre derechos de autor), debe comunicarlo a Xpeaking a través del correo <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> con el asunto CONTENIDO INADECUADO.
                        </p>
                        <p class="text text-legal">
                            Todos los derechos no reconocidos expresamente en estas condiciones pertenecen a los propietarios del contenido y estos términos no conceden ninguna licencia implícita.
                        </p>
                        <p class="text text-legal">
                            Si así lo desea, el Usuario nos puede enviar por su cuenta ideas relativas a promociones, productos, servicios, aplicaciones, tecnologías o procesos, entre otros (de forma colectiva, "Propuestas"). El Usuario se obliga a abstenerse de transmitir Propuestas a los Servicios o a Plataformas de terceros, o por medio de ellos, o a Nosotros por correo electrónico si estima que están protegidas por la confidencialidad o por derechos de titularidad. El Usuario conviene en aceptar que no estamos obligados a conceder a las Propuestas un tratamiento confidencial ni privado. El Usuario tiene plena responsabilidad por todas las Propuestas y cualquier contenido que remite. El Usuario acepta que, al enviarnos cualquier información, contenido y/o propuestas (videos, audios, conceptos, ideas o conocimientos especializados, sin exclusión de otras), nos otorga por la presente una licencia transferible, perpetua, mundial, sin exclusividad, exenta de derechos y susceptible de sublicencia para emplear, reproducir, distribuir, vender, explotar y mostrar las propuestas, así como preparar obras derivadas de ellas, en relación con los Servicios y la actividad tanto de Xpeaking como de sus derechohabientes, lo que incluye sin restricciones la promoción y la redistribución de las Propuestas y sus obras derivadas en todo o en parte, con cualquier formato multimedia y por cualquier canal de comunicación conocido o por desarrollar, sin que devenguen pagos ni cargas a favor del Usuario ni de ninguna otra parte. No tenemos obligación de valorar, revisar, ni utilizar ninguna Propuesta.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            13. Precios, pagos e impuestos
                        </h5>
                        <p class="text text-legal">
                            <span class="font-weight-bold mr-1">1. Precios.- </span> Los precios están basado en una suscripción mensual de S/29 (veintinueve soles) para tener acceso a nuestra plataforma con ciertas imitaciones de presentarse algún servicio PREMIUM. Los estudiantes se obligan a abonar los montos respectivos por los servicios que adquieran y, por la presente, nos autorizan a cargarlas en su tarjeta de crédito y/o débito. Si se rechaza la tarjeta, el usuario se compromete a pagarnos el monto en un plazo máximo de tres (3) días desde nuestra notificación.
                        </p>
                        <p class="text text-legal">
                            <span class="font-weight-bold mr-1">2. Pagos.-</span> Todos los pagos se hacen a Xpeaking S.A.C. Si el usuario hace clic en el botón de compra, el usuario confirma su deseo de incluirlos de inmediato en su cuenta y, por la presente, renuncia a todos los derechos de cancelación que le amparen en virtud de las leyes aplicables. Xpeaking se exime de toda responsabilidad o daño que pueda ocasionar bloquear, censurar o eliminar la cuenta de un usuario que no vaya de acorde a los fines de la empresa.
                        </p>
                        <p class="text text-legal">
                            <span class="font-weight-bold mr-1">3. Impuestos.- </span>En caso de que la venta o entrega de cualquier contenido remitido a un estudiante en la República del Perú, está sujeto al impuesto sobre el valor añadido ("IGV"), bajo la ley aplicable, Xpeaking cobrará y enviará el IGV a las autoridades fiscales competentes. Xpeaking puede bajo su propio criterio aumentar el precio de la suscripción. Aceptas indemnizar y exonerar de responsabilidad a Xpeaking ante cualquier reclamación de las autoridades fiscales por el impago del IGV, específicamente ante la exportación del servicio, así como por cualquier sanción y/o intereses devengados.
                        </p>
                        <p class="text text-legal">
                            En las ventas de suscripciones a países que no sean en la República del Perú, tú eres el responsable de remitir los impuestos a la autoridad fiscal competente (que puede ser diferente de la autoridad fiscal de tu localidad). Xpeaking no puede ofrecerte asesoramiento fiscal, por lo que deberás consultar con tu asesor fiscal.
                        </p>
                        <p class="text text-legal">
                            En el caso de los embajadores, para el canje de sus puntos por dinero tendrá que presentar Factura por concepto de comisiones. Asimismo, Xpeaking está facultado para hacer las retenciones tributarias según sea el caso.
                        </p>
                        <p class="text text-legal">
                            <span class="font-weight-bold mr-1">4. Reembolsos.-</span> Xpeaking no ofrece a los estudiantes una garantía de devolución del dinero. Si el usuario no está satisfecho puede cancelar su suscripción mensual simplemente no abonando el dinero.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            14. Marcas registradas
                        </h5>
                        <p class="text text-legal">
                            Las marcas registradas, las marcas de servicio y los logotipos (en lo sucesivo y de forma colectiva, "Marcas registradas") utilizadas y mostradas en los Servicios o en el Contenido de la empresa son marcas comerciales registradas de Xpeaking, de nuestros proveedores o de terceros y están protegidas por leyes sobre marcas registradas tanto de Perú como de otros países. Están reservados todos los derechos. El Usuario se obliga a abstenerse de alterar u ocultar las marcas registradas o de crear enlaces a ellas sin nuestra aprobación previa.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            15. Descargo de responsabilidad de garantía
                        </h5>
                        <p class="text text-legal">
                            LOS SERVICIOS Y TODO EL CONTENIDO DE LA COMPAÑÍA SE OFRECEN TAL CUAL, SIN GARANTÍAS DE NINGUNA CLASE, NI EXPLÍCITAS NI IMPLÍCITAS. EN LA MEDIDA EN QUE LO PERMITA LA LEGISLACIÓN APLICABLE, TANTO XPEAKING COMO SUS ANUNCIANTES, PATROCINADORES Y EMBAJADORES EXCLUYEN TODAS LAS GARANTÍAS, EXPLÍCITAS O IMPLÍCITAS, ENTRE OTRAS, LAS GARANTÍAS IMPLÍCITAS DE TITULARIDAD, PRECISIÓN, INEXISTENCIA DE VULNERACIÓN, POSIBILIDAD DE COMERCIALIZACIÓN E IDONEIDAD PARA UN FIN PARTICULAR, ASÍ COMO CUALQUIER GARANTÍA QUE SE ESGRIMA DURANTE LA TRAMITACIÓN, LA EJECUCIÓN O EL USO MERCANTIL. TANTO XPEAKING COMO SUS ANUNCIANTES, PATROCINADORES Y EMBAJADORES SE ABSTIENEN DE GARANTIZAR UN USO DE LOS SERVICIOS SEGURO, SIN INTERRUPCIONES NI ERRORES DEL SISTEMA, LA CORRECCIÓN DE LOS DEFECTOS O LA AUSENCIA DE VIRUS, LOS HACKERS U OTROS COMPONENTES DAÑOSOS EN LOS SERVICIOS, LOS ELEMENTOS REMITIDOS, LOS SERVIDORES DONDE SE ALOJAN LOS SERVICIOS O CUALQUIER OTRO SERVICIO DISPONIBLE EN PLATAFORMAS DE TERCEROS. NINGUNA OPINIÓN, SUGERENCIA NI DECLARACIÓN DE XPEAKING NI DE SUS ANUNCIANTES, PATROCINADORES Y EMBAJADORES  O VISITANTES, MANIFESTADA POR MEDIO DE LOS SERVICIOS, EN PLATAFORMAS DE TERCEROS O DE CUALQUIER OTRO MODO, CONSTITUYE GARANTÍA DE NINGUNA CLASE. EL USUARIO EMPLEA LOS SERVICIOS, INCLUIDOS, SIN EXCLUSIÓN ALGUNA, LOS SERVICIOS PRESTADOS EN PLATAFORMAS DE TERCEROS, BAJO SU EXCLUSIVA RESPONSABILIDAD.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            16. Limitación de responsabilidad
                        </h5>
                        <p class="text text-legal">
                            TANTO XPEAKING COMO SUS SOCIOS, REPRESENTANTES, ANUNCIANTES, TRABAJADORES SE EXIMEN DE TODA RESPONSABILIDAD POR CUALQUIER DAÑO O PERJUICIO INDIRECTO, FORTUITO, CONSECUENTE, ESPECIAL, EJEMPLAR, PUNITIVO O DE OTRA NATURALEZA (INCLUIDOS, ENTRE OTROS, DAÑOS Y PERJUICIOS POR PÉRDIDA DE NEGOCIOS, ROBO DE USUARIOS, DATOS O LUCRO CESANTE) QUE SE PRODUZCA POR CIRCUNSTANCIAS CONTRACTUALES, NEGLIGENCIA, RESPONSABILIDAD OBJETIVA U OTRO MOTIVO EN RELACIÓN CON LOS SERVICIOS, LOS MATERIALES, LOS ELEMENTOS REMITIDOS, ASÍ COMO CON EL CÓDIGO, O LOS SERVICIOS ADQUIRIDOS EN NUESTRA PLATAFORMA.
                        </p>
                        <p class="text text-legal">
                            XPEAKING SE EXIME DE TODA RESPONSABILIDAD COMO PRODUCTO DE LA DISCONFORMIDAD CON LOS SERVICIOS, LOS MATERIALES (INCLUIDOS PRODUCTOS O SERVICIOS DISPONIBLES POR MEDIO DE PLATAFORMAS DE TERCEROS), LOS ELEMENTOS REMITIDOS O LOS SITIOS WEB ENLAZADOS CONSISTE EN DEJAR DE UTILIZAR EL COMPONENTE QUE CORRESPONDA. NINGUNA COMUNICACIÓN DE CUALQUIER CLASE ENTRE EL USUARIO Y XPEAKING O SUS REPRESENTANTES CONSTITUYE LA RENUNCIA A NINGUNA LIMITACIÓN DE RESPONSABILIDAD AQUÍ ESTIPULADA, NI TAMPOCO NINGUNA GARANTÍA ADICIONAL A LAS QUE CONSTEN DE MANERA EXPRESA EN LOS TÉRMINOS. EL LÍMITE PECUNIARIO ES LA DEVOLUCIÓN DE UN (1) MES DEL PRECIO DE LA SUSCRIPCIÓN  INDICADO EN ESTE INSTRUMENTO NO SE INCREMENTA AUNQUE SE INTERPONGA VARIAS RECLAMACIONES. EL USUARIO Y EMBAJADOR CONVIENE EN ACEPTAR QUE LAS EXCLUSIONES DE DAÑOS Y PERJUICIOS SEÑALADAS EN ESTOS TÉRMINOS DE USO SON DE APLICACIÓN AUN CUANDO EL RECURSO NO CUMPLA SU OBJETIVO ESENCIAL.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1"> Imprecisiones. </span>Cabe la posibilidad de que los Servicios prestados en Plataformas de terceros incluyan errores o imprecisiones o bien información o materiales que vulneren los presentes Términos. También cabe la posibilidad de que terceros realicen sin autorización alteraciones en los Servicios disponibles en Plataformas de terceros. Aunque Xpeaking trata de garantizar la integridad de estos, no ofrece garantía alguna en cuanto a su compleción, exactitud o calidad. Si se produce cualquier situación fortuita negativa al uso de los Servicios, rogamos al Usuario que envíe un mensaje a hola@xpeaking.com  con el asunto "ERROR EN EL SISTEMA" y, si es posible, en el cuerpo del correo, la descripción de los servicios que se deben comprobar y su dirección URL exacta dentro de nuestro conjunto de Servicios o en la Plataforma de terceros, además de datos de contacto suficientes. Nos comprometemos a abordar el problema en cuanto sea factible. Si la reclamación atañe a la vulneración de los derechos de autor, debe enviarnos un mensaje a hola@xpeaking.com con el asunto “PROPIEDAD INTELECTUAL”.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Interrupción del sistema:</span> Xpeaking programa paradas periódicas del sistema de los Servicios con distintos fines, entre ellos, el de mantenimiento. Aparte, se pueden producir interrupciones imprevistas. El Usuario conviene en eximir a Xpeaking de toda responsabilidad sin compensación alguna por lo siguiente: a) La ausencia de disponibilidad de los Servicios, incluidos los ofrecidos mediante Plataformas de terceros; b) Cualquier pérdida de materiales, datos, transacciones, retiros, transferencias o información que resulte de dichas interrupciones del sistema o de alguna persona; c) La demora, los errores o el incumplimiento en la entrega de datos, transacciones, información o materiales que resulten de dichas interrupciones del sistema; o d) Cualquier interrupción provocada por terceros, incluidas, a título meramente enunciativo, las empresas o los servidores que alojan los Servicios, proveedores de servicios de Internet, Plataformas de terceros o redes e instalaciones de Internet.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            17. Exoneración
                        </h5>
                        <p class="text text-legal">
                            Por la presente, el usuario exonera tanto a Xpeaking como a sus filiales, consejeros, responsables, representantes, socios, empleados, licenciadores y terceros proveedores de toda responsabilidad por pérdidas, robo, bloqueos, suspensión y/o eliminación de cuentas de usuarios; también sobre  gastos, daños y perjuicios, costes, reclamaciones y demandas razonablemente previsibles (con inclusión de minutas proporcionadas de asesoramiento jurídico y costes y gastos relacionados) que surjan por su incumplimiento de cualquier manifestación o garantía estipulada en este documento. Nos reservamos el derecho de asumir, a nuestras expensas, la defensa y el control exclusivos de cualquier asunto sujeto a resarcimiento por parte del usuario conforme, y en tal caso, el usuario se obliga a prestar una colaboración plena en cuanto sea sensato en la defensa y en la reivindicación de los argumentos oportunos.
                        </p>
                        <p class="text text-legal">
                            Prescripción. Cualquier reclamación o demanda justificada que surja en relación con el uso de los Servicios, los Términos o cualquier servicio o información disponible por medio de Plataformas de terceros se debe presentar en un plazo máximo de treinta (30) días calendarios después de que se produzca su motivación, aun cuando existan leyes en contrario. Si no se cursa en dicho plazo, queda descartada a perpetuidad.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            18. Rescisión
                        </h5>
                        <p class="text text-legal">
                            Xpeaking se reserva el derecho de rescindir, suspender, modificar, bloquear o eliminar, a su sola discreción, a) Contenido remitido de la empresa o Servicios, b) El acceso del Usuario a nuestros Servicios o la Cuenta en las circunstancias siguientes:
                        </p>
                        <p class="text text-legal pl-4">
                            1. Si el usuario (estudiante o embajador embajador incumple o vulnera cualquiera de estas condiciones o de las políticas aplicables que se publican en nuestros servicios cuando procede, Xpeaking se reserva el derecho de tomar las medidas oportunas enseguida, sin previa notificación. Si nos acogemos a este derecho, nos eximimos de toda responsabilidad ante el usuario por la suscripción pagada que haya adquirido o por cualquier otro uso de los servicios asociado a la cuenta. Para evitar distintas interpretaciones, el Usuario conviene en aceptar que, en ninguna circunstancia, recibirá compensación alguna, ni tendrá derecho a reembolso alguno por la pérdida del acceso a los servicios.  Asimismo, el embajador podrá ser bloqueado o eliminado dentro del sistema por las razones ya expuestas.
                        </p>
                        <p class="text text-legal pl-4">
                            2. Asimismo, nos reservamos el derecho de tomar las medidas oportunas por cualquier motivo o sin justificación. Si Nos acogemos a este derecho, a) en el caso de los Estudiantes, les reembolsamos el importe equivalente a la pérdida de acceso en un (1) mes anterior a la rescisión, siempre con sujeción a los términos de nuestra política y b) en el caso de los Embajadores, la rescisión conlleva la pérdida del derecho de tener tal calidad de usuario, en cuyo caso Xpeaking se compromete a abonarles los importes pendientes hasta la fecha de rescisión.
                        </p>
                        <p class="text text-legal pl-4">
                            3. El usuario Embajador que promueva un servicio, producto, plan de mercadeo, plan de marketing o contenido de otra compañía similar al de Xpeaking, será bloqueado y retirado de la plataforma sin compensación alguna.
                        </p>
                        <p class="text text-legal">
                            El Usuario tiene derecho a rescindir el uso de los Servicios en cualquier momento, bien dejando de acceder a ellos, bien eliminando su cuenta con el procedimiento explicado en nuestra Política de Privacidad y con sujeción a los términos estipulados en el presente instrumento. No tenemos obligación alguna de conservar su Cuenta ni el Contenido remitido durante plazo alguno, salvo el que exija la legislación aplicable. Después de la rescisión, el Usuario se compromete a dejar de utilizar los servicios y el contenido.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            19. Avisos electrónicos
                        </h5>
                        <p class="text text-legal">
                            Al usar los servicios o ponerse en contacto con Xpeaking, el usuario conviene en aceptar que Xpeaking puede comunicarse con él por medios electrónicos y/o celular por temas de seguridad, privacidad y administración que atañen al uso de los servicios o a estos Términos. Si Xpeaking tiene noticia de algún problema del sistema de seguridad, lo puede notificar publicando un aviso en los servicios o enviando un correo electrónico. Es posible que el usuario tenga derecho legal a recibir avisos de esas características por escrito; para recibirlos de manera gratuita o para anular el consentimiento para recibir avisos electrónicos, debe escribir a la dirección hola@xpeaking.com con el asunto AVISOS ELECTRÓNICOS. Se considera recibida una circular veinticuatro (24) horas después de enviar el correo electrónico, a menos que se notifique al remitente que la dirección de destino no es válida. Xpeaking también puede remitir avisos legales a la dirección postal que facilita el usuario, si lo desea, cuando utiliza alguno de los servicios; en ese caso, se considera recibido veinticuatro (24) horas después de enviar el correo electrónico, a menos que se notifique al remitente que la dirección de destino no es válida.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            20. Varios
                        </h5>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Contrato íntegro.</span>  Estos Términos y todas las políticas aplicables al Usuario que se publiquen en los Servicios constituyen el acuerdo íntegro entre las partes con respecto al objeto del presente documento y sustituyen todos los acuerdos anteriores, escritos o verbales, entre ellas con el mismo objeto.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Divisibilidad.</span> Si alguna disposición de estos Términos resulta ser ilegal, nula o inaplicable, se considera separable de los Términos, por lo que no afecta la validez ni la aplicabilidad de las disposiciones restantes.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Anulación.</span> Para anular cualquier disposición o argumento señalado en estos Términos, es imprescindible que la parte con derecho a beneficiarse de ella formalice por escrito un instrumento al respecto. El hecho de que Xpeaking no ejercite ni aplique cualquier derecho o disposición de estos Términos y Condiciones no constituye una renuncia a tal derecho o disposición.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Notificaciones.</span> Cualquier aviso o comunicación conforme con lo dispuesto en este documento se debe realizar por escrito y se debe entregar por correo electrónico certificado con acuse de recibo.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Sin compromiso de representación.</span> No debe interpretarse que nada de lo estipulado en estos Términos inviste a ninguna de las partes como socio, empresa conjunta, apoderado, representante legal, empleado ni contratista de la contraparte. Ni Xpeaking ni ninguna otra parte están facultadas para expresar manifestaciones, contraer compromisos, ni tomar ninguna medida que sean vinculantes para la contraparte, ni tampoco pueden arrogarse esa autoridad ante ningún tercero, salvo que así se estipule en este documento o lo autorice por escrito la parte vinculada.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Fuero.</span> Tanto los Términos como su uso de los Servicios se rigen por el derecho y las leyes del Estado Peruano, sin tener en consideración las preferencias ni los principios de conflicto de leyes que exigirán la aplicación del derecho de otra jurisdicción, y se consideran formalizados y aceptados en dicho estado.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            21. Acuerdo sobre arbitraje y renuncia a la participación en demandas colectivas
                        </h5>
                        <p class="text text-legal">
                            Antes de iniciar un procedimiento formal, rogamos al Usuario que se ponga en contacto con nuestro equipo de soporte en la dirección <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> . La mayoría de las controversias se resuelven por esa vía.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Acuerdo sobre arbitraje.</span> Si las partes no resuelven sus diferencias de forma amistosa, el Usuario y Xpeaking convienen en someterse a arbitraje vinculante definitivo para dirimir cualquier controversia relativa a estos Términos o cualquiera de los otros términos publicados en nuestros Servicios en cualquier momento. Esto es aplicable a cualquier clase de reclamación, amparada por cualquier doctrina.
                        </p>
                        <p class="text text-legal">
                            Además, si Usted o Xpeaking presentan una reclamación ante un juzgado que debe someterse a arbitraje, pero cualquiera de nosotros rechaza arbitrar una reclamación que debería someterse a arbitraje, la otra parte puede pedir al juzgado que nos obligue a pasar al arbitraje para resolver la reclamación (es decir, exigir el arbitraje). Usted o Xpeaking también pueden pedir a un juzgado que suspenda un procedimiento judicial mientras haya un procedimiento de arbitraje en curso.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Prohibición de demandas colectivas.</span> Todos acordamos que solo podemos presentar reclamaciones contra la otra parte de manera individual. Eso significa lo siguiente: (a) ni Usted ni Xpeaking pueden presentar una reclamación como demandante o miembro de una demanda colectiva, demanda consolidada o demanda representativa; (b) un árbitro no puede combinar reclamaciones de más de una persona en un único caso y no puede presidir ningún procedimiento de arbitraje consolidado, colectivo o representativo (a menos que ambos acordemos cambiar este punto); (c) la decisión o adjudicación de un árbitro en el caso de una persona solo puede afectar a la persona que ha presentado la reclamación, no a otros Usuarios, y no se puede utilizar para tomar decisiones sobre otras disputas con otros Usuarios. Si un juzgado decide que esta subsección sobre "Prohibición de demandas colectivas" no se puede aplicar o no es válida, toda la sección será nula y quedará invalidada, pero el resto de las Condiciones seguirá aplicándose.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Proceso de arbitraje.</span> Cualquier disputa entre Usted y Xpeaking relacionada con los Servicios que implique una reclamación por valor de menos de S/. 50.000 debe resolverse exclusivamente a través de un arbitraje vinculante sin comparecencia. Además, Usted y Xpeaking acuerdan que se aplicarán las siguientes reglas al procedimiento de arbitraje: (a) el arbitraje deberá llevarse a cabo, según lo decida la parte demandante, por teléfono, online o exclusivamente mediante remisiones por escrito; (b) el arbitraje no implicará ninguna comparecencia personal de las partes o los testigos, a menos que acuerden lo contrario mutuamente las partes; y (c) cualquier decisión sobre la adjudicación tomada por el árbitro puede presentarse en cualquier juzgado que disponga de jurisdicción competente.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            22. Información General de la Empresa
                        </h5>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Xpeaking S.A.C</span>
                        </p>
                        <p class="text text-legal mt-2">
                            RUC N°  20604297339  
                        </p>
                        <p class="text text-legal mt-2">
                            Correo Electrónico: <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a>
                        </p>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
@endsection
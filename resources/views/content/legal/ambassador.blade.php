@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => true,
                            'social' => true,
                            'footerMessage' => true,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
    <section id="about" class="xp-section py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="section-title text-center">
                        <h4 class="title xp-text-primary">
                            Acuerdo con el usuario Embajador
                        </h4>
                    </div> <!-- section title -->

                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <p class="text text-legal">
                            Estos Términos y Condiciones de “Acuerdo con el usuario Embajador”, se actualizaron por última vez el 22 de Febrero del 2020.
                        </p>
                        <p class="text text-legal">
                            ESTE ES UN ACUERDO VINCULANTE LEGAL ENTRE USTED Y XPEAKING SOCIEDAD ANÓNIMA CERRADA. AL USAR LOS SERVICIOS Y MANIFESTAR SU VOLUNTAD DE SER EMBAJADOR, ACEPTA LOS TÉRMINOS DE ESTE ACUERDO. Usted contrata con XPEAKING S.A.C., una sociedad con RUC N° 20604297339 con sede en la ciudad de Ica, Perú. Xpeaking respeta sus derechos, y le proporcionamos esta Política de Términos y Condiciones del Embajador ("Acuerdo con el usuario Embajador") para ayudarle a entender cómo usted puede crear una unidad de negocios trabajando conjuntamente con nosotros y generar ingresos extras recomendando nuestros servicios a través de nuestra sistema de puntuación. En estos Términos y Condiciones también se describen los derechos y obligaciones del Embajador sobre su cuenta usuario y Puntos.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">1. Definiciones claves</h5>
                        <p class="text text-legal">
                            "Embajador" hace referencia al usuario estudiante que paga su suscripción mensual de S/29 (incluye IGV - Impuesto general a las ventas) y que recomienda nuestros servicios invitando a sus amigos y familiares a registrarse en Xpeaking. Para ser Embajador tienes que registrar a 3 personas directamente en tu equipo (nivel 1°). El gran beneficio que tiene el EMBAJADOR es que sus puntos podrán ser canjeados por dinero, y puede utilizar estos puntos para el pago de sus facturas pendientes. Actualmente 1 punto = 0.50 céntimos. La solicitud de canje procede en las fechas indicadas en la plataforma, teniendo en consideración que el monto canjeado se depositará en tu cuenta bancaria en un plazo máximo de 5 días hábiles.
                        </p>
                        <p class="text text-legal">
                            “El Embajador” sólo tiene 3 invitaciones como máximo, pero tiene la posibilidad de solicitar el aumento de estas invitaciones, quedando a criterio de Xpeaking si acepta o deniega esta solicitud sin reclamo alguno. “El Embajador” puede realizar esta solicitud vía whatsapp al +51 939992192.
                        </p>
                        <p class="text text-legal">
                            "Puntos" hace referencia a las puntos regalos que obtiene el EMBAJADOR por cada recomendación directa e indirecta. El límite máximo para ganar puntos es hasta el nivel 10°.
                        </p>
                        <p class="text text-legal">
                            "Comisiones por canje de puntos" hace referencia al canje de puntos por dinero.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            2. Acuerdo de Puntos y Comisiones
                        </h5>
                        <p class="text text-legal">
                            El objeto del presente contrato es un acuerdo entre el Embajador y Xpeaking S.A.C. La compañía se compromete a otorgar puntos por cada recomendación efectiva; es decir, el Embajador ganará 1 punto por cada persona de su equipo que pague su Suscripción (hasta el nivel 10°). Xpeaking brindará todo el soporte técnico con la finalidad de que el Embajador administre todos sus puntos, y saber en tiempo real sus ingresos, canjes, usuarios que forman parte de su equipo, y demás. Esto puntos luego pueden ser canjeados por dinero; para ello, el usuario embajador se compromete a emitir una factura por concepto de COMISIONES.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            3. Su relación con los Embajadores
                        </h5>
                        <p class="text text-legal">
                            Los Embajadores no tienen una relación contractual laboral con Xpeaking. Entiende y acepta que indemnizará a Xpeaking por cualquier problema que surja por la manipulación inadecuada del uso de los datos relacionados con sus servicios y el de sus estudiantes, entiéndase que estos datos sólo tendrán un fin educativo dentro de nuestra plataforma.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            4. Obligaciones
                        </h5>
                        <p class="text text-legal">
                            Como Embajador, representa, garantiza y asegura que:
                        </p>
                        <p class="text text-legal pl-4">
                            1. Ha leído todos y cada uno de los términos que se estipula en el presente acuerdo y los demás documentos legales de Xpeaking;
                        </p>
                        <p class="text text-legal pl-4">
                            2. No tiene ninguna relación contractual laboral o de cualquier otra índole con Xpeaking, por lo tanto no puede actuar en nombre de aquella;
                        </p>
                        <p class="text text-legal pl-4">
                            3. Debe seguir la misión y visión planteada por Xpeaking, brindando siempre información veraz y clara de nuestra Plataforma de Idiomas, sin añadir falsas promesas;
                        </p>
                        <p class="text text-legal pl-4">
                            4. Debe cuidar los servicios que presta Xpeaking, ni infringir o malversar los derechos de propiedad intelectual de nuestra empresa o de un tercero;
                        </p>
                        <p class="text text-legal pl-4">
                            5. No publicará ni proporcionará a Xpeaking ni a sus estudiantes ninguna información o contenido inadecuado, ofensivo, racista, lleno de odio, sexista, pornográfico, falso, engañoso, incorrecto, ilegítimo, difamatorio o calumnioso;
                        </p>
                        <p class="text text-legal pl-4">
                            6. No subirá, publicará ni transmitirá de otro modo publicidad no solicitada o no autorizada, materiales promocionales, correo basura, spam, cartas en cadena, estafas piramidales o cualquier otra forma (comercial o de otro tipo) a través de los Servicios o a cualquier Usuario. El Embajador sólo está autorizado a aplicar nuestro Plan de crecimiento o Estrategia de Marketing por whatsapp, queda totalmente prohibido publicar en redes sociales avisos, contenidos o información que promuevan al registro colectivo; porque las invitaciones se hacen de manera directa al círculo más cercano del usuario Embajador;
                        </p>
                        <p class="text text-legal pl-4">
                            7. No copiará, modificará, distribuirá, realizará ingeniería inversa, desfigurará, manchará, mutilará, saboteará o interferirá con el Contenido de la empresa y/o los Servicios u operaciones de la misma, excepto según lo permitido;
                        </p>
                        <p class="text text-legal pl-4">
                            8. No se hará pasar por otra persona ni obtendrá acceso sin autorización a la cuenta de otra persona (cualquier usuario);
                        </p>
                        <p class="text text-legal pl-4">
                            9. Su uso de los Servicios está sujeto a la aprobación de Xpeaking, que podemos conceder o denegar bajo nuestro criterio exclusivo;
                        </p>
                        <p class="text text-legal pl-4">
                            10. No introducirá ningún virus, gusano, programa espía o cualquier código, archivo o programa informático que pueda o pretenda dañar o controlar el funcionamiento de cualquier hardware, software o equipo de telecomunicaciones, o cualquier otro aspecto de los Servicios o de su funcionamiento; no extraerá, buscará ni usará un robot u otros medios automatizados de cualquier tipo para tener acceso a los Servicios;
                        </p>
                        <p class="text text-legal pl-4">
                            11. No interferirá ni evitará que otros estudiantes sean Embajadores, y se compromete a respetar a los usuarios de otros equipos.
                        </p>
                        <p class="text text-legal pl-4">
                            12. Mantendrá información precisa sobre su Cuenta; y sobre la seguridad de ella;
                        </p>
                        <p class="text text-legal pl-4">
                            13. Es mayor de 18 años o, en caso contrario, está bajo la supervisión de un apoderado o tutor legal, ha aceptado estos Términos del Embajador, así como todos nuestros términos y políticas según se publiquen en nuestros Servicios cada cierto tiempo y asumirá la responsabilidad y obligación por su desempeño y cumplimiento de este documento.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            5. Licencia para Xpeaking
                        </h5>
                        <p class="text text-legal">
                            Por la presente otorga a Xpeaking un derecho y licencia exclusiva para reproducir, distribuir, representar públicamente, ofrecer, comercializar, usar y explotar su imagen y voz a través de nuestros servicios y redes sociales cuando nos envían testimonios, gane un viaje, premio, sorteo, dinero y/o cualquier otro premio. Para obtener más información sobre cómo podemos usar lo precitado, consulte nuestra Política de privacidad.
                        </p>
                        <p class="text text-legal">
                            En caso de que el Embajador utilice el Set de Grabación de Xpeaking para la grabación, edición y elaboración de un contenido, por la presente otorga a Xpeaking un derecho y licencia exclusiva e ilimitada para reproducir, distribuir, representar públicamente, ofrecer, comercializar, usar y explotar todo este contenido a través de nuestros Servicios y Redes Sociales. Lo mismo se aplicará para toda la información remitida por parte de cualquier usuario, sea alumno o embajador.
                        </p>
                        <p class="text text-legal">
                            Por la presente, en ambos casos acepta que podamos manejar todo o una parte del Contenido (incluso el contenido remitido) para gestionar y ejecutar nuestro Programa de Marketing y/o Programa de Recompensas; asimismo, para controlar la calidad y entregar, comercializar, promocionar, hacer demostraciones y gestionar los servicios, concede permiso a Xpeaking para usar su nombre personal o corporativo, retrato, imagen o voz, y renuncia a todos los derechos de privacidad, publicidad o cualquier otro derecho de naturaleza semejante, hasta donde lo permita la ley vigente.
                        </p>
                        <p class="text text-legal">
                            Xpeaking realizará todos los esfuerzos corporativos para que usted como alumno y Embajador tenga éxito; sin embargo, no garantiza ningún nivel de éxito mínimo en relación con los Programas de marketing y/o Programa de Recompensas. Es usted el único responsable de su éxito y prosperidad.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            6. Suscripción
                        </h5>
                        <p class="text text-legal">
                            La suscripción es mensual y su precio es de S/29 (veintinueve soles). Acepta que Xpeaking puede realizar el cambio de precio cuando lo considere oportuno.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            7. Canjes y Pagos
                        </h5>
                        <p class="text text-legal">
                            Xpeaking se compromete a canjear los puntos de los EMBAJADORES por dinero u otros premios adicionales; pero el embajador entiende que hay premios que son de una empresa proveedora; por lo cual, Xpeaking no tiene dominio ni jurisdicción sobre sus servicios brindados; por lo tanto, Xpeaking no se responsabiliza por las controversias o incomodidades que el embajador pueda tener como consecuencia del provecho de estos premios, viajes, artículos, etc. Xpeaking se compromete a canjear sus puntos por dinero, considerando que 1 punto es igual a 0.50 céntimos. El pago se realizará de forma oportuna y puntual con un plazo no mayor de cinco (5) días hábiles.
                        </p>
                        <p class="text text-legal">
                            Para que Xpeaking pueda pagarle en el plazo debido, debe brindar su “Factura” disponer de una cuenta bancaria o manifestar los datos personales requeridos según la entidad bancaria, cuando El Embajador no se encuentra en el Perú y se proceda con el envío de dinero. El envío de dinero será en moneda dólares americanos. Xpeaking no se responsabiliza por la información brindada por su persona en caso haya digitado mal su cuenta bancaria. Asimismo, Xpeaking no se responsabiliza por las comisiones deducibles por las transferencias, detracciones y/o envíos de dinero. Usted es el responsable de proporcionar a su organismo recaudador los impuestos correspondientes, y usted es responsable de determinar si es apto para ser pagado por una empresa peruana.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            8. Reembolsos
                        </h5>
                        <p class="text text-legal">
                            En el caso de error personal o tecnológico, sea el evento de otorgarle más puntos, un premio o viaje que no le correspondía, un depósito mal efectuado, depositarle más dinero, o de otra índole, entiende y acepta que está en la obligación de reembolsar dicho monto; caso contrario, será bloqueada su cuenta usuario.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            9. Impuestos
                        </h5>
                        <p class="text text-legal">
                            Entiende y acepta que usted es responsable de cualquier impuesto sobre sus ingresos. Nos reservamos el derecho a retener el pago si no recibimos la documentación tributaria correspondiente (Factura) sólo para Embajadores en el Perú. Xpeaking no puede ofrecerle asesoramiento fiscal, por lo que deberá consultar con su asesor fiscal o contador.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            10. Eliminación de Su Cuentao
                        </h5>
                        <p class="text text-legal">
                            Si quieres eliminar tu cuenta de usuario, puedes hacerlo enviando un correo a holaxpeaking.com con el asunto ELIMINAR CUENTA. Haremos todo lo posible desde el punto de vista comercial para realizar los pagos restantes programados que se te deban antes de eliminar tu cuenta. Por el presente, entiendes y aceptas que, si solicitas primero la eliminación de tu cuenta antes que el canje de tus puntos, no tendrás derecho a éste último. Xpeaking se reserva el derecho de eliminar la cuenta o transferirlo a otro usuario para un mejor provecho.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            11. Modificaciones de estos Términoss
                        </h5>
                        <p class="text text-legal">
                            Ocasionalmente, es posible que actualicemos estos términos para Embajadores a fin de aclarar o reflejar prácticas nuevas o diferentes, como cuando agregamos nuevas características. Xpeaking se reserva el derecho de modificar estas Condiciones (Acuerdo con el usuario Embajador) o aplicarles cambios en cualquier momento. Si aplicamos cambios importantes, se lo informaremos con una anticipación no mayor a setenta y dos (72) horas a través de medios eficaces, como un aviso por correo electrónico a la dirección especificada en su Cuenta o una notificación en nuestra Plataforma Educativa Online. Otras modificaciones entrarán en vigencia un (1) día después de su publicación, a menos que se indique lo contrario. Si continúa usando los servicios después de la fecha de entrada en vigencia de cualquier cambio, dicho acceso o uso será considerado una aceptación a los términos del “Acuerdo del Usuario Embajador”. Las Condiciones para Instructores modificadas prevalecen sobre las anteriores.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            12. Varios
                        </h5>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Contrato íntegro.</span> Estos Términos y todas las políticas aplicables al usuario Embajador que se publiquen en los Servicios constituyen el acuerdo íntegro entre las partes con respecto al objeto del presente documento y sustituyen todos los acuerdos anteriores, escritos o verbales, entre ellas con el mismo objeto.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Divisibilidad.</span> Si alguna disposición de estos Términos resulta ser ilegal, nula o inaplicable, se considera separable del Acuerdo del Usuario Embajador, por lo que no afecta la validez ni la aplicabilidad de las disposiciones restantes.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Anulación y/o Modificación.</span> Para anular y/o modificar cualquier disposición, término o condición establecida en el presente documento, es imprescindible que la parte con derecho a beneficiarse de ella se comunique con nosotros a la dirección electrónica <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> con el asunto ANULACIÓN Y/O MODIFICACIÓN. El hecho de que Xpeaking no ejercite ni aplique cualquier derecho o disposición de los Términos y Condiciones del Acuerdo del Usuario Embajador no constituye una renuncia a tal derecho o disposición.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Notificaciones.</span> Cualquier aviso o comunicación conforme con lo dispuesto en este documento se debe realizar por escrito y se debe entregar por correo electrónico certificado con acuse de recibo.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Sin compromiso de representación.</span> No debe interpretarse que nada de lo estipulado en este documento inviste a ninguna de las partes como socio, empresa conjunta, apoderado, representante legal, empleado ni contratista de la contraparte. Ni Xpeaking ni ninguna otra parte están facultadas para expresar manifestaciones, contraer compromisos, ni tomar ninguna medida que sean vinculantes para la contraparte, ni tampoco pueden arrogarse esa autoridad ante ningún tercero, salvo que así se estipule en este documento o lo autorice por escrito la parte vinculada.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            13. Acuerdo sobre arbitraje y renuncia a la participación en demandas colectivas
                        </h5>
                        <p class="text text-legal">
                            Antes de iniciar un procedimiento formal, rogamos al Embajador se ponga en contacto con nuestro equipo de soporte en la dirección <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a>, teniendo en consideración, que la mayoría de las controversias se resolverán por esa vía.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Acuerdo sobre arbitraje.</span> Si las partes no resuelven sus diferencias de forma amistosa, el Embajador y Xpeaking convienen en someterse a arbitraje vinculante definitivo para dirimir cualquier controversia relativa del Acuerdo del Usuario Embajador o cualquiera de los otros términos publicados en nuestros Servicios en cualquier momento. Esto es aplicable a cualquier clase de reclamación, amparada por cualquier doctrina.
                        </p>
                        <p class="text text-legal">
                            Además, si Usted o Xpeaking presentan una reclamación ante un juzgado que debe someterse a arbitraje, pero cualquiera de nosotros rechaza arbitrar una reclamación que debería someterse a arbitraje, la otra parte puede pedir al juzgado que nos obligue a pasar al arbitraje para resolver la reclamación (es decir, exigir el arbitraje). Usted o Xpeaking también pueden pedir a un juzgado que suspenda un procedimiento judicial mientras haya un procedimiento de arbitraje en curso.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Prohibición de demandas colectivas.</span> Todos acordamos que solo podemos presentar reclamaciones contra la otra parte de manera individual. Eso significa lo siguiente: (a) ni Usted ni Xpeaking pueden presentar una reclamación como demandante o miembro de una demanda colectiva, demanda consolidada o demanda representativa; (b) un árbitro no puede combinar reclamaciones de más de una persona en un único caso y no puede presidir ningún procedimiento de arbitraje consolidado, colectivo o representativo (a menos que ambos acordemos cambiar este punto); (c) la decisión o adjudicación de un árbitro en el caso de una persona solo puede afectar a la persona que ha presentado la reclamación, no a otros Instructores, y no se puede utilizar para tomar decisiones sobre otras disputas con otros Embajadores. Si un juzgado decide que esta subsección sobre "Prohibición de demandas colectivas" no se puede aplicar o no es válida, toda la sección será nula y quedará invalidada, pero el resto de las Condiciones seguirá aplicándose.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mx-1">Proceso de arbitraje.</span> Cualquier disputa entre Usted y Xpeaking relacionada con los Servicios que implique una reclamación por valor de menos de S/. 50.000 debe resolverse exclusivamente a través de un arbitraje vinculante sin comparecencia. Además, Usted y Xpeaking acuerdan que se aplicarán las siguientes reglas al procedimiento de arbitraje: (a) el arbitraje deberá llevarse a cabo, según lo decida la parte demandante, por teléfono, online o exclusivamente mediante remisiones por escrito; (b) el arbitraje no implicará ninguna comparecencia personal de las partes o los testigos, a menos que acuerden lo contrario mutuamente las partes; y (c) cualquier decisión sobre la adjudicación tomada por el árbitro puede presentarse en cualquier juzgado que disponga de jurisdicción competente.
                        </p>
                        <p class="text text-legal">
                            Entiéndase que, este Acuerdo con el Usuario Embajador inicia cuando Xpeaking publica y/o pone a disposición sus servicios a través del lanzamiento de su plataforma educativa online, a partir de ese momento el Embajador y Xpeaking se obligan a cumplir con cada uno de estos términos y condiciones (“Acuerdo del Usuario Embajador”).
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            14. Información General de la Empresa
                        </h5>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Xpeaking S.A.C</span>
                        </p>
                        <p class="text text-legal mt-2">
                            RUC N°  20604297339  
                        </p>
                        <p class="text text-legal mt-2">
                            Correo Electrónico: <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a>
                        </p>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
@endsection
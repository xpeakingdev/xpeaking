@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => true,
                            'social' => true,
                            'footerMessage' => true,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
    <section id="about" class="xp-section py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="section-title text-center">
                        <h4 class="title xp-text-primary">
                            Política de Privacidad
                        </h4>
                    </div> <!-- section title -->

                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <p class="text text-legal">
                            Esta Política de privacidad se actualizó por última vez el 22 de Febrero del 2020.
                        </p>
                        <p class="text text-legal">
                            Usted contrata con Xpeaking S.A.C., una sociedad con sede en la ciudad de Ica, Perú. Xpeaking respeta sus derechos de privacidad e información personal, y le proporcionamos esta Política de Privacidad ("Política de Privacidad") para ayudarle a entender cómo recopilamos, utilizamos, compartimos, almacenamos  y reservamos la información que obtenemos de usted cuando utiliza nuestros servicios. Esta Política de privacidad se aplica a su uso de los servicios. En este documento, el término "uso" se refiere a las actividades de uso, acceso, instalación, inicio de sesión, conexión, descarga, visita o navegación relacionadas con los todos nuestros servicios. Además, cualquier información relacionada con la asistencia de nuestra metodología de idiomas, eventos, reuniones virtuales y presenciales entre coaches, embajadores y/o estudiantes. Asimismo, se indica en este documento "Información de nuestra plataforma". Xpeaking le permite suscribirse pagando una mensualidad de S/29 (veintinueve soles).
                        </p>
                        <p class="text text-legal">
                            En esta Política de Privacidad también se describen las opciones de que dispone en relación con el uso de su información personal, su acceso,  actualización y corrección de la misma.
                        </p>
                        <p class="text text-legal">
                            ESTE ES UN ACUERDO VINCULANTE LEGAL ENTRE USTED Y XPEAKING. AL USAR LOS SERVICIOS, ACEPTA LOS TÉRMINOS DE ESTE ACUERDO. Debe leer detenidamente esta Política de privacidad, junto con nuestros Términos de uso y cualquier otro acuerdo o política proporcionada por nosotros que rija el uso de nuestros Servicios.
                        </p>
                        <p class="text text-legal">
                            No debe usar los Servicios si no acepta esta Política de Privacidad, nuestros Términos y Condiciones de uso y cualquier otro acuerdo o política que rija el uso de nuestros Servicios.
                        </p>
                        <p class="text text-legal">
                            Contenidos
                        </p>
                        <p class="text text-legal">
                            1. Ámbito de esta Política de privacidad y sitios cubiertos.
                        </p>
                        <p class="text text-legal">
                            2. Política relativa a los niños y adolescentes.
                        </p>
                        <p class="text text-legal">
                            3. Tipos de información que podemos recopilar.
                        </p>
                        <p class="text text-legal">
                            4. Información de pago.
                        </p>
                        <p class="text text-legal">
                            5. Cómo utilizamos la información que recopilamos.
                        </p>
                        <p class="text text-legal">
                            6. Cuándo y cómo podemos compartir o revelar su información a terceros.
                        </p>
                        <p class="text text-legal">
                            7. Cómo mantenemos la seguridad de su información.
                        </p>
                        <p class="text text-legal">
                            8. Sus opciones respecto al uso de su información.
                        </p>
                        <p class="text text-legal">
                            9.Procedimiento para eliminar la cuenta y situación tras anular o eliminar la cuenta.
                        </p>
                        <p class="text text-legal">
                            10. Modificaciones de esta Política de privacidad.
                        </p>
                        <p class="text text-legal">
                            11. Preguntas.
                        </p>
                        <p class="text text-legal">
                            12. Información General de la Empresa.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            1. Ámbito de esta Política de privacidad y sitios cubiertos
                        </h5>
                        <p class="text text-legal">
                            Esta Política de privacidad es válida en todo el mundo y se aplica a los servicios en los que proporcionamos un enlace a la misma, a menos que se indique lo contrario o a menos que proporcionemos un enlace a una política diferente.
                        </p>
                        <p class="text text-legal">
                            Esta Política de privacidad se aplica a la información que recopilamos online mediante el uso de los servicios. Los servicios pueden contener enlaces a Plataformas de terceros. Si utiliza nuestros servicios en o a través de una Plataforma de terceros, inclusive a través de cualquier Dispositivo inalámbrico, su privacidad también estará sujeta a las políticas de privacidad y los términos de servicio de la Plataforma de terceros.
                        </p>
                        <p class="text text-legal">
                            No controlamos ni nos hacemos responsables de los términos de servicio, las políticas de privacidad o las prácticas de ninguna Plataforma de terceros que usted pueda utilizar para obtener acceso a los Servicios. Debe leer detenidamente las políticas y los acuerdos de tales Plataformas de terceros.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            2. Política relativa a los menores de edad
                        </h5>
                        <p class="text text-legal">
                            Reconocemos los intereses de privacidad de los menores de edad y alentamos a los padres y tutores a desempeñar un papel activo en las actividades y los intereses online de los niños y adolescentes a su cargo. Si es menor de edad, no intente utilizar nuestros Servicios, ni enviar información sobre usted a Xpeaking si es que no se encuentra bajo la supervisión de su tutor, apoderado, padre o madre. Si nos percatamos de que hemos recopilado información personal de un menor de edad que no está bajo supervisión de su tutor o apoderado, eliminaremos tal información (usuario), sin perjuicio, reembolso o ni compensación alguna.
                        </p>
                        <p class="text text-legal">
                            Los padres que crean que Xpeaking puede tener información de un menor de edad que no esté autorizado pueden escribirnos a <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> con el asunto ELIMINAR CUENTA para pedir la eliminación de tal información. Queda a criterio de Xpeaking eliminar la cuenta o transferirla a otro usuario o tercera persona.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            3. Tipos de información que podemos recopilar
                        </h5>
                        <p class="text text-legal">
                            Actualmente, Xpeaking no conserva internamente números de tarjetas de crédito y/o débito; para ello, nos hemos asociado con una pasarela de pagos confiable.
                        </p>
                        <p class="text text-legal">
                            Cuando se Suscriba en nuestros servicios, sus datos como Nombres y Apellidos, Email, Usuario pueden mostrarse públicamente a coach y otros Usuarios que formen parte de la familia de Xpeaking.
                        </p>
                        <p class="text text-legal">
                            Xpeaking puede recopilar diferentes tipos de información sobre usted, en función de cómo utilice los servicios. Cuando se inscriba en los servicios de Xpeaking, recopilaremos la información que nos proporcione directamente, como la siguiente:
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Información de registro:</span>  para utilizar ciertas funciones de los Servicios de Xpeaking, está obligado a registrar sus datos (crear un usuario) y pagar una suscripción mensual de S/29 (veintinueve soles). En ese caso, recopilaremos y almacenaremos los datos de registro que comparta con nosotros, como su fecha de nacimiento, edad, dirección geográfica, correo electrónico, contraseña, persona que lo invitó (patrocinador) y otra información.
                        </p>
                        <p class="text text-legal">
                            Por la presente concede permiso a Xpeaking para que proporcione Información de registro o Información de perfil a otros usuarios que son alumnos y embajadores de la plataforma; asimismo, se brindarán estos datos a nuestro soporte técnico para ofrecerle siempre una mejor experiencia en la comunicación. Al ser invitado por un Embajador, es responsable de confirmar sus datos y salvaguardar la Información de registro. Xpeaking no controla estos usos realizados por los Embajadores; por lo tanto, no se hace responsable de estos actos.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            4. Información de Pagos y otros
                        </h5>
                        <p class="text text-legal">
                            Si realiza compras a través de nuestros servicios, podemos recopilar cierta información relativa a la compra (como su nombre) que resulte necesaria para procesar su pedido y Usted estará obligado a proporcionar cierta información de pago y facturación directamente a nuestro(s) socio(s) de procesamiento de pagos, incluidos, entre otros datos, su nombre, información de tarjeta de crédito o débito, dirección de facturación, email y otros. No tenemos acceso directo ni almacenamos o recopilamos su información de tarjeta de crédito o débito y no utilizaremos la información de pago que recopilamos con otros fines que no sean procesar su compra o comunicarnos con usted en relación con su transacción.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Comunicaciones, solicitudes de soporte, posibles abusos:</span> Si se pone en contacto con nosotros para recibir asistencia o para informar de un problema, preocupación, posible abuso u otro tipo de problema relacionado con los servicios o los Usuarios, podemos recopilar y almacenar su información de contacto, comunicación u otros tipos de información sobre Usted, incluidos, entre otros, su nombre, dirección de correo electrónico, ubicación, sistema operativo, dirección IP o actividad, y nos reservamos el derecho de realizar investigaciones y obtener más información según sea necesario. Utilizaremos la información para enviarle una respuesta e investigar su solicitud o comunicación, de conformidad con las disposiciones de esta Política de privacidad.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Sorteos, promociones, entrega de premios y encuestas:</span> Cada cierto tiempo, podemos solicitarle que complete una encuesta u ofrecerle la oportunidad de participar en promociones, entregas de premios por ser Embajador, canje de puntos, etc. Si decide participar en una promoción, recopilaremos y almacenaremos la información que proporcione en relación con su participación en la promoción, como su nombre, dirección de correo electrónico, fecha de nacimiento, número de teléfono y cualquier otra información que proporcione, y utilizaremos o revelaremos tal información de conformidad con esta Política de privacidad. Todas nuestras promociones están dirigidas a personas mayores de edad y/o menores con autorización de su tutor legal o apoderado. En algunos casos, la información recopilada sólo se utilizará para administrar la promoción; en otros casos, la información recuperada se utilizará para los fines de marketing de Xpeaking. Su información puede utilizarse para ponerse en contacto con Usted para notificarle si ha ganado un premio como resultado del sorteo o un premio en su calidad de usuario Embajador, confirmarle la entrega del premio u otros fines relacionados. Su participación en la promoción también puede conllevar su adición a nuestras listas de correo, así como a la de nuestros socios de promociones asociados con el concurso o sorteo. Para aceptar un premio, podría estar obligado (a menos que lo prohíba la ley) a permitirnos que publiquemos parte de su información a través de nuestros Servicios, como, por ejemplo, en una página de ganadores, en caso de ganar un concurso o sorteo, en caso de ganar buenas comisiones, en caso de encontrarse dentro del ranking de alumnos y/o embajadores nos brinda autorización sin restricción alguna a publicar su nombre e imagen personal a través de nuestros diferentes canales (correo, redes sociales, etc).
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            5. Cómo utilizamos la información que recopilamos
                        </h5>
                        <p class="text text-legal">
                            Xpeaking puede utilizar la información que recopila a través del uso que usted realiza de los Servicios con diferentes fines, principalmente relacionados con la prestación de los Servicios y la mejora de su calidad. Por ejemplo, podemos utilizar la información que recopilamos con los siguientes fines:
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span> Proporcionar, administrar y facilitar su uso de los Servicios, incluida la visualización de contenido personalizado y publicidad dirigida;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Notificarle sobre la apertura de nuevos idiomas, nuevos Servicios, funciones, promociones, mejoras y actualizaciones, incluidas actualizaciones de los Servicios, los Términos de uso y la Política de privacidad;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Notificarle sobre el vencimiento de su suscripción, suspensión, bloqueo o eliminación de cuentas;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Enviarle comunicaciones relacionadas con el marketing de nuestros servicios, incluidas ofertas, información de promociones, boletines, campañas de correo electrónico y comunicaciones relativas a productos nuevos, mejorados o existentes, concursos o sorteos;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Resolver problemas del sitio; gestionar o responder a preguntas y problemas de soporte al cliente;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Procesar o atender a sus solicitudes o pedidos de nuestros servicios, información o funciones;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Permitir la comunicación e interacción entre Usuarios y solicitar comentarios y observaciones para mejorar la experiencia del Usuario;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Gestionar el ranking de los usuarios embajadores y las ciudades.
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Supervisar el funcionamiento técnico de los Servicios y la red;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Desarrollar productos, servicios u ofertas nuevas o mejoradas;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Proporcionarle noticias y boletines;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Invitarle a participar de eventos académicos y/o corporativos, talleres, campamentos, conferencias, etc.
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Crear, revisar, analizar y compartir Información agregada;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Identificar, investigar y proteger contra usos indebidos reales, posibles o amenazados, usos sin autorización o usos no deseados de los Servicios, incluidas, entre otras, actividades de investigación de reclamaciones realizadas por o sobre otros Usuarios o sobre los Servicios y actividades que puedan incluir información proporcionada por fuentes de terceros;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Alojar en terceros los Servicios;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Analizar las tendencias y el tráfico de Usuarios;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Resolver disputas;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Solucionar errores o problemas de los Servicios;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Hacer el seguimiento de las suscripciones y la información de uso;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Comunicarnos con Usted sobre su Cuenta;
                        </p>
                        <p class="text text-legal pl-4">
                            <span class="font-weight-bold mr-1">•</span>Pedirle que participe en encuestas sobre nuestros Servicios.
                        </p>
                        <p class="text text-legal">
                            Bajo nuestro criterio exclusivo, determinemos qué datos son necesarios y obligatorios para garantizar la seguridad e integridad de nuestros Usuarios, empleados, socios, inversionistas, terceros, miembros del público y nuestros Servicios. Si ha facilitado su dirección de correo electrónico a Xpeaking, la utilizaremos con fines tales como: (i) Responder a consultas de soporte al cliente; (ii) Mantenerle informado sobre las actividades realizada en el aprendizaje de su idioma, incluido comentarios de los coaches y notificaciones sobre el estado de su suscripción; (iii) Mantenerle al día sobre el lanzamiento de nuevos idiomas, nuevas canciones, actualizaciones de productos y programas de recompensas; y (iv) de conformidad, todas aquellas formas estipuladas en esta Política de Privacidad.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            6. Cuándo y cómo podemos compartir o revelar Su información a terceros
                        </h5>
                        <p class="text text-legal">
                            Sólo compartiremos su información (incluida la información personal) con los siguientes terceros o en las siguientes circunstancias o según se describa en esta Política de privacidad:
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">
                                1. Protección, seguridad, cooperación con las fuerzas de orden público y cumplimiento de obligaciones jurídicas.
                            </span> 
                        </p>
                        <p class="text text-legal">
                            Podemos revelar su información a terceros, incluida su información personal, si creemos de buena fe, bajo nuestro criterio exclusivo, que tal revelación: (1) está permitida o es obligatoria bajo la ley; (2) se ha solicitado en relación con o es pertinente para pesquisas, investigaciones, órdenes o procedimientos judiciales, gubernamentales o jurídicos; (3) es obligatoria o razonablemente necesaria con arreglo a una citación, orden judicial u otra pesquisa o solicitud jurídicamente válida; (4) es razonablemente necesaria para aplicar Nuestros Términos de uso, esta Política de privacidad o cualquier otro acuerdo legal; (5) es obligatoria para detectar, impedir o enfrentarse de otro modo a fraudes, abusos, usos indebidos, posibles infracciones de la ley (o de una norma o reglamento) o problemas técnicos o de seguridad; o (6) es obligatoria o razonablemente necesaria para proteger frente a daños inminentes los derechos, la propiedad o la seguridad de Xpeaking, nuestros Usuarios, empleados, menores de edad, miembros del público y nuestros Servicios. También podemos revelar información sobre Usted a nuestros auditores o asesores jurídicos y contables en relación con el acceso a nuestras obligaciones o derechos de revelación en virtud de esta Política.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">
                                2. Socios comerciales, proveedores de servicios, contratistas o agentes de terceros
                            </span>
                        </p>
                        <p class="text text-legal">
                            Podemos compartir su información con empresas de terceros que presten servicios en nombre nuestro, incluidos procesamiento de pagos, procesamiento de pedidos, análisis de datos, servicios de marketing, campañas de correo electrónico, servicios de alojamiento y servicio al cliente. Mientras nos presten sus servicios, estas empresas pueden tener acceso a su información personal y están obligadas a utilizarla únicamente de acuerdo esta  Política de Privacidad.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">
                                3. Transmisión, venta, fusión o cesión
                            </span>
                        </p>
                        <p class="text text-legal">
                            En caso de que Xpeaking realice cualquier transición empresarial (por ejemplo, fusión, adquisición, cesión o disolución, incluida la quiebra) o bien venda la totalidad o parte de sus activos, nos reservamos el derecho de compartir, revelar o transferir toda la información del usuario, incluida la de carácter personal, a la organización derechohabiente por dicha transición.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">
                                4. Promociones
                            </span>
                        </p>
                        <p class="text text-legal">
                            En lo que respecta a las promociones en las que participe el usuario mediante los servicios, nos reservamos el derecho de compartir su información, siempre que sea indispensable para administrarlas, comercializarlas, patrocinarlas o llevarlas a buen término o bien cuando así lo exijan las leyes, las normas o las normativas aplicables (por ejemplo, para presentar las listas de ganadores, los coaches y usuarios embajadores mejores pagados o las declaraciones pertinentes) o las reglas de las promociones o los sorteos en cuestión.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">
                                5. Permiso
                            </span>
                        </p>
                        <p class="text text-legal">
                            Asimismo, si el usuario acepta de cualquier forma que se comparta su información con terceros con fines de marketing, compartimos sus datos con los terceros aceptados, en cuyo caso el uso de su información está sujeto a sus respectivas políticas de privacidad. Ante la información remitida u obtenida por Xpeaking, el usuario autoriza a la compañía  hacer uso de su imagen, voz, video, texto, testimonio, sin compensación alguna.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">
                                6. Publicidad de productos o servicios ajenos
                            </span>
                        </p>
                        <p class="text text-legal">
                            Aunque, de momento, no aparece publicidad en los servicios, si se ofrece en el futuro, nos reservamos el derecho de emplear y compartir con los anunciantes (u otros terceros) cierta información técnica o conjunta que revele preferencias o datos demográficos de nuestros usuarios. También nos reservamos el derecho de permitir que los anunciantes recopilen información técnica o conjunta, la cual pueden compartir con nosotros, mediante tecnologías de seguimiento como cookies o balizas web. La información recopilada sirve para ofrecer y presentar anuncios que personalicen la experiencia del usuario, pues aumentan la probabilidad de que los productos y servicios anunciados sean de su interés (práctica denominada publicidad comportamental), y para llevar a cabo análisis web (esto es, para analizar el tráfico y otras actividades del usuario a fin de mejorar su experiencia). 
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            7. Cómo mantenemos la seguridad de Su información
                        </h5>
                        <p class="text text-legal">
                            Xpeaking adopta todas las medidas de seguridad razonables desde el punto de vista comercial para proteger la información confidencial que el usuario comparte y que nosotros recopilamos y almacenamos contra el acceso, la modificación, la divulgación o la destrucción sin la debida autorización. Dichas medidas de seguridad incluyen prácticas diversas, entre otras, conservar la información confidencial en servidores protegidos, transmitir los datos confidenciales (como el número de las tarjetas) introducidos en nuestros servicios con tecnología SSL o realizar revisiones internas de nuestras plataformas y prácticas de recopilación de datos, aparte de medidas de seguridad físicas que impiden acceder sin autorización a los sistemas donde almacenamos la información. Por desgracia, no existe ningún sistema completamente seguro, así que resulta imposible garantizar que jamás nadie ajeno consiga acceder sin permiso a las comunicaciones entre el usuario y Xpeaking, y/o iniciar sesión sin su consentimiento. La contraseña es un elemento importante de nuestro sistema de seguridad y, como tal, el usuario tiene la responsabilidad de protegerla; por ello, debe abstenerse de revelar a tercera personas. Si queda comprometida por cualquier motivo, es indispensable modificarla de inmediato y comunicar el problema escribiendo a <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> con el asunto PROBLEMAS EN MI CUENTA.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            8. Sus opciones respecto al uso de Su información
                        </h5>
                        <p class="text text-legal">
                            Tal como se exponía, el Usuario tiene derecho a negarse a facilitarnos cierta información en cualquier momento, pero, en ese caso, quizá tenga vedado el uso de algunas características de los Servicios. Si el Usuario no desea recibir nuestras comunicaciones promocionales, debe enviarnos un mensaje al correo <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> con el asunto ELIMINAR NOTIFICACIONES
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            9. Procedimiento para eliminar la cuenta y situación tras anular o eliminar la cuenta
                        </h5>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">
                                1. Eliminación de cuentas
                            </span>
                        </p>
                        <p class="text text-legal">
                            Si un usuario (estudiante o embajador) desea eliminar su cuenta (ya sea con todos los servicios y beneficios adquiridos o no), debe mandar un mensaje al correo <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> con el asunto ELIMINAR CUENTA. Al recepcionar un mensaje de Eliminar Cuenta, Xpeaking  tendrá un plazo de 72 horas para proceder con este acto, o reservarse el derecho de transferir la titularidad de este usuario.
                        </p>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">
                                2. Rescisión o eliminación de la cuenta
                            </span>
                        </p>
                        <p class="text text-legal">
                            El Usuario debe saber que, aunque se rescinda o elimine su cuenta, su información, en todo o en parte, puede seguir a la vista de otros, incluida, sin restricciones, la información: a) incorporada al contenido, los comentarios, las publicaciones o los envíos de otros usuarios, así como los comentarios del idioma o a los coaches; b) copiada, almacenada o difundida por otros usuarios; c) compartida o difundida por el propio Usuario o por otros, por ejemplo, en publicaciones públicas; o d) publicada en Plataformas de terceros. Xpeaking no puede eliminar ningún dato del usuario de Plataformas de terceros ni tiene obligación de hacerlo.
                        </p>
                        <p class="text text-legal">
                            Es más, aunque se rescinda o elimine su Cuenta, la información del Usuario no se elimina de manera íntegra de nuestros servidores, ni, quizá, de los servidores de la Plataforma de terceros que utilice para acceder a nuestros Servicios. Nos reservamos el derecho de conservar copias de seguridad de la información del Usuario (datos personales incluidos) en nuestros servidores o nuestras bases de datos (o los servidores o las bases de datos de terceros autorizados que usemos) para el cumplimiento de obligaciones jurídicas, la resolución de controversias y la aplicación de acuerdos. Dicha información es susceptible de revelación en virtud de lo estipulado en esta Política de Privacidad, aun cuando la Cuenta esté anulada o eliminada. Nos reservamos el derecho de conservar la información mientras tenga un uso comercial razonable. También las Plataformas de terceros con las que el Usuario acceda a los Servicios pueden conservar copias de seguridad de Su información, aun cuando la Cuenta esté anulada o eliminada, la cual se emplea o revela conforme a lo dispuesto en sus respectivas políticas de privacidad.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            10. Modificaciones de esta Política de privacidad
                        </h5>
                        <p class="text text-legal">
                            La presente Política de Privacidad se actualiza cuando se considere preciso para aclarar nuestras prácticas o para reflejar prácticas de privacidad nuevas o diferentes, por ejemplo, cuando añadimos funciones nuevas. Además, Xpeaking se reserva el derecho de modificar o alterar la Política de privacidad en cualquier momento. Si se trata de un cambio sustancial, se comunica al Usuario por algún medio visible, por ejemplo, remitiendo un correo electrónico a la dirección especificada en la Cuenta o publicando un aviso en nuestros servicios. Cualquier otra modificación se aplica desde el mismo día en que se anuncia, salvo manifestación en contrario.
                        </p>
                        <p class="text text-legal">
                            Si el Usuario accede a los servicios o los utiliza después de la fecha de entrada en vigor de los cambios, se considera que conviene en obligarse a lo dispuesto en la Política de privacidad modificada, la cual sustituye todas las versiones anteriores. Por ese motivo, es recomendable volver a leer la Política de Privacidad cada vez que se empleen los Servicios e imprimir un ejemplar como referencia. Si así lo solicitamos, el Usuario conviene en aceptar o firmar una versión no electrónica de la Política de privacidad y de cualquier política o acuerdo disponible o aplicado en Plataformas de terceros. Entendiéndose, que si continúa utilizando nuestros servicios se toma por aceptado los nuevos términos de la Política de Privacidad.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            11. Preguntas
                        </h5>
                        <p class="text text-legal">
                            El Usuario puede enviarnos sus preguntas y dudas  sobre los términos de esta Política de Privacidad a la dirección <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a> o a través del whatsapp +51 939992192 con el asunto “PREGUNTAS”.
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title text-center">
                        <h5 class="xp-text-primary mt-4 text-left">
                            12. Información General de la Empresa
                        </h5>
                        <p class="text text-legal">
                            <span class="text-cursive mr-1">Xpeaking S.A.C</span>
                        </p>
                        <p class="text text-legal mt-2">
                            RUC N°  20604297339  
                        </p>
                        <p class="text text-legal mt-2">
                            Correo Electrónico: <a href="mailto:hola@xpeaking.com" target="_top" class="xp-link mx-1">hola@xpeaking.com</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
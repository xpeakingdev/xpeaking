@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => true,
                            'footerMessage' => true,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('jumbotron')
    @include('partials.jumbotrons.jumbotron')
@endsection

@section('content')
    <section id="portfolio" class="xp-section xp-bg-black">
        <div class="container pr-0 pr-md-2 pl-0 pl-md-2">
            <content-songs-all><content-songs-all>
        </div>
    </section>
@endsection
@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
{{-- full-data-profile-margin --}}
<div class="full-data-profile ">
    <div class="container px-0 px-md-2">
        <div class="row m-0">
            <div class="col-12 col-md-6 col-lg-4 mx-auto px-3 px-md-0">
                <content-song-info :slug="{{ json_encode($song->slug) }}" :id="{{ json_encode($song->id) }}"></content-song-info>
            </div>
        </div>
    </div>
</div>
@endsection
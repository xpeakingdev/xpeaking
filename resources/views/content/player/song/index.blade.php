@extends('layouts.landing', [
                            'classNavigation' => 'navbar-transparent xp-navbar xp-nav-bar-alt d-none d-md-flex',
                            'classSliderArea' => 'jumbotron xp-jumbotron navbar-height d-none d-md-flex',
                            'jumbotron' => false,
                            'footer' => false,
                            'social' => false,
                            'footerMessage' => false,
                            'backgroundHeader' => false,
                            'showHeader' => true
                        ])

@section('content')
{{-- full-data-profile-margin --}}
<div class="full-data-profile full-player ">
    <div class="container px-0 px-md-2">
        <div class="row m-0 height-player">
            <div class="col-12 col-md-6 col-lg-4 mx-auto height-player px-0">
                <xp-player :id="{{ json_encode($song->id) }}" :slug="{{ json_encode($song->slug) }}" :link="{{ json_encode($song->video) }}"></xp-player>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
  //   var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);;
  // console.log(is_safari);
// var userAgent = navigator.userAgent || navigator.vendor || window.opera;
// var so='';
//   // Windows Phone debe ir primero porque su UA tambien contiene "Android"
//  if (/windows phone/i.test(userAgent)) {
//     so='windows phone'
//  }

//  if (/android/i.test(userAgent)) {
//     so = "Android";
// }

//      if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
//     so = "iOS";
// }
// alert(so);
var so = false;
var iDevices = [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ];

  if (!!navigator.platform) {
    while (iDevices.length) {
      if (navigator.platform === iDevices.pop()){ so=true; }
    }
  }
if(so==true){
    document.addEventListener('DOMContentLoaded', ()=>{
        const player = new Plyr('#player');
        console.log('2')
    })
}

</script>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test', 'TestController@index')->name('test');
Route::get('speechtest','TestController@speech');
Route::post('speechtest','TestController@speechtest');
Route::get('date', 'TestController@showDate')->name('date');

// Auth::routes();

// LANDING PAGE WITH GUEST
Route::get('/', 'HomeController@home')->name('home');
Route::get('embajador-inicio', 'HomeController@ambassador')->name('ambassador');
Route::get('/prueba','HomeController@hometest');
// GLOBAL
// STATUS USER
Route::get('estados/lista', 'ContentController@statusList')->name('global.status.list');

// Profile photos
Route::get('usuario/foto/{route}','PublicPhotosController@Profilephoto');

// LANGUAGES 
Route::get('idiomas', 'LanguageController@list')->name('global.language.list');
Route::get('contenido/canciones/list', 'ContentController@songs')->name('content.songs.list');
Route::get('contenido/canciones/lista/importante', 'ContentController@songsImportant')->name('content.songs.list.important');
Route::get('contenido/canciones/info/{id}/{slug}', 'ContentController@songInfo')->name('content.songs.info');
Route::get('contenido/canciones/antigua', 'ContentController@getSongPrev')->name('content.songs.prev');
Route::get('contenido/artistas/list', 'ContentController@artists')->name('content.artists.list');
Route::get('contenido/artistas/informacion/{slug}', 'ContentController@artist')->name('content.artists.info');

// SEARCH
Route::post('contenido/buscar', 'ContentController@searchContent')->name('content.search.content');

// GENDERS
Route::get('contenido/generos/list', 'ContentController@genders')->name('content.genders.list');

// AMBASSADORS
Route::get('contenido/rangos', 'ContentController@ranges')->name('content.ranges');

// SPONSOR
Route::post('embajador/validar', 'ContentController@validSponsor')->name('content.sponsor.valid');

// VALID USER
Route::get('usuarios/validar/{username}', 'ContentController@validUser')->name('global.users.valid');

// PLAN RECOMMENDED
Route::get('planes/recomendado', 'PlanController@recommended')->name('global.plans.recommended');

// VALUE POINT
Route::get('puntos/valor', 'ContentController@valuePoint')->name('global.points.value');

// INFORMATIONS
Route::get('cancion/{id}/{slug}', 'SongController@show')->name('landing.song.show');
Route::get('artistas/{slug}', 'ArtistController@show')->name('landing.artist.show');

// PLAYER
Route::group(["middleware" => ['suscription']], function() {
     Route::get('cancion/reproductor/{id}/{slug}', 'PlayerController@player')->name('player.song');
});
// PLAYER DATA
Route::post('cancion/reproductor/data', 'PlayerController@getData')->name('player.get.data');
// TRANSLATE
Route::post('translate', 'TranslateController@translate')->name('translate');

// LEGAL
Route::get('terminos-de-uso', 'LegalController@terms')->name('terms');
Route::get('acuerdo-embajador', 'LegalController@agreement')->name('agreement');
Route::get('politicas-de-privacidad', 'LegalController@privacy')->name('privacy');

// SONGS ALL
Route::get('canciones', 'ContentController@songsAll')->name('content.songs.all');

Route::post('contenido/canciones/completa', 'ContentController@songsAllData')->name('content.songs.all.get');
// ARTISTS ALL
Route::get('artistas', 'ContentController@artistsAll')->name('content.artists.all');
Route::post('contenido/artistas/completa', 'ContentController@artistsAllData')->name('content.artists.all.get');

// SEARCHS CONTENT
Route::post('contenido/buscar', 'ContentController@searchAll')->name('content.search.all');

// VIEW SONGS
Route::post('/cancion/visita/agregar', 'ContentController@setView')->name('song.view.store');

// PERMISSIONS
Route::get('permissionsuser', 'Dashboard\DashboardController@permissions');


Route::group(['middleware' => ['guest']], function(){
     Route::get('acceso', 'Auth\LoginController@showLoginForm')->name('login');
     Route::post('acceso', 'Auth\LoginController@login');
     Route::get('registro', 'Auth\RegisterController@showRegistrationForm')->name('register');
     Route::get('registro/{username}', 'Auth\RegisterController@showRegistrationForm')->name('register.sponsor');
     Route::post('registro', 'Auth\RegisterController@registerUser');
     Route::get('acceso/restablecer', 'Auth\LoginController@showLinkRequestForm')->name('password.request');
     Route::post('acceso/correo', 'Auth\LoginController@sendResetLinkEmail')->name('password.email');
     Route::get('acceso/restablecer/{token}', 'Auth\LoginController@showResetForm')->name('password.reset');
     Route::post('acceso/restablecer', 'Auth\LoginController@passwordUpdate')->name('password.update');
     Route::get('/usuario/activacion/{token}', 'ActivationTokenController@activate')->name('activation');
});

Route::group(['middleware' => ['auth']], function(){

     // ACTIVATION ACCOUNT
     Route::get('salir', 'Auth\LoginController@logout')->name('logout');
     Route::get('usuario/inhabilitado', 'HomeController@locked')->name('user.locked');
     Route::get('usuario/verificar', 'HomeController@verified')->name('user.verified');
     Route::get('usuario/datos/autenticado', 'HomeController@dataUserAuth')->name('user.data.auth');


     // LEARN EVALUATION
     Route::post('evaluacion/transcripcion', 'Student\EvaluationController@TranscripTest')->name('transcript.test');
     

     // ONLY USERS ACTIVATED
     // Route::group(['middleware' => ['status.user']], function(){

          Route::group(["middleware" => [sprintf("role:%s", \App\Role::SUPERADMIN)]], function() {
               Route::get("upload/dictionary", "SuperAdminController@uploadDictionary")->name('super.upload.dictionary');
               Route::get("upload/phonetic", "SuperAdminController@uploadPhonetic")->name('super.upload.phonetic');
               Route::get("upload/subtitle", "SuperAdminController@uploadSubtitle")->name('super.upload.subtitle');
               Route::get("upload/scraping", "SuperAdminController@scraping")->name('super.upload.scraping');
               Route::get("upload/words", "SuperAdminController@upLoadWords")->name('super.upload.words');
          });
          
          Route::group(["middleware" => [sprintf("role:%s", \App\Role::STUDENT)]], function() {
              
               // ACCOUNT
               Route::get('perfil', 'ProfileController@profile')->name('profile');
               Route::get('perfil/data/resumen', 'ProfileController@resume')->name('profile.get.resume');
               Route::get('perfil/data/obtener', 'ProfileController@getData')->name('profile.get.data');
               Route::post('perfil/data/guardar', 'ProfileController@storeProfile')->name('profile.store.data');
               Route::post('perfil/password/guardar', 'ProfileController@updatePassword')->name('profile.store.password');
               Route::post('perfil/imagen/guardar', 'ProfileController@storePicture')->name('profile.picture.store');

               // CONTENT LOCKED
               Route::get('perfil/contenido/bloqueo/{search}', 'ProfileController@getLocked')->name('profile.content.locked.get');
               Route::post('perfil/contenido/bloqueo', 'ProfileController@saveLocked')->name('profile.content.locked.store');
               Route::get('perfil/contenido/bloqueado/lista', 'ProfileController@listRestricted')->name('profile.content.restricted.list');
               Route::post('perfil/contenido/bloqueado/guardar', 'ProfileController@storeRestricted')->name('profile.content.restricted.store');
               Route::post('perfil/contenido/bloqueado/mensaje', 'ProfileController@mailConfirm')->name('profile.content.restricted.mail');

               // EMBASSADOR
               Route::get('embajador', 'Student\AmbassadorController@index')->name('student.ambassador');
               Route::get('embajador/list', 'Student\AmbassadorController@list')->name('student.ambassador.list');
               Route::post('embajador/equipo/detalle', 'Student\AmbassadorController@teamDetail')->name('student.ambassador.team.detail');
               Route::post('embajador/usuario/resumen', 'Student\AmbassadorController@userData')->name('student.ambassador.user.data');
               Route::get('embajador/puntos/resumen', 'Student\AmbassadorController@pointsResume')->name('student.ambassador.points.resume');
               Route::post('embajador/puntos/transferir', 'Student\AmbassadorController@transferPoints')->name('student.ambassador.points.transfer');
               Route::post('embajador/suscripcion/pagar', 'Student\AmbassadorController@paymentSuscription')->name('student.ambassador.suscription.payment');

               // DICTIONARY
               Route::get('/usuario/diccionario', 'Student\DictionaryController@index')->name('student.dictionary');
               Route::post('diccionario/lista', 'Student\DictionaryController@list')->name('student.dictionary.list');
               Route::post('diccionario/eliminar', 'Student\DictionaryController@delete')->name('student.dictionary.delete');

               // FAVORITES
               Route::get('/usuario/favoritos', 'Student\FavoriteController@index')->name('student.favorites');
               Route::get('/usuario/favoritos/canciones', 'Student\FavoriteController@songs')->name('student.favorites.songs');
               Route::get('/usuario/favoritos/artistas', 'Student\FavoriteController@artists')->name('student.favorites.artists');

               // SUSCRIPTION
               Route::get('/usuario/suscripcion', 'Student\SuscriptionController@index')->name('student.suscription');
               Route::get('/usuario/suscripcion/datos', 'Student\SuscriptionController@dataSuscription')->name('student.suscription.data');
               Route::post('/usuario/suscripcion/pagar', 'Student\SuscriptionController@payment')->name('student.suscription.payment');
               Route::post('/usuario/suscripcion/pagos/historial', 'Student\SuscriptionController@history')->name('student.suscription.payment.history');
               Route::post('/usuario/suscripcion/subir/archivo', 'Student\SuscriptionController@uploadFile')->name('student.suscription.upload.file');

               // TRACING
               Route::post('/usuario/cancion/visita', 'PlayerController@setView')->name('student.song.view.store');


               // FUNCTIONALITY
               // PLAYER
               // Route::get('cancion/reproductor/{id}/{slug}', 'PlayerController@player')->name('player.song');
               
               Route::post('cancion/reproductor/report', 'PlayerController@report')->name('player.store.report');

               // SAVE GEMS
               Route::post('cancion/reproductor/gemas', 'PlayerController@gemsStore')->name('player.gems.store');

               // FAVORITE ARTIST
               Route::post('favoritos/artistas/guardar', 'FavoriteController@storeArtists')->name('favorite.artists.store');
               Route::get('favoritos/artistas/lista', 'FavoriteController@listArtists')->name('favorite.artists.get');

               // FAVORITE SONG
               Route::post('favoritos/canciones/guardar', 'FavoriteController@storeSongs')->name('favorite.song.store');
               Route::get('favoritos/canciones/lista', 'FavoriteController@listSongs')->name('favorite.song.get');

               // DICTIONARY
               Route::get('dictionario/lista', 'DictionaryController@dictionary')->name('dictionary.get');
               Route::post('diccionario/guardar', 'DictionaryController@dictionaryStore')->name('dictionary.store');

          });

          Route::group(["middleware" => [sprintf("role:%s", \App\Role::ADMIN)]], function() {
               // DASHBOARD
               Route::group(['prefix' => 'panel'], function(){
                    Route::get("check/suscriptions" , "Dashboard\Admin\SuscriptionController@checksuscription");
                    Route::get("check/dayavailable" , "Dashboard\Admin\SuscriptionController@dayavailable");

                    Route::get('/', 'Dashboard\DashboardController@index')->name('dashboard');

                    // PROFILE
                    Route::get('perfil', 'Dashboard\Admin\ProfileController@profile')->name('dashboard.admin.profile');
                    Route::get('perfil/datos', 'Dashboard\Admin\ProfileController@getUser')->name('dashboard.admin.profile.data');
                    Route::get('perfil/datos/obtener', 'Dashboard\Admin\ProfileController@getDataProfile')->name('dashboard.admin.profile.data.get');
                    Route::post('perfil/datos/guardar', 'Dashboard\Admin\ProfileController@storeProfile')->name('dashboard.admin.profile.data.store');
                    Route::post('perfil/password/guardar', 'Dashboard\Admin\ProfileController@storePassword')->name('dashboard.admin.profile.password.store');

                    // FINANCES
                    // // // PENDING PAY
                    Route::post('finanzas/pagos/pandientes/resumen', 'Dashboard\Finances\FinancesController@resume')->name('finances.pending.payment.resume');
                    Route::get('finanzas/pagos/pandientes/list', 'Dashboard\Finances\FinancesController@list')->name('finances.pending.payment.list');
                    Route::get('voucher/foto/{ruta}','PublicPhotosController@VoucherPhoto');
                    // // // PROFITS
                    Route::post('finanzas/ingresos/resumen', 'Dashboard\Finances\FinancesController@profits')->name('finances.profits.resume');
                    Route::post('finanzas/ingresos/comprobante/guardar', 'Dashboard\Finances\FinancesController@sendInvoice')->name('finances.profits.send.invoice');
                    // EGRESOS
                    Route::get('finanzas/egresos/resumen', 'Dashboard\Finances\FinancesController@expenses');

                    // STUDENTS
                    // // // STUDENTS
                    Route::post('estudiantes/resumen', 'Dashboard\Students\StudentController@resume')->name('students.resume');
                    Route::post('estudiantes/verificarcorreo', 'Dashboard\Students\StudentController@verifyaccount')->name('students.verify.store');
                    Route::post('estudiantes/bloquear', 'Dashboard\Students\StudentController@locked')->name('students.locked.store');
                    Route::post('estudiantes/datos/perfil', 'Dashboard\Students\StudentController@userData')->name('students.data.profile.get');
                    Route::post('estudiantes/datos/perfil/guardar', 'Dashboard\Students\StudentController@userDataStore')->name('students.data.profile.store');
                    Route::post('estudiantes/datos/restricciones', 'Dashboard\Students\StudentController@listRestrictered')->name('students.data.restrictered.get');
                    Route::post('estudiantes/limite/invitaciones', 'Dashboard\Students\StudentController@limitInvitations')->name('students.limit.invitations.store');
                    Route::post('estudiantes/limite/niveles', 'Dashboard\Students\StudentController@limitILevels')->name('students.limit.levels.store');
                    Route::post('estudiantes/suscripcion/datos', 'Dashboard\Students\StudentController@getSuscription')->name('students.suscription.get');

                    // STUDENT REPORTS
                    Route::get('estudiantes/reportes/lista','Dashboard\Students\ReportsController@datalist');
                    Route::post('estudiantes/reportes/actualizar','Dashboard\Students\ReportsController@updatereport');

                    // STUDENTS TEAM
                    Route::post('usuario/datos', 'Dashboard\Students\StudentController@getDataUser')->name('admin.students.data');
                    Route::post('embajador/puntos/resumen', 'Dashboard\Students\StudentController@pointsResume')->name('admin.student.ambassador.points');
                    Route::post('embajador/equipo/detalle', 'Dashboard\Students\StudentController@teamDetail')->name('admin.student.ambassador.team.detail');
                    Route::post('embajador/list', 'Dashboard\Students\StudentController@listAmbasador')->name('admin.student.ambassador.team.list');

                    // STUDENT TRACING
                    Route::post('usuario/canciones/vistas', 'Dashboard\Students\StudentController@getTracing')->name('admin.student.tracing.get');

                    // ADMINISTRADORES
                    Route::post('admins/lista','Dashboard\Admin\AdminController@adminlist');
                    Route::post('admins/permisos/datos','Dashboard\Admin\AdminController@UserPermissions');
                    Route::post('admins/permiso/accion','Dashboard\Admin\AdminController@changePermission');
                    Route::post('admins/user/delete','Dashboard\Admin\AdminController@deleteadmin');
                    Route::post('admins/user/info','Dashboard\Admin\AdminController@getadmininfo');
                    Route::post('admins/cambiardatos/admin','Dashboard\Admin\AdminController@changeadmininfo');

                    // ADMINISTRADOR CREAR OTROS ADMINS
                    Route::post('admins/register','Dashboard\Admin\AdminController@register');

                    // ORDERS PAY
                    Route::post('orden/pagar', 'Dashboard\Orders\OrderController@paymentStore')->name('order.pay.store');

                    // SONGS
                    Route::post('canciones/lista', 'Dashboard\Songs\SongController@list')->name('song.list');
                    Route::post('cancion/datos', 'Dashboard\Songs\SongController@getData')->name('song.get');
                    Route::post('cancion/guardar', 'Dashboard\Songs\SongController@store')->name('song.store');
                    Route::post('cancion/verificar', 'Dashboard\Songs\SongController@changeVerified')->name('song.verified');
                    Route::post('cancion/publicar', 'Dashboard\Songs\SongController@changePublished')->name('song.published');

                    // SONGS PREVIEW
                    Route::get('cancion/reproductor/{id}/{slug}', 'Dashboard\Songs\PreviewSongController@getsong');

                    // SONG SUBTITLES
                    Route::post('canciones/subtitulos/lista', 'Dashboard\Songs\Subtitles\SubtitlesController@list')->name('dashboard.song.subtitles.list');
                    Route::post('canciones/subtitulos/datos', 'Dashboard\Songs\Subtitles\SubtitlesController@getData')->name('dashboard.song.subtitles.data.get');
                    Route::post('canciones/subtitulos/guardar', 'Dashboard\Songs\Subtitles\SubtitlesController@store')->name('dashboard.song.subtitles.store');
                    Route::post('canciones/subtitulos/activar', 'Dashboard\Songs\Subtitles\SubtitlesController@setActive')->name('dashboard.song.subtitles.active.store');
                    Route::post('canciones/subtitulos/eliminar', 'Dashboard\Songs\Subtitles\SubtitlesController@delete')->name('dashboard.song.subtitles.delete');
                    Route::post('canciones/subtitulos/archivo/vista', 'Dashboard\Songs\Subtitles\SubtitlesController@previewFile')->name('dashboard.song.subtitles.file.get');
                    Route::post('canciones/subtitulos/archivo/vista/guardar', 'Dashboard\Songs\Subtitles\SubtitlesController@SaveEvaluation')->name('dashboard.song.subtitles.file.evaluation.save');
                    Route::post('canciones/subtitulos/palabras/buscar', 'Dashboard\Songs\Subtitles\SubtitlesController@wordsExist')->name('dashboard.song.subtitles.words.get');
                    Route::post('canciones/subtitulos/archivo/guardar', 'Dashboard\Songs\Subtitles\SubtitlesController@saveFile')->name('dashboard.song.subtitles.words.save');
                    Route::post('canciones/subtitulos/palabras/guardar', 'Dashboard\Songs\Subtitles\SubtitlesController@saveWords')->name('dashboard.song.subtitles.file.save');

                    // SONG SUBTITLES TRANSLATE
                    Route::post('canciones/subtitulos/translate/vista', 'Dashboard\Songs\Subtitles\SubtitleTranslateController@getSubtitle')->name('dashboard.song.subtitles.translate.get');
                    Route::post('canciones/subtitulos/translate/guardar', 'Dashboard\Songs\Subtitles\SubtitleTranslateController@storesubtitle')->name('dashboard.song.subtitles.translate.save');

                    //SUBTITLES EDIT FILE SRT

                    Route::resource('subtitulo/editarsrt','Dashboard\Songs\Subtitles\SrtfileController');
                    Route::get('subtitulo/descargar/{id}','Dashboard\Songs\Subtitles\SrtfileController@download');

                    // ARTISTS
                    Route::post('artistas/lista', 'Dashboard\Artists\ArtistController@list')->name('dashboard.artists.list');
                    Route::post('artistas/datos', 'Dashboard\Artists\ArtistController@getData')->name('dashboard.artists.get');
                    Route::post('artistas/guardar', 'Dashboard\Artists\ArtistController@store')->name('dashboard.artists.store');
                    Route::post('artistas/publicar', 'Dashboard\Artists\ArtistController@changePublished')->name('dashboard.artists.published');

                    // GENDERS
                    Route::post('generos/lista', 'Dashboard\Genders\GenderController@list')->name('dashboard.genders.list');
                    Route::post('generos/datos', 'Dashboard\Genders\GenderController@getData')->name('dashboard.genders.get');
                    Route::post('generos/guardar', 'Dashboard\Genders\GenderController@store')->name('dashboard.genders.store');

                    // LANGUAGES
                    Route::post('lenguajes/lista', 'Dashboard\Languages\LanguageController@list')->name('dashboard.languages.list');
                    Route::post('lenguajes/datos', 'Dashboard\Languages\LanguageController@getData')->name('dashboard.languages.get');
                    Route::post('lenguajes/guardar', 'Dashboard\Languages\LanguageController@store')->name('dashboard.languages.store');

                    // DICTIONARIES
                    Route::get('diccionarios/lista', 'Dashboard\Dictionaries\DictionariesController@list');
                    Route::post('diccionarios/editar', 'Dashboard\Dictionaries\DictionariesController@edit');
                    Route::post('diccionarios/guardar', 'Dashboard\Dictionaries\DictionariesController@save');
                    Route::post('diccionarios/audio', 'Dashboard\Dictionaries\DictionariesController@audio');
                    Route::post('diccionarios/eliminar', 'Dashboard\Dictionaries\DictionariesController@delete');


               });
          });

     // });
});


Route::get('/{username}', 'Auth\RegisterController@validUserSponsor');

/*
function __construct()
{
     $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show']]);
     $this->middleware('permission:product-create', ['only' => ['create','store']]);
     $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
     $this->middleware('permission:product-delete', ['only' => ['destroy']]);
}
*/
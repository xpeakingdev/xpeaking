<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Profile;
use App\Student;
use App\LanguageStudent;
use App\Order;
use App\Suscription;
use Carbon\Carbon;
use App\Plan;

class UserSeeder extends Seeder
{
    public function run()
    {
        // CREATE USERS

        // SUPER ADMIN
        factory(User::class,1)->create([
            'email' => 'super@gmail.com',
            'password' => bcrypt('secret'),
            'username' => 'super',
            'role_id' => Role::SUPERADMIN,
            'status' => User::ACTIVE
        ]);

        // USER ADMIN
        factory(User::class,1)->create([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'username' => 'xpeaking',
            'role_id' => Role::ADMIN,
            'status' => User::ACTIVE
        ]);

        // USER
        // factory(User::class,1)->create([
        //     'email' => 'student@gmail.com',
        //     'password' => bcrypt('secret'),
        //     'username' => 'student',
        //     'role_id' => Role::STUDENT,
        //     'status' => User::ACTIVE
        // ])->each(function(User $user){
        //     factory(Student::class, 1)->create([
        //         'user_id' => $user->id
        //     ])->each(function(Student $student){
        //         factory(LanguageStudent::class, 1)->create([
        //             "speak" => true,
        //             "learn" => false,
        //             "principal" => true,
        //             "language_id" => 1,
        //             "student_id" => $student->id
        //         ]);
        //         factory(LanguageStudent::class, 1)->create([
        //             "speak" => false,
        //             "learn" => true,
        //             "principal" => true,
        //             "language_id" => 1,
        //             "student_id" => $student->id
        //         ]);
        //         factory(Order::class, 1)->create([
        //             "payment_mode" => Plan::POINTS,
        //             "description" => "",
        //             "ammount" => 10,
        //             "status" => Order::COMPLETE,
        //             "date_pay" => Carbon::now(),
        //             "points" => 69,
        //             "paid_by" => 1,
        //             "user_id" => $student->user_id,
        //             "plan_id" => Plan::inRandomOrder()->first()->id
        //         ]);
        //         factory(Suscription::class, 1)->create([
        //             "active" => true,
        //             "init_at" => Carbon::now(),
        //             "expires_at" => Carbon::now(),
        //             "user_id" => $student->user_id,
        //             "plan_id" => Plan::inRandomOrder()->first()->id,
        //             "order_id" => Order::inRandomOrder()->first()->id
        //         ]);
        //     });

        //     factory(Profile::class, 1)->create([
        //         'user_id' => $user->id
        //     ]);
            
        // });
    }
}

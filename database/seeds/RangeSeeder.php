<?php

use Illuminate\Database\Seeder;
use App\Range;
use App\Award;

class RangeSeeder extends Seeder
{
  
    public function run()
    {
        $ranges = [
            [
                'description' => '1', 'level' => 1, 'limit' => 3, 'active' => true,
                'awards' => []
            ],
            [
                'description' => '2', 'level' => 2, 'limit' => 9, 'active' => true,
                'awards' => []
            ],
            [
                'description' => '3', 'level' => 3, 'limit' => 27, 'active' => true,
                'awards' => []
            ],
            [
                'description' => '4', 'level' => 4, 'limit' => 81, 'active' => true,
                'awards' => []
            ],
            [
                'description' => '5', 'level' => 5, 'limit' => 243, 'active' => true,
                'awards' => []
            ],
            [
                'description' => '6', 'level' => 6, 'limit' => 729, 'active' => true,
                'awards' => [
                    [ 
                        'description' => 'S/ 500',
                        'active' => true,
                        'range_id' => 0,
                        'picture' => 'award-range-1.svg' ]
                ]
            ],
            [
                'description' => '7', 'level' => 7, 'limit' => 2187, 'active' => true,
                'awards' => [
                    [ 
                        'description' => 'S/ 1,000',
                        'active' => true,
                        'range_id' => 0,
                        'picture' => 'award-range-2.svg' ]
                ]
            ],
            [
                'description' => '8', 'level' => 8, 'limit' => 6561, 'active' => true,
                'awards' => [
                    [ 
                        'description' => 'S/ 3,000',
                        'active' => true,
                        'range_id' => 0,
                        'picture' => 'award-range-3.svg' ]
                ]
            ],
            [
                'description' => '9', 'level' => 9, 'limit' => 19683, 'active' => true,
                'awards' => [
                    [ 
                        'description' => 'S/ 10,000',
                        'active' => true,
                        'range_id' => 0,
                        'picture' => 'award-range-4.svg' ]
                ]
            ],
            [
                'description' => '10', 'level' => 10, 'limit' => 59049, 'active' => true,
                'awards' => [
                    [ 
                        'description' => 'S/ 30,000',
                        'active' => true,
                        'range_id' => 0,
                        'picture' => 'award-range-5.svg' ]
                ]             
            ]
        ];

        foreach($ranges as $rangeD){
            factory(Range::class,1)->create([
                "description" => $rangeD["description"],
                "level" => $rangeD["level"],
                "limit" => $rangeD["limit"],
                "active" => $rangeD["active"]
            ])
            ->each(function(Range $range) use($rangeD){
                foreach($rangeD["awards"] as $awardD){
                    factory(Award::class,1)->create([
                        "description" => $awardD["description"],
                        "picture" => $awardD["picture"],
                        "active" => $awardD["active"],
                        "range_id" => $range->id
                    ]);
                }
            });
        }
    }
}

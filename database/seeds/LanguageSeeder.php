<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguageSeeder extends Seeder
{
    public function run()
    {
        $languages = [
            ['id' => Language::SPANISH, 'name' => 'Español', 'code' => 'es'],
            ['id' => Language::ENGLISH, 'name' => 'Inglés', 'code' => 'en']
        ];

        foreach($languages as $language){
            factory(Language::class,1)->create([
                'id' => $language['id'],
                'name' => $language['name'], 
                'code' => $language["code"]
            ]);
        }
    }
}

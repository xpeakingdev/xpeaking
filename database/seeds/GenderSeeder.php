<?php

use Illuminate\Database\Seeder;
use App\Gender;
use App\Artist;
use App\Subtitle;
use App\Song;
use App\Language;
use App\ArtistGender;
use App\GenderSong;
use App\LanguageSong;
use App\ArtistLanguage;
use App\ArtistSong;
use Carbon\Carbon;

class GenderSeeder extends Seeder
{
    // GENDERS SONGS

    public function run()
    {
        $genders = [
            ["id" => "", "name" => "Alternativa/independiente"],
            ["id" => "", "name" => "Blue-eyed soul"],
            ["id" => "", "name" => "Dance"],
            ["id" => "", "name" => "Dance pop"],
            ["id" => "", "name" => "Electropop"],
            ["id" => "", "name" => "Funk"],
            ["id" => "", "name" => "Funk rock"],
            ["id" => "", "name" => "Hip hop"],
            ["id" => "", "name" => "Indie pop"],
            ["id" => "", "name" => "Neo soul"],
            ["id" => "", "name" => "New jack"],
            ["id" => "", "name" => "Pop"],
            ["id" => "", "name" => "Pop rock"],
            ["id" => "", "name" => "Reggae"],
            ["id" => "", "name" => "Rock"],
            ["id" => "", "name" => "R&B contemporáneo"],
            ["id" => "", "name" => "Soft rock"],
            ["id" => "", "name" => "Soul"],
            ["id" => "", "name" => "Swing"],
        ];

        $artists = [
            // SELENA GOMEZ
            [
                "name" => "Selena",
                "last_name" => "Gómez",
                "stage_name" => "Selena Gómez",
                "slug" => "selena-gomez",
                "picture" => "selena-gomez.png",
                "genders" => [
                    ["name" => "Pop"],
                    ["name" => "Electropop"],
                    ["name" => "Dance pop"]
                ],
                "languages" => [
                    ["name" => "Ingles", "native" => true]
                ],
                "songs" => [
                    [
                        "name" => "Lose You To Love Me",
                        "slug" => "lose-you-to-love-me",
                        "video" => "https://www.youtube.com/watch?v=zlJDTxahav0",
                        "cover" => "lose-you-to-love-me-selenagomez.jpg",
                        "duration" => "03:22",
                        "year_release" => 2019,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Pop"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "selena-es.srt",
                                "native" => false,
                                "status" => 70,
                                "language" => "Español"
                            ],
                            [
                                "file" => "selena-en.srt",
                                "native" => true,
                                "status" => 80,
                                "language" => "Ingles"
                            ]
                        ]
                    ]
                ]
            ],
            // BRUNO MARS
            [
                "name" => "Bruno",
                "last_name" => "Mars",
                "stage_name" => "Bruno Mars",
                "slug" => "bruno-mars",
                "picture" => "bruno-mars.jpg",
                "languages" => [
                    ["name" => "Ingles", "native" => true]
                ],
                "genders" => [
                    ["name" => "Pop"],
                    ["name" => "Soul"],
                    ["name" => "Rock"],
                    ["name" => "R&B contemporáneo"],
                    ["name" => "Reggae"],
                    ["name" => "Funk"]
                ],
                "songs" => [
                    [
                        "name" => "That's What I Like",
                        "slug" => "that-s-what-i-like",
                        "video" => "https://www.youtube.com/watch?v=PMivT7MJ41M",
                        "cover" => "that-s-what-i-like.jpg",
                        "duration" => "03:22",
                        "year_release" => 2019,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Pop"],
                            ["name" => "R&B contemporáneo"],
                            ["name" => "New jack"],
                            ["name" => "Swing"],
                            ["name" => "Hip hop"],
                            ["name" => "Soul"],
                            ["name" => "Funk"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "that-s-what-i-like-es.srt",
                                "native" => false,
                                "status" => 90,
                                "language" => "Español"
                            ],
                            [
                                "file" => "that-s-what-i-like-en.srt",
                                "native" => true,
                                "status" => 100,
                                "language" => "Ingles"
                            ]
                        ]
                    ]
                ]
            ],
            // JUSTIN BIEBER
            [
                "name" => "Justin",
                "last_name" => "Bieber",
                "stage_name" => "Justin Bieber",
                "slug" => "justin-bieber",
                "picture" => "justin-bieber.jpg",
                "languages" => [
                    ["name" => "Ingles", "native" => true]
                ],
                "genders" => [
                    ["name" => "Pop"],
                    ["name" => "Hip hop"],
                    ["name" => "R&B contemporáneo"],
                ],
                "songs" => [
                    [
                        "name" => "Yummy",
                        "slug" => "yummy",
                        "video" => "https://www.youtube.com/watch?v=8EJ3zbKTWQ8",
                        "cover" => "yummy.jpg",
                        "duration" => "02:35",
                        "year_release" => 2020,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Pop"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "yummy-es.srt",
                                "native" => false,
                                "status" => 90,
                                "language" => "Español"
                            ],
                            [
                                "file" => "yummy-en.srt",
                                "native" => true,
                                "status" => 100,
                                "language" => "Ingles"
                            ]
                        ]
                    ]
                ]
            ],
            // MAROON 5
            [
                "name" => "Maroon 5",
                "last_name" => "Maroon 5",
                "stage_name" => "Maroon 5",
                "slug" => "maroon-5",
                "picture" => "maroon-5.jpg",
                "languages" => [
                    ["name" => "Ingles", "native" => true]
                ],
                "genders" => [
                    ["name" => "Pop"],
                    ["name" => "Pop rock"],
                    ["name" => "Funk rock"],
                    ["name" => "Blue-eyed soul"],
                    ["name" => "Neo soul"],
                    ["name" => "Soft rock"]
                ],
                "songs" => [
                    [
                        "name" => "Girls Like You",
                        "slug" => "girls-like-you",
                        "video" => "https://www.youtube.com/watch?v=aJOTlE1K90k",
                        "cover" => "girls-like-you.jpg",
                        "duration" => "03:35",
                        "year_release" => 2017,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Pop"],
                            ["name" => "Pop rock"],
                            ["name" => "R&B contemporáneo"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "girls-like-you-es.srt",
                                "native" => false,
                                "status" => 90,
                                "language" => "Español"
                            ],
                            [
                                "file" => "girls-like-you-en.srt",
                                "native" => true,
                                "status" => 100,
                                "language" => "Ingles"
                            ]
                        ]
                    ],
                    [
                        "name" => "If I never see your face again",
                        "slug" => "if-i-never-see-your-face-again",
                        "video" => "https://www.youtube.com/watch?v=MbtajuiuLMU",
                        "cover" => "if-i-never-see-your-face-again.jpg",
                        "duration" => "03:35",
                        "year_release" => 2007,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Pop"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "if-i-never-see-your-face-again-es.srt",
                                "native" => false,
                                "status" => 90,
                                "language" => "Español"
                            ],
                            [
                                "file" => "if-i-never-see-your-face-again-en.srt",
                                "native" => true,
                                "status" => 100,
                                "language" => "Ingles"
                            ]
                        ]
                    ],
                    [
                        "name" => "Memories",
                        "slug" => "memories",
                        "video" => "https://www.youtube.com/watch?v=SlPhMPnQ58k",
                        "cover" => "memories.jpg",
                        "duration" => "04:15",
                        "year_release" => 2019,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Pop"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "memories-es.srt",
                                "native" => false,
                                "status" => 90,
                                "language" => "Español"
                            ],
                            [
                                "file" => "memories-en.srt",
                                "native" => true,
                                "status" => 100,
                                "language" => "Ingles"
                            ]
                        ]
                    ],
                    [
                        "name" => "She Will Be Loved",
                        "slug" => "she-will-be-loved",
                        "video" => "https://www.youtube.com/watch?v=nIjVuRTm-dc",
                        "cover" => "she-will-be-loved.jpg",
                        "duration" => "02:45",
                        "year_release" => 2002,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Pop"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "she-will-be-loved-es.srt",
                                "native" => false,
                                "status" => 90,
                                "language" => "Español"
                            ],
                            [
                                "file" => "she-will-be-loved-en.srt",
                                "native" => true,
                                "status" => 100,
                                "language" => "Ingles"
                            ]
                        ]
                    ],
                    [
                        "name" => "What Lovers Do",
                        "slug" => "what-lovers-do",
                        "video" => "https://www.youtube.com/watch?v=5Wiio4KoGe8",
                        "cover" => "what-lovers-do.jpg",
                        "duration" => "03:23",
                        "year_release" => 2004,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Pop"],
                            ["name" => "R&B contemporáneo"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "what-lovers-do-es.srt",
                                "native" => false,
                                "status" => 90,
                                "language" => "Español"
                            ],
                            [
                                "file" => "what-lovers-do-en.srt",
                                "native" => true,
                                "status" => 100,
                                "language" => "Ingles"
                            ]
                        ]
                    ]
                ]
            ],
            // TONES AND I
            [
                "name" => "Tones and I",
                "last_name" => "Tones and I",
                "stage_name" => "Tones and I",
                "slug" => "tones-and-i",
                "picture" => "tones-and-i.jpg",
                "languages" => [
                    ["name" => "Ingles", "native" => true]
                ],
                "genders" => [
                    ["name" => "Indie pop"],
                    ["name" => "Dance"]
                ],
                "songs" => [
                    [
                        "name" => "Dance Monkey",
                        "slug" => "dance-monkey",
                        "video" => "https://www.youtube.com/watch?v=q0hyYWKXF0Q",
                        "cover" => "dance-monkey.jpg",
                        "duration" => "03:22",
                        "year_release" => 2019,
                        "published" => "1",
                        "free" => true,
                        "languages" => ["Ingles"],
                        "genders" => [
                            ["name" => "Alternativa/independiente"]
                        ],
                        "subtitles" => [
                            [
                                "file" => "dance-monkey-es.srt",
                                "native" => false,
                                "status" => 90,
                                "language" => "Español"
                            ],
                            [
                                "file" => "dance-monkey-en.srt",
                                "native" => true,
                                "status" => 100,
                                "language" => "Ingles"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        foreach($genders as $index => $genderD){
            factory(Gender::class,1)->create(["name" => $genderD["name"]])
            ->each(function(Gender $gender) use($index){
                $genders[$index]["id"] = $gender->id;
            });
        }

        foreach($artists as $index => $artistD){
            // LANGUAGE ID
            // $langArt = Language::where('name', $artistD["language"])->first();
            factory(Artist::class,1)->create([
                "name" => $artistD["name"],
                "last_name" => $artistD["last_name"],
                "stage_name" => $artistD["stage_name"],
                "slug" => $artistD["slug"],
                "picture" => $artistD["picture"]
            ])
            ->each(function(Artist $artist) use($artistD, $genders){
                // LANGUAGES
                foreach($artistD["languages"] as $languageArtist){
                    // ARTIST LANGUAGE
                    $langArtData = Language::where('name', $languageArtist["name"])->first();
                    if($langArtData){
                        factory(ArtistLanguage::class,1)->create([
                            "language_id" => $langArtData->id,
                            "artist_id" => $artist->id,
                            "native" => $languageArtist["native"]
                        ]);
                    }
                }

                // GENDERS
                foreach($artistD["genders"] as $genderArtist){
                    // ARTIST GENDER
                    $artGenderData = Gender::where('name', $genderArtist["name"])->first();
                    if($artGenderData){
                        factory(ArtistGender::class,1)->create([
                            "artist_id" => $artist->id,
                            "gender_id" => $artGenderData->id
                        ]);
                    }
                }

                // SONGS
                foreach($artistD["songs"] as $songD){
                    factory(Song::class,1)->create([
                        "name" => $songD["name"],
                        "slug" => $songD["slug"],
                        "duration" => $songD["duration"],
                        "video" => $songD["video"],
                        "cover" => $songD["cover"],
                        "year_release" => $songD["year_release"],
                        "published" => $songD["published"],
                        "free" => $songD["free"]
                    ])
                    ->each(function(Song $song) use($songD, $genders, $artist){

                        // ARTIST SONG
                        factory(ArtistSong::class,1)->create([
                            "artist_id" => $artist->id,
                            "song_id" => $song->id
                        ]);

                        // GENDERS
                        foreach($songD["genders"] as $genderSong){
                            $songGenderData = Gender::where('name', $genderSong["name"])->first();
                            if($songGenderData){
                                factory(GenderSong::class,1)->create([
                                    "song_id" => $song->id,
                                    "gender_id" => $songGenderData->id
                                ]);
                            }


                            // SEARCH ID GENDER
                            $genderSongId = 0;
                            foreach($genders as $genderSongItem){
                                if($genderSongItem["name"] == $genderSong["name"]){
                                    $genderSongId = $genderSongItem["id"];
                                }
                            }

                            if($genderSongId != 0){
                                factory(GenderSong::class,1)->create([
                                    "song_id" => $song->id,
                                    "gender_id" => $genderSongId
                                ]);
                            }
                        }

                        // SUBTITLES
                        foreach($songD["subtitles"] as $subtitleD){
                            // LANGUAGE IF
                            $langSub = Language::where('name', $subtitleD["language"])->first();

                            if($langSub){
                                factory(Subtitle::class,1)->create([
                                    "file" => $subtitleD["file"],
                                    "status" => $subtitleD["status"],
                                    "native" => $subtitleD["native"],
                                    "language_id" => $langSub->id,
                                    "song_id" => $song->id
                                ]);
                            }
                        }

                        // LANGUAGES
                        foreach($songD["languages"] as $languageD){
                            // LANGUAGE IF
                            $langSong = Language::where('name', $languageD)->first();
                            if($langSong){
                                factory(LanguageSong::class,1)->create([
                                    "language_id" => $langSong->id,
                                    "song_id" => $song->id
                                ]);
                            }
                        }
                    });
                }

            });
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{

    public function run()
    {
        // CREATE ROLES
        factory(Role::class,1)->create(['id' => Role::SUPERADMIN,'name' => 'superadmin', 'static' => true]);
        factory(Role::class,1)->create(['id' => Role::ADMIN,'name' => 'admin', 'static' => true]);
        factory(Role::class,1)->create(['id' => Role::STUDENT,'name' => 'student', 'static' => true]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Plan;

class PlanSeeder extends Seeder
{
    public function run()
    {
        factory(Plan::class,1)->create([
            'name' => 'Plan 29',
            'description' => 'Plan 29',
            'price' => 29.00,
            'months' => 1,
            'active' => true,
            'recommended' => true
        ]);
    }
}

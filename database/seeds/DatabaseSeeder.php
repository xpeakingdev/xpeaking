<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
     // Storage::deleteDirectory('artists');
     // Storage::makeDirectory('artists');

    public function run()
    {
        $this->call([
            LanguageSeeder::class,
            PlanSeeder::class,
            CountrySeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            // GenderSeeder::class,
            RangeSeeder::class,
            PermissionSeeder::class
        ]);
    }
}

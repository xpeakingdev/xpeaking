<?php

use Illuminate\Database\Seeder;

use App\Country;
use App\Region;
use App\City;


class CountrySeeder extends Seeder
{
    
    
    
    public function run()
    {
        $countries = [
            [
                "countryName"=>"Perú",
                "picture" => "peru.svg",
                "regions"=>[
                        [
                            "regionName" => "AMAZONAS",
                            "provinces"=> [
                                [
                                    "provinceName"=>"CHACHAPOYAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHACHAPOYAS",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Chachapoyas",
                                                    "address"=>"JR. OCTAVIO ORTIZ ARRIETA 270",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ASUNCION",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"BALSAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHETO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHILIQUIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHUQUIBAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"GRANADA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUANCAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LA JALCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LEIMEBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LEVANTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MAGDALENA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARISCAL CASTILLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MOLINOPAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MONTEVIDEO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OLLEROS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"QUINJALCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO DE DAGUAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN ISIDRO DE MAINO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SOLOCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SONCHE",
                                            "price"=>14
                                        ]
    
                                    ]
                                ],
                                [
                                    "provinceName"=>"BAGUA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"BAGUA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Bagua",
                                                    "address"=>"PSJ. ALFONSO UGARTE 171",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"LA PECA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ARAMANGO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COPALLIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"EL PARCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"IMAZA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"BONGARA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JUMBILLA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHISQUILLA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHURUJA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COROSHA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CUISPES",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"FLORIDA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"JAZAN",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Pedro Ruiz Gallo",
                                                    "address"=>"Av. Sacsahuaman S/N",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"RECTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN CARLOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SHIPASBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VALERA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YAMBRASBAMBA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CONDORCANQUI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"NIEVA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"EL CENEPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"RIO SANTIAGO",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LUYA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LAMUD",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CAMPORREDONDO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COCABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COLCAMAR",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CONILA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"INGUILPATA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LONGUITA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LONYA CHICO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LUYA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LUYA VIEJO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OCALLI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"OCUMAL",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PISUQUIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PROVIDENCIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN CRISTOBAL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO DEL YESO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JERONIMO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE LOPECANCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA CATALINA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTO TOMAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TINGO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TRITA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"RODRIGUEZ DE MENDOZA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN NICOLAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHIRIMOTO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COCHAMAL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAMBO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LIMABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LONGAR",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARISCAL BENAVIDES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MILPUC",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OMIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"TOTORA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VISTA ALEGRE",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"UTCUBAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"UTCUBAMBA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Uctubamba",
                                                    "address"=>"JR. MESONES MURO 394",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BAGUA GRANDE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CAJARURO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CUMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"EL MILAGRO",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina El Milagro",
                                                    "address"=>"AV. EL EJERCITO 199 EL MILAGRO",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"JAMALCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LONYA GRANDE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YAMON",
                                            "price"=>14
                                        ]
                                    ]
                                ]
                            ],
                        ],
                        [
                            "regionName" => "ANCASH",
                            "provinces"=> [
                                [
                                    "provinceName"=>"HUARAZ",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUARAZ",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Huaraz",
                                                    "address"=>"Jr. San Martin N° 673",
                                                    "delivery_time"=>"24"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"COCHABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COLCABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANCHAY",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"INDEPENDENCIA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"JANGAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LA LIBERTAD",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OLLEROS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAMPAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PARIACOTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PIRA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TARICA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"AIJA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"AIJA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CORIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUACLLAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LA MERCED",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SUCCHA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ANTONIO RAYMONDI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LLAMELLIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ACZO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHACCHO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHINGAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MIRGAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE RONTOY",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ASUNCION",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHACAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ACOCHACA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"BOLOGNESI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHIQUIAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ABELARDO PARDO LEZAMETA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ANTONIO RAYMONDI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"AQUIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CAJACAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CANIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COLQUIOC",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUALLANCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUASTA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAYLLACAYAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LA PRIMAVERA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MANGAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PACLLON",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN MIGUEL DE CORPANQUI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TICLLOS",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CARHUAZ",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CARHUAZ",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ACOPAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"AMASHCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ANTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ATAQUERO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARCARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PARIAHUANCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN MIGUEL DE ACO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SHILLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TINCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YUNGAR",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CARLOS FERMIN FITZCARRALD",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN LUIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN NICOLAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"YAUYA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CASMA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CASMA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Casma",
                                                    "address"=>"Av.Magdalena 240",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BUENA VISTA ALTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COMANDANTE NOEL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YAUTAN",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CORONGO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CORONGO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ACO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"BAMBAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CUSCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LA PAMPA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"YANAC",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"YUPAN",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUARI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUARI",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Huari",
                                                    "address"=>"JR. MANUEL ALVAREZ 1065",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ANRA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CAJAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHAVIN DE HUANTAR",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUACACHI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUACCHIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUACHIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUANTAR",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MASIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAUCAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PONTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"RAHUAPAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"RAPAYAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN MARCOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE CHANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"UCO",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUARMEY",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUARMEY",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Huarmey",
                                                    "address"=>"Av. Alberto Reyes 239",
                                                    "delivery_time"=>"24"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"COCHAPETI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CULEBRAS",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"HUAYAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MALVAS",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUAYLAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CARAZ",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Caraz",
                                                    "address"=>"JR. RAMON CASTILLA S/N",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"HUALLANCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUATA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYLAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MATO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAMPAROMAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PUEBLO LIBRE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTO TORIBIO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YURACMARCA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"MARISCAL LUZURIAGA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PISCOBAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CASCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ELEAZAR GUZMAN BARRON",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"FIDEL OLIVAS ESCUDERO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LLAMA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LLUMPA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LUCMA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MUSGA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"OCROS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"OCROS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ACAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CAJAMARQUILLA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CARHUAPAMPA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COCHAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CONGAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LLIPA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN CRISTOBAL DE RAJAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE CHILCAS",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PALLASCA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CABANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"BOLOGNESI",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CONCHUCOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUACASCHUQUE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUANDOVAL",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LACABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LLAPO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PALLASCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PAMPAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"TAUCA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"POMABAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"POMABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYLLAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAROBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUINUABAMBA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"RECUAY",
                                    "districts" =>[
                                        [
                                            "districtName"=>"RECUAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CATAC",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COTAPARACO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAYLLAPAMPA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LLACLLIN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAMPAS CHICO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PARARIN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"TAPACOCHA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"TICAPAMPA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SANTA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHIMBOTE",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Chimbote",
                                                    "address"=>"Av. Jose Pardo 420",
                                                    "delivery_time"=>"24"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CACERES DEL PERU",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COISHCO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MACATE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MORO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"NEPEÑA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAMANCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"NUEVO CHIMBOTE",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SIHUAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SIHUAS",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"ACOBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ALFONSO UGARTE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CASHAPAMPA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHINGALPO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAYLLABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUICHES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"RAGASH",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SICSIBAMBA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"YUNGAY",
                                    "districts" =>[
                                        [
                                            "districtName"=>"YUNGAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CASCAPARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MANCOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MATACOTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUILLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"RANRAHIRCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SHUPLUY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YANAMA",
                                            "price"=>30
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "APURIMAC",
                            "provinces"=> [
                                [
                                    "provinceName"=>"ABANCAY",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ABANCAY",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Abancay",
                                                    "address"=>"Jr. Elias N° 118",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHACOCHE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CIRCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CURAHUASI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUANIPACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LAMBRAMA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PICHIRHUA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE CACHORA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TAMBURCO",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ANDAHUAYLAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ANDAHUAYLAS",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Andahuaylas",
                                                    "address"=>"Jr. Juan Francisco Ramos 559",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ANDARAPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHIARA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUANCARAMA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUANCARAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUAYANA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"KISHUARA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PACOBAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PACUCHA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PAMPACHIRI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"POMACOCHA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO DE CACHI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JERONIMO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN MIGUEL DE CHACCRAMPA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SANTA MARIA DE CHICMO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TALAVERA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TUMAY HUARACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TURPO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"KAQUIABAMBA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ANTABAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ANTABAMBA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"EL ORO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUAQUIRCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"JUAN ESPINOZA MEDRANO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"OROPESA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PACHACONAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SABAINO",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"AYMARAES",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHALHUANCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CAPAYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CARAYBAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHAPIMARCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COLCABAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COTARUSE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUAYLLO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JUSTO APU SAHUARAURA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LUCRE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"POCOHUANCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE CHACÑA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAÑAYCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SORAYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TAPAIRIHUA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TINTAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TORAYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YANACA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"COTABAMBAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TAMBOBAMBA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COTABAMBAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COYLLURQUI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HAQUIRA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"MARA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHALLHUAHUACHO",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHINCHEROS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHINCHEROS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ANCO-HUALLO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COCHARCAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUACCANA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"OCOBAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ONGOY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"URANMARCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"RANRACANCHA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"GRAU",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHUQUIBAMBILLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CURPAHUASI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"GAMARRA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUAYLLATI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MAMARA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MICAELA BASTIDAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PATAYPAMPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PROGRESO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TURPAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VILCABAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VIRUNDO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CURASCO",
                                            "price"=>20
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "AREQUIPA",
                            "provinces"=> [
                                [
                                    "provinceName"=>"AREQUIPA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"AREQUIPA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Cercado",
                                                    "address"=>"Calle San Jose 103 - A",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ALTO SELVA ALEGRE",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"CAYMA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Cayma",
                                                    "address"=>"AV. EJERCITO 1025-B",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CERRO COLORADO",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Cerro Colorado",
                                                    "address"=>"Calle Miguel Grau 216 - Plaza Las Américas",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHARACATO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHIGUATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JACOBO HUNTER",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"LA JOYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MARIANO MELGAR",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"MIRAFLORES",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Miraflores (AQP)",
                                                    "address"=>"Calle Manuel Muñoz Najar 410 / OFC 4 – 2° PISO",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"MOLLEBAYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PAUCARPATA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Paucarpata",
                                                    "address"=>"Av Jhon Kennedy 1301",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"POCSI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"POLOBAYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"QUEQUEÑA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SABANDIA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SACHACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE SIGUAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE TARUCANI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SANTA ISABEL DE SIGUAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANTA RITA DE SIGUAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SOCABAYA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"TIABAYA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"UCHUMAYO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VITOR",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YANAHUARA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Yanahuara",
                                                    "address"=>"Calle Pampita Zevallos 175",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"YARABAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YURA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JOSE LUIS BUSTAMANTE Y RIVERO",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Jose Luis Bustamante Y Rivero",
                                                    "address"=>"Calle Daniel Alcides Carrion 269 La Pampilla",
                                                    "delivery_time"=>"48"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CAMANA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CAMANA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Camana",
                                                    "address"=>"Av. Mariscal Castilla 156",
                                                    "delivery_time"=>"36"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"JOSE MARIA QUIMPER",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MARIANO NICOLAS VALCARCEL",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MARISCAL CACERES",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"NICOLAS DE PIEROLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"OCOÑA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"QUILCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAMUEL PASTOR",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CARAVELI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CARAVELI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ACARI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ATICO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ATIQUIPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"BELLA UNION",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CAHUACHO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHALA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHAPARRA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUANUHUANU",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JAQUI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LOMAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"QUICACHA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YAUCA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CASTILLA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"APLAO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ANDAGUA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"AYO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHACHAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHILCAYMARCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHOCO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUANCARQUI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MACHAGUAY",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ORCOPAMPA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PAMPACOLCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TIPAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"UÑON",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"URACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VIRACO",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CAYLLOMA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHIVAY",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Chivay",
                                                    "address"=>"Puente Del Inca 103",
                                                    "delivery_time"=>"36"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ACHOMA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CABANACONDE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CALLALLI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CAYLLOMA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COPORAQUE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUAMBO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUANCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ICHUPAMPA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LARI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LLUTA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"MACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MADRIGAL",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO DE CHUCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SIBAYO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TAPAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TISCO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TUTI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YANQUE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MAJES",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Majes (El Pedregal)",
                                                    "address"=>"Av. Carlos Shuton Mz A-21 Lte. 1",
                                                    "delivery_time"=>"36"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CONDESUYOS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHUQUIBAMBA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ANDARAY",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CAYARANI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHICHAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"IRAY",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"RIO GRANDE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SALAMANCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YANAQUIHUA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ISLAY",
                                    "districts" =>[
                                        [
                                            "districtName"=>"MOLLENDO",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina  Mollendo",
                                                    "address"=>"CALLE DEAN VALDIVIA 388",
                                                    "delivery_time"=>"36"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"COCACHACRA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"DEAN VALDIVIA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ISLAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MEJIA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PUNTA DE BOMBON",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LA UNION",
                                    "districts" =>[
                                        [
                                            "districtName"=>"COTAHUASI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ALCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHARCANA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUAYNACOTAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PAMPAMARCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PUYCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"QUECHUALLA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAYLA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TAURIA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TOMEPAMPA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TORO",
                                            "price"=>35
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "AYACUCHO",
                            "provinces"=> [
                                [
                                    "provinceName"=>"HUAMANGA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"AYACUCHO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Ayacucho Central",
                                                    "address"=>"Urb.Mariscal Caceres Mz. L Lote 45",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ACOCRO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ACOS VINCHOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CARMEN ALTO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHIARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OCROS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACAYCASA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUINUA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE DE TICLLAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN BAUTISTA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE PISCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SOCOS",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"TAMBILLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VINCHOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"JESUS NAZARENO",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CANGALLO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CANGALLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHUSCHI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LOS MOROCHUCOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARIA PARADO DE BELLIDO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PARAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"TOTOS",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUANCA SANCOS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SANCOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CARAPO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SACSAMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE LUCANAMARCA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUANTA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUANTA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Huanta",
                                                    "address"=>"Jr. Gervasio Santillana N° 340",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AYAHUANCO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAMANGUILLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"IGUAIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LURICOCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTILLANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SIVIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LLOCHEGUA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LA MAR",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN MIGUEL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ANCO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO(AYNA)",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Francisco",
                                                    "address"=>"JR. Ayacucho N° 212 - Las Palmeras",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHILCAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHUNGUI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LUIS CARRANZA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TAMBO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LUCANAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PUQUIO",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Puquio",
                                                    "address"=>"Jr. Tacna 698 - Ref. Fte Comisaria PNP",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AUCARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CABANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CARMEN SALCEDO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHAVIÑA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHIPAO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAC-HUAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LARAMATE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LEONCIO PRADO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LLAUTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LUCANAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OCAÑA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"OTOCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAISA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN CRISTOBAL",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE PALCO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANCOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTA ANA DE HUAYCAHUACHO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTA LUCIA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PARINACOCHAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CORACORA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHUMPI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CORONEL CASTAÑEDA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PACAPAUSA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PULLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PUYUSCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO DE RAVACAYCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"UPAHUACHO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PAUCAR DEL SARA SARA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PAUSA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COLTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CORCULLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARCABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OYOLO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PARARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JAVIER DE ALPABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE DE USHUA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SARA SARA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SUCRE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"QUEROBAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"BELEN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHALCOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHILCAYOC",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUACAÑA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MORCOLLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAICO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE LARCAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN SALVADOR DE QUIJE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE PAUCARAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SORAS",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"VICTOR FAJARDO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUANCAPI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ALCAMENCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"APONGO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ASQUIPATA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CANARIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CAYARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COLCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAMANQUIQUIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANCARAYLLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SARHUA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VILCANCHOS",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"VILCAS HUAMAN",
                                    "districts" =>[
                                        [
                                            "districtName"=>"VILCAS HUAMAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ACCOMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CARHUANCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CONCEPCION",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAMBALPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"INDEPENDENCIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAURAMA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VISCHONGO",
                                            "price"=>14
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "CAJAMARCA",
                            "provinces"=> [
                                [
                                    "provinceName"=>"CAJAMARCA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CAJAMARCA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Cajamarca",
                                                    "address"=>"Los Nogales N° 426",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ASUNCION",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHETILLA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COSPAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ENCAÑADA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JESUS",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"LLACANORA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LOS BAÑOS DEL INCA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"MAGDALENA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MATARA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"NAMORA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CAJABAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CAJABAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CACHACHI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CONDEBAMBA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SITACOCHA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CELENDIN",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CELENDIN",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Celendin",
                                                    "address"=>"Jr. Pardo N°. 282",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHUMUCH",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CORTEGANA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUASMIN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JORGE CHAVEZ",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"JOSE GALVEZ",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MIGUEL IGLESIAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"OXAMARCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SOROCHUCO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SUCRE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"UTCO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LA LIBERTAD DE PALLAN",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHOTA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHOTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ANGUIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHADIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHIGUIRIP",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHIMBAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHOROPAMPA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COCHABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CONCHAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAMBOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LAJAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LLAMA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MIRACOSTA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PACCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PION",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"QUEROCOTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE LICUPIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"TACABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TOCMOCHE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHALAMARCA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CONTUMAZA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CONTUMAZA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHILETE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CUPISNIQUE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"GUZMANGO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN BENITO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ DE TOLED",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TANTARICA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YONAN",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CUTERVO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CUTERVO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CALLAYUC",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHOROS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CUJILLO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LA RAMADA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PIMPINGOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"QUEROCOTILLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN ANDRES DE CUTERVO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE CUTERVO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN LUIS DE LUCMA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTO DOMINGO DE LA CAPILLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTO TOMAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SOCOTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TORIBIO CASANOVA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUALGAYOC",
                                    "districts" =>[
                                        [
                                            "districtName"=>"BAMBAMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHUGUR",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUALGAYOC",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"JAEN",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JAEN",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Jaen",
                                                    "address"=>"San Martin 1255",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BELLAVISTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHONTALI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COLASAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUABAL",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LAS PIRIAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"POMAHUACA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PUCARA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Pucara",
                                                    "address"=>"Carretera A Chiclayo Restaurante Los Pucara",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
    
                                        ],
                                        [
                                            "districtName"=>"SALLIQUE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN FELIPE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE DEL ALTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SAN IGNACIO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN IGNACIO",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Ignacio",
                                                    "address"=>"JR.Jaen Cdr. 1 S/N Cruce Jr.Zarumilla",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHIRINOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUARANGO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LA COIPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"NAMBALLE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE DE LOURDES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TABACONAS",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SAN MARCOS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PEDRO GALVEZ",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHANCAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"EDUARDO VILLANUEVA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"GREGORIO PITA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ICHOCAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JOSE MANUEL QUIROZ",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JOSE SABOGAL",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SAN MIGUEL",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN MIGUEL",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Miguel",
                                                    "address"=>"Jr. Grau 338",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BOLIVAR",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CALQUIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CATILLUC",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"EL PRADO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LA FLORIDA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LLAPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"NANCHOC",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"NIEPOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN GREGORIO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN SILVESTRE DE COCHAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TONGOD",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"UNION AGUA BLANCA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SAN PABLO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN PABLO",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Pablo (Cajamarca)",
                                                    "address"=>"JR. NESTOR BATANEROS 478",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SAN BERNARDINO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN LUIS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TUMBADEN",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SANTA CRUZ",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SANTA CRUZ",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Santa Cruz",
                                                    "address"=>"Jr. Zarumilla 150",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ANDABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CATACHE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHANCAYBAÑOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LA ESPERANZA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"NINABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PULAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAUCEPAMPA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SEXI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"UTICYACU",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"YAUYUCAN",
                                            "price"=>30
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "CALLAO",
                            "provinces"=> [
                                [
                                    "provinceName"=>"CALLAO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CALLAO",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Almacén Olva General",
                                                    "address"=>"Av. Argentina 4458 Callao",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BELLAVISTA",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"CARMEN DE LA LEGUA REYNOSO",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"LA PERLA",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"LA PUNTA",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"VENTANILLA",
                                            "price"=>8
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "CUSCO",
                            "provinces"=> [
                                [
                                    "provinceName"=>"CUSCO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CUSCO",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Cusco",
                                                    "address"=>"Av. Pardo 575B - Paseo De Los Heroes",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CCORCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"POROY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JERÓNIMO",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SAN SEBASTIÁN",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SAYLLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"WANCHAQ",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Wanchaq",
                                                    "address"=>"Av. Micaela Bastidas N°329 B",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ACOMAYO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ACOMAYO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ACOPIA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ACOS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"MOSOC LLACTA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"POMACANCHI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"RONDOCAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANGARARA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ANTA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ANTA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ANCAHUASI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CACHIMAYO",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"CHINCHAYPUJIO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUAROCONDO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LIMATAMBO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MOLLEPATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PUCYURA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ZURITE",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CALCA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CALCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LAMAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LARES",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PISAC",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN SALVADOR",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TARAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YANATILE",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CANAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"YANAOCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHECCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"KUNTURKANKI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LANGUI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LAYO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PAMPAMARCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"QUEHUE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TUPAC AMARU",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CANCHIS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SICUANI",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Sicuani",
                                                    "address"=>"JR.GARCILAZO DE LA VEGA 145 PLAZA ARMAS",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHECACUPE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COMBAPATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MARANGANI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PITUMARCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN PABLO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TINTA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHUMBIVILCAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SANTO TOMAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CAPACMARCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHAMACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COLQUEMARCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LIVITACA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LLUSCO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"QUIÑOTA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VELILLE",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ESPINAR",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ESPINAR",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CONDOROMA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COPORAQUE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"OCORURO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PALLPATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PICHIGUA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SUYCKUTAMBO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ALTO PICHIGUA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LA CONVENCION",
                                    "districts" =>[
                                        [
                                            "districtName"=>"QUILLABAMBA (SANTA ANA)",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"tienda Quillabamba",
                                                    "address"=>"Av. San Martín N° 685 – Santa Anta – La Convención",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ECHARATE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUAYOPATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MARANURA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"OCOBAMBA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"QUELLOUNO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"KIMBIRI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANTA TERESA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"VILCABAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PICHARI",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Pichari",
                                                    "address"=>"AV. INCA GARCILASO DE LA VEGA S/N",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PARURO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PARURO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ACCHA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CCAPI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COLCHA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUANOQUITE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"OMACHA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PACCARITAMBO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PILLPINTO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YAURISQUE",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PAUCARTAMBO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PAUCARTAMBO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CAICAY",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHALLABAMBA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COLQUEPATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUANCARANI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"KOSÑIPATA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"QUISPICANCHI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"URCOS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ANDAHUAYLILLAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CAMANTI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CCARHUAYO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CCATCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CUSIPATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUARO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LUCRE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MARCAPATA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"OCONGATE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"OROPESA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"QUIQUIJANA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"URUBAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"URUBAMBA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Urubamba",
                                                    "address"=>"AV. MARISCAL CASTILLA 501-B",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHINCHERO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUAYLLABAMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MACHUPICCHU",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MARAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"OLLANTAYTAMBO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YUCAY",
                                            "price"=>20
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "HUANCAVELICA",
                            "provinces"=> [
                                [
                                    "provinceName"=>"HUANCAVELICA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUANCAVELICA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ACOBAMBILLA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ACORIA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Acoria",
                                                    "address"=>"Av. 28 De Julio N°126",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CONAYCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CUENCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUACHOCOLPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYLLAHUARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"IZCUCHACA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LARIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MANTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARISCAL CACERES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MOYA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"NUEVO OCCORO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PALCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PILCHACA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VILCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"YAULI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ASCENSION",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANDO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ACOBAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ACOBAMBA",
                                            "price"=>30,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Acobamba (HCV)",
                                                    "address"=>"Av. Manuel Candamo S/N Cerdado",
                                                    "delivery_time"=>"5760"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ANDABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ANTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CAJA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARCAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAUCARA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Paucara",
                                                    "address"=>"Jr. Lima S/N Paucara",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"POMACOCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ROSARIO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ANGARAES",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LIRCAY",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Lircay",
                                                    "address"=>"JR CAHUIDE 146 PUEBLO NUEVO - ANGARAES",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ANCHONGA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CALLANMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CCOCHACCASA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHINCHO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CONGALLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANCA-HUANCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYLLAY GRANDE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"JULCAMARCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO DE ANTAPARCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTO TOMAS DE PATA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SECCLLA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CASTROVIRREYNA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CASTROVIRREYNA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ARMA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"AURAHUA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CAPILLAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHUPAMARCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COCAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUACHOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAMATAMBO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MOLLEPAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA ANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TANTARA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"TICRAPO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHURCAMPA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHURCAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ANCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHINCHIHUASI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"EL CARMEN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LA MERCED",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LOCROJA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAUCARBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN MIGUEL DE MAYOCC",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE CORIS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACHAMARCA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUAYTARA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUAYTARA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Huaytara",
                                                    "address"=>"26 DE SETIEMBRE S/N - PLAZA DE ARMAS",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AYAVI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CORDOVA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYACUNDO ARMA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LARAMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OCOYO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PILPICHACA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUERCO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"QUITO-ARMA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO DE CUSICANCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO DE SANGAYAICO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN ISIDRO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE CHOCORVOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE QUIRAHUARA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTO DOMINGO DE CAPILLAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TAMBO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"TAYACAJA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PAMPAS",
                                            "price"=>30,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Pampas",
                                                    "address"=>"JR. CORDOVA 530 CERCADO",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ACOSTAMBO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ACRAQUIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"AHUAYCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COLCABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"DANIEL HERNANDEZ",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUACHOCOLPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUARIBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ÑAHUIMPUQUIO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAZOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUISHUAR",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SALCABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SALCAHUASI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN MARCOS DE ROCCHAC",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SURCUBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TINTAY PUNCU",
                                            "price"=>30
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "HUANUCO",
                            "provinces"=> [
                                [
                                    "provinceName"=>"HUANUCO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUANUCO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Huanuco",
                                                    "address"=>"DAMASO BERAUN 995",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AMARILIS",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHINCHAO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHURUBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARGOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUISQUI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO DE CAYRAN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE CHAULAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA MARIA DEL VALLE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YARUMAYO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PILLCO MARCA",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"AMBO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"AMBO",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Ambo",
                                                    "address"=>"Jr. El Progreso N° 120-A",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CAYNA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COLPAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CONCHAMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUACAR",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN RAFAEL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TOMAY KICHWA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"DOS DE MAYO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LA UNION",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHUQUIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MARIAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PACHAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"QUIVILLA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"RIPAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SHUNQUI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SILLAPATA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"YANAS",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUACAYBAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUACAYBAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CANCHABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COCHABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PINRA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUAMALIES",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LLATA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ARANCAY",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHAVIN DE PARIARCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"JACAS GRANDE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"JIRCAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MIRAFLORES",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MONZON",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PUNCHAO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PUÑOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SINGA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"TANTAMAYO",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LEONCIO PRADO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TINGO MARÍA(RUPA-RUPA)",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Tingo María",
                                                    "address"=>"JR. SAN ALEJANDRO 264",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"DANIEL ALOMIAS ROBLES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HERMILIO VALDIZAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"JOSE CRESPO Y CASTILLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LUYANDO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARIANO DAMASO BERAUN",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"MARAÑON",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUACRACHUCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHOLON",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN BUENAVENTURA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PACHITEA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PANAO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHAGLLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MOLINO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"UMARI",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PUERTO INCA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PUERTO INCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CODO DEL POZUZO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HONORIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TOURNAVISTA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YUYAPICHIS",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LAURICOCHA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JESUS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"BAÑOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"JIVIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"QUEROPALCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"RONDOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO DE ASIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN MIGUEL DE CAURI",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"YAROWILCA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHAVINILLO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CAHUAC",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHACABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"APARICIO POMARES",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"JACAS CHICO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OBAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PAMPAMARCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHORAS",
                                            "price"=>30
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "ICA",
                            "provinces"=> [
                                [
                                    "provinceName"=>"ICA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ICA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Ica",
                                                    "address"=>"CALLE GUATEMALA 161 URB. SAN FRANCISCO",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"LA TINGUIÑA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LOS AQUIJES",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"OCUCAJE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACHACUTEC",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PARCONA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PUEBLO NUEVO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SALAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE DE LOS MOLINOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN BAUTISTA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SUBTANJALLA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"TATE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YAUCA DEL ROSARIO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHINCHA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHINCHA ALTA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Olva Chincha",
                                                    "address"=>"SANTO DOMINGO 223 CHINCHA ALTA",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ALTO LARAN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHAVIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHINCHA BAJA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"EL CARMEN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"GROCIO PRADO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PUEBLO NUEVO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE YANAC",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE HUACARPANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SUNAMPE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"TAMBO DE MORA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"NAZCA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"NAZCA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Nazca",
                                                    "address"=>"Calle Ignacio Morsesqui 401 Of. 07",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHANGUILLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"EL INGENIO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARCONA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VISTA ALEGRE",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PALPA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PALPA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LLIPATA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"RIO GRANDE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TIBILLO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PISCO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PISCO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Pisco",
                                                    "address"=>"JR. CALLAO 556",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"HUANCANO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUMAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"INDEPENDENCIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PARACAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN ANDRES",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAN CLEMENTE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"TUPAC AMARU INCA",
                                            "price"=>9
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "JUNIN",
                            "provinces"=> [
                                [
                                    "provinceName"=>"HUANCAYO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUANCAYO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Huancayo",
                                                    "address"=>"JR. PUNO N° 141",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CARHUACALLANGA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHACAPAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHICCHE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHILCA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHONGOS ALTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHUPURO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"COLCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CULLHUAS",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"EL TAMBO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Huancayo Almacén El Tambo",
                                                    "address"=>"Av.Julio Sumar 162 El Tambo",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"HUACRAPUQUIO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUALHUAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANCAN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"HUASICANCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYUCACHI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"INGENIO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PARIAHUANCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PILCOMAYO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PUCARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUICHUAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUILCAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN AGUSTIN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAN JERONIMO DE TUNAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAÑO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAPALLANGA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SICAYA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTO DOMINGO DE ACOBAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"VIQUES",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CONCEPCION",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CONCEPCION",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ACO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"ANDAMARCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHAMBARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COCHAS",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"COMAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HEROINAS TOLEDO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MANZANARES",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MARISCAL CASTILLA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MATAHUASI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MITO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"NUEVE DE JULIO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"ORCOTUNA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE DE QUERO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA DE OCOPA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHANCHAMAYO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHANCHAMAYO (LA MERCED)",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva La Merced",
                                                    "address"=>"JR. LIMA 227",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"PERENE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PICHANAQUI",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Pichanaqui",
                                                    "address"=>"AV.CARRETERA MARGINAL 481",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SAN LUIS DE SHUARO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN RAMON",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva San Ramón",
                                                    "address"=>"Jirón Progreso 418-Distrito De San Ramón",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"VITOC",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"JAUJA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JAUJA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Jauja",
                                                    "address"=>"JR BOLIVAR 845 (COSTADO BCO NACION)",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ACOLLA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"APATA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ATAURA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CANCHAYLLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CURICACA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"EL MANTARO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"HUAMALI",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"HUARIPAMPA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"HUERTAS",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"JANJAILLO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"JULCAN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LEONOR ORDOÑEZ",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LLOCLLAPAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARCO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MASMA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MASMA CHICCHE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MOLINOS",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MONOBAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MUQUI",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MUQUIYAUYO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PACA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PACCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PANCAN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PARCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"POMACANCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"RICRAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN LORENZO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE CHUNAN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAUSA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SINCOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TUNAN MARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YAULI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YAUYOS",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"JUNIN",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JUNIN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CARHUAMAYO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ONDORES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ULCUMAYO",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SATIPO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SATIPO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Satipo",
                                                    "address"=>"JR.MANUEL PRADO 239",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"COVIRIALI",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LLAYLLA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MAZAMARI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAMPA HERMOSA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PANGOA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Pangoa(Satipo)",
                                                    "address"=>"Av. 28 De Julio 744",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"RIO NEGRO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"RIO TAMBO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"TARMA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TARMA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Tarma",
                                                    "address"=>"JR.PASCO 464",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ACOBAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUARICOLCA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"HUASAHUASI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LA UNION",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PALCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PALCAMAYO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE CAJAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TAPO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"YAULI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LA OROYA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHACAPALPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAY-HUAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARCAPOMACOCHA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MOROCOCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACCHA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SANTA BARBARA DE CARHUACAYAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA DE SACCO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SUITUCANCHA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YAULI",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHUPACA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHUPACA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"AHUAC",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHONGOS BAJO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUACHAC",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAMANCACA CHICO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE YSCOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE JARPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TRES DE DICIEMBRE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YANACANCHA",
                                            "price"=>14
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "LA LIBERTAD",
                            "provinces"=> [
                                [
                                    "provinceName"=>"TRUJILLO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TRUJILLO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Trujillo",
                                                    "address"=>"Av. Tupac Amaru N° 1720 Urb. Alto Mochica",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"EL PORVENIR",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"FLORENCIA DE MORA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"HUANCHACO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LA ESPERANZA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LAREDO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MOCHE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"POROTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SALAVERRY",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SIMBAL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VICTOR LARCO HERRERA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Paujiles",
                                                    "address"=>"Psje. Los Paujiles Mz.A Lt. 04 Urb. Los Pinos",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ASCOPE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ASCOPE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHICAMA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHOCOPE",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Chocope",
                                                    "address"=>"Calle Diego De Mora 235",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"MAGDALENA DE CAO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAIJAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"RAZURI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE CAO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CASA GRANDE",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"BOLIVAR",
                                    "districts" =>[
                                        [
                                            "districtName"=>"BOLIVAR",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"BAMBAMARCA",
                                            "price"=>13
                                        ],
                                        [
                                            "districtName"=>"CONDORMARCA",
                                            "price"=>13
                                        ],
                                        [
                                            "districtName"=>"LONGOTEA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"UCHUMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"UCUNCHA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHEPEN",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHEPEN",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Chepen",
                                                    "address"=>"AV. GONZALES CACEDA 707",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"PACANGA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PUEBLO NUEVO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"JULCAN",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JULCAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CALAMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CARABAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUASO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"OTUZCO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"OTUZCO",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Otuzco",
                                                    "address"=>"Calle Progreso N°666",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AGALLPAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHARAT",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUARANCHAL",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LA CUESTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MACHE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PARANDAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SALPO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SINSICAP",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"USQUIL",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PACASMAYO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN PEDRO DE LLOC",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"GUADALUPE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"JEQUETEPEQUE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PACASMAYO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Pacasmayo",
                                                    "address"=>"AV. 28 DE JULIO Nº 120",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PATAZ",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TAYABAMBA",
                                            "price"=>30,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Tayabamba",
                                                    "address"=>"Jr. Salaverry Cdra. 2 Puesto 3",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BULDIBUYO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHILLIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANCASPATA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAYLILLAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ONGON",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PARCOY",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Parcoy/Retamas",
                                                    "address"=>"Carretera Tayabamba S/N (Frente Com. Silva)",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"PATAZ",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"PIAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE CHALLAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TAURIJA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"URPAY",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SANCHEZ CARRION",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUAMACHUCO",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Huamachuco",
                                                    "address"=>"Jr. Leoncio Prado 527",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHUGAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COCHORCO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CURGOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MARCABAL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANAGORAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SARIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SARTIMBAMBA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SANTIAGO DE CHUCO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SANTIAGO DE CHUCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ANGASMARCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CACHICADAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MOLLEBAMBA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MOLLEPATA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"QUIRUVILCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ DE CHUCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SITABAMBA",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"GRAN CHIMU",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CASCAS",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Cascas",
                                                    "address"=>"Calle 28 De Julio N° 301",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"LUCMA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COMPIN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAYAPULLO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"VIRU",
                                    "districts" =>[
                                        [
                                            "districtName"=>"VIRU",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Viru",
                                                    "address"=>"AV. PANAMERICANA NORTE 381 PUENTE VIRU",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHAO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"GUADALUPITO",
                                            "price"=>9
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "LAMBAYEQUE",
                            "provinces"=> [
                                [
                                    "provinceName"=>"CHICLAYO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHICLAYO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Santa Victoria",
                                                    "address"=>"Av. Grau 675, Urb. Santa Victoria",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHONGOYAPE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ETEN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"ETEN PUERTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"JOSE LEONARDO ORTIZ",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LA VICTORIA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LAGUNAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MONSEFU",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"NUEVA ARICA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OYOTUN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PICSI",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PIMENTEL",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"REQUE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAÑA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CAYALTI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PATAPO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"POMALCA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PUCALA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TUMAN",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"FERREÑAFE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"FERREÑAFE",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Ferreñafe",
                                                    "address"=>"Calle Nicanor Carmona 124",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CAÑARIS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"INCAHUASI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MANUEL ANTONIO MESONES MURO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PITIPO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PUEBLO NUEVO",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LAMBAYEQUE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LAMBAYEQUE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHOCHOPE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ILLIMO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"JAYANCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MOCHUMI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MORROPE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"MOTUPE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OLMOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACORA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SALAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"TUCUME",
                                            "price"=>14
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "LIMA",
                            "provinces"=> [
                                [
                                    "provinceName"=>"LIMA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LIMA",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Cercado De Lima",
                                                    "address"=>"Psje.Acuña 131",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ANCON",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"ATE",
                                            "price"=>8
                                        ],
                                        [
                                            "districtName"=>"BARRANCO",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Barranca",
                                                    "address"=>"Av. Primavera 251",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BREÑA",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"CARABAYLLO",
                                            "price"=>8
                                        ],
                                        [
                                            "districtName"=>"CHACLACAYO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHORRILLOS",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Chorrillos",
                                                    "address"=>"Prol. Av. Paseo De La República 1515",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CIENEGUILLA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"COMAS",
                                            "price"=>8,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Comas",
                                                    "address"=>"Av. Universitaria Norte 6971 - 6973",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"EL AGUSTINO",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"INDEPENDENCIA",
                                            "price"=>8
                                        ],
                                        [
                                            "districtName"=>"JESUS MARIA",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"LA MOLINA",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda La Molina",
                                                    "address"=>"Av Cnel Juan Pringues 458 Tda 2047",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"LA VICTORIA",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"LINCE",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Lince",
                                                    "address"=>"Av. Gral Alvarez De Arenales 1775",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"LOS OLIVOS",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Los Olivos",
                                                    "address"=>"Av. Carlos Izaguirre 1006 - Urb Palmeras",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHOSICA(LURIGANCHO)",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Chosica",
                                                    "address"=>"Prol. Av. Paseo De La República 1515",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"LURIN",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Lurin",
                                                    "address"=>"Jr. Bolognesi 398",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"MAGDALENA DEL MAR",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"PUEBLO LIBRE(MAGDALENA VIEJA)",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Pueblo Libre",
                                                    "address"=>"Av. Bolivar 1427 A",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"MIRAFLORES",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Punto De Venta Autorizado Mail Boxes ETC. (Miraflores)",
                                                    "address"=>"Calle Berlin 219 , Miraflores",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"PACHACAMAC",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PUCUSANA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PUENTE PIEDRA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PUNTA HERMOSA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PUNTA NEGRA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"RIMAC",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"SAN BARTOLO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAN BORJA",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Borja",
                                                    "address"=>"Av. Aviación Nro. 3532 - San Borja",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SAN ISIDRO",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Punto De Venta Autorizado Mail Boxes ETC. (San Isidro)",
                                                    "address"=>"Calle Begonias 774 San Isidro",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE LURIGANCHO",
                                            "price"=>8,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Juan De Lurigancho",
                                                    "address"=>"Proceres De La Independ.2532 Mz W Lt 24",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE MIRAFLORES",
                                            "price"=>8,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Juan De Miraflores",
                                                    "address"=>"Av. San Juan 713",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SAN LUIS",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Luis",
                                                    "address"=>"Av. Del Aire 1322 - San Luis",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SAN MARTIN DE PORRES",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"SAN MIGUEL",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Punto De Venta Autorizado Aduacargo Express (San Miguel)",
                                                    "address"=>"Calle Intisuyo 295, Urb. Maranga, Etapa 7",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SANTA ANITA",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Santa Anita",
                                                    "address"=>"Av. Santa Rosa 131",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SURCO",
                                            "price"=>7,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Surco",
                                                    "address"=>"Av. El Polo 108",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SANTA MARIA DEL MAR",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE SURCO",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"SURQUILLO",
                                            "price"=>7
                                        ],
                                        [
                                            "districtName"=>"VILLA EL SALVADOR",
                                            "price"=>8
                                        ],
                                        [
                                            "districtName"=>"VILLA MARIA DEL TRIUNFO",
                                            "price"=>8
                                        ],
                                    ]
                                ],
                                [
                                    "provinceName"=>"BARRANCA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"BARRANCA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PARAMONGA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"PATIVILCA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SUPE",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SUPE PUERTO",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CAJATAMBO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CAJATAMBO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"GORGOR",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUANCAPON",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MANAS",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CANTA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CANTA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"ARAHUAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAMANTANGA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAROS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LACHAQUI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN BUENAVENTURA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA DE QUIVES",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CAÑETE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN VICENTE DE CAÑETE",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Cañete",
                                                    "address"=>"Jr. 2 De Mayo 601-D San Vicente",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ASIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CALANGO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CERRO AZUL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHILCA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"COAYLLO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"IMPERIAL",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LUNAHUANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"MALA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Mala",
                                                    "address"=>"Av. Antigua Pan. Sur 243",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"NUEVO IMPERIAL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACARAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUILMANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN LUIS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ DE FLORES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ZUÑIGA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUARAL",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUARAL",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Huaral",
                                                    "address"=>"Jr. Dos De Mayo N° 235",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ATAVILLOS ALTO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ATAVILLOS BAJO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"AUCALLAMA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHANCAY",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Chancay",
                                                    "address"=>"Calle Leoncio Prado 114",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"IHUARI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LAMPIAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACARAOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN MIGUEL DE ACOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ DE ANDAMARCA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SUMBILCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VEINTISIETE DE NOVIEMBRE",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUAROCHIRI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"MATUCANA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"ANTIOQUIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CALLAHUANCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CARAMPOMA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CHICLA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CUENCA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUACHUPAMPA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUANZA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAROCHIRI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LAHUAYTAMBO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LANGA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LARAOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MARIATANA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"RICARDO PALMA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN ANDRES DE TUPICOCHA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN BARTOLOME",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN DAMIAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE IRIS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE TANTARANCHE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN LORENZO DE QUINTI",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN MATEO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN MATEO DE OTAO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE CASTA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE HUANCAYRE",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANGALLAYA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ DE COCACHACRA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTA EULALIA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE ANCHUCAYA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE TUNA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTO DOMINGO DE LOS OLLEROS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SURCO",
                                            "price"=>30
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUAURA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUACHO",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Huacho",
                                                    "address"=>"Jr. Mariscal Castilla 117",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AMBAR",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CALETA DE CARQUIN",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"CHECRAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUALMAY",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"HUAURA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"LEONCIO PRADO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACCHO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA LEONOR",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SANTA MARIA",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"SAYAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VEGUETA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"OYON",
                                    "districts" =>[
                                        [
                                            "districtName"=>"OYON",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ANDAJES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CAUJUL",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COCHAMARCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"NAVAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PACHANGARA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"YAUYOS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"YAUYOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"ALIS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"AYAUCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"AYAVIRI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"AZANGARO",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"CACRA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CARANIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CATAHUASI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHOCOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"COCHAS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"COLONIA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HONGOS",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUAMPARA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANCAYA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANGASCAR",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"HUANTAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAÑEC",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"LARAOS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"LINCHA",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MADEAN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"MIRAFLORES",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"OMAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PUTINZA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"QUINCHES",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"QUINOCAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN JOAQUIN",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE PILAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TANTA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TAURIPAMPA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TOMAS",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TUPE",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VIÑAC",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"VITIS",
                                            "price"=>14
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "LORETO",
                            "provinces"=> [
                                [
                                    "provinceName"=>"MAYNAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"IQUITOS",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Nauta",
                                                    "address"=>"Jr. Junín N° 279",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ALTO NANAY",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"FERNANDO LORES",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"INDIANA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LAS AMAZONAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"MAZAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"NAPO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PUNCHANA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"PUTUMAYO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TORRES CAUSANA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"BELEN",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN BAUTISTA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"TENIENTE MANUEL CLAVERO",
                                            "price"=>0
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ALTO AMAZONAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"YURIMAGUAS",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Yurimaguas",
                                                    "address"=>"ARICA 318",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BALSAPUERTO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JEBEROS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LAGUNAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SANTA CRUZ",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TENIENTE CESAR LOPEZ ROJAS",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LORETO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"NAUTA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PARINARI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TIGRE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TROMPETEROS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"URARINAS",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"MARISCAL RAMON CASTILLA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"RAMON CASTILLA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PEBAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YAVARI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN PABLO",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"REQUENA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"REQUENA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ALTO TAPICHE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CAPELO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"EMILIO SAN MARTIN",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"MAQUIA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PUINAHUA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAQUENA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SOPLIN",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TAPICHE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"JENARO HERRERA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YAQUERANA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"UCAYALI",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CONTAMANA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"INAHUAYA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PADRE MARQUEZ",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PAMPA HERMOSA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SARAYACU",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VARGAS GUERRA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"DATEM DEL MARAÑON",
                                    "districts" =>[
                                        [
                                            "districtName"=>"BARRANCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CAHUAPANAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MANSERICHE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"MORONA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PASTAZA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ANDOAS",
                                            "price"=>35
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "MADRE DE DIOS",
                            "provinces"=> [
                                [
                                    "provinceName"=>"TAMBOPATA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TAMBOPATA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Puerto Maldonado",
                                                    "address"=>"Av. Leon Velarde 770",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"INAMBARI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LAS PIEDRAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LABERINTO",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"MANU",
                                    "districts" =>[
                                        [
                                            "districtName"=>"MANU",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"FITZCARRALD",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MADRE DE DIOS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUEPETUHE",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"TAHUAMANU",
                                    "districts" =>[
                                        [
                                            "districtName"=>"IÑAPARI",
                                            "price"=>35,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Iñapari",
                                                    "address"=>"JR. GARCILASO DE LA VEGA S/N P. JUDICIAL",
                                                    "delivery_time"=>"5760"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"IBERIA",
                                            "price"=>35,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Iberia",
                                                    "address"=>"CALLE LORETO 356 IBERIA",
                                                    "delivery_time"=>"5760"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"TAHUAMANU",
                                            "price"=>35
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "MOQUEGUA",
                            "provinces"=> [
                                [
                                    "provinceName"=>"MARISCAL NIETO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"MOQUEGUA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Moquegua",
                                                    "address"=>"Calle Lima Nº 220",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CARUMAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CUCHUMBAYA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAMEGUA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN CRISTOBAL",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TORATA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"GENERAL SANCHEZ CERRO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"OMATE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CHOJATA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COALAQUE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ICHUÑA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LA CAPILLA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LLOQUE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"MATALAQUE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PUQUINA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"QUINISTAQUILLAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"UBINAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YUNGA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ILO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ILO",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Ilo",
                                                    "address"=>"Calle Callao 230-B",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"EL ALGARROBAL",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PACOCHA",
                                            "price"=>20
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "PASCO",
                            "provinces"=> [
                                [
                                    "provinceName"=>"PASCO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHAUPIMARCA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Cerro de Pasco",
                                                    "address"=>"Pje. Agustín Gamarra 109 URB. San Juan",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"HUACHON",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUARIACA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUAYLLAY",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"NINACACA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PALLANCHACRA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAUCARTAMBO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN FRANCISCO DE ASIS DE YARUSYACAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SIMON BOLIVAR",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"TICLACAYAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TINYAHUARCO",
                                            "price"=>9
                                        ],
                                        [
                                            "districtName"=>"VICCO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"YANACANCHA",
                                            "price"=>9
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"DANIEL ALCIDES CARRION",
                                    "districts" =>[
                                        [
                                            "districtName"=>"YANAHUANCA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"CHACAYAN",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"GOYLLARISQUIZGA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PAUCAR",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE PILLAO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"SANTA ANA DE TUSI",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"TAPUC",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"VILCABAMBA",
                                            "price"=>14
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"OXAPAMPA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"OXAPAMPA",
                                            "price"=>9,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Oxapampa",
                                                    "address"=>"JR. BOLIVAR 565",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CHONTABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"HUANCABAMBA",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PALCAZU",
                                            "price"=>30
                                        ],
                                        [
                                            "districtName"=>"POZUZO",
                                            "price"=>14
                                        ],
                                        [
                                            "districtName"=>"PUERTO BERMUDEZ",
                                            "price"=>30,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Puerto Bermudez",
                                                    "address"=>"Bosquecillo S/N Puesto 2",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"VILLA RICA",
                                            "price"=>14,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Villa Rica",
                                                    "address"=>"AV. LEOPOLDO KRAUSSE 646",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "PIURA",
                            "provinces"=> [
                                [
                                    "provinceName"=>"PIURA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PIURA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Piura",
                                                    "address"=>"Av. Marcavelica Mz.D2 Lt. 13 II Etapa Los Ficus",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CASTILLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CATACAOS",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"CURA MORI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"EL TALLAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LA ARENA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LA UNION",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda La Unión",
                                                    "address"=>"Calle Libertad 601- La Unión",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"LAS LOMAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TAMBO GRANDE",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Tambo Grande",
                                                    "address"=>"Jr. Lima 388",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"AYABACA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"AYABACA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"FRIAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"JILILI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LAGUNAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"MONTERO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PACAIPAMPA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PAIMAS",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAPILLICA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SICCHEZ",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SUYO",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUANCABAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUANCABAMBA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CANCHAQUE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"EL CARMEN DE LA FRONTERA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUARMACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LALAQUIZ",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN MIGUEL DE EL FAIQUE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SONDOR",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SONDORILLO",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"MORROPON",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CHULUCANAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"BUENOS AIRES",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHALACO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LA MATANZA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MORROPON",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SALITRAL",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE BIGOTE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANTA CATALINA DE MOSSA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SANTO DOMINGO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YAMANGO",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PAITA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PAITA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Paita",
                                                    "address"=>"Jr. Independencia 137",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AMOTAPE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ARENAL",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COLAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LA HUACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TAMARINDO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VICHAYAL",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SULLANA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SULLANA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Sullana",
                                                    "address"=>"Calle Bolivar 489",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BELLAVISTA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"IGNACIO ESCUDERO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LANCONES",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MARCAVELICA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MIGUEL CHECA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"QUERECOTILLO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SALITRAL",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"TALARA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PARIÑAS",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Talara",
                                                    "address"=>"AV. San Martín H – 43, Pariñas.",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"EL ALTO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LA BREA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LOBITOS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LOS ORGANOS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MANCORA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SECHURA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SECHURA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Sechura",
                                                    "address"=>"Esquina Constitución Con Sucre 613 – Sechura",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BELLAVISTA DE LA UNION",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"BERNAL",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CRISTO NOS VALGA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VICE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"RINCONADA LLICUAR",
                                            "price"=>20
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "PUNO",
                            "provinces"=> [
                                [
                                    "provinceName"=>"PUNO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PUNO",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Puno",
                                                    "address"=>"Jr. Arequipa 120",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ACORA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"AMANTANI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ATUNCOLLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CAPACHICA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHUCUITO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MAÑAZO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PAUCARCOLLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PICHACANI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PLATERIA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TIQUILLACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VILQUE",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"AZANGARO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"AZANGARO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ACHAYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ARAPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ASILLO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CAMINACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHUPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JOSE DOMINGO CHOQUEHUANCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MUÑANI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"POTONI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAMAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN ANTON",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JOSE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE SALINAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANTIAGO DE PUPUJA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TIRAPATA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CARABAYA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"MACUSANI",
                                            "price"=>35,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Macusani",
                                                    "address"=>"Jr. Arequipa N° 109 - B",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AJOYANI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"AYAPATA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COASA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CORANI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CRUCERO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ITUATA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"OLLACHEA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN GABAN",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"USICAYOS",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CHUCUITO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JULI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"DESAGUADERO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUACULLANI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"KELLUYO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PISACOMA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"POMATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ZEPITA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"EL COLLAO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ILAVE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CAPAZO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PILCUYO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CONDURIRI",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUANCANE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"HUANCANE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"COJATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUATASANI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"INCHUPALLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PUSI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ROSASPATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TARACO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VILQUE CHICO",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LAMPA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LAMPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CABANILLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CALAPUJA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"NICASIO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"OCUVIRI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PALCA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PARATIA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PUCARA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANTA LUCIA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"VILAVILA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"MELGAR",
                                    "districts" =>[
                                        [
                                            "districtName"=>"AYAVIRI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ANTAUTA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CUPI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"LLALLI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MACARI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"NUÑOA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ORURILLO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"UMACHIRI",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"MOHO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"MOHO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CONIMA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUAYRAPATA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TILALI",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SAN ANTONIO DE PUTINA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PUTINA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ANANEA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PEDRO VILCA APAZA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"QUILCAPUNCU",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SINA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SAN ROMAN",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JULIACA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Juliaca",
                                                    "address"=>"JR. 7 De Junio 116 - Juliaca",
                                                    "delivery_time"=>"1440"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CABANA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CABANILLAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CARACOTO",
                                            "price"=>16
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SANDIA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SANDIA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CUYOCUYO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"LIMBANI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PATAMBUCO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PHARA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"QUIACA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DEL ORO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YANAHUAYA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"ALTO INAMBARI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN PEDRO DE PUTINA PUNCO",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"YUNGUYO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"YUNGUYO",
                                            "price"=>35,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Yunguyo",
                                                    "address"=>"Jr. Huascar 305",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ANAPIA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"COPANI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CUTURAPI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"OLLARAYA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TINICACHI",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"UNICACHI",
                                            "price"=>35
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "SAN MARTIN",
                            "provinces"=> [
                                [
                                    "provinceName"=>"MOYOBAMBA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"MOYOBAMBA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Moyobamba",
                                                    "address"=>"Jr. Reyes Guerra N° 411",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CALZADA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HABANA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JEPELACIO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SORITOR",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Soritor",
                                                    "address"=>"Jr. Ramón Castilla 900-Plaza De Armas",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"YANTALO",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"BELLAVISTA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"BELLAVISTA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Bellavista",
                                                    "address"=>"Prolongación Augusto B Leguía 344 - Barrio San Juan",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ALTO BIAVO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"BAJO BIAVO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HUALLAGA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN PABLO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN RAFAEL",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"EL DORADO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAN JOSE DE SISA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San José de Sisa",
                                                    "address"=>"Jr. Huascar Nº 285",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AGUA BLANCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN MARTIN",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SANTA ROSA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SHATOJA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"HUALLAGA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"SAPOSOA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Saposoa",
                                                    "address"=>"Jr. Cajamarca Cdra 4 S/N Saposoa",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ALTO SAPOSOA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"EL ESLABON",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PISCOYACU",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SACANCHE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TINGO DE SAPOSOA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"LAMAS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LAMAS",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Lamas",
                                                    "address"=>"Jr. Tobías Noriega 174 - Lamas",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ALONSO DE ALVARADO",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Pacayzapa",
                                                    "address"=>"Centro Poblado Menor Pacayzapa",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BARRANQUITA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CAYNARACHI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CUÑUMBUQUI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PINTO RECODO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"RUMISAPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN ROQUE DE CUMBAZA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SHANAO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TABALOSOS",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Tabalosos",
                                                    "address"=>"JR. Bolognesi 182",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ZAPATERO",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"MARISCAL CACERES",
                                    "districts" =>[
                                        [
                                            "districtName"=>"JUANJUI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CAMPANILLA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Campanilla",
                                                    "address"=>"Jr. Julio Romero Castro S/N, Campanilla. Frente A La Plaza De Armas.",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"HUICUNGO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PACHIZA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PAJARILLO",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PICOTA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PICOTA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Picota",
                                                    "address"=>"Jr. Bolognesi 386",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"BUENOS AIRES",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CASPISAPA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"PILLUANA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PUCACACA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN CRISTOBAL",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN HILARION",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda San Hilarion",
                                                    "address"=>"AV. Fernando Belaunde Terry N° 388",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"SHAMBOYACU",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TINGO DE PONASA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TRES UNIDOS",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"RIOJA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"RIOJA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Rioja",
                                                    "address"=>"Jr. Almirante Grau N° 730",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"AWAJUN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ELIAS SOPLIN VARGAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"NUEVA CAJAMARCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PARDO MIGUEL",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"POSIC",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN FERNANDO",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YORONGOS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"YURACYACU",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"SAN MARTIN",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TARAPOTO",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Tarapoto",
                                                    "address"=>"Jr. Maynas 389",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ALBERTO LEVEAU",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CACATACHI",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"CHAZUTA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CHIPURANA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"EL PORVENIR",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUIMBAYOC",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"JUAN GUERRA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"LA BANDA DE SHILCAYO",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"MORALES",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"PAPAPLAYA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SAN ANTONIO",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SAUCE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SHAPAJA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"TOCACHE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TOCACHE",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Tocache",
                                                    "address"=>"Jr. Jorge Chavez N° 393",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"NUEVO PROGRESO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"POLVORA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SHUNTE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"UCHIZA",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Tienda Uchiza",
                                                    "address"=>"Av. Leoncio Prado 824",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "TACNA",
                            "provinces"=> [
                                [
                                    "provinceName"=>"TACNA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TACNA",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Tacna",
                                                    "address"=>"CALLE BOLIVAR 667",
                                                    "delivery_time"=>"2880"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"ALTO DE LA ALIANZA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"CALANA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"CIUDAD NUEVA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"INCLAN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PACHIA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"PALCA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"POCOLLAY",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SAMA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CORONEL GREGORIO ALBARRACIN LANCHIPA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CANDARAVE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"CANDARAVE",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CAIRANI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CAMILACA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"CURIBAYA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"HUANUARA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"QUILAHUANI",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"JORGE BASADRE",
                                    "districts" =>[
                                        [
                                            "districtName"=>"LOCUMBA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ILABAYA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ITE",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"TARATA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TARATA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"HEROES ALBARRACIN",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ESTIQUE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"ESTIQUE-PAMPA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SITAJARA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"SUSAPAYA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"TARUCACHI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TICACO",
                                            "price"=>35
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "TUMBES",
                            "provinces"=> [
                                [
                                    "provinceName"=>"TUMBES",
                                    "districts" =>[
                                        [
                                            "districtName"=>"TUMBES",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Tumbes",
                                                    "address"=>"AV.TUMBES NORTE 293",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CORRALES",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"LA CRUZ",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"PAMPAS DE HOSPITAL",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SAN JACINTO",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"SAN JUAN DE LA VIRGEN",
                                            "price"=>16
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"CONTRALMIRANTE VILLAR",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ZORRITOS",
                                            "price"=>20,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Olva Zorritos",
                                                    "address"=>"Av. Grau 521 Contralmirante Villar",
                                                    "delivery_time"=>"4320"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CASITAS",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CANOAS DE PUNTA SAL",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"ZARUMILLA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"ZARUMILLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"AGUAS VERDES",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"MATAPALO",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"PAPAYAL",
                                            "price"=>20
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "regionName" => "UCAYALI",
                            "provinces"=> [
                                [
                                    "provinceName"=>"CORONEL PORTILLO",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PUCALLPA(CALLERIA)",
                                            "price"=>16,
                                            "officesCourier"=>[
                                                [
                                                    "name"=>"Oficina Pucallpa",
                                                    "address"=>"Jr. Tacna 727",
                                                    "delivery_time"=>"24"
                                                ]
                                            ]
                                        ],
                                        [
                                            "districtName"=>"CAMPOVERDE",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"IPARIA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"MASISEA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YARINACOCHA",
                                            "price"=>16
                                        ],
                                        [
                                            "districtName"=>"NUEVA REQUENA",
                                            "price"=>16
                                        ],
                                    ]
                                ],
                                [
                                    "provinceName"=>"ATALAYA",
                                    "districts" =>[
                                        [
                                            "districtName"=>"RAYMONDI",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"SEPAHUA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"TAHUANIA",
                                            "price"=>35
                                        ],
                                        [
                                            "districtName"=>"YURUA",
                                            "price"=>20
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PADRE ABAD",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PADRE ABAD",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"IRAZOLA",
                                            "price"=>20
                                        ],
                                        [
                                            "districtName"=>"CURIMANA",
                                            "price"=>35
                                        ]
                                    ]
                                ],
                                [
                                    "provinceName"=>"PURUS",
                                    "districts" =>[
                                        [
                                            "districtName"=>"PURUS",
                                            "price"=>16
                                        ]
                                    ]
                                ]
                            ]
                        ]
                ]
            ]
        ];

        foreach($countries as $countryC){

            factory(Country::class,1)->create([
                'name' => $countryC['countryName'],
                'picture' => $countryC['picture']
            ])
            ->each(function(Country $country) use($countryC){

                foreach($countryC['regions'] as $regionC){

                    factory(Region::class,1)->create([
                        'name' => $regionC['regionName'],
                        'country_id' => $country->id
                    ])->each(function(Region $region) use($regionC){

                        foreach($regionC['provinces'] as $provinceC){

                            factory(City::class,1)->create([
                                'name' => $provinceC['provinceName'],
                                'region_id' => $region->id
                            ]);

                        }

                    });

                }

            });

        }
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\PermissionUser;
use App\User;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = [
            [
                'name' => 'songs', 'guard_name' => 'canciones',
                'permissionusers' => [
                        'user_id' => 2
                ]
            ],
            [
                'name' => 'dictionary', 'guard_name' => 'diccionarios',
                'permissionusers' => [
                        'user_id' => 2
                ]
            ],
            [
                'name' => 'users', 'guard_name' => 'estudiantes',
                'permissionusers' => [
                        'user_id' => 2
                ]
            ],
            [
                'name' => 'income', 'guard_name' => 'ingresos',
                'permissionusers' => [
                        'user_id' => 2
                ]
            ],
            [
                'name' => 'admins', 'guard_name' => 'administradores',
                'permissionusers' => [
                        'user_id' => 2
                ]
            ],
        ];

        foreach($permissions as $permission){
            factory(Permission::class,1)->create([
                "name" => $permission["name"],
                "guard_name" => $permission["guard_name"]
            ])
            ->each(function(Permission $permissionguard) use($permission){
            	$user = User::where('username', 'xpeaking')->first();
                factory(PermissionUser::class,1)->create([
                    "user_id" => $user->id,
                    "permission_id" => $permissionguard->id
                ]);
            });
        }
    }
}

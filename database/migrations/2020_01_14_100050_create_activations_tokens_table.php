<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivationsTokensTable extends Migration
{
    public function up()
    {
        Schema::create('activations_tokens', function(Blueprint $table){
            $table->string('token', 60)->index();
            
            $table->unsignedBigInteger('user_id');
            // $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('activations_tokens');
    }
}

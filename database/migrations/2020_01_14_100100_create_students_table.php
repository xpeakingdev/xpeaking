<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->smallInteger('gems')->default(0);
            $table->smallInteger('gems_available')->default(0);
            
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            // LANGUAGE NATIVE FROM LANGUAGES
            // $table->unsignedBigInteger('language_native_id')->nullable();
            // LANGUAGE LEARN FROM LANGUAGES
            // $table->unsignedBigInteger('language_learn_id')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('students');
    }
}

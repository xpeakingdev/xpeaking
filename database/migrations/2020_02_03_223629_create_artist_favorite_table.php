<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistFavoriteTable extends Migration
{
    public function up()
    {
        Schema::create('artist_favorite', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('student_id');
            $table->foreign('student_id')->references('id')->on('students');

            $table->unsignedBigInteger('artist_id');
            $table->foreign('artist_id')->references('id')->on('artists');


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('artist_favorite');
    }
}

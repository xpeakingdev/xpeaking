<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRangesTable extends Migration
{

    public function up()
    {
        Schema::create('ranges', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description', 100);
            $table->integer('limit')->default(0);
            $table->tinyInteger('level')->default(0);
            $table->boolean('active')->default(true);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ranges');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorysTable extends Migration
{

    public function up()
    {
        Schema::create('historys', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description');
            $table->tinyInteger('points');
            $table->string('type', 100)->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('order_id')->nullable();
            $table->foreign('order_id')->references('id')->on('orders');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('historys');
    }
}

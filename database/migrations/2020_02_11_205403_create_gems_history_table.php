<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGemsHistoryTable extends Migration
{
    public function up()
    {
        Schema::create('gems_history', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description');
            $table->integer('gems');
            
            $table->unsignedBigInteger('student_id')->nullable();
            $table->foreign('student_id')->references('id')->on('students');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('gems_history');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAwardsTable extends Migration
{

    public function up()
    {
        Schema::create('awards', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description', 500);
            $table->boolean('active')->default(true);
            $table->string('picture', 500);
            
            $table->unsignedBigInteger('range_id')->nullable();
            $table->foreign('range_id')->references('id')->on('ranges');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('awards');
    }
}

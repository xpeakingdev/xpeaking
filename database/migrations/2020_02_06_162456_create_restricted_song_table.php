<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestrictedSongTable extends Migration
{

    public function up()
    {
        Schema::create('restricted_song', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('song_id');
            $table->foreign('song_id')->references('id')->on('songs');

            $table->unsignedBigInteger('student_id');
            $table->foreign('student_id')->references('id')->on('students');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('restricted_song');
    }
}

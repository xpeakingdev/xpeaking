<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubtitlesTable extends Migration
{

    public function up()
    {
        Schema::create('subtitles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('file', 500)->nullable();
            $table->tinyInteger('status');
            $table->boolean('native')->default(false);
            $table->boolean('active')->default(true);

            $table->unsignedBigInteger('language_id');
            $table->foreign('language_id')->references('id')->on('languages');

            $table->unsignedBigInteger('song_id');
            $table->foreign('song_id')->references('id')->on('songs');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('subtitles');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDictionariesTable extends Migration
{

    public function up()
    {
        Schema::create('dictionaries', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('word', 100);

            $table->unsignedBigInteger('student_id');
            $table->foreign('student_id')->references('id')->on('students');

            $table->unsignedBigInteger('source_language_id');
            $table->unsignedBigInteger('target_language_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dictionaries');
    }
}

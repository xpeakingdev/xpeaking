<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenderRestrictedTable extends Migration
{

    public function up()
    {
        Schema::create('gender_restricted', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('gender_id');
            $table->foreign('gender_id')->references('id')->on('genders');

            $table->unsignedBigInteger('student_id');
            $table->foreign('student_id')->references('id')->on('students');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('gender_restricted');
    }
}

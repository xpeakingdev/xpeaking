<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTracingTable extends Migration
{

    public function up()
    {
        Schema::create('tracing', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->date('date');
            $table->time('time_start');
            $table->time('time_end')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('song_id');
            $table->foreign('song_id')->references('id')->on('songs');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tracing');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistLanguageTable extends Migration
{

    public function up()
    {
        Schema::create('artist_language', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->boolean('native')->default(false);

            $table->unsignedBigInteger('artist_id');
            $table->foreign('artist_id')->references('id')->on('artists');

            $table->unsignedBigInteger('language_id');
            $table->foreign('language_id')->references('id')->on('languages');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('artist_language');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenderSongTable extends Migration
{

    public function up()
    {
        Schema::create('gender_song', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('gender_id');
            $table->foreign('gender_id')->references('id')->on('genders');

            $table->unsignedBigInteger('song_id');
            $table->foreign('song_id')->references('id')->on('songs');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('gender_song');
    }
}

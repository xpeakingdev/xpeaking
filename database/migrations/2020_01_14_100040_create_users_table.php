<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('username', 250);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('points')->default(0);
            $table->integer('points_available')->default(0);
            $table->tinyInteger('limit_invitations')->default(3);
            $table->tinyInteger('limit_levels')->default(10);
            // $table->string('code', 20);
            $table->string('picture', 500)->nullable();
            $table->string('status', 10)->default(App\User::ACTIVE);
            $table->boolean('permission_role')->default(true);

            $table->unsignedBigInteger('role_id')->default(\App\Role::STUDENT);
            $table->foreign('role_id')->references('id')->on('roles');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSongsTable extends Migration
{

    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 500);
            $table->text('slug');
            $table->string('cover', 500)->default('song.svg');
            $table->string('duration', 20);
            $table->text('video');
            $table->date('release');
            $table->boolean('free')->default(false);
            $table->boolean('published')->default(true);
            $table->timestamp('date_published')->nullable();
            $table->boolean('important')->default(false);
            $table->boolean('verified')->default(false);
            $table->unsignedBigInteger('views')->default(0);
            $table->unsignedTinyInteger('order')->default(0);
            
            // $table->unsignedBigInteger('language_id');
            // $table->foreign('language_id')->references('id')->on('languages');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('songs');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDictionarySpanishEnglishTable extends Migration
{
    public function up()
    {
        Schema::create('dictionary_spanish_english', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('word', 500);
            $table->string('translation', 1000)->nullable();
            $table->string('phonetics', 200)->nullable();
            $table->string('audio', 250)->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dictionary_spanish_english');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistsTable extends Migration
{

    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 250)->nullable();
            $table->string('last_name', 250)->nullable();
            $table->string('stage_name', 500);
            $table->string('slug', 500);
            $table->string('picture', 500)->default('artist.svg');
            // $table->boolean('active')->default(true);
            $table->boolean('important')->default(false);
            $table->boolean('published')->default(false);
            $table->timestamp('date_published')->nullable();
            $table->unsignedBigInteger('views')->default(0);
            $table->unsignedTinyInteger('order')->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('artists');
    }
}

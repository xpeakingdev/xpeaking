<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistGenderTable extends Migration
{

    public function up()
    {
        Schema::create('artist_gender', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('artist_id');
            $table->foreign('artist_id')->references('id')->on('artists');

            $table->unsignedBigInteger('gender_id');
            $table->foreign('gender_id')->references('id')->on('genders');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('artist_gender');
    }
}

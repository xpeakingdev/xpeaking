<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguageStudentTable extends Migration
{
    public function up()
    {
        Schema::create('language_student', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->boolean('speak')->default(false);
            $table->boolean('learn')->default(false);
            $table->boolean('principal')->default(false);

            $table->unsignedBigInteger('language_id');
            $table->foreign('language_id')->references('id')->on('languages');

            $table->unsignedBigInteger('student_id');
            $table->foreign('student_id')->references('id')->on('students');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('language_student');
    }
}

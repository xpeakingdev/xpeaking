<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
        
            $table->tinyInteger('payment_mode')->nullable();
            $table->string('description', 500)->nullable();
            $table->decimal('ammount', 18, 2);
            $table->tinyInteger('type_document')->nullable();
            $table->boolean('send_invoice')->default(false);
            $table->tinyInteger('status');
            // DATE PAY ORDER PENDING//CUANDO DEBE PAGAR LA ORDEN
            $table->timestamp('date_pay')->nullable();
            // DATE OREDER PAYED / CUANDO PAGO LA ORDEN
            $table->timestamp('date_payment')->nullable();
            $table->tinyInteger('points')->nullable();
            $table->unsignedBigInteger('paid_by')->nullable();
            $table->string('file', 500)->nullable();
            $table->timestamp('upload_file')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('plan_id');
            $table->foreign('plan_id')->references('id')->on('plans');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

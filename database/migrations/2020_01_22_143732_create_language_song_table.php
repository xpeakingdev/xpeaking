<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguageSongTable extends Migration
{
 
    public function up()
    {
        Schema::create('language_song', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('language_id');
            $table->foreign('language_id')->references('id')->on('languages');

            $table->unsignedBigInteger('song_id');
            $table->foreign('song_id')->references('id')->on('songs');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('language_song');
    }
}

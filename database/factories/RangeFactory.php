<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Range;
use Faker\Generator as Faker;

$factory->define(Range::class, function (Faker $faker) {
    return [
        'description' => $faker->sentence
    ];
});

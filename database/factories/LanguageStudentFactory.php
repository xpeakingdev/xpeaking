<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\LanguageStudent;
use Faker\Generator as Faker;
use App\Student;

$factory->define(LanguageStudent::class, function (Faker $faker) {
    return [
        'student_id' => Student::all()->random()->id
    ];
});

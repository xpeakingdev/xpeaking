<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Subtitle;
use Faker\Generator as Faker;

$factory->define(Subtitle::class, function (Faker $faker) {
    return [
        'file' => $faker->sentence
    ];
});

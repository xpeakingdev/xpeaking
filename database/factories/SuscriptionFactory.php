<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Suscription;
use Faker\Generator as Faker;

$factory->define(Suscription::class, function (Faker $faker) {
    return [
        'active' => true
    ];
});

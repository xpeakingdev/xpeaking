﻿1
00:00:00,400 --> 00:00:09,600

2
00:00:09,633 --> 00:00:11,065
You promised the world

3
00:00:11,100 --> 00:00:13,100
and I fell for it

4
00:00:14,566 --> 00:00:15,500
I put you first

5
00:00:15,533 --> 00:00:17,266
and you adored it

6
00:00:17,300 --> 00:00:20,266
You set fires to my forest

7
00:00:20,300 --> 00:00:21,400
And you let it burn

8
00:00:21,433 --> 00:00:24,500
Sang off-key in my chorus

9
00:00:24,533 --> 00:00:26,633
'Cause it wasn't yours

10
00:00:28,566 --> 00:00:29,532
I saw the signs

11
00:00:29,566 --> 00:00:31,400
and I ignored it

12
00:00:33,366 --> 00:00:34,466
Rose-colored glasses

13
00:00:34,500 --> 00:00:35,666
all distorted

14
00:00:35,700 --> 00:00:38,666
You set fire to my purpose

15
00:00:38,700 --> 00:00:40,133
And you let it burn

16
00:00:40,166 --> 00:00:43,433
You got off on the hurtin'

17
00:00:43,466 --> 00:00:46,133
When it wasn't yours

18
00:00:46,166 --> 00:00:47,400
Yeah

19
00:00:47,433 --> 00:00:51,566
We'd always go into it blindly

20
00:00:52,233 --> 00:00:56,132
I needed to lose you to find me

21
00:00:56,666 --> 00:01:00,632
This dancing, was killing me softly

22
00:01:01,466 --> 00:01:05,600
I needed to hate you to love me

23
00:01:06,133 --> 00:01:07,100
To love, love, yeah

24
00:01:07,466 --> 00:01:09,133
To love, love, yeah

25
00:01:09,166 --> 00:01:10,700
To love, yeah

26
00:01:10,733 --> 00:01:15,400
I needed to lose you to love me

27
00:01:15,433 --> 00:01:16,500
To love, love, yeah

28
00:01:16,733 --> 00:01:18,433
To love, love, yeah

29
00:01:18,466 --> 00:01:20,100
To love, yeah

30
00:01:20,133 --> 00:01:24,166
I needed to lose you to love me

31
00:01:24,466 --> 00:01:25,500
I gave my all

32
00:01:25,533 --> 00:01:27,433
and they all know it

33
00:01:29,266 --> 00:01:30,200
You tore me down

34
00:01:30,233 --> 00:01:31,600
and now it's showing

35
00:01:31,633 --> 00:01:34,300
In two months you replaced us

36
00:01:34,566 --> 00:01:36,366
Like it was easy

37
00:01:36,400 --> 00:01:39,400
Made me think I deserved it

38
00:01:39,433 --> 00:01:42,166
In the thick of healing

39
00:01:42,200 --> 00:01:43,433
Yeah

40
00:01:43,466 --> 00:01:48,265
We'd always go into it blindly

41
00:01:48,300 --> 00:01:52,399
I needed to lose you to find me

42
00:01:52,600 --> 00:01:57,399
This dancing, was killing me softly

43
00:01:57,433 --> 00:02:01,566
I needed to hate you to love me

44
00:02:01,600 --> 00:02:03,065
To love, love, yeah

45
00:02:03,466 --> 00:02:05,166
To love, love, yeah

46
00:02:05,200 --> 00:02:06,566
To love, yeah

47
00:02:06,600 --> 00:02:11,200
I needed to lose you to love me

48
00:02:11,233 --> 00:02:12,366
To love, love, yeah

49
00:02:13,200 --> 00:02:14,366
To love, love, yeah

50
00:02:14,400 --> 00:02:16,200
To love, yeah

51
00:02:16,233 --> 00:02:20,366
I needed to lose you to love me

52
00:02:20,400 --> 00:02:21,666
You promised the world

53
00:02:21,700 --> 00:02:23,433
and I fell for it

54
00:02:25,200 --> 00:02:26,266
I put you first

55
00:02:26,300 --> 00:02:27,600
and you adored it

56
00:02:27,633 --> 00:02:30,633
You set fires to my forest

57
00:02:30,666 --> 00:02:32,233
And you let it burn

58
00:02:32,266 --> 00:02:34,632
Sang off-key in my chorus

59
00:02:34,666 --> 00:02:38,733

60
00:02:38,766 --> 00:02:40,299
To love, love, yeah

61
00:02:40,700 --> 00:02:42,399
To love, love, yeah

62
00:02:42,433 --> 00:02:44,200
To love, yeah

63
00:02:44,233 --> 00:02:48,400
I needed to hate you to love me

64
00:02:48,433 --> 00:02:49,533
To love, love, yeah

65
00:02:49,766 --> 00:02:51,566
To love, love, yeah

66
00:02:51,600 --> 00:02:53,533
To love, yeah

67
00:02:53,566 --> 00:02:57,433
I needed to lose you to love me

68
00:02:57,633 --> 00:02:58,700
To love, love, yeah

69
00:02:59,366 --> 00:03:00,766
To love, love, yeah

70
00:03:01,000 --> 00:03:02,566
To love, yeah

71
00:03:02,600 --> 00:03:05,700
And now the chapter is closed

72
00:03:05,733 --> 00:03:06,700
and done

73
00:03:06,733 --> 00:03:08,333
To love, love, yeah

74
00:03:08,566 --> 00:03:10,333
To love, love, yeah

75
00:03:10,366 --> 00:03:11,566
To love, yeah

76
00:03:11,600 --> 00:03:14,233
And now it's goodbye

77
00:03:14,266 --> 00:03:16,266
it's goodbye for us

78
00:03:18,266 --> 00:03:24,333
﻿1
00:00:00,400 --> 00:00:09,600

2
00:00:09,633 --> 00:00:11,065
Prometiste el mundo

3
00:00:11,100 --> 00:00:13,100
y me enamoré de él

4
00:00:14,566 --> 00:00:15,500
Te puse primero

5
00:00:15,533 --> 00:00:17,266
y lo adoraste

6
00:00:17,300 --> 00:00:20,266
Prendiste fuego a mi bosque

7
00:00:20,300 --> 00:00:21,400
Y lo dejaste arder

8
00:00:21,433 --> 00:00:24,500
Canté fuera de tono en mi coro

9
00:00:24,533 --> 00:00:26,633
Porque no era tuyo

10
00:00:28,566 --> 00:00:29,532
Vi las señales

11
00:00:29,566 --> 00:00:31,400
y lo ignoré

12
00:00:33,366 --> 00:00:34,466
Gafas de color rosa

13
00:00:34,500 --> 00:00:35,666
todas distorsionadas

14
00:00:35,700 --> 00:00:38,666
Prendiste fuego a mi propósito

15
00:00:38,700 --> 00:00:40,133
Y lo dejaste arder

16
00:00:40,166 --> 00:00:43,433
Te bajaste en el dolor

17
00:00:43,466 --> 00:00:46,133
Cuando no era tuyo

18
00:00:46,166 --> 00:00:47,400
Sí

19
00:00:47,433 --> 00:00:51,566
Siempre íbamos a ciegas

20
00:00:52,233 --> 00:00:56,132
Necesitaba perderte para encontrarme

21
00:00:56,666 --> 00:01:00,632
Este baile, estaba matándome suavemente

22
00:01:01,466 --> 00:01:05,600
Necesitaba odiarte para amarme

23
00:01:06,133 --> 00:01:07,100
Amar, amar, sí

24
00:01:07,466 --> 00:01:09,133
Amar, amar, sí

25
00:01:09,166 --> 00:01:10,700
Amar, sí

26
00:01:10,733 --> 00:01:15,400
Necesitaba perderte para amarme

27
00:01:15,433 --> 00:01:16,500
Amar, amar, sí

28
00:01:16,733 --> 00:01:18,433
Amar, amar, sí

29
00:01:18,466 --> 00:01:20,100
Amar, sí

30
00:01:20,133 --> 00:01:24,166
Necesitaba perderte para amarme

31
00:01:24,466 --> 00:01:25,500
Di mi todo

32
00:01:25,533 --> 00:01:27,433
y todos lo saben

33
00:01:29,266 --> 00:01:30,200
Me derribaste

34
00:01:30,233 --> 00:01:31,600
y ahora está demostrado

35
00:01:31,633 --> 00:01:34,300
En dos meses nos reemplazaste

36
00:01:34,566 --> 00:01:36,366
Como si fuera fácil

37
00:01:36,400 --> 00:01:39,400
Me hizo pensar que lo merecía

38
00:01:39,433 --> 00:01:42,166
En medio de la curación

39
00:01:42,200 --> 00:01:43,433
Sí

40
00:01:43,466 --> 00:01:48,265
Siempre íbamos a ciegas

41
00:01:48,300 --> 00:01:52,399
Necesitaba perderte para encontrarme

42
00:01:52,600 --> 00:01:57,399
Este baile, estaba matándome suavemente

43
00:01:57,433 --> 00:02:01,566
Necesitaba odiarte para amarme

44
00:02:01,600 --> 00:02:03,065
Amar, amar, sí

45
00:02:03,466 --> 00:02:05,166
Amar, amar, sí

46
00:02:05,200 --> 00:02:06,566
Amar, sí

47
00:02:06,600 --> 00:02:11,200
Necesitaba perderte para amarme

48
00:02:11,233 --> 00:02:12,366
Amar, amar, sí

49
00:02:13,200 --> 00:02:14,366
Amar, amar, sí

50
00:02:14,400 --> 00:02:16,200
Amar, sí

51
00:02:16,233 --> 00:02:20,366
Necesitaba perderte para amarme

52
00:02:20,400 --> 00:02:21,666
Prometiste el mundo 

53
00:02:21,700 --> 00:02:23,433
y me enamoré de él

54
00:02:25,200 --> 00:02:26,266
Te puse primero

55
00:02:26,300 --> 00:02:27,600
y lo adoraste

56
00:02:27,633 --> 00:02:30,633
Prendiste fuego a mi bosque

57
00:02:30,666 --> 00:02:32,233
Y lo dejaste arder

58
00:02:32,266 --> 00:02:34,632
Canté fuera de tono en mi coro

59
00:02:34,666 --> 00:02:38,733

60
00:02:38,766 --> 00:02:40,299
Amar, amar, sí

61
00:02:40,700 --> 00:02:42,399
Amar, amar, sí

62
00:02:42,433 --> 00:02:44,200
Amar, sí

63
00:02:44,233 --> 00:02:48,400
Necesitaba odiarte para amarme

64
00:02:48,433 --> 00:02:49,533
Amar, amar, sí

65
00:02:49,766 --> 00:02:51,566
Amar, amar, sí

66
00:02:51,600 --> 00:02:53,533
Amar, sí

67
00:02:53,566 --> 00:02:57,433
Necesitaba perderte para amarme

68
00:02:57,633 --> 00:02:58,700
Amar, amar, sí

69
00:02:59,366 --> 00:03:00,766
Amar, amar, sí

70
00:03:01,000 --> 00:03:02,566
Amar, sí

71
00:03:02,600 --> 00:03:05,700
Y ahora el capítulo está cerrado

72
00:03:05,733 --> 00:03:06,700
y hecho

73
00:03:06,733 --> 00:03:08,333
Amar, amar, sí

74
00:03:08,566 --> 00:03:10,333
Amar, amar, sí

75
00:03:10,366 --> 00:03:11,566
Amar, sí

76
00:03:11,600 --> 00:03:14,233
Y ahora es adiós

77
00:03:14,266 --> 00:03:16,266
es adiós para nosotros

78
00:03:18,266 --> 00:03:24,333